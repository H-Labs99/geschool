<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use backend\models\HistoriqueApplication;
use backend\models\User;
use backend\controllers\Utils;
use backend\models\Anneescolaire;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'forget_password', 'reset_password', 'resend_code', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        return $this->redirect(['login/']);
    }

    public function actionForget_password() {
        Yii::$app->user->logout();
        $this->layout = 'main_login';
        $model = new User;
        $model->setScenario('forget-password');
        if (Yii::$app->request->post()) {
            if ($model->load(Yii::$app->request->post())) {

                if (trim($model->username) != "") {
                    if (trim($model->canal_code) != "") {

                        //$testPhone = explode("_", $model->username);
                        //if (sizeof($testPhone) == 1) {
                        //$model->username = preg_replace("/[^0-9]/", "", $model->username);
                        //verifier l'existance du numero de telephone
                        $find_user = User::findOne(['username' => $model->username, 'status' => 10]);
                        if ($find_user != null) {
                            $continue = true;
                            $message_plus = "";

                            $verification_code = rand(10000, 99999);
                            $find_user->password_reset_token = Yii::$app->security->generatePasswordHash($verification_code);
                            if ($find_user->save()) {
                                 $redirect_url = 'reset_password?key1=' . $find_user->auth_key . '&key2=' . $find_user->updated_at. '&key3=' . $model->canal_code;

                                if ($model->canal_code == User::STATUS_SMS) {
                                    $message_plus = " SMS";
                                    //envoie du code par sms
                                    $to = $find_user->telephoneuser;
                                    if ($to != null) {
                                        $contenu = Yii::t('app', 'message_verification') . $verification_code;
                                        Utils::hit_sms($contenu, $to);
                                    } else {
                                        $continue = false;
                                        $message = Yii::t('app', 'update_error');
                                        Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                                    }
                                } else if ($model->canal_code == User::STATUS_EMAIL) {
                                    $message_plus = " email";
                                    //envoie du code par email
                                    $to = $find_user->email;
                                    if ($to != null) {
                                        $contenu = Yii::t('app', 'message_verification');
                                        Utils::send_useremail($contenu, $verification_code, $to, $redirect_url);
                                    } else {
                                        $continue = false;
                                        $message = Yii::t('app', 'update_error');
                                        Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                                    }
                                }

                                //si code envoyé

                                if ($continue == true) {
                                    $message = Yii::t('app', 'code_send1') . Yii::t('app', 'code_send2') . $message_plus;
                                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                                }


                                return $this->redirect(array('./'.$redirect_url));
                            } else {
                                $message = Yii::t('app', 'update_error');
                                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                            }
                        } else {
                            Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'username_incorrect'));
                        }
                        /* } else {
                          $message = Yii::t('app', 'phone_format_error');
                          Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                          } */
                    } else {
                        Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'input_empty'));
                    }
                } else {
                    Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'input_empty'));
                }
            } else {
                Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'input_empty'));
            }
        }

        return $this->render('forget', [
                    'model' => $model,
        ]);
    }

    public function actionReset_password() {

        if (isset($_GET['key1']) != "" && isset($_GET['key2']) != "" && isset($_GET['key3']) != "") {

            $key1 = $_GET['key1'];
            $key2 = $_GET['key2'];
            $key3 = $_GET['key3'];

            $find_user = User::find()->where(['auth_key' => $key1, 'updated_at' => $key2, 'status' => 10])->one();
            if ($find_user !== null) {
                Yii::$app->user->logout();
                $this->layout = 'main_login';
                $model = new User;
                if (Yii::$app->request->post()) {
                    if ($model->load(Yii::$app->request->post())) {

                        $post = Yii::$app->request->post();
                        $model->password = $post['User']['password'];
                        $model->confirm_password = $post['User']['confirm_password'];

                        if (trim($model->username) != "" && trim($model->password) != "" && trim($model->confirm_password) != "") {

                            $password = $find_user->password_reset_token;
                            if (Yii::$app->security->validatePassword($model->username, $password)) {

                                if (strcmp(trim($model->password), trim($model->confirm_password)) == 0) {

                                    $find_user->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                                    $find_user->password_reset_token = "";
                                    if ($find_user->save()) {

                                        $message = Yii::t('app', 'reset_success');
                                        Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                                        return $this->redirect(array('./login'));
                                    } else {
                                        $message = Yii::t('app', 'update_error');
                                        Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                                    }
                                } else {
                                    $message = Yii::t('app', 'new_passe_identique');
                                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                                }
                            } else {
                                Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'verification_error'));
                            }
                        } else {
                            Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'input_empty'));
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'input_empty'));
                    }
                }

                return $this->render('reset', [
                            'model' => $model,
                            'key1' => $key1,
                            'key2' => $key2,
                            'key3' => $key3,
                ]);
            } else {
                return $this->redirect(['login/']);
            }
        } else {
            return $this->redirect(['login/']);
        }
    }

    public function actionResend_code() {

        if (isset($_GET['key1']) != "" && isset($_GET['key2']) != "" && isset($_GET['key3']) != "") {

            $key1 = $_GET['key1'];
            $key2 = $_GET['key2'];
            $key3 = $_GET['key3'];

            $find_user = User::find()->where(['auth_key' => $key1, 'updated_at' => $key2, 'status' => 10])->one();
            if ($find_user !== null) {
                $continue = true;
                $verification_code = rand(10000, 99999);
                $find_user->password_reset_token = Yii::$app->security->generatePasswordHash($verification_code);
                
                 $redirect_url = 'reset_password?key1=' . $find_user->auth_key . '&key2=' . $find_user->updated_at. '&key3=' . $key3;

                if ($find_user->save()) {

                    if ($key3 == User::STATUS_SMS) {
                        //envoie du code par sms
                        $to = $find_user->telephoneuser;
                        if ($to != null) {
                            $contenu = Yii::t('app', 'message_verification') . $verification_code;
                            Utils::hit_sms($contenu, $to);
                        } else {
                            $continue = false;
                            $message = Yii::t('app', 'update_error');
                            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                        }
                    } else if ($key3 == User::STATUS_EMAIL) {
                        //envoie du code par email
                        $to = $find_user->email;
                        if ($to != null) {
                            $contenu = Yii::t('app', 'message_verification');
                            Utils::send_useremail($contenu, $verification_code, $to, $redirect_url);
                        } else {
                            $continue = false;
                            $message = Yii::t('app', 'update_error');
                            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                        }
                    }

                    //si code envoyé

                    if ($continue == true) {
                      $message = Yii::t('app', 'code_resend');
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                    }
                    
                } else {

                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
             return $this->redirect(array('./'.$redirect_url));

            } else {
                return $this->redirect(['login/']);
            }
        } else {
            return $this->redirect(['login/']);
        }
    }

    public function actionLogin() {
        Yii::$app->user->logout();
        $this->layout = 'main_login';
        $model = new LoginForm();


        if (Yii::$app->request->post()) {
            if ($model->load(Yii::$app->request->post())) {

                if ($model->login()) {

                    $annee_scolaire_id = 6; // pOUR LES TESTS PUISQUE TOUT VIENT DE L'API (LA REQUETE CI-DESSOUS NE RETOURNERA RIEN)
                    $annee_scolaire = Anneescolaire::find()->where(['status' => 1])->one();

                    if (isset($annee_scolaire->id))
                        $annee_scolaire_id = $annee_scolaire->id;
                    $id_center = Yii::$app->user->identity->default_center;
                    $session = Yii::$app->session;
                    $session->set('default_center', $id_center);
                    $session->set('annee_scolaire', $annee_scolaire_id);

                    $new_historique = new HistoriqueApplication;
                    $new_historique->idUser = Yii::$app->user->identity->id;
                    $new_historique->dateConnexion = date("Y-m-d H:i:s");

                    $new_historique->dateDeconnexion = date("Y-m-d H:i:s");
                    $new_historique->etatHistorique = 1;
                    $new_historique->typehistorique = "CONNEXION";
                    $new_historique->save();

                    return $this->redirect(['account/']);
                } else {
                    Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'connexion_false'));
                }
            } else {
                Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'input_empty'));
            }
        }

        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
