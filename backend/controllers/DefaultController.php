<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;


/**
 * Site controller
 */
class DefaultController extends Controller
{
   public $const_admin = 'Administrateur';
	
	const CONST_ADMIN 		=  'Administrateur';
        const MENU_DASHBOARD 	= 'welcome';
	
        
    public $page_title = '';
    public $breadcrumb = '';
    public $select_menu = '';
    public $select_sous_menu = '';
	
    
    public function beforeAction($action)
    {
       if(isset(Yii::$app->session['langue_RDC'])){
			Yii::$app->language=Yii::$app->session['langue_RDC'];
		}
		
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
    }
	
}
