<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\models\Mproduits;
use backend\models\Filter;
use backend\models\User;

class WelcomeController extends DefaultController {

    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "BOARD";
    public $calling = 'true';

    /*
      public function behaviors()
      {
      return [
      'access' => [
      'class' => AccessControl::className(),
      'rules' => [
      [
      'allow' => true,
      // 'actions' => ['index','update_password','update_info','get_userstart'],
      'actions' => ['index','update_password','update_info'],
      'roles' => ['@'],
      ],
      ],
      ],
      ];
      }
     */

    public function beforeAction($action) {

        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'update_password', 'update_info', 'mycenter', 'mycenter', 'center_params'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionMycenter() {

        $session = Yii::$app->session;
        $session->set('default_center', $_GET["center"]);
    }

    public function actionIndex() {

        $title = Yii::t('app', 'dashboard');
        $board = array();

        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);
        return $this->render('index');
    }

    public function actionUpdate_password() {

        $model = new User;

        if (Yii::$app->request->post()) {

            if ($model->load(Yii::$app->request->post())) {

                $post = Yii::$app->request->post();
                $model->nom = $post['User']['nom'];
                $model->prenoms = $post['User']['prenoms'];
                $model->username = $post['User']['username'];
                if (trim($model->nom) != "" && trim($model->prenoms) != "" && trim($model->username) != "") {
                    $password = Yii::$app->user->identity->password_hash;

                    if (Yii::$app->security->validatePassword($model->nom, $password)) {

                        if (strcmp(trim($model->prenoms), trim($model->username)) == 0) {

                            if (strcmp(trim($model->nom), trim($model->username)) != 0) {

                                $find_user = User::findOne(['id' => Yii::$app->user->identity->id]);
                                if ($find_user !== null) {
                                    $find_user->password_hash = Yii::$app->security->generatePasswordHash($model->username);
                                    if ($find_user->save()) {

                                        $message = Yii::t('app', 'update_success');
                                        Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                                        return $this->redirect(array('./account'));
                                    } else {
                                        $message = Yii::t('app', 'update_error');
                                        Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                                    }
                                } else {
                                    $message = Yii::t('app', 'update_error');
                                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                                }
                            } else {
                                $message = Yii::t('app', 'last_new_passe');
                                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                            }
                        } else {
                            $message = Yii::t('app', 'new_passe_identique');
                            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                        }
                    } else {
                        $message = Yii::t('app', 'old_passe_incorrect');
                        Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                    }
                } else {
                    $message = Yii::t('app', 'input_empty');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'input_empty');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        }

        $title = Yii::t('app', 'update_password');
        $board = array("update_password" => $title);

        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);
        return $this->render('update_password', array('model' => $model));
    }

    public function actionUpdate_info() {

        $model = new User;
        $user_info = Yii::$app->user->identity;
        $model->email = $user_info->email;
        $model->username = $user_info->username;
        $model->telephoneuser = $user_info->telephoneuser;
        $model->nom = $user_info->nom;
        $model->prenoms = $user_info->prenoms;

        if (Yii::$app->request->post()) {


            if ($model->load(Yii::$app->request->post())) {
                if (trim($model->email) != "" && trim($model->username) != "" && trim($model->telephoneuser) != "" && trim($model->nom) != "" && trim($model->prenoms) != "") {
                    $test_email = User::find()->where(['email' => $model->email])->andWhere(['!=', 'id', Yii::$app->user->identity->id])->one();
                    
                    if ($test_email == null) {
                        $test_phone = User::find()->where(['telephoneuser' => $model->telephoneuser])->andWhere(['!=', 'id', Yii::$app->user->identity->id])->one();
                        
                        if ($test_phone == null) {

                            $find_user = User::findOne(['id' => Yii::$app->user->identity->id]);
                            if ($find_user !== null) {
                                $find_user->nom = $model->nom;
                                $find_user->prenoms = $model->prenoms;
                                $find_user->username = $model->username;
                                $find_user->telephoneuser = $model->telephoneuser;
                                $find_user->email = $model->email;
                                if ($find_user->save()) {

                                    $message = Yii::t('app', 'update_success');
                                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                                    return $this->redirect(array('./account'));
                                } else {
                                    $message = Yii::t('app', 'update_error');
                                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                                }
                            } else {
                                $message = Yii::t('app', 'update_error');
                                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                            }
                        } else {
                            $message = Yii::t('app', 'phone_used');
                            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                        }
                    } else {
                        $message = Yii::t('app', 'email_used');
                        Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                    }
                } else {
                    $message = Yii::t('app', 'input_empty');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'input_empty');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        }

        $title = Yii::t('app', 'update_profil');
        $board = array("update_profil" => $title);

        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);
        return $this->render('update_profil', array('model' => $model));
    }

    public function actionCenter_params() {

        $model = new User;
        $user_info = Yii::$app->user->identity;
        $model->default_center = $user_info->default_center;

        if (Yii::$app->request->post()) {


            if ($model->load(Yii::$app->request->post())) {
                if (trim($model->default_center) != "") {

                    $find_user = User::findOne(['id' => Yii::$app->user->identity->id]);
                    if ($find_user !== null) {
                        $find_user->default_center = $model->default_center;
                        if ($find_user->save()) {
                            $session = Yii::$app->session;
                            $session->set('default_center', $model->default_center);
                            $message = Yii::t('app', 'update_success');
                            Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                            return $this->redirect(array('./account'));
                        } else {
                            $message = Yii::t('app', 'update_error');
                            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                        }
                    } else {
                        $message = Yii::t('app', 'update_error');
                        Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                    }
                } else {
                    $message = Yii::t('app', 'input_empty');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'input_empty');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        }

        $title = Yii::t('app', 'ucenter_params');
        $board = array("ucenter_params" => $title);

        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);
        return $this->render('center_params', array('model' => $model));
    }

}
