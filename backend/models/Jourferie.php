<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jourferie".
 *
 * @property integer $id
 * @property integer $idannee
 * @property string $evenement
 * @property string $keyjourferie
 * @property string $centreconcerne
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 * @property string $datejourferie
 *
 * @property Anneescolaire $idannee0
 */
class Jourferie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jourferie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idannee', 'evenement', 'keyjourferie', 'created_at', 'create_by', 'datejourferie'], 'required'],
            [['idannee', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['centreconcerne'], 'string'],
            [['datejourferie'], 'safe'],
            [['evenement'], 'string', 'max' => 254],
            [['keyjourferie'], 'string', 'max' => 32],
            [['idannee'], 'exist', 'skipOnError' => true, 'targetClass' => Anneescolaire::className(), 'targetAttribute' => ['idannee' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idannee' => 'Idannee',
            'evenement' => 'Evenement',
            'keyjourferie' => 'Keyjourferie',
            'centreconcerne' => 'Centreconcerne',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
            'datejourferie' => 'Datejourferie',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdannee0()
    {
        return $this->hasOne(Anneescolaire::className(), ['id' => 'idannee']);
    }
}
