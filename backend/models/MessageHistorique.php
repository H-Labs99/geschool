<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "message_historique".
 *
 * @property integer $id_message
 * @property string $destinataire
 * @property string $content
 * @property integer $nbre_page
 * @property string $datemessage
 * @property string $response_message
 * @property string $operateur
 */
class MessageHistorique extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_historique';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['destinataire', 'content', 'nbre_page', 'datemessage', 'operateur'], 'required'],
            [['content', 'response_message'], 'string'],
            [['nbre_page'], 'integer'],
            [['datemessage'], 'safe'],
            [['destinataire'], 'string', 'max' => 20],
            [['operateur'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_message' => 'Id Message',
            'destinataire' => 'Destinataire',
            'content' => 'Content',
            'nbre_page' => 'Nbre Page',
            'datemessage' => 'Datemessage',
            'response_message' => 'Response Message',
            'operateur' => 'Operateur',
        ];
    }
}
