<?php

namespace backend\modules\users\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\modules\users\models\User;
use backend\modules\users\models\UserProfil;
use backend\modules\users\models\UserAccess;
use backend\controllers\Utils;
use backend\controllers\DefaultController;
use backend\models\ApiCurl;
use backend\models\SituationMatrimoniale;
use backend\modules\users\models\Grade;
use backend\modules\users\models\Personnel;
use backend\modules\users\models\Planaffectation;
use backend\modules\users\models\SchoolCenter;
use backend\modules\users\models\Section;
use backend\modules\users\models\Typepersonnel;
use DateTime;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class UserController extends DefaultController
{

	public $page_title = "";
	public $breadcrumb = "";
	public $select_menu = "ADMIN";

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['add_user', 'all_user', 'operation', 'update_user', 'reset_user', 'create_planaffectation', 'user_planaffectations', 'save_planaffectation', 'delete_affectation'],
						'roles' => ['@'],
					],
				],
			],
		];
	}


	public function actionAdd_user()
	{
		Utils::Rule("manage_admin", "CREATE");

		$model = new User;
		// $model->setScenario('add_user');
		$model_personnel = new Personnel;
		$allsection = Section::find()->where(['status' => 1])->all();
		$alltype_personnel = Typepersonnel::find()->where(['status' => 1])->all();
		$all_situation_matrimoniale = SituationMatrimoniale::find()->where(['status' => 1])->all();
		$all_grade = Grade::find()->where(['status' => 1])->all();
		$userProfil = "";

		if (Yii::$app->request->post()) {

			$all_center = "";
			if ($model->load(Yii::$app->request->post()) && $model_personnel->load(Yii::$app->request->post())) {
				$post = Yii::$app->request->post();

				// print_r($post['User']['username']);
				// die;

				$model->password = $post['User']['password'];
				$model->password2 = $post['User']['password2'];
				$model->username = $post['User']['username'];


				$photo = UploadedFile::getInstance($model, 'photo');
				if ($photo != null) {
					$model->photo = $photo;
					$nomPhoto = 'PHOTO_PROFIL' . time() . '.' . $model->photo->extension;
					$path = 'uploads/';
					// $path = Yii::getAlias("@web/") . 'uploads/';
					if (!is_dir($path)) {
						mkdir($path, 0777);
					}
					$photoPath = $path . $nomPhoto;
					$model->photo->saveAs($photoPath);
					$model->photo = $nomPhoto;
				}

				$model->username = trim($post['User']['username']);
				$espace = " ";

				// Teste si le nom d'utilisateur choisi contient des espaces
				if (strpos($model->username, $espace) === false) {
					// "L'espace n'existe pas!";

					$matricule = rand(111111, 999999);
					$unique_matricule = Personnel::find()->where(['matricule' => $matricule])->one();

					while ($unique_matricule != null) {
						$matricule = rand(111111, 999999);
					}

					$model_personnel->indicegrade = trim($post['Personnel']['indicegrade']);
					$model_personnel->situationmatrimoniale = trim($post['Personnel']['situationmatrimoniale']);
					$model_personnel->dateembauche = trim($post['Personnel']['dateembauche']);
					$model_personnel->sectionconcerne = trim($post['Personnel']['sectionconcerne']);
					$model_personnel->typepersonnel = trim($post['Personnel']['typepersonnel']);
					$model_personnel->matricule = "" . $matricule;
					$model_personnel->status = 1;
					$model_personnel->created_at = time();
					$model_personnel->create_by = Yii::$app->user->identity->id;

					$newProfil = "";

					foreach ($post as $key => $value) {

						if (!in_array($key, array("_csrf", "User", "updatebutton"))) {
							$test_key = explode("geschoolcenter_", $key);
							if (sizeof($test_key) > 1) {

								$response = $test_key[1];
								if (isset($post['profilcenter_' . $response]) && trim($post['profilcenter_' . $response]) != "") {
									if ($all_center != "") $all_center .= ",";
									if ($newProfil != "") $newProfil .= ",";
									$all_center .= "-" . $response . "-";
									$newProfil .= $post['profilcenter_' . $response];
								}
							}
						}
					}
					$userProfil = $newProfil;

					if ($all_center != "") {

						if (trim($model->email) != "" && trim($model->username) != "" && trim($model->telephoneuser) != "" && trim($model->nom) != "" && trim($model->prenoms) != "" && trim($model->password) != "" && trim($model->password2) != "") {

							$testPhone = explode("_", $model->telephoneuser);
							if (sizeof($testPhone) == 1) {
								$model->telephoneuser = preg_replace("/[^0-9]/", "", $model->telephoneuser);

								if (
									preg_match('/^2289[0-4][0-9]{6}$/', $model->telephoneuser)
									or preg_match('/^22870[0-9]{6}$/', $model->telephoneuser)
									or preg_match('/^2289[6-9][0-9]{6}$/', $model->telephoneuser)
									or preg_match('/^22879[0-9]{6}$/', $model->telephoneuser)
								) {

									$today = date('Y-m-d');

									if (strtotime($model_personnel->dateembauche) <= strtotime($today)) {
										if (strcmp(trim($model->password), trim($model->password2)) == 0) {
											$test_email = User::find()->where(['email' => $model->email, 'status' => ['10', '20']])->one();
											if ($test_email == null) {
												$test_phone = User::find()->where(['telephoneuser' => $model->telephoneuser, 'status' => ['10', '20']])->one();
												if ($test_phone == null) {
													$test_username = User::find()->where(['username' => $model->username, 'status' => ['10', '20']])->one();
													if ($test_username == null) {

														$model->role = 10;
														$model->status = 10;
														$model->id_center = $all_center;
														$model->created_at = time();
														$model->updated_at = time();
														$model->idP = Yii::$app->user->identity->id;
														$model->auth_key = Yii::$app->security->generateRandomString(32);
														$model->password_hash = Yii::$app->security->generatePasswordHash($model->password);

														if ($model->save()) {

															//enregistrement dans la table du personnel
															$model_personnel->iduser = $model->id;
															$model_personnel->keypersonnel = Yii::$app->security->generateRandomString(32);
															if ($model_personnel->save());

															//enregistrement des profil de ce utilisateur dans la table converne
															$allProfil = explode(",", $userProfil);
															if (sizeof($allProfil) > 0) {
																foreach ($allProfil as $idProfil) {
																	$modelAccess = new UserAccess;
																	$modelAccess->id_user = $model->id;
																	$modelAccess->id_user_profil = $idProfil;
																	$modelAccess->etat = 1;
																	$modelAccess->date_create = date("Y-m-d H:i:s");
																	$modelAccess->created_by = Yii::$app->user->identity->id;
																	$modelAccess->save();
																}
															}

															$message = Yii::t('app', 'succes_creation_user');
															Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
															$model = new User;
															$model_personnel = new Personnel;
															$userProfil = "";
														} else {
															$message = Yii::t('app', 'update_error');
															print_r($model->getErrors());
															die;
															Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
														}
													} else {
														$message = Yii::t('app', 'username_used');
														Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
													}
												} else {
													$message = Yii::t('app', 'phone_used');
													Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
												}
											} else {
												$message = Yii::t('app', 'email_used');
												Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
											}
										} else {
											$message = Yii::t('app', 'passe_identique');
											Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
										}
									} else {
										$message = Yii::t('app', 'date_emb_error');
										Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
									}
								} else {
									$message = Yii::t('app', 'telephone_error');
									Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
								}
							} else {
								$message = Yii::t('app', 'phone_format_error');
								Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
							}
						} else {
							$message = Yii::t('app', 'input_empty');
							Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
						}
					} else {
						$message = Yii::t('app', 'empty_center');
						Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
					}
				} else {
					// "L'espace existe!";
					$message = Yii::t('app', 'username_espace_error');
					Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
				}
			} else {
				$message = Yii::t('app', 'input_empty');
				Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
			}
		}

		$title = Yii::t('app', 'add_user_menu');
		$board = array("add_user" => $title);

		$this->page_title = $title;
		$this->breadcrumb = Utils::retour($board);

		return $this->render(
			'add',
			array(
				'model' => $model, 'model_personnel' => $model_personnel, 'userProfil' => $userProfil, 'type' => 'CREATE',
				'allsection' => $allsection,
				'alltype_personnel' => $alltype_personnel,
				'all_grade' => $all_grade,
				'all_situation_matrimoniale' => $all_situation_matrimoniale
			)
		);
	}

	public function actionAll_user()
	{
		Utils::Rule("manage_admin", "READ");

		$idCentre = "-" . Yii::$app->session->get('default_center') . "-";
		$donnee['all_user'] = new ActiveDataProvider([
			'query' => User::find()->where(['status' => ['10', '20']])->andWhere(['!=', 'idP', 0])->andWhere(['like', 'id_center', $idCentre]),
			'pagination' => [
				'pageSize' => -1,
			],
		]);

		$title = Yii::t('app', 'all_admin');
		$board = array("add_user" => $title);

		$this->page_title = $title;
		$this->breadcrumb = Utils::retour($board);

		return $this->render('liste', array('donnee' => $donnee));
	}

	public function actionOperation()
	{
		Utils::Rule("manage_admin", "DELETE");
		if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

			$etat = $_POST['operation'];
			$auth_key = $_POST['id'];

			$find_user = User::find()->where(['auth_key' => $auth_key, 'status' => ['10', '20']])->one();
			if ($find_user !== null) {
				$find_user->status = $etat;
				$find_user->updated_at = time();
				if ($find_user->save()) {
					$message = "";
					if ($etat == 10) {
						$message = Yii::t('app', 'active_user_success');
					} else if ($etat == 20) {
						$message = Yii::t('app', 'desactive_user_success');
					} else if ($etat == 30) {
						$message = Yii::t('app', 'delete_user_success');
					}
					Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
				} else {
					$message = Yii::t('app', 'update_error3') . json_decode($find_user->getErrors());
					Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
				}
			} else {
				$message = Yii::t('app', 'update_error2');
				Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
			}
		} else {
			$message = Yii::t('app', 'update_error1');
			Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
		}
	}

	public function actionUpdate_user()
	{
		Utils::Rule("manage_admin", "UPDATE");
		if (isset($_GET["key"])) {
			$auth_key = $_GET["key"];

			$users = User::find()->where(['auth_key' => $auth_key, 'status' => ['10', '20']])->one();
			if ($users !== null and $users->id != Yii::$app->user->identity->id) {

				$model = $users;
				$infoAccess = UserAccess::find()->select(['GROUP_CONCAT(id_user_profil) key_user_access'])->where(['id_user' => $users->id, 'etat' => 1])->one();
				$userProfil = $infoAccess->key_user_access;

				$model_personnel = Personnel::find()->where(['iduser' => $users->id, 'status' => ['1']])->one();
				// print_r($model_personnel);
				// die;
				$allsection = Section::find()->where(['status' => 1])->all();
				$alltype_personnel = Typepersonnel::find()->where(['status' => 1])->all();
				$all_situation_matrimoniale = SituationMatrimoniale::find()->where(['status' => 1])->all();
				$all_grade = Grade::find()->where(['status' => 1])->all();

				if (Yii::$app->request->post()) {
					if ($model->load(Yii::$app->request->post())) {

						$post = Yii::$app->request->post();
						// print_r($post);
						// die;
						$photo = UploadedFile::getInstance($model, 'photo');
						if ($photo != null) {
							$model->photo = $photo;
							$nomPhoto = 'PHOTO_PROFIL' . time() . '.' . $model->photo->extension;
							// $path = Yii::getAlias("@web/") . 'uploads/';
							$path = 'uploads/';
							if (!is_dir($path)) {
								mkdir($path, 0777);
							}
							$photoPath = $path . $nomPhoto;
							$model->photo->saveAs($photoPath);
							$model->photo = $nomPhoto;
						}
						$model->username = trim($post['User']['username']);
						$espace = " ";

						// Teste si le nom d'utilisateur choisi contient des espaces
						if (strpos($model->username, $espace) !== true) {
							// "L'espace n'existe pas!";
							$model_personnel->indicegrade = trim($post['Personnel']['indicegrade']);
							$model_personnel->situationmatrimoniale = trim($post['Personnel']['situationmatrimoniale']);
							$model_personnel->dateembauche = trim($post['Personnel']['dateembauche']);
							$model_personnel->sectionconcerne = trim($post['Personnel']['sectionconcerne']);
							$model_personnel->typepersonnel = trim($post['Personnel']['typepersonnel']);
							$model_personnel->updated_at = time();
							$model_personnel->updated_by = Yii::$app->user->identity->id;


							$all_center = "";
							$post = Yii::$app->request->post();
							$newProfil = "";
							foreach ($post as $key => $value) {

								if (!in_array($key, array("_csrf", "User", "updatebutton"))) {
									$test_key = explode("geschoolcenter_", $key);
									if (sizeof($test_key) > 1) {

										$response = $test_key[1];
										if (isset($post['profilcenter_' . $response]) && trim($post['profilcenter_' . $response]) != "") {
											if ($all_center != "") $all_center .= ",";
											if ($newProfil != "") $newProfil .= ",";
											$all_center .= "-" . $response . "-";
											$newProfil .= $post['profilcenter_' . $response];
										}
									}
								}
							}
							$userProfil = $newProfil;

							if ($all_center != "") {


								if (trim($model->email) != "" && trim($model->username) != "" && trim($model->telephoneuser) != "" && trim($model->nom) != "" && trim($model->prenoms) != "") {

									//verifier si le phone est correcte
									$testPhone = explode("_", $model->telephoneuser);
									if (sizeof($testPhone) == 1) {
										$model->telephoneuser = preg_replace("/[^0-9]/", "", $model->telephoneuser);

										if (
											preg_match('/^2289[0-4][0-9]{6}$/', $model->telephoneuser)
											or preg_match('/^22870[0-9]{6}$/', $model->telephoneuser)
											or preg_match('/^2289[6-9][0-9]{6}$/', $model->telephoneuser)
											or preg_match('/^22879[0-9]{6}$/', $model->telephoneuser)
										) {

											$today = date('Y-m-d');

											if (strtotime($model_personnel->dateembauche) <= strtotime($today)) {

												$test_email = User::find()->where(['email' => $model->email, 'status' => ['10', '20']])->andWhere(['!=', 'id', $users->id])->one();
												if ($test_email == null) {
													$test_phone = User::find()->where(['telephoneuser' => $model->telephoneuser, 'status' => ['10', '20']])->andWhere(['!=', 'id', $users->id])->one();
													if ($test_phone == null) {
														$test_username = User::find()->where(['username' => $model->username, 'status' => ['10', '20']])->andWhere(['!=', 'id', $users->id])->one();
														if ($test_username == null) {
															$model->updated_at = time();
															$model->id_center = $all_center;
															if ($model->save()) {

																// Enregistrement de la mise à jour dans la table personnel
																$model_personnel->save();

																//mise a jour de son profil		
																UserAccess::updateAll(array('etat' => 3), 'id_user = ' . $users->id . ' and etat=1');

																//enregistrement des profil de ce utilisateur dans la table converne
																$allProfil = explode(",", $userProfil);
																if (sizeof($allProfil) > 0) {
																	foreach ($allProfil as $idProfil) {
																		$modelAccess = new UserAccess;
																		$modelAccess->id_user = $model->id;
																		$modelAccess->id_user_profil = $idProfil;
																		$modelAccess->etat = 1;
																		$modelAccess->date_create = date("Y-m-d H:i:s");
																		$modelAccess->created_by = Yii::$app->user->identity->id;
																		$modelAccess->save();
																	}
																}

																$message = Yii::t('app', 'update_success');
																Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
																return $this->redirect(array('/all_user'));
															} else {
																print_r($model->getErrors());
																die;
																$message = Yii::t('app', 'update_error');
																Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
															}
														} else {
															$message = Yii::t('app', 'username_used');
															Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
														}
													} else {
														$message = Yii::t('app', 'phone_used');
														Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
													}
												} else {
													$message = Yii::t('app', 'email_used');
													Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
												}
											} else {
												$message = Yii::t('app', 'date_emb_error');
												Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
											}
										} else {
											$message = Yii::t('app', 'telephone_error');
											Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
										}
									} else {
										$message = Yii::t('app', 'phone_format_error');
										Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
									}
								} else {
									$message = Yii::t('app', 'input_empty');
									Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
								}
							} else {
								$message = Yii::t('app', 'empty_center');
								Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
							}
						} else {
							// "L'espace existe!";
							$message = Yii::t('app', 'username_espace_error');
							Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
						}
					} else {
						$message = Yii::t('app', 'input_empty');
						Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
					}
				}

				$title = Yii::t('app', 'update_information') . ": " . $users->nom . " " . $users->prenoms;
				$board = array("all_user" => Yii::t('app', 'all_admin'), "update_user" => $title);

				$this->page_title = $title;
				$this->breadcrumb = Utils::retour($board);

				return $this->render(
					'add',
					array(
						'model' => $model, 'userProfil' => $userProfil, 'type' => 'UPDATE',
						'model_personnel' => $model_personnel,
						'allsection' => $allsection,
						'alltype_personnel' => $alltype_personnel,
						'all_grade' => $all_grade,
						'all_situation_matrimoniale' => $all_situation_matrimoniale
					)
				);
			} else {
				return $this->redirect(array('/all_user'));
			}
		} else {
			return $this->redirect(array('/all_user'));
		}
	}

	public function actionReset_user()
	{
		Utils::Rule("manage_admin", "UPDATE");
		if (isset($_GET["key"])) {
			$auth_key = $_GET["key"];
			$model = new User;
			$find_user = User::find()->where(['auth_key' => $auth_key, 'status' => ['10', '20']])->one();
			if ($find_user !== null and $find_user->id != Yii::$app->user->identity->id) {


				if (Yii::$app->request->post()) {

					$post = Yii::$app->request->post();
					$model->password = Yii::$app->request->post()['User']['password'];
					$model->password2 = Yii::$app->request->post()['User']['password2'];

					if (trim($model->password) != "" && trim($model->password2) != "") {
						if (strcmp(trim($model->password), trim($model->password2)) == 0) {


							$find_user->updated_at = time();
							$find_user->password_hash = Yii::$app->security->generatePasswordHash($model->password);

							if ($find_user->save()) {
								$message = Yii::t('app', 'update_success');
								Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
								return $this->redirect(array('/all_user'));
							} else {
								$message = Yii::t('app', 'update_error');
								Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
							}
						} else {
							$message = Yii::t('app', 'passe_identique');
							Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
						}
					} else {
						$message = Yii::t('app', 'input_empty');
						Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
					}
				}

				$title = Yii::t('app', 'reset_password') . ": " . $find_user->nom . " " . $find_user->prenoms;
				$board = array("all_user" => Yii::t('app', 'all_admin'), "update_user" => $title);

				$this->page_title = $title;
				$this->breadcrumb = Utils::retour($board);

				return $this->render('reset', array('model' => $model));
			} else {
				return $this->redirect(array('/all_user'));
			}
		} else {
			return $this->redirect(array('/all_user'));
		}
	}

	//Cette fonction permet de charger le formulaire d'ajout ou de modification d'un plan d'affectation
	public function actionCreate_planaffectation()
	{
		$this->layout = false;
		$return_info = "";
		$manage_planaffectation = Utils::have_access("manage_planaffectation");
		if ($manage_planaffectation == "0_0_0_0") {
			return  $return_info;
		}
		$test = explode('_', $manage_planaffectation);

		if ($test[0] == 0 && $test[2] == 0) {
			return  $return_info;
		}
		if (Yii::$app->request->isPost) {
			$info = Yii::$app->request->post();
			$apicurl = new ApiCurl();


			$key = $info['key'];
			$data = array();

			//On récupère la liste des centres
			$all_center = SchoolCenter::find()->where(['etat' => 1])->orderBy(['denomination_center' => SORT_ASC])->all();

			if ($key == '0') {
				$type = 'CREATE';
				$model = new Planaffectation();
			} else {
				$type = 'UPDATE';
				$data['planaffectation_key'] = $key;
				$response = $apicurl->ApiPost('planaffectations/planaffectation', $data);
				$decode = json_decode($response);
				$status = $decode->status;
				//Fin recherche


				if ($status == '000') {
					$cs = SchoolCenter::findOne(['id_center' => $decode->data->idcenter]);
					$decode->data->idcenter = $cs->key_center;
					//On convertie l'objet en model
					$find_planaffectation = new Planaffectation();
					$find_planaffectation->load((array)$decode->data, '');
					$model = $find_planaffectation;
				} else {
					$model = new Planaffectation();
				}
			}

			$return_info = $this->render('create_affectation', array(
				"model" => $model,
				"all_center" => $all_center,
				// "all_affectation" => $all_affectation,
				// "user_id" => $all_affectation,
				"type" => $type
			));
		}
		return $return_info;
	}

	//Cette fonction permet de charger le formulaire d'ajout ou de modification d'un plan d'affectation
	public function actionUser_planaffectations()
	{
		$this->layout = false;
		$return_info = "";
		$manage_planaffectation = Utils::have_access("manage_planaffectation");
		if ($manage_planaffectation == "0_0_0_0") {
			return  $return_info;
		}
		$test = explode('_', $manage_planaffectation);

		if ($test[0] == 0 && $test[2] == 0) {
			return  $return_info;
		}
		if (Yii::$app->request->isPost) {
			$info = Yii::$app->request->post();
			$apicurl = new ApiCurl();

			$key = $info['key'];

			$data = array();
			$all_affectation = new ArrayDataProvider();

			// On récupère la liste des affectation du concerné
			$data['iduser'] = $key;
			$response = $apicurl->ApiPost('planaffectations/planaffectations', $data);
			$decode = json_decode($response);
			$status = $decode->status;
			if ($status == '000') {
				$all_affectation->allModels = $decode->data;
			} else {
				$all_affectation->allModels = array();
			}


			$data['iduser'] = $key;
			$response = $apicurl->ApiPost('planaffectations/planaffectation', $data);
			$decode = json_decode($response);
			$status = $decode->status;
			//Fin recherche


			$return_info = $this->render('liste_affectation', array(
				"all_affectation" => $all_affectation,
			));
		}
		return $return_info;
	}


	//Cette fonction permet de sauvegarder un nouveau plan d'affectation 
	public function actionSave_planaffectation()
	{
		$return_info = "";
		$this->layout = false;
		$manage_planaffectation = Utils::have_access("manage_planaffectation");
		if ($manage_planaffectation == "0_0_0_0") {
			return  $return_info;
		}
		$test = explode('_', $manage_planaffectation);

		if ($test[0] == 0 && $test[2] == 0) {
			return  $return_info;
		}
		if (Yii::$app->request->isPost) {

			$info = Yii::$app->request->post();

			$key = urldecode($info['key']);
			$user_id = urldecode($info['user']);
			$center_key = urldecode($info['center']);
			$date_debut = urldecode($info['date_debut']);
			$date_fin = urldecode($info['date_fin']);
			$date_debut_effective = urldecode($info['date_debut_effective']);
			$date_fin_effective = urldecode($info['date_fin_effective']);

			$fullInfo = array();
			$fullInfo['useraffectation_id'] = $user_id;
			$fullInfo['centreaffectation_key'] = $center_key;
			$fullInfo['datedebut'] = $date_debut;
			$fullInfo['datefin'] = $date_fin;
			$fullInfo['operation'] = 1;
			$fullInfo['datedebuteffective'] = $date_debut_effective;
			$fullInfo['datedfineffective'] = $date_fin_effective;



			if ($key != '0') {
				$fullInfo['operation'] = 2;
				$fullInfo['planaffectation_key'] = $key;
			}


			$apicurl = new ApiCurl();
			$response = $apicurl->ApiPost('planaffectations/addplanaffectation', $fullInfo);
			$decode = json_decode($response);
			$status = $decode->status;


			if (isset($status)) {
				if ($status == '000') { // TOUT S'EST BIEN PASSÉ

					$return_alert = '<div class="alert alert-success alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';

					$all_affectation = new ArrayDataProvider();
					$all_affectation->allModels = $decode->data;
					$user_planaffectations = $this->render('liste_affectation', array(
						"all_affectation" => $all_affectation,
					));
					$return_info = '1###' . $return_alert . '###' . $user_planaffectations;
				} else if ($status == '001') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
					$return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';
					$return_info = '1###' . $return_alert;
				} else if ($status == '002') { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
					$return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
				}
			} else {
				$return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
			}
		}
		return $return_info;
	}


	public function actionDelete_affectation()
	{
		$this->layout = false;
		//On v�rifie les droits de l'user
		Utils::Rule("manage_planaffectation", "DELETE");
		$return_info = "";
		if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

			// $etat = $_POST['operation'];
			$key = $_POST['id'];

			$fullInfo = array(
				'planaffectation_key' => $key,
			);

			//On recherche la ressource � modifier
			$apicurl = new ApiCurl();
			$response = $apicurl->ApiPost('planaffectations/planaffectation', $fullInfo);
			$decode = json_decode($response);
			$status = $decode->status;
			//Fin recherche

			if ($status == '000') {
				//Execution de l'api pour la modification
				// $fullInfo['operation'] = $etat;
				$response = $apicurl->ApiPost('planaffectations/deleteplanaffectation', $fullInfo);
				$decode = json_decode($response);
				$status = $decode->status;
				//Fin execution

				if ($status == '000') { // TOUT S'EST BIEN PASSÉ

					$return_alert = '<div class="alert alert-success alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';
					$all_affectation = new ArrayDataProvider();
					$all_affectation->allModels = $decode->data;
					$user_planaffectations = $this->render('liste_affectation', array(
						"all_affectation" => $all_affectation,
					));
					$return_info = '1###' . $return_alert . '###' . $user_planaffectations;
				} else if ($status == '001') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
					$return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';
					$return_info = '1###' . $return_alert;
				} else if ($status == '002') { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
					$return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
				}
			} else {
				$return_info = "0";
			}
		} else {
			$return_info = "0";
		}
		return $return_info;
	}
}
