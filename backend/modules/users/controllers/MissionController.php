<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Mission;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\modules\users\models\User;
use yii\filters\AccessControl;

/**
 * MissionController implements the CRUD actions for Mission model.
 */
class MissionController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "ADMIN";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_mission', 'operation', 'create_mission', 'save_mission'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les missions
    public function actionAll_mission()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_mission", "READ");
        $id_center = Yii::$app->session->get('default_center');
        $response_data = array();


        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "missions/missions";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);

        // print_r($response);
        // die;


        // if ($response->status == '000') {
        //     $response_data = $response->data;
        // }

        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } else if (isset($response->status) && ($response->status == '001')) {
            $response_data = array();
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('warning', Yii::t('app', $message));
        }


        $donnee['all_mission'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_mission_menu');
        $board = array("add_mission" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }


    //Cette fonction permet de charger le formulaire d'ajout ou de modification diplome
    public function actionCreate_mission()
    {
        $return_info = "";
        $this->layout = false;
        $manage_mission = Utils::have_access("manage_mission");
        if ($manage_mission == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_mission);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {

            $info = Yii::$app->request->post();
            $id_center = Yii::$app->session->get('default_center');
            $key = $info['key'];

            if ($key == '0') {
                $model = new Mission();
            } else {

                $url_api = Utils::getApiUrl() . "missions/mission";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'mission_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche


                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_mission = new Mission();
                    $find_mission->load((array)$response->data, '');
                    $model = $find_mission;
                } else {
                    $model = new Mission();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model));
        }
        return $return_info;
    }


    //Cette fonction permet de sauvegarder une nouvelle mission 
    public function actionSave_mission()
    {
        $return_info = "";
        $this->layout = false;
        $manage_mission = Utils::have_access("manage_mission");
        if ($manage_mission == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_mission);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $motif = urldecode($info['motif']);
            $date_depart = urldecode($info['datedepart']);
            $lieu_mission = urldecode($info['lieumission']);
            $date_retour_probable = urldecode($info['dateretourprob']);
            $itineraire_retenu = urldecode($info['itineraireretenu']);


            $fullInfo = array();
            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['motif'] = $motif;
            $fullInfo['lieu_mission'] = $lieu_mission;
            $fullInfo['date_depart'] = $date_depart;
            $fullInfo['date_retour_probable'] = $date_retour_probable;
            $fullInfo['itineraire_retenu'] = $itineraire_retenu;
            $fullInfo['operation'] = 1;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;

            if ($key != '0') {
                $fullInfo['mission_key'] = $key;
                $fullInfo['operation'] = 2;
            }


            $url_api = Utils::getApiUrl() . "missions/addmission";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);

            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_mission'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } elseif ($response->status == '001') {
                    // Un element manque sur le formulaire soumis
                    $return_info = "1";
                } elseif ($response->status == '003') {
                    // L'element existe déjà
                    $return_info = "3";
                } else {
                    $return_info = "0";
                }
            } else {
                $return_info = "0";
            }
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un mission
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_mission", "DELETE");
        $id_center = Yii::$app->session->get('default_center');

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keymission = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "missions/mission";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'mission_key' => $keymission,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "missions/deletemission";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'mission_key' => $keymission,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_mission_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_mission_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_mission_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
