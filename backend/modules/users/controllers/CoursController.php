<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Cours;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use yii\filters\AccessControl;

/**
 * CoursController implements the CRUD actions for cours model.
 */
class CoursController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "COURS";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_cours', 'operation', 'create_cours', 'save_cours'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les cours
    public function actionAll_cours()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_cours", "READ");
        $id_center = Yii::$app->session->get('default_center');
        // $response_data = Cours::find()->where(['status' => [1, 2]])->all();
        $response_data = array();

        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "cours/cours";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);


        // print_r($response);die;

        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_cours'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_cours_menu');
        $board = array("add_cours" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification cours
    public function actionCreate_cours()
    {
        $return_info = "";
        $this->layout = false;
        $manage_cours = Utils::have_access("manage_cours");
        if ($manage_cours == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_cours);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Cours();
            } else {
                $url_api = Utils::getApiUrl() . "cours/cour";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'cours_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche

                // print_r($response);
                // die;

                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_cours = new Cours();
                    $find_cours->load((array)$response->data, '');
                    $model = $find_cours;
                } else {
                    $model = new Cours();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model));
        }
        return $return_info;
    }

    //Cette fonction permet de sauvegarder un type de personnel
    public function actionSave_cours()
    {
        $return_info = "";
        $this->layout = false;
        $manage_cours = Utils::have_access("manage_cours");
        if ($manage_cours == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_cours);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $designation = urldecode($info['designation']);
            $typecours = urldecode($info['typecours']);
            $fullInfo = array();


            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['designation'] = $designation;
            $fullInfo['typecours'] = $typecours;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['cours_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $url_api = Utils::getApiUrl() . "cours/addcours";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);


            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_cours'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } else {
                    // $return_info = "1" . "###" . $response->message;
                    $return_info = '1###<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px"> <div class="alert-body">' . Yii::t('app', $response->message) . ' </div></div>';
                }
            } else {
                $return_info = "0";
            }


            /*
                if (isset($response->status)) {
                    if ($response->status == '000') {
                        $donnee['all_cours'] = new ArrayDataProvider([
                            'allModels' => $response->data,
                            'pagination' => [
                                'pageSize' => -1,
                            ],
                        ]);

                        $return_info = $this->render('liste', array('donnee' => $donnee));
                    } elseif ($response->status == '001') {
                        // Un element manque sur le formulaire soumis
                        $return_info = "1";
                    } elseif ($response->status == '003') {
                        // L'element existe déjà
                        $return_info = "3";
                    } else {
                        $return_info = "0";
                    }
                } else {
                    $return_info = "0";
                }
            */
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un cours
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_cours", "DELETE");
        $id_center = Yii::$app->session->get('default_center');

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keycours = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "cours/cour";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'cours_key' => $keycours,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "cours/deletecours";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'cours_key' => $keycours,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_cours_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_cours_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_cours_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
