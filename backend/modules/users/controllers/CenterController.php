<?php

namespace backend\modules\users\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\modules\users\models\SchoolCenter;
use backend\modules\users\models\SchoolModule;
use backend\modules\users\models\AccessRight;
use backend\controllers\Utils;
use backend\controllers\DefaultController;

use yii\data\ActiveDataProvider;

/**
 * Site controller
 */
class CenterController extends DefaultController
{

	public $page_title = "";
	public $breadcrumb = "";
	public $select_menu = "CENTER";

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['add_center', 'all_center', 'operation', 'update_center', 'center_module', 'checking_center'],
						'roles' => ['@'],
					],
				],
			],
		];
	}


	public function actionChecking_center()
	{
		$response = "";
		if (isset($_POST['id_center']) != "") {

			$find_module = SchoolModule::find()->select(['GROUP_CONCAT(id_right) key_school_module'])->where(['id_center' => trim($_POST['id_center']), 'etat' => 1])->one();
			if ($find_module != null) {
				$response = $find_module->key_school_module;
			}
		}
		return $response;
		exit();
	}

	public function actionCenter_module()
	{
		Utils::Rule("manage_module", "CREATE");
		$this->select_menu = "MODULE";

		$model = new SchoolModule;

		if (Yii::$app->request->post()) {

			$post = Yii::$app->request->post();
			$model->id_center = $post['SchoolModule']['id_center'];

			if (trim($model->id_center) != "") {

				$tab_active = [];

				foreach ($post as $key => $value) {

					if (!in_array($key, array("_csrf", "SchoolModule", "updatebutton"))) {
						$test_key = explode("right_", $key);
						if (sizeof($test_key) > 1) {
							$tab_active[] = $test_key[1];
						}
					}
				}
				//renitialisation des droits disponible					
				SchoolModule::updateAll(array('etat' => 3, 'updated_by' => Yii::$app->user->identity->id), 'id_center = ' . $model->id_center . ' and etat=1');

				$access_right = AccessRight::find()->select(['id_right', 'lib_right'])->where(['etat_right' => 1])->all();
				foreach ($access_right as $right) {
					if (!in_array($right->id_right, $tab_active)) {

						$modelSmodule = new SchoolModule;
						$modelSmodule->key_school_module = Yii::$app->security->generateRandomString(32);
						$modelSmodule->id_center = $model->id_center;
						$modelSmodule->id_right = $right->id_right;
						$modelSmodule->etat = 1;
						$modelSmodule->date_create = date("Y-m-d H:i:s");
						$modelSmodule->created_by = Yii::$app->user->identity->id;
						$modelSmodule->save();
					}
				}

				$message = Yii::t('app', 'success_operation');
				Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
				$model = new SchoolModule;
			} else {
				$message = Yii::t('app', 'input_empty');
				Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
			}
		}

		$title = Yii::t('app', 'center_module');
		$board = array("add_center" => $title);

		$this->page_title = $title;
		$this->breadcrumb = Utils::retour($board);

		return $this->render('center_module', array('model' => $model));
	}

	public function actionAdd_center()
	{
		Utils::Rule("manage_center", "CREATE");


		$model = new SchoolCenter;

		if (Yii::$app->request->post()) {
			if ($model->load(Yii::$app->request->post())) {

				if (trim($model->denomination_center) != "" && trim($model->localite_center) != "" && trim($model->sms_disponible) != "") {
					$test_denomination_center = SchoolCenter::find()->where(['denomination_center' => $model->denomination_center, 'etat' => ['1', '2']])->one();
					if ($test_denomination_center == null) {

						$model->etat = 1;
						$model->date_create = date("Y-m-d H:i:s");
						$model->created_by = Yii::$app->user->identity->id;
						$model->key_center = Yii::$app->security->generateRandomString(32);

						if ($model->save()) {
							$message = Yii::t('app', 'succes_creation_center');
							Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
							$model = new SchoolCenter;
						} else {
							$message = Yii::t('app', 'update_error');
							Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
						}
					} else {
						$message = Yii::t('app', 'center_used');
						Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
					}
				} else {
					$message = Yii::t('app', 'input_empty');
					Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
				}
			} else {
				$message = Yii::t('app', 'input_empty');
				Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
			}
		}

		$title = Yii::t('app', 'add_center_menu');
		$board = array("add_center" => $title);

		$this->page_title = $title;
		$this->breadcrumb = Utils::retour($board);

		return $this->render('add', array('model' => $model));
	}

	public function actionAll_center()
	{
		Utils::Rule("manage_center", "READ");

		$tab_request['etat'] = ['1', '2'];
		$id_center = str_replace('-', '', Yii::$app->user->identity->id_center);
		$test_center = explode(",", $id_center);
		if ($id_center != "1") {
			$tab_request['id_center'] = $test_center;
		}

		$donnee['all_center'] = new ActiveDataProvider([
			'query' => SchoolCenter::find()->where($tab_request),
			'pagination' => [
				'pageSize' => -1,
			],
		]);

		$title = Yii::t('app', 'all_center_menu');
		$board = array("add_center" => $title);

		$this->page_title = $title;
		$this->breadcrumb = Utils::retour($board);

		return $this->render('liste', array('donnee' => $donnee));
	}

	public function actionOperation()
	{
		Utils::Rule("manage_center", "DELETE");
		if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

			$etat = $_POST['operation'];
			$key_center = $_POST['id'];


			$tab_request['etat'] = ['1', '2'];
			$tab_request['key_center'] = $key_center;
			$id_center = str_replace('-', '', Yii::$app->user->identity->id_center);
			$test_center = explode(",", $id_center);
			if ($id_center != "1") {
				$tab_request['id_center'] = $test_center;
			}


			$find_center = SchoolCenter::find()->where($tab_request)->one();
			if ($find_center !== null) {
				$find_center->etat = $etat;
				$find_center->updated_by = Yii::$app->user->identity->id;
				if ($find_center->save()) {
					$message = "";
					if ($etat == 1) {
						$message = Yii::t('app', 'active_center_success');
					} else if ($etat == 2) {
						$message = Yii::t('app', 'desactive_center_success');
					} else if ($etat == 3) {
						$message = Yii::t('app', 'delete_center_success');
					}
					Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
				} else {
					$message = Yii::t('app', 'update_error');
					Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
				}
			} else {
				$message = Yii::t('app', 'update_error');
				Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
			}
		} else {
			$message = Yii::t('app', 'update_error');
			Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
		}
	}

	public function actionUpdate_center()
	{
		Utils::Rule("manage_center", "UPDATE");
		if (isset($_GET["key"])) {
			$key_center = $_GET["key"];


			$tab_request['etat'] = ['1', '2'];
			$tab_request['key_center'] = $key_center;
			$id_center = str_replace('-', '', Yii::$app->user->identity->id_center);
			$test_center = explode(",", $id_center);
			if ($id_center != "1") {
				$tab_request['id_center'] = $test_center;
			}

			$find_center = SchoolCenter::find()->where($tab_request)->one();
			if ($find_center !== null) {

				$model = $find_center;
				if (Yii::$app->request->post()) {
					if ($model->load(Yii::$app->request->post())) {


						if (trim($model->denomination_center) != "" && trim($model->localite_center) != "" && trim($model->sms_disponible) != "") {

							$test_denomination_center = SchoolCenter::find()->where(['denomination_center' => $model->denomination_center, 'etat' => ['1', '2']])->andWhere(['!=', 'id_center', $find_center->id_center])->one();
							if ($test_denomination_center == null) {
								$model->updated_by = Yii::$app->user->identity->id;
								if ($model->save()) {
									$message = Yii::t('app', 'update_success');
									Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
									return $this->redirect(array('/all_center'));
								} else {
									$message = Yii::t('app', 'update_error');
									Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
								}
							} else {
								$message = Yii::t('app', 'center_used');
								Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
							}
						} else {
							$message = Yii::t('app', 'input_empty');
							Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
						}
					} else {
						$message = Yii::t('app', 'input_empty');
						Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
					}
				}

				$title = Yii::t('app', 'update_information') . ": " . $find_center->denomination_center;
				$board = array("all_center" => Yii::t('app', 'all_center_menu'), "update_center" => $title);

				$this->page_title = $title;
				$this->breadcrumb = Utils::retour($board);

				return $this->render('add', array('model' => $model));
			} else {
				return $this->redirect(array('/all_center'));
			}
		} else {
			return $this->redirect(array('/all_center'));
		}
	}
}
