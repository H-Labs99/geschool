<?php
namespace backend\modules\users\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use backend\controllers\Utils;
use backend\controllers\DefaultController;

use backend\modules\users\models\User;
use backend\modules\users\models\UserProfil;
use backend\modules\users\models\ProfilAccess;
use backend\modules\users\models\AccessRight;
use backend\modules\users\models\UserAccess;



/**
 * Site controller
 */
class ProfilController extends DefaultController
{
	public $page_title="";
	public $breadcrumb="";
	public $select_menu ="PROFIL";
   
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [                  
                    [
                        'allow' => true,
                        'actions' => ['add_profil','all_profil','operation','update_profil'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
	
	
    
	public function actionAdd_profil()
	{    
		Utils::Rule("manage_profil","CREATE");
        $model=new UserProfil;	
		$id_center=Yii::$app->session->get('default_center');
		
		if (Yii::$app->request->post()){
			if ($model->load(Yii::$app->request->post())){
					$post=Yii::$app->request->post();
				
					$module_id=Yii::$app->request->post()['all_module'];
					if(trim($model->nameProfil)!=""){	
						$recup=explode("_",$module_id);
						$all_module="";
						
						if(sizeof($recup)>0){
								foreach ($recup as $id_role){
								
									 $create_info="0";
									 $delete_info="0";
									 $read_info="0";
									 $update_info="0";
									
									if(isset($_POST[$id_role.'_CREATE'])){
										if($_POST[$id_role.'_CREATE']==1)   $create_info=1; 
									}
									if(isset($_POST[$id_role.'_LIST'])){
										if($_POST[$id_role.'_LIST']==1)     $read_info=1; 
									}
									
									if(isset($_POST[$id_role.'_UPDATE'])){
										if($_POST[$id_role.'_UPDATE']==1)   $update_info=1; 
									}
									
									if(isset($_POST[$id_role.'_DELETE'])){
										if($_POST[$id_role.'_DELETE']==1)   $delete_info=1; 
									}
									
									$information=$id_role.'_'.$create_info.'_'.$read_info.'_'.$update_info.'_'.$delete_info;
									
									  
									if($all_module==""){           
											$all_module=$information;
									}else {           
											$all_module.=";".$information ;
									}
									
								}
								
								$idUser=Yii::$app->user->identity->id;
					 
								
								$test_name = UserProfil::findOne(['nameProfil'=>$model->nameProfil,'id_center'=>$id_center,'etat_user_profil'=>['1','2']]);
								if($test_name !== null ){                
									$message=Yii::t('app', 'profil_exist');
									Yii::$app->getSession()->setFlash('error',Yii::t('app', $message)); 
								}else{		
										date_default_timezone_set('UTC'); 
										
										$new_profil=new UserProfil();
										$new_profil->nameProfil=$model->nameProfil;
										$new_profil->descriptionProfil=$model->descriptionProfil;										
										$new_profil->etat_user_profil=1;
										$new_profil->id_center =$id_center;
										$new_profil->id_userUpdate=0;
										$new_profil->id_userCreate=$idUser;
										$new_profil->date_create_profil=date("Y-m-d H:i:s");
										$new_profil->date_update_profil=date("Y-m-d H:i:s");
										$new_profil->key_profil=Yii::$app->security->generateRandomString(32);
										
										if($new_profil->save()){
											$array_all_module=explode(';',$all_module);													   
											foreach($array_all_module as $module){
													$info_module=explode('_',$module);
													$new_access=new ProfilAccess();
													$new_access->id_profil=$new_profil->id_user_profil;
													$new_access->id_right=$info_module[0];
													$new_access->pcreate=$info_module[1];
													$new_access->pread=$info_module[2];
													$new_access->pupdate=$info_module[3];
													$new_access->pdelete=$info_module[4];
													$new_access->id_userCreate=$idUser;
													$new_access->etat_profil_access=1;
													$new_access->date_create_aprofil=date("Y-m-d H:i:s");
													$new_access->save();
											}
											$model=new UserProfil;												
											$message=Yii::t('app', 'profil_succes');
											Yii::$app->getSession()->setFlash('success',Yii::t('app', $message)); 																					
										}else{		
											$message=Yii::t('app', 'update_error');
											Yii::$app->getSession()->setFlash('error',Yii::t('app', $message)); 																					
										}
										
										
								}
								
								
						}else{
							$message=Yii::t('app', 'update_error');
							Yii::$app->getSession()->setFlash('error',Yii::t('app', $message)); 
						}
					}else {
							 $message=Yii::t('app', 'profil_empty');
							 Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));            
					}		
			}else {
					$message=Yii::t('app', 'profil_empty');
					Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
			}
		} 
		
		
		   
		$title=Yii::t('app', 'add_profil_menu');		
		$board=array("add_user"=>$title);
		
		$this->page_title=$title;
		$this->breadcrumb=Utils::retour($board);		
		return $this->render('add',array('model'=>$model,'user_module'=>array()));    
	
	}
	
	public function actionAll_profil()
	{        
		    Utils::Rule("manage_profil","READ");
			$donnee['all_profils'] = new ActiveDataProvider([
				'query' => UserProfil::find()->where(['etat_user_profil'=>['1','2'],'id_center'=>Yii::$app->session->get('default_center')])->orderBy("nameProfil ASC"),
				'pagination' => [
					'pageSize' => -1,
				],
			]);
		   
			$title=Yii::t('app', 'liste_profil_menu');		
			$board=array("add_user"=>$title);
			
			$this->page_title=$title;
			$this->breadcrumb=Utils::retour($board);
		
		   return $this->render('liste',array('donnee'=>$donnee)); 
	}	
	
	public function actionOperation(){
	 
		Utils::Rule("manage_profil","DELETE");
		if(isset($_POST['operation'])!="" and isset($_POST['id'])!=""){
			
				$etat=$_POST['operation'];
				$key_profil=$_POST['id'];
				
				
				$find_profil = UserProfil::findOne(['key_profil'=>$key_profil,'id_center'=>Yii::$app->session->get('default_center')]);
				if($find_profil !== null){
						//verifier si ce profil est deja utilise
						$test_name=UserAccess::find()->where(['id_user_profil'=>$find_profil->id_user_profil,'etat'=>['1','2']])->all();            
						
						if(sizeof($test_name)>0 and ($etat==3 or $etat==2)){                
							$message=Yii::t('app','delete_profil_error');
							Yii::$app->getSession()->setFlash('error',Yii::t('app', $message)); 
						}else{
								$find_profil->etat_user_profil=$etat;
								if($find_profil->save()){
										$message="";
										if($etat==1){
											$message=Yii::t('app','active_profil_success');
										}else if($etat==2){
											$message=Yii::t('app','desactive_profil_success');
										}else if($etat==3){
											$message=Yii::t('app','delete_profil_success');
											ProfilAccess::updateAll(array('etat_profil_access'=>$etat), 'id_profil = '.$find_profil->id_user_profil );
										}
										Yii::$app->getSession()->setFlash('success',Yii::t('app', $message)); 
								
								}else{							
										$message=Yii::t('app','update_error')  ;
										Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
								}
														
						}
				
				}else{							
						$message=Yii::t('app','update_error')  ;
						Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
				}
		}else{							
				$message=Yii::t('app','update_error')  ;
				Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
		}
	}
		
	public function actionUpdate_profil()
	{  
		Utils::Rule("manage_profil","UPDATE");   
		if(isset($_GET["key"])){  
		    $key_profil=$_GET["key"];
			
			$id_center=Yii::$app->session->get('default_center');
			$profil_info = UserProfil::findOne(['key_profil'=>$key_profil,'id_center'=>$id_center,'etat_user_profil'=>['1','2']]);		
			
			if($profil_info !== null){			
					
					
					$model=$profil_info;
					
					if (Yii::$app->request->post()){
						if ($model->load(Yii::$app->request->post())){
							$post=Yii::$app->request->post();
						
							$module_id=Yii::$app->request->post()['all_module'];
							if(trim($model->nameProfil)!=""){	
						
								$recup=explode("_",$module_id);
								$all_module="";
								
								if(sizeof($recup)>0){
										foreach ($recup as $id_role){
										
											 $create_info="0";
											 $delete_info="0";
											 $read_info="0";
											 $update_info="0";
											
											
											 if(isset($_POST[$id_role.'_CREATE'])){
												if($_POST[$id_role.'_CREATE']==1)   $create_info=1; 
											}
											if(isset($_POST[$id_role.'_LIST'])){
												if($_POST[$id_role.'_LIST']==1)     $read_info=1; 
											}
											
											if(isset($_POST[$id_role.'_UPDATE'])){
												if($_POST[$id_role.'_UPDATE']==1)   $update_info=1; 
											}
											
											if(isset($_POST[$id_role.'_DELETE'])){
												if($_POST[$id_role.'_DELETE']==1)   $delete_info=1; 
											}
											
											$information=$id_role.'_'.$create_info.'_'.$read_info.'_'.$update_info.'_'.$delete_info;                                    
											  
											if($all_module==""){           
													$all_module=$information;
											}else {           
													$all_module.=";".$information ;
											}
											
										}
										
										
										$test_name = UserProfil::find()->where(['id_center'=>$id_center,'nameProfil'=>$model->nameProfil,'etat_user_profil'=>['1','2']])->andWhere(['!=', 'id_user_profil', $profil_info->id_user_profil])->all();
										if(sizeof($test_name) != 0){                
											$message=Yii::t('app', 'profil_exist');
											Yii::$app->getSession()->setFlash('error',Yii::t('app', $message)); 
										}else{		
												$idUser=Yii::$app->user->identity->id;
							
												date_default_timezone_set('UTC'); 
												
												$profil_info->id_center=$id_center;
												$profil_info->nameProfil=$model->nameProfil;
												$profil_info->descriptionProfil=$model->descriptionProfil;										
												$profil_info->date_update_profil=date("Y-m-d H:i:s");
												$profil_info->id_userUpdate=$idUser;
												
												if($profil_info->save()){
													
													ProfilAccess::deleteAll('id_profil = :id_profil', [':id_profil' => $profil_info->id_user_profil]);
													
													$array_all_module=explode(';',$all_module);													   
													foreach($array_all_module as $module){
															$info_module=explode('_',$module);
															$new_access=new ProfilAccess();
															$new_access->id_profil=$profil_info->id_user_profil;
															$new_access->id_right=$info_module[0];
															$new_access->pcreate=$info_module[1];
															$new_access->pread=$info_module[2];
															$new_access->pupdate=$info_module[3];
															$new_access->pdelete=$info_module[4];
															$new_access->id_userCreate=$idUser;
															$new_access->etat_profil_access=1;
															$new_access->date_create_aprofil=date("Y-m-d H:i:s");
															$new_access->save();
													}
													$message=Yii::t('app', 'update_success');
													Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
													return $this->redirect(array('/all_profil'));																					
												}else{		
													$message=Yii::t('app', 'update_error');
													Yii::$app->getSession()->setFlash('error',Yii::t('app', $message)); 																					
												}
										}		
										
								}else{
									$message=Yii::t('app','update_error');
									Yii::$app->getSession()->setFlash('error',Yii::t('app', $message)); 
								}
					
							
							} else {
								$message=Yii::t('app', 'profil_empty');
								Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
							}
						} else {
								$message=Yii::t('app', 'profil_empty');
								Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
						}
					} 
					
					
					   
					$user_module=ProfilAccess::find()->where(['id_profil'=>$profil_info->id_user_profil,'etat_profil_access'=>1])->all();
						
									
					$title=Yii::t('app','update_information').": ".$profil_info->nameProfil ;
					$board=array("all_profil"=>Yii::t('app','liste_profil_menu'),"update_profil"=>$title);
					
					$this->page_title=$title;
					$this->breadcrumb=Utils::retour($board);		
					return $this->render('add',array('model'=>$model,'user_module'=>$user_module)); 
			}else{
			  return $this->redirect(array('/all_profil'));
			}
		}else{
		   return $this->redirect(array('/all_profil'));
		}				
	}
	
}
