<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "grade".
 *
 * @property integer $id
 * @property string $key_grade
 * @property string $libelle
 * @property integer $status
 * @property integer $date_create
 * @property integer $date_update
 * @property integer $create_by
 * @property integer $update_by
 */
class Grade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelle'], 'required'],
            [['status', 'date_create', 'date_update', 'create_by', 'update_by'], 'integer'],
            [['key_grade'], 'string', 'max' => 32],
            [['libelle'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key_grade' => 'Key Grade',
            'libelle' => 'Libelle',
            'status' => 'Status',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
        ];
    }
}
