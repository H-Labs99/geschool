<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "absence".
 *
 * @property integer $id
 * @property integer $idtypeabsence
 * @property integer $iduser
 * @property string $keyabsence
 * @property string $datedemandeabsence
 * @property string $motifabsence
 * @property string $datedebutabsence
 * @property string $heuredebutabsence
 * @property string $datefinabsence
 * @property string $heurefinabsence
 * @property string $coordonneesabsence
 * @property string $lieudestinantionabsence
 * @property integer $heurearattraper
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Personnel $iduser0
 * @property Typeabsence $idtypeabsence0
 */
class Absence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'absence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idtypeabsence', 'iduser', 'keyabsence', 'datedemandeabsence', 'motifabsence', 'datedebutabsence', 'datefinabsence', 'created_at', 'create_by'], 'required'],
            [['idtypeabsence', 'iduser', 'heurearattraper', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['datedemandeabsence', 'datedebutabsence', 'heuredebutabsence', 'datefinabsence', 'heurefinabsence'], 'safe'],
            [['motifabsence'], 'string'],
            [['keyabsence'], 'string', 'max' => 32],
            [['coordonneesabsence'], 'string', 'max' => 254],
            [['lieudestinantionabsence'], 'string', 'max' => 100],
            [['iduser'], 'exist', 'skipOnError' => true, 'targetClass' => Personnel::className(), 'targetAttribute' => ['iduser' => 'iduser']],
            [['idtypeabsence'], 'exist', 'skipOnError' => true, 'targetClass' => Typeabsence::className(), 'targetAttribute' => ['idtypeabsence' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idtypeabsence' => 'Idtypeabsence',
            'iduser' => 'Iduser',
            'keyabsence' => 'Keyabsence',
            'datedemandeabsence' => 'Datedemandeabsence',
            'motifabsence' => 'Motifabsence',
            'datedebutabsence' => 'Datedebutabsence',
            'heuredebutabsence' => 'Heuredebutabsence',
            'datefinabsence' => 'Datefinabsence',
            'heurefinabsence' => 'Heurefinabsence',
            'coordonneesabsence' => 'Coordonneesabsence',
            'lieudestinantionabsence' => 'Lieudestinantionabsence',
            'heurearattraper' => 'Heurearattraper',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIduser0()
    {
        return $this->hasOne(Personnel::className(), ['iduser' => 'iduser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdtypeabsence0()
    {
        return $this->hasOne(Typeabsence::className(), ['id' => 'idtypeabsence']);
    }
}
