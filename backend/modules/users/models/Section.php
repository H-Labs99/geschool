<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property integer $idtypeformation
 * @property string $designation
 * @property string $keysection
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Centresection[] $centresections
 * @property Classe[] $classes
 */
class Section extends \yii\db\ActiveRecord
{
    public $idtypeformation;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['designation', 'keysection', 'created_at', 'create_by'], 'required'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['designation'], 'string', 'max' => 254],
            [['keysection'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idtypeformation' => 'Idtypeformation',
            'designation' => 'Designation',
            'keysection' => 'Keysection',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentresections()
    {
        return $this->hasMany(Centresection::className(), ['idsection' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasses()
    {
        return $this->hasMany(Classe::className(), ['idsection' => 'id']);
    }
}
