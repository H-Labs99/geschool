<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "typedecoupage".
 *
 * @property integer $id
 * @property string $libelle
 * @property string $keytypedecoupage
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Decoupage[] $decoupages
 */
class Typedecoupage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typedecoupage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelle', 'keytypedecoupage', 'created_at', 'create_by'], 'required'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['libelle'], 'string', 'max' => 100],
            [['keytypedecoupage'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'libelle' => 'Libelle',
            'keytypedecoupage' => 'Keytypedecoupage',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDecoupages()
    {
        return $this->hasMany(Decoupage::className(), ['idtypedecoupage' => 'id']);
    }
}
