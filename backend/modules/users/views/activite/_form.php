<div class="modal fade Form-modal-lg" tabindex="-1" role="dialog" aria-labelledby="FormLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <h4 class="modal-title" id="FormLargeModalLabel">Large modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- body modal -->
            <div class="modal-body" id="Formcontent_image">
                ...
            </div>

            <div class="modal-body" id="FormLargeModalContent">
                ...
            </div>

            <input type="hidden" value="0" name="key_form" id="key_form" />
            <div class="card-footer" id="Formcontent_footer">
                <button type="button" id="Formcontent_close" class="btn btn-sm btn-dark" style="margin-right:100px" data-dismiss="modal"><?= Yii::t('app', 'cancel') ?></button>
                <button type="button" class="btn btn-sm btn-success antosubmit" onclick="save_member()"><?= Yii::t('app', 'submit') ?></button>
            </div>

        </div>
    </div>
</div>