<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\controllers\Utils;	
$required_sign=Utils::required();
?>
<div class="card">
			<?php $form = ActiveForm::begin([
			'id' => 'form-signup',
			'fieldConfig' => [
				'options'=>[
					'tag'=>false,
				]
			]
			]); ?>	 
			<div class="card-body card-block">
				
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'user_password')?> <?=$required_sign?></label></div>                     
					<div class="input-group col-sm-5" >
						<div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-lock"></i></div></div>
						<?= $form->field($model, 'password')->textInput(['required'=>'required','type'=>'password'])->error(false)->Label(false); ?>
								
					</div>					 
				</div>
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'cuser_password')?> <?=$required_sign?></label></div>                     
					<div class="input-group col-sm-5" >
						<div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-lock"></i></div></div>
						<?= $form->field($model, 'password2')->textInput(['required'=>'required','type'=>'password'])->error(false)->Label(false); ?>
								
					</div>					 
				</div>			
			</div>
			<div class="card-footer">
				<?=Utils::resetbtn();?>
				<?= Html::submitButton(Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
			</div>
		<?php ActiveForm::end(); ?>
</div>