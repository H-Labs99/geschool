<div class="modal fade bs-example-modal-lg" style="z-index: 999999;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<!-- header modal -->
			<div class="modal-header">
				<h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
				<button type="button" id="button_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<!-- body modal -->
			<div class="modal-body" id="myLargeModalContent">
				...
			</div>
			<input type="hidden" value="0" name="type_operation" id="type_operation" />
			<input type="hidden" value="0" name="id_member" id="id_member" />
			<input type="hidden" value="#" name="id_row" id="id_row" />
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= Yii::t('app', 'cancel') ?></button>
				<button type="button" class="btn btn-sm btn-primary antosubmit" onclick="do_operation()"><?= Yii::t('app', 'submit') ?></button>
			</div>

		</div>
	</div>
</div>