<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\users\models\SchoolCenter;
use backend\modules\users\models\AccessRight;
use backend\controllers\Utils;

$required_sign = Utils::required();


$access_right = AccessRight::find()->where(['etat_right' => 1])->all();

$urlcocher   = '<a href="#"   onclick="checkall_info(1);return false"  >' . Yii::t('app', 'check_all') . '</a>';
$urldecocher = '<a href="#" onclick="checkall_info(0);return false" >' . Yii::t('app', 'uncheck_all') . '</a>';



$id_center = str_replace('-', '', Yii::$app->user->identity->id_center);
$test_center = explode(",", $id_center);

if ($id_center == 1) {
	$allcenter = SchoolCenter::find()->where(['etat' => 1])->orderBy(['denomination_center' => SORT_ASC])->all();
} else {
	$allcenter = SchoolCenter::find()->where(['etat' => 1, 'id_center' => $test_center])->orderBy(['denomination_center' => SORT_ASC])->all();
}

?>



<script type="application/javascript">
	function change_right(id_center) {

		var all_element = document.getElementById('module_page').getElementsByTagName('input');
		if (id_center == "") {

			for (var i = 0; i < all_element.length; i++) {
				if (all_element[i].type == 'checkbox') {
					all_element[i].checked = false;
				}
			}

		} else {
			$("#content_loading").html("<center><img src='<?= Yii::$app->request->baseUrl ?>/theme/img/ajax-loader.gif' style='height:100px;width:100px' /></center>");
			$("#content_loading").show();
			$("#module_page").hide();

			var url_submit = "<?php echo Yii::$app->homeUrl ?>checking_center";

			$.ajax({
				url: url_submit,
				type: "POST",
				data: {
					id_center: encodeURIComponent(id_center),
				},
				success: function(data) {


					for (var i = 0; i < all_element.length; i++) {
						if (all_element[i].type == 'checkbox') {
							all_element[i].checked = true;
						}
					}
					if (data != "") {
						res = data.split(",");
						if (res.length > 0) {
							for (var i = 0; i < res.length; i++) {
								document.getElementById('right_' + res[i]).checked = false;
							}
						}
					}

					$("#content_loading").hide();
					$("#module_page").show();

				}
			});
		}

	}

	function checkall_info(status) {
		if (status == 1) var new_status = true;
		if (status == 0) var new_status = false;
		var all_element = document.getElementById('module_page').getElementsByTagName('input');

		for (var i = 0; i < all_element.length; i++) {
			if (all_element[i].type == 'checkbox') {
				all_element[i].checked = new_status;
			}
		}
	}
</script>

<div class="card">
	<?php $form = ActiveForm::begin([
		'id' => 'form-signup',
		'fieldConfig' => [
			'options' => [
				'tag' => false,
			]
		]
	]); ?>
	<div class="card-body card-block">


		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'denomination_center') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<?= $form->field($model, 'id_center')->dropdownList(
					ArrayHelper::map(
						$allcenter,
						'id_center',
						'denomination_center'
					),
					['prompt' => Yii::t('app', 'select_center'), 'onchange' => 'change_right(this.value)', 'required' => true],
					['class' => 'form-control']
				)->error(false)->label(false); ?>
			</div>
		</div>


		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'allcenter_module') ?> <?= $required_sign ?></label></div>

			<div class="input-group col-sm-5">
				<div class="col-sm-12" id="content_loading">
				</div>
				<div class="col-sm-12" id="module_page">
					<label><?php echo Yii::t('app', 'gbobal_privileg') . " ( " . $urlcocher . " / " . $urldecocher . " )"; ?></label>
					<table class="table table-bordered table-striped">
						<tr>
							<th style="width:80%"><?php echo Yii::t('app', 'm_module') ?></th>
							<th><?php echo Yii::t('app', 'activer') ?></th>
						</tr>


						<?php
						foreach ($access_right as $valus) {
						?>
							<tr>
								<td><?php echo Yii::t('app', $valus->lib_right); ?></td>
								<td><input type="checkbox" value="1" id="right_<?= $valus->id_right ?>" name="right_<?= $valus->id_right ?>"></td>
							</tr>
						<?php } ?>


					</table>
				</div>
			</div>
		</div>



	</div>
	<div class="card-footer">
		<?= Utils::resetbtn(); ?>
		<?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> ' . Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>