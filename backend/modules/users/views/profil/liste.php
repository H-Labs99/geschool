<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\controllers\Utils;
?>

<script type="text/javascript">
    function delete_member(id){
			$('#myLargeModalLabel').html('<?php echo Yii::t('app','confirm_delete') ?>'); 		   
		    $('#myLargeModalContent').html('<?php echo Yii::t('app','info_delete_profil') ?>'); 	
			document.getElementById('type_operation').value='3';
			document.getElementById('id_member').value=id;
			document.getElementById('submit_mod').style.display="block";
	}
	function active_member(id){
			$('#myLargeModalLabel').html('<?php echo Yii::t('app','confirm_active') ?>'); 		   
		    $('#myLargeModalContent').html('<?php echo Yii::t('app','info_active_profil') ?>'); 
			document.getElementById('type_operation').value='1';
			document.getElementById('id_member').value=id;
			document.getElementById('submit_mod').style.display="block";
	}
	function desactive_member(id){
			$('#myLargeModalLabel').html('<?php echo Yii::t('app','confirm_desactive') ?>'); 		   
		    $('#myLargeModalContent').html('<?php echo Yii::t('app','info_desactive_profil') ?>'); 
			document.getElementById('type_operation').value='2';
			document.getElementById('id_member').value=id;
			document.getElementById('submit_mod').style.display="block";
	}
	
	function do_operation(){
	
			var url = "<?php echo Yii::$app->request->baseUrl ?>/profil_operation";
									
			var mypostrequest = null;
			if(window.XMLHttpRequest){// Firefox et autres
				mypostrequest = new XMLHttpRequest();
			}else if(window.ActiveXObject){ // Internet Explorer
				try {  mypostrequest = new ActiveXObject("Msxml2.XMLHTTP");}
				catch (e) {mypostrequest = new ActiveXObject("Microsoft.XMLHTTP");}
			}

			mypostrequest.onreadystatechange=function(){
				if (mypostrequest.readyState==4){
					  if (mypostrequest.status==200){
					  
							document.location.href=location.href;
					  }													  
				}
			}
									
			var operation=encodeURIComponent(document.getElementById('type_operation').value);
			var id=encodeURIComponent(document.getElementById('id_member').value);
			
			var parameters="operation="+operation+"&id="+id ;
			
			mypostrequest.open("POST", url , true) ;
			mypostrequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded") ;
			mypostrequest.send(parameters) ;		
	}	
	
</script>

<div class="card card-body card-block">
 <?= $this->render("/_modal") ?>	
 	
						<?php
								$manage_profil=Utils::have_access("manage_profil");
								$test= explode('_',$manage_profil);
								
								echo GridView::widget([
									'dataProvider' => $donnee['all_profils'],
									'layout' => '{items}',
									'showOnEmpty' => false,
									'emptyText' => Utils::emptyContent(),
									'tableOptions' => [
										'class' => 'table table-striped table-bordered table-hover',
										'id' => 'example1',
									],
									'columns' => [																				
										
										[
											    'label' => Yii::t('app', 'nameProfil'),
												'value' => 'nameProfil',																
										],
										[
											    'label' => Yii::t('app', 'descriptionProfil'),
												'value' => 'descriptionProfil',																
										],
										
																				
										[
											   'class' => 'yii\grid\ActionColumn',
												'template' => '{update}',
												'headerOptions' => ['width' => '40'],
												'visible' => $test[2]==1 ? true : false ,
												'buttons' => [
													'update' => function ($url,$data) {
														$url=Yii::$app->homeUrl.'update_profil?key='.$data->key_profil;
														return Html::a(
															'<span title="'.Yii::t('app', 'updatebutton').'" class="fas fa-edit"></span>',
															$url);
													},
												],
																
										],
										[
											    'class' => 'yii\grid\ActionColumn',
												'template' => '{active}',
												'headerOptions' => ['width' => '40'],
												'visible' => $test[3]==1 ? true : false ,
												'buttons' => [
													'active' => function ($url,$data) {
														
														   if($data->etat_user_profil==1){
																return '<a title="'.Yii::t('app', 'desactive').'" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="desactive_member(\''.$data->key_profil.'\')"><i class="fa fa-check" style="color:red"></i></a>';
														   }else{
																return '<a title="'.Yii::t('app', 'active').'" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="active_member(\''.$data->key_profil.'\')"><i class="fa fa-check" style="color:blue"></i></a>';
														   }
													},
												],
																
										],
										[
											    'class' => 'yii\grid\ActionColumn',
												'template' => '{delete}',
												'headerOptions' => ['width' => '40'],
												'visible' => $test[3]==1 ? true : false ,
												'buttons' => [
													'delete' => function ($url,$data) {
														return '<a title="'.Yii::t('app', 'delete').'" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_member(\''.$data->key_profil.'\')"><i class="fa fa-window-close" style="color:red"></i></a>';
														
													},
												],
																
										]
									],
									 
								]);
							   ?>
							
</div>								
                
				
              