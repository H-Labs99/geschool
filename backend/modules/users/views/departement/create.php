<?php

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\controllers\Utils;

$required_sign = Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\Typepersonnel */


?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>
<div class="card-body card-block">

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'section') ?> <?= $required_sign ?> </label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'idsection')->dropdownList(
                ArrayHelper::map(
                    $all_section,
                    'id',
                    'designation'
                ),
                ['prompt' => Yii::t('app', 'select_section'), 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'libelle') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'libelledepartement')->textInput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>