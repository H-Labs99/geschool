<?php

namespace backend\modules\parametres\controllers;

use Yii;
use backend\modules\parametres\models\Specialite;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\models\ApiCurl;

/**
 * SpecialiteController implements the CRUD actions for Specialite model.
 */
class SpecialiteController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add_specialite', 'all_specialite', 'operation', 'create_specialite', 'center_module', 'checking_center', 'save_specialite'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister toutes les specialites d'un centre
    public function actionAll_specialite()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_specialite", "READ");
        $response_data = array();

        $apicurl = new ApiCurl();
        $response = $apicurl->ApiPost('specialites/specialites', []);
        $decode = json_decode($response);
        $status = $decode->status;

        if (isset($status) && ($status == '000')) {
            if (sizeof($decode->data) > 0) {
                $response_data = $decode->data;
            }
        }
        elseif (isset($status) && ($status == '002')) {
            //Liste vide
            $message = $decode->message; // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_specialite'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_specialite_menu');
        $board = array("add_specialite" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }


    //Cette fonction permet de charger le formulaire  de modification d'une specialite
    public function actionCreate_specialite()
    {
        $return_info = "";
        $data = array();
        $this->layout = false;
        $manage_specialite = Utils::have_access("manage_specialite");
        if ($manage_specialite == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_specialite);

        if ($test[2] == 0) {
            return  $return_info;
        }


        if (Yii::$app->request->isPost) {

            /* Début Liste des sections */
            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('sections/sections', $data);
            $decode = json_decode($response);
            $status = $decode->status;
            // $message = $decode->message;
            if ($status == '000') {
                //Filtrer les section actives
                $all_section = $decode->data;
                $i = 0;
                foreach ($all_section as $item){
                    if ($item->status == 2 or $item->status == 3){
                        unset($all_section[$i]);
                    }
                    $i++;
                }
            }
            /* Fin Liste des sections */

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Specialite;
            } else {

                /* Debut recherche de la specialite */
                $data['specialite_key'] = $key;

                $response = $apicurl->ApiPost('specialites/specialite', $data);
                $decode = json_decode($response);
                $status = $decode->status;
                /* Fin recherche de la specialite */

                if ($status == '000') {
                    //Si Specialte trouve, On convertie l'objet de type Api en model
                    $find_specialite = new Specialite();
                    $find_specialite->load((array)$decode->data, '');
                    $model = $find_specialite;
                } else {
                    $model = new Specialite;
                }
            }

            $this->layout = false;
            return $this->render('create', array('model' => $model, 'all_section' => $all_section));
        }

        return $return_info;
    }


    //Cette fonction permet de sauvegarder une nouvelle specialite
    public function actionSave_specialite()
    {
        $return_info = "";
        $this->layout = false;
        $manage_specialite = Utils::have_access("manage_specialite");
        if ($manage_specialite == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_specialite);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            // print_r($info['type_absence']);
            // die;


            $key = urldecode($info['key']);
            $libelle = urldecode($info['libelle']);
            $idsection = urldecode($info['idsection']);


            $fullInfo = array();
            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['libelle'] = $libelle;
            $fullInfo['idsection'] = $idsection;
            $fullInfo['operation'] = 1;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;

            if ($key != '0') {
                $fullInfo['specialite_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('specialites/addspecialite', $fullInfo);
            $decode = json_decode($response);
            $status = $decode->status;

            if (isset($status)) {
                if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                    $donnee['all_specialite'] = new ArrayDataProvider([
                        'allModels' => $decode->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } else if ($status == '001') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
                    $return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';
                    $return_info = '1###' . $return_alert;
                    // $return_info = '1###<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px"> <div class="alert-body">' . Yii::t('app', $decode->message) . ' </div></div>';
                } else if ($status == '002') { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
                    $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
                }
            } else {
                $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
            }
        }
        return $return_info;
    }


    //Cette fonction permet de  supprimer une specialite
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_specialite", "DELETE");
        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keyspecialite = $_POST['id'];

            $fullInfo = array(
                'specialite_key' => $keyspecialite,
            );

            //On recherche la ressource � modifier

            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('specialites/specialite', $fullInfo);
            $decode = json_decode($response);
            $status = $decode->status;
            //Fin recherche

            if ($status == '000') {
                //Execution de l'api pour la modification
                $fullInfo['operation'] = $etat;
                $response = $apicurl->ApiPost('specialites/deletespecialite', $fullInfo);
                $decode = json_decode($response);
                $status = $decode->status;
                //Fin execution

                if ($status == '000') {
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_specialite_success');
                    }
                    else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_specialite_success');
                    }
                    else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_specialite_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error3');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else if ($status == '001'){
                $message = $decode->message;
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            } else if ($status == '002') {
                $message = $decode->message;
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            } else {
                $message = Yii::t('app', 'update_error2');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error1');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }


}
