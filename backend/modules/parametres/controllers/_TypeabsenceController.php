<?php

namespace backend\modules\users\controllers;

use backend\controllers\DefaultController;
use backend\controllers\Utils;
use Yii;
use backend\modules\users\models\Typeabsence;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;


/**
 * TypeabsenceController implements the CRUD actions for Typeabsence model.
 */
class TypeabsenceController extends DefaultController
{
    public $page_title="";
    public $breadcrumb="";
    public $select_menu ="TYPEABSENCE";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add_typeabsence','all_typeabsence','update_typeabsence','operation'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les types d'absences

    public function actionAll_typeabsence()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_admin","READ");

        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl()."/liste_type_abscence";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => 'SzrQt6TazKv2ayDHqs754qvvAvD',
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        $donnee['all_typeabsence'] = new ArrayDataProvider([
            'allModels' => $response->data,
            'pagination' => [
                //'totalCount' => count($response->data),
                'pageSize' =>-1,
            ],
        ]);

        //Initialisation des titres de page
        $title=Yii::t('app', 'all_typeabsence_menu');
        $board=array("add_typeabsence"=>$title);
        $this->page_title=$title;
        $this->breadcrumb=Utils::retour($board);

        return $this->render('index',array('donnee'=>$donnee));

    }

    //Cette fonction permet d'ajouter un type d'absence
    public function actionAdd_typeabsence()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_admin","CREATE");
        $model = new Typeabsence();

        //Initialisation des titres de page
        $title=Yii::t('app', 'add_typeabsence_menu');
        $board=array("add_typeabsence"=>$title);
        $this->page_title=$title;
        $this->breadcrumb=Utils::retour($board);

        if (Yii::$app->request->post()){
            if ($model->load(Yii::$app->request->post()) ) {

                //Execution de l'api pour l'ajout
                $url_api = Utils::getApiUrl()."/ajouter_modifier_type_abscence";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => 'SzrQt6TazKv2ayDHqs754qvvAvD',
                        'designation' => $model->designationtypeabsence,
                        'operation' => 1,
                        //'user' => Yii::$app->user->identity->id,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                if (isset($response->status) && ($response->status =='000')) {
                    //if($model->save()){
                    $message=Yii::t('app', 'succes_creation_typeabsence');
                    Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
                    $model=new Typeabsence();
                }else{
                    $message=Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                }

            } else {
                $message=Yii::t('app', 'input_empty');
                Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
            }
        }

        return $this->render('create',array('model'=>$model));
    }

    //Cette fonction permet de modifer un type d'absence
    public function actionUpdate_typeabsence()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_admin","UPDATE");

        if(isset($_GET["key"])){
            $keytypeabsence=$_GET["key"];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl()."/verifier_type_abs_modification";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' => 'SzrQt6TazKv2ayDHqs754qvvAvD',
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'type_abscence_key' =>$keytypeabsence,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if( $response->status =='000'){
                //On convertie l'objet en model
                $find_typeabsence = new Typeabsence();
                $find_typeabsence->load((array)$response->data,'');
                $model= $find_typeabsence;

                if (Yii::$app->request->post()){
                    if ($model->load(Yii::$app->request->post())){
                        if(trim($model->designationtypeabsence)!=""){
                            $model->updated_by=Yii::$app->user->identity->id;
                            $model->updated_at=time();

                            //Execution de l'api pour la modification
                            $url_api = Utils::getApiUrl()."/ajouter_modifier_type_abscence";
                            $ch = curl_init();
                            $curlConfig = array(
                                CURLOPT_URL => $url_api,
                                CURLOPT_POST => true,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_CONNECTTIMEOUT => 5,
                                CURLOPT_POSTFIELDS => array(
                                    'token' => 'SzrQt6TazKv2ayDHqs754qvvAvD',
                                    'designation' => $model->designationtypeabsence,
                                    'type_abscence_key' => $keytypeabsence,
                                    'operation' => 2,
                                    //'user' => Yii::$app->user->identity->id,
                                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                                ),
                            );
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                            curl_setopt_array($ch, $curlConfig);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            //Fin execution
                            $response = json_decode($result);
                            if (isset($response->status)){
                                if($response->status =='000'){
                                    $message=Yii::t('app', 'update_success');
                                    Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
                                    return $this->redirect(array('/all_typeabsence'));
                                }elseif(isset($response->status) && ($response->status =='001')){
                                    $message=Yii::t('app', 'typeabsence_used');
                                    Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                                }else{
                                    $message=Yii::t('app', 'update_error');
                                    Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                                }
                            }else{
                                $message=Yii::t('app', 'update_error');
                                Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                            }

                        }else {
                            $message=Yii::t('app', 'input_empty');
                            Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                        }
                    }else{
                        $message=Yii::t('app', 'input_empty');
                        Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                    }

                }

                //Initialisation des titres de page
                $title=Yii::t('app','update_information').": ".$find_typeabsence->designationtypeabsence;
                $board=array("all_typeabsence"=>Yii::t('app', 'all_typeabsence_menu'),
                    "update_typeabsence"=>$title);
                $this->page_title=$title;
                $this->breadcrumb=Utils::retour($board);

                return $this->render('create',array('model'=>$model));
            }else{
                return $this->redirect(array('/all_typeabsence'));
            }

        }else{
            return $this->redirect(array('/all_typeabsence'));
        }
    }

    //Cette fonction permet de desactiver/activer & supprimer un type d'absence
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_admin","DELETE");
        if(isset($_POST['operation'])!="" and isset($_POST['id'])!=""){

            $etat=$_POST['operation'];
            $keytypeabsence=$_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl()."/verifier_type_abs_modification";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' => 'SzrQt6TazKv2ayDHqs754qvvAvD',
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'type_abscence_key' =>$keytypeabsence,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if( $response->status =='000'){
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl()."/switch_status_type_abscence";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => 'SzrQt6TazKv2ayDHqs754qvvAvD',
                        'type_abscence_key' => $keytypeabsence,
                        'operation' => $etat,
                        //'user' => Yii::$app->user->identity->id,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if($response->status =='000'){

                    //$message=$response->message;
                    if($etat==1){
                        $message=Yii::t('app','active_typeabsence_success');
                    }else if($etat==2){
                        $message=Yii::t('app','desactive_typeabsence_success');
                    }else if($etat==3){
                        $message=Yii::t('app','delete_typeabsence_success');
                    }
                    Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
                }else{
                    $message=Yii::t('app','update_error')  ;
                    Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                }
            }else{
                $message=Yii::t('app','update_error')  ;
                Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
            }
        }else{
            $message=Yii::t('app','update_error')  ;
            Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
        }

    }
}
