<?php

namespace backend\modules\parametres\controllers;

use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\models\ApiCurl;
use Yii;
use backend\modules\parametres\models\Salle;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;

/**
 * SalleController implements the CRUD actions for Salle model.
 */
class SalleController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add_salle', 'all_salle', 'operation', 'create_salle', 'save_salle'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    //Cette fonction permet de lister toutes les salles d'un centre
    public function actionAll_salle()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_salle", "READ");
        $response_data = array();

        $apicurl = new ApiCurl();
        $response = $apicurl->ApiPost('salles/salles', []);
        $decode = json_decode($response);
        $status = '';

        if (isset($decode->status)) $status = $decode->status;

        if ($status == '000') {
            $response_data = $decode->data;
        } elseif ($status == '001') {
            $response_data;
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
        $donnee['all_salle'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_salle_menu');
        $board = array("add_salle" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }


    //Cette fonction permet de charger le formulaire  de modification d'une salle
    public function actionCreate_salle()
    {
        $return_info = "";
        $this->layout = false;
        $manage_salle = Utils::have_access("manage_salle");
        if ($manage_salle == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_salle);

        if ($test[2] == 0) {
            return  $return_info;
        }

        if (Yii::$app->request->isPost) {

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Salle();
            } else {

                /* Debut recherche de la salle */
                $data = array();
                $data['salle_key'] = $key;

                $apicurl = new ApiCurl();
                $response = $apicurl->ApiPost('salles/salle', $data);
                $decode = json_decode($response);
                $status = $decode->status;
                /* Fin recherche de la salle */

                if ($status == '000') {
                    //Si Specialte trouve, On convertie l'objet de type Api en model
                    $find_salle = new Salle();
                    $find_salle->load((array)$decode->data, '');
                    $model = $find_salle;
                } else {
                    $model = new Salle();
                }
            }

            $this->layout = false;
            //            return $this->render('create', array('model' => $model, 'all_centre' => $all_centre));
            return $this->render('create', array('model' => $model));
        }

        return $return_info;
    }


    //Cette fonction permet de sauvegarder une nouvelle salle
    public function actionSave_salle()
    {
        $return_info = "";
        $this->layout = false;
        $manage_salle = Utils::have_access("manage_salle");
        if ($manage_salle == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_salle);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $libelle = urldecode($info['libelle']);
            $capacite = urldecode($info['capacite']);

            $fullInfo = array();
            $fullInfo['libelle'] = $libelle;
            $fullInfo['capacite'] = $capacite;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['salle_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('salles/addsalle', $fullInfo);
            $decode = json_decode($response);

            $status = '';
            if (isset($decode->status)) $status = $decode->status;


            if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                $donnee['all_salle'] = new ArrayDataProvider([
                    'allModels' => $decode->data,
                    'pagination' => [
                        'pageSize' => -1,
                    ],
                ]);

                $return_info = $this->render('liste', array('donnee' => $donnee));
            } else if ($status == '001' || $status == '003') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
                $return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';
                $return_info = '1###' . $return_alert;
            } else { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
                $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
            }
        }
        return $return_info;
    }


    //Cette fonction permet de  supprimer une salle
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_salle", "DELETE");
        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $operation = $_POST['operation'];
            $keysalle = $_POST['id'];

            $fullInfo = array(
                'salle_key' => $keysalle,
                'operation' => $operation,
            );

            $apicurl = new ApiCurl();
            //Execution de l'api pour la modification
            $response = $apicurl->ApiPost("salles/deletesalle", $fullInfo);
            $decode = json_decode($response);
            //Fin execution
            $status = '';

            if (isset($decode->status)) {
                $status = $decode->status;
                $message = $decode->message;
            }

            if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
            } else if ($status == '001') { // UNE ERREUR UTILISATEUR EST SURVENUE
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            } else { // UNE ERREUR NE DEPENDANT PAS DE L'UTILISATEUR EST SURVENUE
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
