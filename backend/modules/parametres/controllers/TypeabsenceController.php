<?php

namespace backend\modules\parametres\controllers;

use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\modules\parametres\models\Typepersonnel;
use Yii;
use backend\modules\parametres\models\Typeabsence;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;


/**
 * TypeabsenceController implements the CRUD actions for Typeabsence model.
 */
class TypeabsenceController extends DefaultController
{
    public $page_title="";
    public $breadcrumb="";
    public $select_menu ="PARAMSYSTEME";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_typeabsence','operation','create_typeabsence','save_typeabsence'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les types d'absences
    public function actionAll_typeabsence()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_typeabsence","READ");

        $id_center=Yii::$app->session->get('default_center');

        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl()."typeabsences/typeabsences";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        $donnee['all_typeabsence'] = new ArrayDataProvider([
            'allModels' => $response->data,
            'pagination' => [
                'pageSize' =>-1,
            ],
        ]);

        //Initialisation des titres de page
        $title=Yii::t('app', 'all_typeabsence_menu');
        $board=array("add_typeabsence"=>$title);
        $this->page_title=$title;
        $this->breadcrumb=Utils::retour($board);

        return $this->render('index',array('donnee'=>$donnee));

    }

    //Cette fonction permet d'ajouter un type d'absence
    public function actionAdd_typeabsence()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_typeabsence","CREATE");
        $id_center=Yii::$app->session->get('default_center');
        $model = new Typeabsence();

        //Initialisation des titres de page
        $title=Yii::t('app', 'add_typeabsence_menu');
        $board=array("add_typeabsence"=>$title);
        $this->page_title=$title;
        $this->breadcrumb=Utils::retour($board);

        if (Yii::$app->request->post()){
            if ($model->load(Yii::$app->request->post()) ) {

                //Execution de l'api pour l'ajout
                $url_api = Utils::getApiUrl()."typeabsences/typeabsence";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'designation' => $model->designationtypeabsence,
                        'operation' => 1,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                if (isset($response->status) && ($response->status =='000')) {
                    //if($model->save()){
                    $message=Yii::t('app', 'succes_creation_typeabsence');
                    Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
                    $model=new Typeabsence();
                }else{
                    $message=Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                }

            } else {
                $message=Yii::t('app', 'input_empty');
                Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
            }
        }

        return $this->render('create',array('model'=>$model));
    }

    //Cette fonction permet de modifer un type d'absence
    /*
    public function actionUpdate_typeabsence()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_typeabsence","UPDATE");
        $id_center=Yii::$app->session->get('default_center');

        if(isset($_GET["key"])){
            $keytypeabsence=$_GET["key"];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl()."/verifier_type_abs_modification";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' => Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'type_abscence_key' =>$keytypeabsence,
                    'id_center' =>$keytypeabsence,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if( $response->status =='000'){
                //On convertie l'objet en model
                $find_typeabsence = new Typeabsence();
                $find_typeabsence->load((array)$response->data,'');
                $model= $find_typeabsence;

                if (Yii::$app->request->post()){
                    if ($model->load(Yii::$app->request->post())){
                        if(trim($model->designationtypeabsence)!=""){
                            $model->updated_by=Yii::$app->user->identity->id;
                            $model->updated_at=time();

                            //Execution de l'api pour la modification
                            $url_api = Utils::getApiUrl()."/ajouter_modifier_type_abscence";
                            $ch = curl_init();
                            $curlConfig = array(
                                CURLOPT_URL => $url_api,
                                CURLOPT_POST => true,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_CONNECTTIMEOUT => 5,
                                CURLOPT_POSTFIELDS => array(
                                    'token' => Utils::getApiKey(),
                                    'designation' => $model->designationtypeabsence,
                                    'type_abscence_key' => $keytypeabsence,
                                    'operation' => 2,
                                    //'user' => Yii::$app->user->identity->id,
                                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                                ),
                            );
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                            curl_setopt_array($ch, $curlConfig);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            //Fin execution
                            $response = json_decode($result);
                            if (isset($response->status)){
                                if($response->status =='000'){
                                    $message=Yii::t('app', 'update_success');
                                    Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
                                    return $this->redirect(array('/all_typeabsence'));
                                }elseif(isset($response->status) && ($response->status =='001')){
                                    $message=Yii::t('app', 'typeabsence_used');
                                    Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                                }else{
                                    $message=Yii::t('app', 'update_error');
                                    Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                                }
                            }else{
                                $message=Yii::t('app', 'update_error');
                                Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                            }

                        }else {
                            $message=Yii::t('app', 'input_empty');
                            Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                        }
                    }else{
                        $message=Yii::t('app', 'input_empty');
                        Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                    }

                }

                //Initialisation des titres de page
                $title=Yii::t('app','update_information').": ".$find_typeabsence->designationtypeabsence;
                $board=array("all_typeabsence"=>Yii::t('app', 'all_typeabsence_menu'),
                    "update_typeabsence"=>$title);
                $this->page_title=$title;
                $this->breadcrumb=Utils::retour($board);

                return $this->render('create',array('model'=>$model));
            }else{
                return $this->redirect(array('/all_typeabsence'));
            }

        }else{
            return $this->redirect(array('/all_typeabsence'));
        }
    }
*/
    //Cette fonction permet de desactiver/activer & supprimer un type d'absence
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_typeabsence","DELETE");
        $id_center=Yii::$app->session->get('default_center');
        if(isset($_POST['operation'])!="" and isset($_POST['id'])!=""){

            $etat=$_POST['operation'];
            $keytypeabsence=$_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl()."typeabsences/typeabsence";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'type_abscence_key' =>$keytypeabsence,
                    'type_abscence_key' =>$keytypeabsence,
                    'id_center' =>$id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if( $response->status =='000'){
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl()."typeabsences/deletetypeabsence";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'type_abscence_key' => $keytypeabsence,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if($response->status =='000'){

                    //$message=$response->message;
                    if($etat==1){
                        $message=Yii::t('app','active_typeabsence_success');
                    }else if($etat==2){
                        $message=Yii::t('app','desactive_typeabsence_success');
                    }else if($etat==3){
                        $message=Yii::t('app','delete_typeabsence_success');
                    }
                    Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
                }else{
                    $message=Yii::t('app','update_error')  ;
                    Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
                }
            }else{
                $message=Yii::t('app','update_error')  ;
                Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
            }
        }else{
            $message=Yii::t('app','update_error')  ;
            Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
        }

    }

    //Cette fonction permet de sauvegarder un type d'absence
    public function actionSave_typeabsence()
    {
        $return_info="";
        $this->layout=false;
        $manage_typeabsence=Utils::have_access("manage_typeabsence");
        if($manage_typeabsence=="0_0_0_0"){
            return  $return_info;
        }
        $test= explode('_',$manage_typeabsence);

        if($test[0]==0 && $test[2]==0){
            return  $return_info;
        }

        if(Yii::$app->request->isPost){
            $id_center=Yii::$app->session->get('default_center');


            $info=Yii::$app->request->post();

            $key=urldecode($info['key']);
            $designation=urldecode($info['designation']);


            $fullInfo=array();
            if($key == '0'){
                $fullInfo['token']=Utils::getApiKey();
                $fullInfo['designation']=$designation;
                $fullInfo['operation']=1;
                $fullInfo['user_auth_key']= Yii::$app->user->identity->getAuthKey();
                $fullInfo['id_center']= $id_center;
            }else{
                $fullInfo['token']=Utils::getApiKey();
                $fullInfo['designation']=$designation;
                $fullInfo['type_abscence_key']=$key;
                $fullInfo['operation']=2;
                $fullInfo['user_auth_key']= Yii::$app->user->identity->getAuthKey();
                $fullInfo['id_center']= $id_center;
            }


            $url_api = Utils::getApiUrl()."typeabsences/addtypeabsence";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);


            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_typeabsence'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } elseif ($response->status == '001') {
                    // Un element manque sur le formulaire soumis
                    $return_info = "1";
                } elseif ($response->status == '003') {
                    // L'element existe déjà
                    $return_info = "3";
                } else {
                    $return_info = "0";
                }
            } else {
                $return_info = "0";
            }

        }
        return $return_info ;

    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification d' un type d'absence
    public function actionCreate_typeabsence()
    {
        $return_info="";
        $this->layout=false;
        $manage_typeabsence=Utils::have_access("manage_typeabsence");
        if($manage_typeabsence=="0_0_0_0"){
            return  $return_info;
        }
        $test= explode('_',$manage_typeabsence);

        if($test[0]==0 && $test[2]==0){
            return  $return_info;
        }


        if(Yii::$app->request->isPost){
            $id_center=Yii::$app->session->get('default_center');

            $info=Yii::$app->request->post();

            $key=$info['key'];


            if($key == '0'){

                $model = new Typeabsence();
            }
            else{

                 $url_api = Utils::getApiUrl()."typeabsences/typeabsence";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'type_abscence_key' =>$key,
                        'id_center' =>$id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche


                if( $response->status =='000') {
                    //On convertie l'objet en model
                    $find_typeabsence = new Typeabsence();
                    $find_typeabsence->load((array)$response->data, '');
                    $model = $find_typeabsence;
                }else{
                    $model = new Typeabsence();
                }
            }

            $this->layout=false;
            $return_info=$this->render('create',array("model"=>$model));


        }
        return $return_info ;

    }


}
