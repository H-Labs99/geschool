<?php

namespace backend\modules\parametres\controllers;

use Yii;
use backend\modules\parametres\models\Typepersonnel;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use yii\filters\AccessControl;

/**
 * TypepersonnelController implements the CRUD actions for Typepersonnel model.
 */
class TypepersonnelController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_typepersonnel', 'operation', 'create_typepersonnel', 'save_typepersonnel'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les types de personnel
    public function actionAll_typepersonnel()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_typepersonnel", "READ");

        $id_center = Yii::$app->session->get('default_center');


        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "typepersonnels/typepersonnels";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);

        $response_data = array();

        // print_r($response);die;

        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_typepersonnel'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);



        //Initialisation des titres de page
        $title = Yii::t('app', 'all_typepersonnel_menu');
        $board = array("add_typepersonnel" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification type de personnel
    public function actionCreate_typepersonnel()
    {
        $return_info = "";
        $this->layout = false;
        $manage_typepersonnel = Utils::have_access("manage_typepersonnel");
        if ($manage_typepersonnel == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_typepersonnel);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }

        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');


            $info = Yii::$app->request->post();

            $key = $info['key'];


            if ($key == '0') {

                $model = new Typepersonnel();
            } else {

                $url_api = Utils::getApiUrl() . "typepersonnels/typepersonnel";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'type_personnel_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche


                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_typepersonnel = new Typepersonnel();
                    $find_typepersonnel->load((array)$response->data, '');
                    $model = $find_typepersonnel;
                } else {
                    $model = new Typepersonnel();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model));
        }
        return $return_info;
    }


    //Cette fonction permet de sauvegarder un type de personnel
    public function actionSave_typepersonnel()
    {
        $return_info = "";
        $this->layout = false;
        $manage_typepersonnel = Utils::have_access("manage_typepersonnel");
        if ($manage_typepersonnel == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_typepersonnel);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }

        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');


            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $libelle = urldecode($info['libelle']);


            $fullInfo = array();
            if ($key == '0') {
                $fullInfo['token'] = Utils::getApiKey();
                $fullInfo['libelle'] = $libelle;
                $fullInfo['operation'] = 1;
                $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
                $fullInfo['id_center'] = $id_center;
            } else {
                $fullInfo['token'] = Utils::getApiKey();
                $fullInfo['libelle'] = $libelle;
                $fullInfo['type_personnel_key'] = $key;
                $fullInfo['operation'] = 2;
                $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
                $fullInfo['id_center'] = $id_center;
            }


            $url_api = Utils::getApiUrl() . "typepersonnels/addtypepersonnel";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);

            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_typepersonnel'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } elseif ($response->status == '001') {
                    // Un element manque sur le formulaire soumis
                    $return_info = "1";
                } elseif ($response->status == '003') {
                    // L'element existe déjà
                    $return_info = "3";
                } else {
                    $return_info = "0";
                }
            } else {
                $return_info = "0";
            }
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un type de personnel
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_typepersonnel", "DELETE");
        $id_center = Yii::$app->session->get('default_center');
        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keytypepersonnel = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "typepersonnels/typepersonnel";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'type_personnel_key' => $keytypepersonnel,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "typepersonnels/deletetypepersonnel";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'type_personnel_key' => $keytypepersonnel,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_typepersonnel_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_typepersonnel_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_typepersonnel_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
