<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "school_module".
 *
 * @property integer $id_school_module
 * @property string $key_school_module
 * @property integer $id_center
 * @property integer $id_right
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 */
class SchoolModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_school_module', 'id_center', 'id_right', 'etat', 'created_by'], 'required'],
            [['id_center', 'id_right', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['key_school_module'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_school_module' => 'Id School Module',
            'key_school_module' => 'Key School Module',
            'id_center' => 'Id Center',
            'id_right' => 'Id Right',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }
}
