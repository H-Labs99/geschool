<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "departement".
 *
 * @property integer $id
 * @property string $keydepartement
 * @property string $libelledepartement
 * @property integer $idsection
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Departement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'departement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keydepartement', 'idsection', 'created_at', 'create_by'], 'required'],
            [['libelledepartement'], 'string'],
            [['idsection', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['keydepartement'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keydepartement' => 'Keydepartement',
            'libelledepartement' => 'Libelledepartement',
            'idsection' => 'Idsection',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }
}
