<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "profil_access".
 *
 * @property integer $id_profil_access
 * @property integer $id_profil
 * @property integer $id_right
 * @property integer $pcreate
 * @property integer $pread
 * @property integer $pupdate
 * @property integer $pdelete
 * @property integer $etat_profil_access
 * @property integer $id_userCreate
 * @property string $date_create_aprofil
 */
class ProfilAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profil_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_profil', 'id_right', 'pcreate', 'pread', 'pupdate', 'pdelete', 'etat_profil_access', 'id_userCreate', 'date_create_aprofil'], 'required'],
            [['id_profil', 'id_right', 'pcreate', 'pread', 'pupdate', 'pdelete', 'etat_profil_access', 'id_userCreate'], 'integer'],
            [['date_create_aprofil'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_profil_access' => 'Id Profil Access',
            'id_profil' => 'Id Profil',
            'id_right' => 'Id Right',
            'pcreate' => 'Pcreate',
            'pread' => 'Pread',
            'pupdate' => 'Pupdate',
            'pdelete' => 'Pdelete',
            'etat_profil_access' => 'Etat Profil Access',
            'id_userCreate' => 'Id User Create',
            'date_create_aprofil' => 'Date Create Aprofil',
        ];
    }
}
