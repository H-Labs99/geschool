<?php

use yii\grid\GridView;
use backend\controllers\Utils;

$manage_departement = Utils::have_access("manage_departement");
$droits = explode('_', $manage_departement);


echo GridView::widget([
    'dataProvider' => $donnee['all_departement'],
    'layout' => '{items}',
    'showOnEmpty' => false,
    'emptyText' => Utils::emptyContent(),
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover',
        'id' => 'example1',
    ],
    'columns' => [

        [
            'label' => Yii::t('app', 'libelle'),
            'value' =>  function ($data) {
                return $data->libelledepartement;
            },
        ],



        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[2] == 1 ? true : false,
            'buttons' => [
                'update' => function ($url, $data) {

                    return '<a title="' . Yii::t('app', 'updatebutton') . '" href="#" data-toggle="modal" data-target=".Form-modal-lg"  onclick="add_member(\'' . $data->keydepartement . '\')"><i class="fa fa-edit" style="color:red"></i></a>';
                },
            ],

        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{active}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3] == 1 ? true : false,
            'buttons' => [
                'active' => function ($url, $data) {

                    if ($data->status == 1) {
                        return '<a title="' . Yii::t('app', 'desactive') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="desactive_member(\'' . $data->keydepartement . '\')"><i class="fa fa-check" style="color:red"></i></a>';
                    } else {
                        return '<a title="' . Yii::t('app', 'active') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="active_member(\'' . $data->keydepartement . '\')"><i class="fa fa-check" style="color:blue"></i></a>';
                    }
                },
            ],

        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3] == 1 ? true : false,
            'buttons' => [
                'delete' => function ($url, $data) {
                    return '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_member(\'' . $data->keydepartement . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
                },
            ],

        ]
    ],

]);
