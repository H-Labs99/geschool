<?php

use yii\grid\GridView;
use backend\controllers\Utils;

$manage_absence = Utils::have_access("manage_absence");
$droits = explode('_', $manage_absence);


echo GridView::widget([
    'dataProvider' => $donnee['all_absence'],
    'layout' => '{items}',
    'showOnEmpty' => false,
    'emptyText' => Utils::emptyContent(),
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover',
        'id' => 'example1',
    ],
    'columns' => [

        [
            'label' => Yii::t('app', 'user'),
            'value' =>  function ($data) {
                // print_r($data);
                // die;
                return $data->nom . ' ' . $data->prenoms;
            },
        ],

        [
            'label' => Yii::t('app', 'typeabsence'),
            'value' =>  function ($data) {
                return $data->designationtypeabsence;
            },
        ],

        [
            'label' => Yii::t('app', 'motif'),
            'value' =>  function ($data) {
                return $data->motifabsence;
            },
        ],

        [
            'label' => Yii::t('app', 'datedebutabsence'),
            'value' =>  function ($data) {
                return Utils::reconversion_date($data->datedebutabsence);
            },
        ],

        [
            'label' => Yii::t('app', 'datefinabsence'),
            'value' =>  function ($data) {
                return Utils::reconversion_date($data->datefinabsence);
            },
        ],

        [
            'label' => Yii::t('app', 'lieudestinantionabsence'),
            'value' =>  function ($data) {
                return $data->lieudestinantionabsence;
            },
        ],

        [
            'label' => Yii::t('app', 'coordonneesabsence'),
            'value' =>  function ($data) {
                return $data->coordonneesabsence;
            },
        ],


        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[2] == 1 ? true : false,
            'buttons' => [
                'update' => function ($url, $data) {
                    return '<a title="' . Yii::t('app', 'updatebutton') . '" href="#" data-toggle="modal" data-target=".Form-modal-lg"  onclick="add_member(\'' . $data->keyabsence . '\')"><i class="fa fa-edit" style="color:red"></i></a>';
                },
            ],

        ],

        /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{active}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3]==1 ? true : false ,
            'buttons' => [
                'active' => function ($url,$data) {

                    if($data->status==1){
                        return '<a title="'.Yii::t('app', 'desactive').'" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="desactive_member(\''.$data->keydiplome.'\')"><i class="fa fa-check" style="color:red"></i></a>';
                    }else{
                        return '<a title="'.Yii::t('app', 'active').'" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="active_member(\''.$data->keydiplome.'\')"><i class="fa fa-check" style="color:blue"></i></a>';
                    }
                },
            ],

        ],*/

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3] == 1 ? true : false,
            'buttons' => [
                'delete' => function ($url, $data) {
                    return '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_member(\'' . $data->keyabsence . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
                },
            ],
        ]
    ],

]);
