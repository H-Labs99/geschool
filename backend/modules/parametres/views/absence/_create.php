<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\parametres\models\UserProfil;
use backend\controllers\Utils;

$required_sign = Utils::required();
?>
<div class="card">
    <?php $form = ActiveForm::begin([
        'id' => 'form-signup',
        'fieldConfig' => [
            'options' => [
                'tag' => false,
            ]
        ]
    ]); ?>
    <div class="card-body card-block">

        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'typeabsence') ?> <?= $required_sign ?></label></div>
            <div class="input-group col-sm-5">
                <?= $form->field($model, 'idtypeabsence')->dropdownList(
                    ArrayHelper::map(
                        $all_typeabsence,
                        'id',
                        'designationtypeabsence'
                    ),
                    ['prompt' => Yii::t('app', 'select_typeabsence'), 'onchange' => '', 'required' => true],
                    ['class' => 'form-control']
                )->error(false)->label(false); ?>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'user') ?> <?= $required_sign ?></label></div>
            <div class="input-group col-sm-5">
                <?= $form->field($model, 'iduser')->dropdownList(
                    ArrayHelper::map(
                        $all_user,
                        'id',
                        'nom'
                    ),
                    ['prompt' => Yii::t('app', 'select_personnel'), 'onchange' => '', 'required' => true],
                    ['class' => 'form-control']
                )->error(false)->label(false); ?>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'motifabsence') ?> <?= $required_sign ?></label></div>
            <div class="input-group col-sm-5">
                <?= $form->field($model, 'motifabsence')->textarea(['required' => 'required', 'placeHolder' => ""])->error(false)->label(false); ?>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datedemandeabsence') ?> <?= $required_sign ?></label></div>
            <div class="input-group col-sm-9">
                <?= $form->field($model, 'datedemandeabsence', [
                    'options' => [
                        // 'tag' => 'div',
                        'class' => 'form-control',
                    ],
                    // 'template' => '<span class="col-md-2 col-lg-2"><label class="control-label">Final item price</label>{input}{error}</span>'
                ])->textInput(['type' => 'date'])->label(false) ?>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datefinabsence') ?> <?= $required_sign ?></label></div>
            <div class="input-group col-sm-3">
                <?= $form->field($model, 'datedebutabsence', ['options' => ['class' => 'form-control']])->textInput(['type' => 'date', 'required' => true])->label(false) ?>
            </div>
            <div class="input-group col-sm-2">
                <?= $form->field($model, 'heuredebutabsence', ['options' => ['class' => 'form-control']])->textInput(['type' => 'time'])->label(false) ?>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datefinabsence') ?> <?= $required_sign ?></label></div>
            <div class="input-group col-sm-3">
                <?= $form->field($model, 'datefinabsence', ['options' => ['class' => 'form-control']])->textInput(['type' => 'date', 'required' => true])->label(false) ?>
            </div>
            <div class="input-group col-sm-2">
                <?= $form->field($model, 'heurefinabsence', ['options' => ['class' => 'form-control']])->textInput(['type' => 'time'])->label(false) ?>
            </div>
        </div>


        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'lieudestinantionabsence') ?> <?= $required_sign ?></label></div>
            <div class="input-group col-sm-5">
                <?= $form->field($model, 'lieudestinantionabsence')->textinput(['required' => 'required'])->error(false)->label(false); ?>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'coordonneesabsence') ?> </label></div>
            <div class="input-group col-sm-5">
                <?= $form->field($model, 'coordonneesabsence')->textarea()->error(false)->label(false); ?>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'heurearattraper') ?> </label></div>
            <div class="input-group col-sm-5">
                <?= $form->field($model, 'heurearattraper')->textinput(['type' => 'number'])->error(false)->label(false); ?>
            </div>
        </div>

    </div>
    <?php ActiveForm::end(); ?>
</div>