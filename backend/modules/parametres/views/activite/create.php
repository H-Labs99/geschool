<?php

use api\modules\geschool\v1\models\SchoolCenter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\controllers\Utils;

$required_sign = Utils::required();

foreach ($all_personnel as $personnel) {
    $personnel->nom = $personnel->nom . ' ' . $personnel->prenoms;
}

$id_center = str_replace('-', '', Yii::$app->user->identity->id_center);
$test_center = explode(",", $id_center);

$allcenter = array();

if ($id_center == 1) {
    $allcenter = SchoolCenter::find()->where(['etat' => [1]])->orderBy(['denomination_center' => SORT_ASC])->all();
} else if (sizeof($test_center) > 1) {
    $allcenter = SchoolCenter::find()->where(['etat' => 1, 'id_center' => $test_center])->orderBy(['denomination_center' => SORT_ASC])->all();
}

?>
<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>

<div class="card-body card-block">

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'categorieactivite') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'idcategorieactivite')->dropdownList(
                ArrayHelper::map(
                    $all_categorie_activite,
                    'id',
                    'libelle'
                ),
                ['prompt' => Yii::t('app', 'select_categorieactivite'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'libelle') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'designationactivite')->textarea(['required' => 'required', 'placeHolder' => ""])->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datedebutactivite') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'datedebutactivite', [
                'options' => [
                    'class' => 'form-control',
                ],
            ])->textInput(['type' => 'date'])->label(false) ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datefinactivite') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'datefinactivite', [
                'options' => [
                    'class' => 'form-control',
                ],
            ])->textInput(['type' => 'date'])->label(false) ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'iduserexec') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'iduserexec')->dropdownList(
                ArrayHelper::map(
                    $all_personnel,
                    'id',
                    'nom'
                ),
                ['prompt' => Yii::t('app', 'select_iduserexec'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>
    </div>


    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'idusersuivi') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'idusersuivi')->dropdownList(
                ArrayHelper::map(
                    $all_personnel,
                    'id',
                    'nom',
                ),
                ['prompt' => Yii::t('app', 'select_idusersuivi'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>
    </div>


</div>

<?php ActiveForm::end(); ?>