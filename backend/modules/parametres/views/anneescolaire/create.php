<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\controllers\Utils;

$required_sign = Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\parametres\models\Typepersonnel */


?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>

<script src="<?= Yii::$app->homeUrl ?>theme/js/form-inputmask-personnalize.js"></script>

<div class="card-body card-block">

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'libelle') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-sm-9">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
            </div>
            <?= $form->field($model, 'libelleanneescolaire')->textInput(['required' => 'required', 'class' => 'form-control annee_scolaire-inputmask'])->error(false)->label(false); ?>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>