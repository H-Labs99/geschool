<?php


use yii\helpers\Html;
use backend\controllers\Utils;

$manage_anneescolaire = Utils::have_access("manage_anneescolaire");
$droits = explode('_', $manage_anneescolaire);
?>


<script type="text/javascript">
    function add_member(key_information) {

        var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/create_anneescolaire";
        var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px",]) ?></center>';

        $('#successInfo').hide();
        $('#FormLargeModalContent').hide();
        $('#Formcontent_footer').hide();
        $('#Formcontent_image').show();
        $('#Formcontent_image').html(url_image);
        $('#key_form').val(key_information);
        if (key_information == "0") {
            $('#FormLargeModalLabel').html('<?php echo Yii::t('app', 'add_anneescolaire_menu') ?>');
        } else {
            $('#FormLargeModalLabel').html('<?php echo Yii::t('app', 'update_anneescolaire_menu') ?>');
        }

        $.ajax({
            url: url_submit,
            type: "POST",
            data: {
                key: encodeURIComponent(key_information),
            },
            success: function(data) {
                if (data == '') {
                    var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px"> <div class="alert-body"><?php echo Yii::t('app', 'noaccess') ?></div></div>';
                    $('#Formcontent_image').html(error);
                } else {
                    $('#Formcontent_image').hide();
                    $('#Formcontent_footer').show();
                    $('#FormLargeModalContent').show();
                    $('#FormLargeModalContent').html(data);
                }
            }
        });
    }

    function save_member() {

        var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/save_anneescolaire";
        var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px",]) ?></center>';

        var key_form = $("#key_form").val();
        var datedebut_annee = $("#anneescolaire-datedebutannee").val();
        var datefin_annee = $("#anneescolaire-datefinannee").val();
        // var annee_debut = annee_scolaire.split('-')[0];
        // var annee_fin = annee_scolaire.split('-')[1];

        if (datedebut_annee != '' && datefin_annee != '') {
            $('#FormLargeModalContent').hide();
            $('#Formcontent_footer').hide();
            $('#Formcontent_image').show();
            $('#Formcontent_image').html(url_image);

            $.ajax({
                url: url_submit,
                type: "POST",
                data: {
                    key: encodeURIComponent(key_form),
                    date_debut: encodeURIComponent(datedebut_annee),
                    date_fin: encodeURIComponent(datefin_annee),
                },
                success: function(data) {

                    var head_back = data.charAt(0);
                    if (head_back.trim() == "0") {
                        msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                            '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'update_error') ?> </div> </div>';

                        $('#Formcontent_image').html(msg);
                        $('#Formcontent_footer').show();
                        $('#FormLargeModalContent').show();
                    } else if (head_back.trim() == "1") {

                        msg = data.split('###')[1];

                        $('#Formcontent_image').html(msg);
                        $('#Formcontent_footer').show();
                        $('#FormLargeModalContent').show();
                    } else {

                        if (key_form == "0") {
                            $('#bodyInfo').html('<?php echo Yii::t('app', 'succes_creation_anneescolaire') ?>');
                        } else {
                            <?php Yii::t('app', 'succes_update_absence') ?>
                            $('#bodyInfo').html('<?php echo Yii::t('app', 'succes_update_anneescolaire') ?>');
                        }

                        $('#Formcontent_close')[0].click();
                        $('#successInfo').show();
                        $('#contentList').html(data);
                    }

                }
            });
        } else {
            var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'input_empty') ?> </div> </div>';
            $('#Formcontent_image').show();
            $('#Formcontent_image').html(error);
        }


    }

    function delete_member(id) {
        $('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_delete') ?>');
        $('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_delete_anneescolaire') ?>');
        document.getElementById('type_operation').value = '3';
        document.getElementById('id_member').value = id;
        document.getElementById('submit_mod').style.display = "block";

    }

    function active_member(id) {
        $('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_active') ?>');
        $('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_active_anneescolaire') ?>');
        document.getElementById('type_operation').value = '1';
        document.getElementById('id_member').value = id;
        document.getElementById('submit_mod').style.display = "block";
    }

    function desactive_member(id) {
        $('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_desactive') ?>');
        $('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_desactive_anneescolaire') ?>');
        document.getElementById('type_operation').value = '2';
        document.getElementById('id_member').value = id;
        document.getElementById('submit_mod').style.display = "block";
    }

    function cloture_member(id) {
        $('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_cloture') ?>');
        $('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_cloture_anneescolaire') ?>');
        document.getElementById('type_operation').value = '111';
        document.getElementById('id_member').value = id;
        document.getElementById('submit_mod').style.display = "block";
    }

    function do_operation() {

        var url = "<?php echo Yii::$app->request->baseUrl ?>/anneescolaire_operation";

        var mypostrequest = null;
        if (window.XMLHttpRequest) { // Firefox et autres
            mypostrequest = new XMLHttpRequest();
        } else if (window.ActiveXObject) { // Internet Explorer
            try {
                mypostrequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                mypostrequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        mypostrequest.onreadystatechange = function() {
            if (mypostrequest.readyState == 4) {
                if (mypostrequest.status == 200) {

                    document.location.href = location.href;
                }
            }
        }

        var operation = encodeURIComponent(document.getElementById('type_operation').value);
        var id = encodeURIComponent(document.getElementById('id_member').value);

        var parameters = "operation=" + operation + "&id=" + id;

        mypostrequest.open("POST", url, true);
        mypostrequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        mypostrequest.send(parameters);
    }
</script>

<div class="card card-body card-block">
    <?= $this->render("/_modal") ?>
    <?= $this->render("_form") ?>

    <div id="successInfo" style="display: none" class="alert alert-success alert-dismissible show fade" style="margin-bottom: 30px">
        <div class="alert-body" id="bodyInfo"><?php echo Yii::t('app', 'success_operation') ?></div>
    </div>

    <?php
    if ($droits[0] == 1) {
    ?>
        <div class="buttons">
            <a onclick="add_member('0')" href="#" data-toggle="modal" data-target=".Form-modal-lg" class="btn btn-icon icon-left btn-primary"><i class="far fa-save"></i> <?= Yii::t('app', 'add_anneescolaire_menu') ?></a>
        </div>
    <?php }    ?>

    <div id="contentList">
        <?= $this->render("liste", array('donnee' => $donnee)) ?>
    </div>

</div>