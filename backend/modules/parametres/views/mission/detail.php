<?php

use yii\helpers\Html;
?>

<script>
    function add_update_detail(key_information) {

        var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/create_detailmission";
        var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px"]) ?></center>';

        $('#successInfo').hide();
        $('#FormLargeModalContent').hide();
        $('#Formcontent_footer').hide();
        $('#Formcontent_image').show();
        $('#Formcontent_image').html(url_image);
        $('#key_form').val(key_information);
        if (key_information != '0') {
            $('#FormLargeModalLabel').html('<?php echo Yii::t('app', 'update_detailmission_menu') ?>');
        } else {
            $('#FormLargeModalLabel').html('<?php echo Yii::t('app', 'add_detailmission_menu') ?>');
        }

        $.ajax({
            url: url_submit,
            type: "POST",
            data: {
                key: encodeURIComponent(key_information),
            },
            success: function(data) {

                if (data == '') {
                    var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px"> <div class="alert-body"><?php echo Yii::t('app', 'noaccess') ?></div></div>';

                    $('#Formcontent_image').html(error);
                } else {
                    $('#Formcontent_image').hide();
                    $('#Formcontent_footer').show();
                    $('#FormLargeModalContent').show();
                    $('#FormLargeModalContent').html(data);
                }
            }
        });
    }

    function delete_detail(key) {
        $('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_delete') ?>');
        $('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_delete_detailmission') ?>');
        document.getElementById('type_operation').value = '3';
        document.getElementById('id_member').value = key;
        document.getElementById('submit_mod').style.display = "block";

    }

    function save_detail() {
        var curr_url = window.location.href;
        var params = (new URL(curr_url)).searchParams;
        var mission_key = params.get('key');

        var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/save_detailmission";
        var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px",]) ?></center>';
        var dateretour_eff = '';

        var key_form = $("#key_form").val();
        var iduser = $("#detailmission-iduser").val();
        var frais = $("#detailmission-frais").val();
        var contact_abs = $("#detailmission-contactabsence").val();
        if ($("#detailmission-dateretoureffmission").val() != 'undefined' && $("#detailmission-dateretoureffmission").val() != '') {
            dateretour_eff = $("#detailmission-dateretoureffmission").val();
        }

        if (iduser != '' || frais != '' || contact_abs != '') {
            $('#FormLargeModalContent').hide();
            $('#Formcontent_footer').hide();
            $('#Formcontent_image').show();
            $('#Formcontent_image').html(url_image);

            $.ajax({
                url: url_submit,
                type: "POST",
                data: {
                    key: encodeURIComponent(key_form),
                    iduser: encodeURIComponent(iduser),
                    frais: encodeURIComponent(frais),
                    contact_abs: encodeURIComponent(contact_abs),
                    dateretour_eff: encodeURIComponent(dateretour_eff),
                    mission_key: encodeURIComponent(mission_key),
                },
                success: function(data) {

                    var head_back = data.charAt(0);
                    if (head_back.trim() == "0") {
                        var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                            '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'update_error') ?> </div> </div>';

                        $('#Formcontent_image').html(error);
                        $('#Formcontent_footer').show();
                        $('#FormLargeModalContent').show();
                    } else if (head_back.trim() == "1") {

                        error_msg = data.split('###')[1];

                        $('#Formcontent_image').html(error_msg);
                        $('#Formcontent_footer').show();
                        $('#FormLargeModalContent').show();
                    } else {

                        if (key_form == "0") {
                            $('#bodyInfo').html('<?php echo Yii::t('app', 'succes_creation_detailmission') ?>');
                        } else {
                            <?php Yii::t('app', 'succes_update_absence') ?>
                            $('#bodyInfo').html('<?php echo Yii::t('app', 'succes_update_detailmission') ?>');
                        }

                        $('#Formcontent_close')[0].click();
                        $('#successInfo').show();
                        $('#contentList').html(data);
                    }

                }
            });
        } else {
            var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'input_empty') ?> </div> </div>';
            $('#Formcontent_image').show();
            $('#Formcontent_image').html(error);
        }
    }

    function delete_detail_operation() {

        var url = "<?php echo Yii::$app->request->baseUrl ?>/delete_detailmission";

        var mypostrequest = null;
        if (window.XMLHttpRequest) { // Firefox et autres
            mypostrequest = new XMLHttpRequest();
        } else if (window.ActiveXObject) { // Internet Explorer
            try {
                mypostrequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                mypostrequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        mypostrequest.onreadystatechange = function() {
            if (mypostrequest.readyState == 4) {
                if (mypostrequest.status == 200) {
                    document.location.href = location.href;
                }
            }
        }

        var operation = encodeURIComponent(document.getElementById('type_operation').value);
        var id = encodeURIComponent(document.getElementById('id_member').value);

        var parameters = "operation=" + operation + "&id=" + id;

        mypostrequest.open("POST", url, true);
        mypostrequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        mypostrequest.send(parameters);
    }
</script>

<div class="card card-body card-block">

    <?= $this->render("_modal_delete") ?>
    <?= $this->render("_form_detail") ?>


    <div id="contentList">
        <?= $this->render("liste_detail", array('donnee' => $donnee)) ?>
    </div>



</div>