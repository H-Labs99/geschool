<?php


use backend\controllers\Utils;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

$manage_mission = Utils::have_access("manage_mission");
$droits = explode('_', $manage_mission);
?>


<div class="row">
    <div class="col-md-6">
        <?php

        // Mission
        echo DetailView::widget([
            'model' => $donnee['model_mission'],
            'attributes' => [
                [
                    'label' => Yii::t('app', 'motif'),
                    'value' =>  function ($data) {
                        return $data->motif;
                    },
                ],
                [
                    'label' => Yii::t('app', 'lieumission'),
                    'value' =>  function ($data) {
                        return $data->lieumission;
                    },
                ],
                [
                    'label' => Yii::t('app', 'itineraireretenu'),
                    'value' =>  function ($data) {
                        return $data->itineraireretenu;
                    },
                ],
                [
                    'label' => Yii::t('app', 'datedepart'),
                    'value' =>  function ($data) {
                        return $data->datedepart;
                    },
                ],
                [
                    'label' => Yii::t('app', 'dateretourprob'),
                    'value' =>  function ($data) {
                        return $data->dateretourprob;
                    },
                ],
            ],
        ]);

        ?>
    </div>

    <div class="col-md-6">

        <div id="successInfo" style="display: none" class="alert alert-success alert-dismissible show fade" style="margin-bottom: 30px">
            <div class="alert-body"> <button class="close" data-dismiss="alert"> <span>×</span> </button>
                <div id="bodyInfo"> <?php echo Yii::t('app', 'success_operation') ?></div>
            </div>
        </div>

        <div class="buttons">
            <a onclick="add_update_detail('0')" href="#" data-toggle="modal" data-target=".Form-modal-lg" class="btn btn-icon icon-left btn-primary"><i class="far fa-save"></i> <?= Yii::t('app', 'add_detailmission') ?></a>
        </div>

        <?php
        // Détails mission
        echo GridView::widget([
            'dataProvider' => $donnee['all_detailmission'],
            'layout' => '{items}',
            'showOnEmpty' => false,
            'emptyText' => Utils::emptyContent(),
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover',
                'id' => 'example1',
            ],
            'columns' => [

                [
                    'label' => Yii::t('app', 'user'),
                    'value' =>  function ($data) {
                        return $data->nom . ' ' . $data->prenoms;
                    },
                ],
                [
                    'label' => Yii::t('app', 'frais_mission'),
                    'value' =>  function ($data) {
                        return $data->frais;
                    },
                ],
                [
                    'label' => Yii::t('app', 'contact_abs'),
                    'value' =>  function ($data) {
                        return $data->contactabsence;
                    },
                ],
                [
                    'label' => Yii::t('app', 'dateretour_eff'),
                    'value' =>  function ($data) {
                        $date = '';
                        if (isset($data->dateretoureffmission) && $data->dateretoureffmission != '') $date = $data->dateretoureffmission;
                        return $date;
                    },
                ],


                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'headerOptions' => ['width' => '40'],
                    'visible' => $droits[2] == 1 ? true : false,
                    'buttons' => [
                        'update' => function ($url, $data) {
                            return '<a title="' . Yii::t('app', 'updatebutton') . '" href="#" data-toggle="modal" data-target=".Form-modal-lg"  onclick="add_update_detail(\'' . $data->keydetailmission . '\')"><i class="fa fa-edit" style="color:green"></i></a>';
                        },
                    ],

                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'headerOptions' => ['width' => '40'],
                    'visible' => $droits[3] == 1 ? true : false,
                    'buttons' => [
                        'delete' => function ($url, $data) {
                            return '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_detail(\'' . $data->keydetailmission . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
                        },
                    ],

                ]
            ],

        ]);

        ?>
    </div>
</div>