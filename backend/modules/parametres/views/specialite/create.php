<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\controllers\Utils;

$required_sign = Utils::required();

/* @var $this yii\web\View */
/* @var $model backend\modules\parametres\models\Specialite */

// print_r($model);
// die;
?>
<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>

<div class="card-body card-block">

    <!--  Section selection deb  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'section') ?><?= $required_sign ?></label></div>

        <div class="input-group col-md-8">
            <?= $form->field($model, 'idsection')->dropdownList(
                ArrayHelper::map($all_section, 'id', 'designation'),
                ['prompt' => Yii::t('app', 'select_section'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>

    </div>
    <!--  Section selection fin  -->

    <!--  libelle specialite deb  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'libellespecialite') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'libellespecialite')->textinput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>
    <!--  libelle specialite fin  -->

</div>

<?php ActiveForm::end(); ?>