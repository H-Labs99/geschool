<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\parametres\models\AccessRight;
use backend\modules\parametres\models\SchoolModule;
use backend\controllers\Utils;	
$required_sign=Utils::required();

$default_center = Yii::$app->session->get('default_center');
?>

<script type="application/javascript">
            function checkall_line(id) {     
                 var new_status=document.getElementById(id+'_ALL').checked;  
                  
                  document.getElementById(id+'_UPDATE').checked=new_status;
                  document.getElementById(id+'_CREATE').checked=new_status;
                  document.getElementById(id+'_DELETE').checked=new_status;
                  document.getElementById(id+'_LIST').checked=new_status;
             }   
               
            function uncheckall_line(id) {   
                   
                     var pupdate=document.getElementById(id+'_UPDATE').checked;  
                     var pcreate=document.getElementById(id+'_CREATE').checked;  
                     var pdelete=document.getElementById(id+'_DELETE').checked;  
                     var plist=document.getElementById(id+'_LIST').checked;  

                      if(pupdate==true && pcreate==true && pdelete==true && plist==true){   
                            document.getElementById(id+'_ALL').checked=true;
                      }else{ 
                            document.getElementById(id+'_ALL').checked=false;
                      }  
                
              }   
                   
            function checkone(id) {   
                   document.getElementById(id).checked=true ;
             }
                     
            function checkall_info(status) {      
                  if(status==1)var new_status=true;  
                  if(status==0) var new_status=false;  
                     var all_element=document.getElementById('module_page').getElementsByTagName('input');  

                     for(var i=0; i< all_element.length; i++)  {    
                          if(all_element[i].type=='checkbox'){                           
                                    all_element[i].checked=new_status ;    
                           }
                     }
             }      
                                           
</script> 

		<div class="card">
			<?php $form = ActiveForm::begin([
			'id' => 'form-signup',
			'fieldConfig' => [
				'options'=>[
					'tag'=>false,
				]
			]
			]); ?>	 	 
			    <div class="card-body card-block">	  
									
						<div class="row form-group">
							<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?php echo Yii::t('app', 'nameProfil') ?> <?=$required_sign?></label></div>                     
							<div class="input-group col-sm-5" >
								<?= $form->field($model, 'nameProfil')->textInput(['required'=>'required'])->error(false)->label(false); ?>
							</div>
						</div>
						<div class="row form-group">
							  <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?php echo Yii::t('app', 'descriptionProfil') ?></label></div>                     
							  <div class="input-group col-sm-5" >
								<?= $form->field($model, 'descriptionProfil')->textarea(['rows'=>4])->error(false)->label(false); ?>						   
							  </div>					   
						</div>
						<?php 
							$access_right=AccessRight::find()->where(['etat_right'=>1])->andWhere(['NOT IN','id_right',SchoolModule::find()->select(['id_right'])->where(['id_center'=>$default_center,'etat'=>1])])->all();
							if(sizeof($access_right)>0){   
							   $urlcocher   ='<a href="#"   onclick="checkall_info(1);return false"  >'.Yii::t('app', 'check_all').'</a>';
							   $urldecocher='<a href="#" onclick="checkall_info(0);return false" >'.Yii::t('app', 'uncheck_all').'</a>';                               
						?>
						<div class="row form-group">	
							
							<div class="col-sm-8" id="module_page" style="margin-top: 30px;padding-right: 0px">   
						   
								<label ><?php echo Yii::t('app', 'gbobal_privileg')." ( ".$urlcocher." / ".$urldecocher." )" ; ?></label>
								 <table class="table table-bordered table-striped">
											 <tr>                                                        
											   <th style="width:60%"><?php echo Yii::t('app', 'm_module') ?></th> 
											   <th><?php echo Yii::t('app', 'm_create') ?></th> 
											   <th><?php echo Yii::t('app', 'm_read') ?></th> 
											   <th><?php echo Yii::t('app', 'm_delete') ?></th> 
											   <th><?php echo Yii::t('app', 'm_update') ?></th>
											   <th><?php echo Yii::t('app', 'm_all') ?></th>
											  </tr>
											

											<?php $all_module='';
											
											
											foreach($access_right as $valus){
											  
											  if($all_module=='')$all_module=$valus->id_right;
											  else $all_module.="_".$valus->id_right;
											  
											  $id_right=$valus->id_right;
											
											
											?>
											  <tr>
													<td><?php echo Yii::t('app', $valus->lib_right) ; ?></td>
													<td><input type="checkbox"  id="<?php echo $id_right.'_CREATE' ?>" name="<?php echo $id_right.'_CREATE' ?>" value="1" onchange="uncheckall_line('<?php echo $id_right ?>')"  /></td>
													<td><input type="checkbox" id="<?php echo $id_right.'_LIST' ?>" name="<?php echo $id_right.'_LIST' ?>"  value="1" onchange="uncheckall_line('<?php echo $id_right ?>')"   /></td>
													<td><input type="checkbox" id="<?php echo $id_right.'_DELETE' ?>" name="<?php echo $id_right.'_DELETE' ?>" value="1" onchange="uncheckall_line('<?php echo $id_right ?>')"  /></td>
													<td><input type="checkbox"  id="<?php echo $id_right.'_UPDATE' ?>"  name="<?php echo $id_right.'_UPDATE' ?>"  value="1" onchange="uncheckall_line('<?php echo $id_right ?>')"  /></td>
													<td><input type="checkbox" id="<?php echo $id_right.'_ALL' ?>" onchange="checkall_line('<?php echo $id_right ?>')" ></td>
											  </tr>
											 <?php } ?>
														   
														   
										 <input type="hidden" id="all_module" name="all_module" value="<?php echo $all_module ?>" >
														  
								 </table> 

							</div > 
							
						</div>		  
					    <?php } ?>				   
				</div>
								
				<div class="card-footer ">
					<?=Utils::resetbtn();?>
					<?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> '.Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
				</div>
				
			<?php ActiveForm::end(); ?>
		</div>

<?php  	
     if(sizeof($user_module)>0){      
             foreach($user_module as $info_module){
                     
                      $update= trim($info_module['pupdate']);  
                      $create= trim($info_module['pcreate']);  
                      $delete= trim($info_module['pdelete']);
                      $read= trim($info_module['pread']);
                      $id= trim($info_module['id_right']);
           
                      if($create==1 and $delete==1 and $read==1 and $update==1){          
                                   echo'<script type="application/javascript">
                                              checkone("'.$id.'_ALL");    
                                              checkone("'.$id.'_LIST");  
                                              checkone("'.$id.'_CREATE");  
                                              checkone("'.$id.'_UPDATE");  
                                              checkone("'.$id.'_DELETE");  
                                      </script>
                                 ';
                     }else{
                                 if($create==1)  echo'<script type="application/javascript"> checkone("'.$id.'_CREATE") ;</script>';
                                 if($update==1)  echo'<script type="application/javascript"> checkone("'.$id.'_UPDATE") ;</script>';
                                 if($delete==1)  echo'<script type="application/javascript"> checkone("'.$id.'_DELETE") ;</script>';
                                 if($read==1)  echo'<script type="application/javascript"> checkone("'.$id.'_LIST") ;</script>';
                    }
             }
     }       
?>
