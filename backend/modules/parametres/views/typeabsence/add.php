<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\controllers\Utils;
$required_sign=Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\parametres\models\Typeabsence */

$this->title = 'Create Typeabsence';
$this->params['breadcrumbs'][] = ['label' => 'Typeabsences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="card">
    <?php $form = ActiveForm::begin([
        'id' => 'form-signup',
        'fieldConfig' => [
            'options'=>[
                'tag'=>false,
            ]
        ]
    ]); ?>
    <div class="card-body card-block">


        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'designationtypeabsence')?> <?=$required_sign?></label></div>
            <div class="input-group col-sm-5" >
                <?= $form->field($model, 'designationtypeabsence')->textInput(['required'=>'required'])->error(false)->label(false); ?>
            </div>
        </div>


    </div>
    <div class="card-footer">
        <?=Utils::resetbtn();?>
        <?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> '.Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
