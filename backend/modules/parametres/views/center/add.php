<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\parametres\models\UserProfil;

use backend\controllers\Utils;	
$required_sign=Utils::required();
?>
<div class="card">
			<?php $form = ActiveForm::begin([
			'id' => 'form-signup',
			'fieldConfig' => [
				'options'=>[
					'tag'=>false,
				]
			]
			]); ?>	 
			<div class="card-body card-block"> 	
					
				
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'denomination_center')?> <?=$required_sign?></label></div>                     
					<div class="input-group col-sm-5" >
						<?= $form->field($model, 'denomination_center')->textInput(['required'=>'required'])->error(false)->label(false); ?>
					</div>					 
				</div>
				
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'localite_center')?> <?=$required_sign?></label></div>                     
					<div class="input-group col-sm-5" >
						<?= $form->field($model, 'localite_center')->textInput(['required'=>'required'])->error(false)->label(false); ?>
					</div>					 
				</div>			
				
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'sms_disponible')?> <?=$required_sign?></label></div>                     
					<div class="input-group col-sm-5" >
						<?= $form->field($model, 'sms_disponible')->textInput(["onkeyup" =>"if(isNaN(this.value)){this.value=''}",'required'=>'required'])->error(false)->label(false); ?>
					</div>					 
				</div>
						
			</div>
			<div class="card-footer">
				<?=Utils::resetbtn();?>
				<?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> '.Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
			</div>
			<?php ActiveForm::end(); ?>
</div>