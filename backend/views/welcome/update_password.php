<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\controllers\Utils;	
$required_sign=Utils::required();
?>
		<div class="card">
			<?php $form = ActiveForm::begin([
			'id' => 'form-signup',
			'fieldConfig' => [
				'options'=>[
					'tag'=>false,
				]
			]
			]); ?>	 
			<div class="card-body card-block"> 				
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'old_password')?> <?=$required_sign?></label></div>                       
					<div class="input-group col-sm-5">
						<div class="input-group-prepend">
						  <div class="input-group-text">
							<i class="fas fa-lock"></i>
						  </div>
						</div>
						<?= $form->field($model, 'nom')->passwordInput(['required'=>true])->error(false)->Label(false); ?>							
					</div>					 
				</div>		

				
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'new_password')?> <?=$required_sign?></label></div>                       
					 <div class="input-group col-sm-5">
						<div class="input-group-prepend">
						  <div class="input-group-text">
							<i class="fas fa-lock"></i>
						  </div>
						</div>
						<?= $form->field($model, 'prenoms')->passwordInput(['required'=>true])->error(false)->Label(false); ?>
					</div>					 
				</div>			
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?=Yii::t('app', 'repeat_password')?> <?=$required_sign?></label></div>                       
					<div class="input-group col-sm-5">
						<div class="input-group-prepend">
						  <div class="input-group-text">
							<i class="fas fa-lock"></i>
						  </div>
						</div>
						<?= $form->field($model, 'username')->passwordInput(['required'=>true])->error(false)->Label(false); ?>
					</div>					 
				</div>					
			</div>
			<div class="card-footer">
				<?=Utils::resetbtn();?>
				<?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> '.Yii::t('app', 'updatebutton'), ['class' => 'btn btn-success btn-sm', 'name' => 'updatebutton']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>