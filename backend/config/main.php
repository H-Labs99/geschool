<?php
include 'urlManager.php';

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;
$baseUrl = str_replace('/backend/web', '/manager', (new Request)->getBaseUrl());
return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'language'=>'fr_FR',
    'timeZone'=>'Africa/Lome',
    'version'=>'2.1',
    'charset'=>'utf-8',
    'layout'=>'main',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'backend\controllers',
	'modules' => [		
            'users' => [
                'class' => 'backend\modules\users\Users',
            ],
            'parametres' => [
                'class' => 'backend\modules\parametres\Parametres',
            ],
		
		
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
         'request' => [
            'baseUrl' => $baseUrl,
            'cookieValidationKey' => 'qJAWMCCBQcxfEQG0PxfrMXFPXj6Uxltx',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/index',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $tab_url
        ]
       
    ],
    
    
    'params' => $params,
    
	/*
    'modules' => [
		   'gridview' =>  [
				'class' => '\kartik\grid\Module'
			]
	]*/
];
