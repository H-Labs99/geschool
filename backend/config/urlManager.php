<?php
$tab_url = array(

	'login' => 'site/login',
	'logout' => 'site/logout',
	'forget_password' => 'site/forget_password',
	'reset_password' => 'site/reset_password',
	'resend_code' => 'site/resend_code',

	'account' => 'welcome/index',
	'update_info' => 'welcome/update_info',
	'update_password' => 'welcome/update_password',
	'center_params' => 'welcome/center_params',


	'add_user' => 'users/user/add_user',
	'all_user' => 'users/user/all_user',
	'user_operation' => 'users/user/operation',
	'update_user' => 'users/user/update_user',
	'reset_user' => 'users/user/reset_user',
	'create_planaffectation' => 'users/user/create_planaffectation',
	'save_planaffectation' => 'users/user/save_planaffectation',
	'user_planaffectations' => 'users/user/user_planaffectations',
	'delete_affectation' => 'users/user/delete_affectation',



	'add_muser' => 'users/muser/add_user',
	'all_muser' => 'users/muser/all_user',
	'muser_operation' => 'users/muser/operation',
	'update_muser' => 'users/muser/update_user',
	'reset_muser' => 'users/muser/reset_user',
	'add_profil' => 'users/profil/add_profil',
	'all_profil' => 'users/profil/all_profil',
	'update_profil' => 'users/profil/update_profil',
	'profil_operation' => 'users/profil/operation',


	'add_center' => 'parametres/center/add_center',
	'all_center' => 'parametres/center/all_center',
	'center_operation' => 'parametres/center/operation',
	'update_center' => 'parametres/center/update_center',
	'center_module' => 'parametres/center/center_module',
	'checking_center' => 'parametres/center/checking_center',

	//Chemins TypePersonnel
	'all_typepersonnel' => 'users/typepersonnel/all_typepersonnel',
	'typepersonnel_operation' => 'users/typepersonnel/operation',
	'create_typepersonnel' => 'users/typepersonnel/create_typepersonnel',
	'save_typepersonnel' => 'users/typepersonnel/save_typepersonnel',

	//Chemins Diplome
	'all_diplome' => 'users/diplome/all_diplome',
	'diplome_operation' => 'users/diplome/operation',
	'create_diplome' => 'users/diplome/create_diplome',
	'save_diplome' => 'users/diplome/save_diplome',

	//Chemins TypeAbsence
	'all_typeabsence' => 'users/typeabsence/all_typeabsence',
	'add_typeabsence' => 'users/typeabsence/add_typeabsence',
	'typeabsence_operation' => 'users/typeabsence/operation',
	'update_typeabsence' => 'users/typeabsence/update_typeabsence',
	'create_typeabsence' => 'users/typeabsence/create_typeabsence',
	'save_typeabsence' => 'users/typeabsence/save_typeabsence',

	//Chemins Absence Personnel
	'all_absencepersonnel' => 'users/absence/all_absence',
	'add_absencepersonnel' => 'users/absence/add_absence',
	'absence_operation' => 'users/absence/operation',
	'create_absence' => 'users/absence/create_absence',
	'save_absence' => 'users/absence/save_absence',

	'create_typeabsence' => 'users/typeabsence/create_typeabsence',
	'save_typeabsence' => 'users/typeabsence/save_typeabsence',



	//Chemins Mission
	'all_mission' => 'users/mission/all_mission',
	// 'add_mission' => 'users/mission/create_mission',
	'add_mission' => 'users/mission/add_mission',
	'save_mission' => 'users/mission/save_mission',
	'mission_operation' => 'users/mission/operation',



	//Chemins Activite
	'all_activite' => 'users/activite/all_activite',
	'create_activite' => 'users/activite/create_activite',
	'save_activite' => 'users/activite/save_activite',
	'activite_operation' => 'users/activite/operation',

	//Chemins Decoupage
	'all_decoupage' => 'parametres/decoupage/all_decoupage',
	'create_decoupage' => 'parametres/decoupage/create_decoupage',
	'save_decoupage' => 'parametres/decoupage/save_decoupage',
	'decoupage_operation' => 'parametres/decoupage/operation',

	//Chemins Typedecoupage
	'all_typedecoupage' => 'users/typedecoupage/all_typedecoupage',
	'create_typedecoupage' => 'users/typedecoupage/create_typedecoupage',
	'save_typedecoupage' => 'users/typedecoupage/save_typedecoupage',
	'typedecoupage_operation' => 'users/typedecoupage/operation',

	//Chemins Typedecoupage
	'all_anneescolaire' => 'users/anneescolaire/all_anneescolaire',
	'create_anneescolaire' => 'users/anneescolaire/create_anneescolaire',
	'save_anneescolaire' => 'users/anneescolaire/save_anneescolaire',
	'anneescolaire_operation' => 'users/anneescolaire/operation',


	//Chemins Jour ferie
	'all_jourferie' => 'users/jourferie/all_jourferie',
	'create_jourferie' => 'users/jourferie/create_jourferie',
	'save_jourferie' => 'users/jourferie/save_jourferie',
	'jourferie_operation' => 'users/jourferie/operation',


	//Chemins Cours
	'all_cours' => 'users/cours/all_cours',
	'create_cours' => 'users/cours/create_cours',
	'save_cours' => 'users/cours/save_cours',
	'cours_operation' => 'users/cours/operation',

	//Chemins Section
	'all_section' => 'users/section/all_section',
	'create_section' => 'users/section/create_section',
	'save_section' => 'users/section/save_section',
	'section_operation' => 'users/section/operation',

	//Chemins Departement
	'all_departement' => 'users/departement/all_departement',
	'create_departement' => 'users/departement/create_departement',
	'save_departement' => 'users/departement/save_departement',
	'departement_operation' => 'users/departement/operation',

	//Chemins Type Formation 25/05/2021
	'all_typeformation' => 'parametres/typeformation/all_typeformation',
	'create_typeformation' => 'parametres/typeformation/create_typeformation',
	'save_typeformation' => 'parametres/typeformation/save_typeformation',
	'typeformation_operation' => 'parametres/typeformation/operation',

	//Chemins Specialite 25/05/2021
	'all_specialite' => 'parametres/specialite/all_specialite',
	'create_specialite' => 'parametres/specialite/create_specialite',
	'save_specialite' => 'parametres/specialite/save_specialite',
	'specialite_operation' => 'parametres/specialite/operation',

	//Chemins Salle 31/05/2021
	'all_salle' => 'parametres/salle/all_salle',
	'create_salle' => 'parametres/salle/create_salle',
	'save_salle' => 'parametres/salle/save_salle',
	'salle_operation' => 'parametres/salle/operation',

	//Chemins Ressource 01/06/2021
	'all_ressource' => 'parametres/ressource/all_ressource',
	'create_ressource' => 'parametres/ressource/create_ressource',
	'save_ressource' => 'parametres/ressource/save_ressource',
	'ressource_operation' => 'parametres/ressource/operation',

);
