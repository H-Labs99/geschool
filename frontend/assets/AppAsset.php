<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
//	"theme/css/assets.css",
//	"theme/css/typography.css",
//	"theme/css/shortcodes/shortcodes.css",
//	"theme/css/style.css",
//	"theme/css/color/color-1.css",
//	"theme/vendors/revolution/css/layers.css",
//	"theme/vendors/revolution/css/settings.css",
//	"theme/vendors/revolution/css/navigation.css",
      
    ];
    public $js = [
//        "theme/js/jquery.min.js",
//        "theme/vendors/bootstrap/js/popper.min.js",
//        "theme/vendors/bootstrap/js/bootstrap.min.js",
//        "theme/vendors/bootstrap-select/bootstrap-select.min.js",
//        "theme/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js",
//        "theme/vendors/magnific-popup/magnific-popup.js",
//        "theme/vendors/counter/waypoints-min.js",
//        "theme/vendors/counter/counterup.min.js",
//        "theme/vendors/imagesloaded/imagesloaded.js",
//        "theme/vendors/masonry/masonry.js",
//        "theme/vendors/masonry/filter.js",
//        "theme/vendors/owl-carousel/owl.carousel.js",
//        "theme/js/functions.js",
//        "theme/js/contact.js",
//        "theme/vendors/switcher/switcher.js",
//        "theme/vendors/revolution/js/jquery.themepunch.tools.min.js",
//        "theme/vendors/revolution/js/jquery.themepunch.revolution.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.actions.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.carousel.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.kenburn.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.migration.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.navigation.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.parallax.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js",
//        "theme/vendors/revolution/js/extensions/revolution.extension.video.min.js",
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
