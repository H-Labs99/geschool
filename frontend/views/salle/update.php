<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\parametres\models\Salle */

$this->title = 'Update Salle: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Salles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="salle-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
