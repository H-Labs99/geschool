<?= $this->title = "Conditions Générales d'utilisations (ABusiness)"; ?>
<div class="container" style="margin-top: 150px;margin-bottom:50px;">
    <!--==========================
      Contact Section
    ============================-->
    <section id="cgu">
      <div class="container-fluid">

        <div class="section-header">
          <h3><?= Yii::t('app','l_cgu_titre') ?></h3>
        </div>

        <div class="row wow fadeInUp" align="justify">

            <?= $data ?>

        </div>

      </div>
    </section><!-- #contact -->
</div>