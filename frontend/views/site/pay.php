<?php
use yii\helpers\Html;
$this->title = Yii::t('app', 'l_title');
?>

<link rel="stylesheet" href="abusiness/select2/css/select2.min.css">
<style>
    .wrap_started {
      height: 100%;
      display: flex;
      align-items: center;
      /* float: right; */
      justify-content: center;
    }
  
  .button_started {
      min-width: 300px;
      min-height: 60px;
      font-family: 'Nunito', sans-serif;
      font-size: 22px;
      text-transform: uppercase;
      letter-spacing: 1.3px;
      font-weight: 700;
      color: #313133;
      background: #4FD1C5;
      background: linear-gradient(90deg, rgba(129,230,217,1) 0%, rgba(79,209,197,1) 100%);
      border: none;
      border-radius: 1000px;
      box-shadow: 12px 12px 24px rgba(79,209,197,.64);
      transition: all 0.3s ease-in-out 0s;
      cursor: pointer;
      outline: none;
      position: relative;
      padding: 10px;
    }
  
  button_started::before {
    content: '';
    border-radius: 1000px;
    min-width: calc(300px + 12px);
    min-height: calc(60px + 12px);
    border: 6px solid #00FFCB;
    box-shadow: 0 0 60px rgba(0,255,203,.64);
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    opacity: 0;
    transition: all .3s ease-in-out 0s;
  }
  
  .button_started:hover, .button_started:focus {
    color: #313133;
    transform: translateY(-6px);
  }
  
  button_started:hover::before, button_started:focus::before {
    opacity: 1;
  }
  
  button_started::after {
    content: '';
    width: 30px; height: 30px;
    border-radius: 100%;
    border: 6px solid #00FFCB;
    position: absolute;
    z-index: -1;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    animation: ring 1.5s infinite;
  }
  
  button_started:hover::after, button_started:focus::after {
    animation: none;
    display: none;
  }
  
  @keyframes ring {
    0% {
      width: 30px;
      height: 30px;
      opacity: 1;
    }
    100% {
      width: 300px;
      height: 300px;
      opacity: 0;
    }
  }

  
  .videoWrapper {
    position: relative;
    padding-bottom: 56.25%; /* 16:9 */
    height: 0;
  }
  .videoWrapper video {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }


</style>


<div class="site-index">
  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
    <?php echo $this->render('_link_animation') ?>
    <div class="container">
    <!-- <div style="width:calc(100% - 400px); margin-left:auto;margin-right:auto;padding-right: 15px;padding-left: 15px;"> -->


      <div class="intro-img">
        <!-- <img src="abusiness/img/la_maman.png" width="500px" alt="" class="img-fluid"> -->
        <!--Carousel Wrapper-->
          <div id="video-carousel-example" class="carousel slide carousel-fade" data-ride="carousel" data-interval="10000">
            <!--Indicators-->
            <ol class="carousel-indicators">
              <li data-target="#video-carousel-example" data-slide-to="0" class="active"></li>
              <li data-target="#video-carousel-example" data-slide-to="1"></li>
            </ol>
            <!--/.Indicators-->
            <!--Slides-->
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <div class="videoWrapper">
                  <video class="video-fluid" controls>
                    <source src="abusiness/video/pub.mp4" type="video/mp4"/>
                  </video>
                </div>
              </div>
              <div class="carousel-item">
                <!-- <img class="d-block" src="abusiness/img/la_maman.png" width="500px" alt=""> -->
                <img src="abusiness/img/la_maman.png" width="500px" alt="" class="img-fluid">
              </div>
            </div>
            <!--/.Slides-->
            <!--Controls-->
            <a class="carousel-control-prev" href="#video-carousel-example" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#video-carousel-example" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
            <!--/.Controls-->
          </div>
        <!--Carousel Wrapper-->
      </div>
      
      <!-- <div class="wrap_started">
        <button class="button_started">Submit</button>
      </div> -->

        <div class="intro-info">
          <h2 style="font-size: 41px;"><br><?= Yii::t('app', 'l_solution'); ?><br></h2><h3 style="    color: #000;
            margin-bottom: 40px;
            font-size: 30px;
            font-weight: 700;"><?= Yii::t('app', 'l_solution_suite'); ?></h3>
          <div>
            <a href="#about" class="btn-get-started scrollto"><?= Yii::t('app', 'l_functionalities'); ?></a>
            <a href="#services" class="btn-services scrollto"><?= Yii::t('app', 'l_services'); ?></a>
            <!-- <input type="hidden" class="btn-get-started scrollto" value="<?= Yii::t('app', 'l_chatbot_access'); ?>"> -->
            <a href="#services" class="btn-get-started scrollto"><?= Yii::t('app', 'l_chatbot_access'); ?></a>
            <div style="width:350px;" align="center">
              <a href="#services" class="scrollto"><img src="abusiness/img/telegram.png" alt=""></a>
              <a href="#services" class="scrollto"><img src="abusiness/img/whatsapp.png" alt=""></a> 
              <a href="#services" class="scrollto"><img src="abusiness/img/android.png" alt=""></a> 
              <a href="#services" class="scrollto"><img src="abusiness/img/windows.png" alt=""></a> 
            </div>
          </div>
        </div>

    </div>
    
  </section><!-- #intro -->
      

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3><?= Yii::t('app', 'l_concept'); ?><img src="<?= Yii::$app->homeUrl ?>abusiness/img/logo.png" alt="" class="img-fluid"  style="max-height: 96px;"> </h3>
          <p><?= Yii::t('app', 'l_concept_intro'); ?></p>
        </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <p><?= Yii::t('app', 'l_concept_intro_suite'); ?></p>

            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-shopping-bag"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app', 'l_concept_target_1_titre'); ?></a></h4>
              <p class="description"><?= Yii::t('app', 'l_concept_target_1_text'); ?></p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fas fa-shopping-cart"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app', 'l_concept_target_2_titre'); ?></a></h4>
              <p class="description"><?= Yii::t('app', 'l_concept_target_2_text'); ?></p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fas fa-shopping-basket"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app', 'l_concept_target_3_titre'); ?></a></h4>
              <p class="description"><?= Yii::t('app', 'l_concept_target_3_text'); ?></p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="abusiness/img/capture_ab1.png" class="img-fluid" alt="">
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="abusiness/img/capture_ab2.png" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
            <h4><?= Yii::t('app', 'l_concept_solution'); ?></h4>
            <p><?= Yii::t('app', 'l_concept_solution_text_1'); ?></p>
            <p><?= Yii::t('app', 'l_concept_solution_text_2'); ?></p>
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="abusiness/img/capture_ab3.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
            <h4><?= Yii::t('app', 'l_concept_securite'); ?></h4>
            <p>
              <ul>
                <li><?= Yii::t('app', 'l_concept_securite_li_1'); ?></li>
                <li><?= Yii::t('app', 'l_concept_securite_li_2'); ?></li>
                <li><?= Yii::t('app', 'l_concept_securite_li_3'); ?></li>
                <li><?= Yii::t('app', 'l_concept_securite_li_4'); ?></li>
              </ul>
            </p>
            <p><?= Yii::t('app', 'l_concept_why_chatbot'); ?></p>
          </div>
          
        </div>

        <div class="row about-extra">
          <div class="col-lg-12 wow fadeInUp pt-5 pt-lg-0">
            <p align="justify"><?= Yii::t('app', 'l_concept_conclusion'); ?></p>
            <p>
              <?= Yii::t('app', 'l_concept_dispo'); ?>
            <ul>
              <li><?= Yii::t('app', 'l_concept_dispo_li_1'); ?></li>
              <li><?= Yii::t('app', 'l_concept_dispo_li_2'); ?></li>
            </ul>
            </p>
          </div>
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3><?= Yii::t('app','l_service') ?></h3>
          <div class="form-group" style="max-width: 300px;margin:auto;margin-bottom:50px;">
            <!-- <select class="selectpicker countrypicker" data-flag="true" ></select> -->
              <select class="custom-select" id="countries_flag" onchange="getCountryChatbot(this.value)">
                <!-- <option value="">---Sélectionner votre pays---</option> -->
                <?php
                   // $img = '<img src="abusiness/flag/tg.gif">';
                    $size = sizeof($countries);
                    for ($i=0; $i < $size; $i++) {
                        echo '<option data-img_src="abusiness/flag/tg.gif" id="'.$countries[$i]->pays.'" value="'.$countries[$i]->phone.'###'.$countries[$i]->lien.'">'. $countries[$i]->pays.'</option>';
                    }

                    
                ?>
            </select>
          </div>
          <div id="country_chatbot"></div>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-plus-outline" style="color: #ff689b;"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app','l_service_box_1_titre') ?></a></h4>
              <p class="description"><?= Yii::t('app','l_service_box_1_text') ?></p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-plus-outline" style="color: #e9bf06;"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app','l_service_box_2_titre') ?></a></h4>
              <p class="description"><?= Yii::t('app','l_service_box_2_text') ?></p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-cart-outline" style="color: #3fcdc7;"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app','l_service_box_3_titre') ?></a></h4>
              <p class="description"><?= Yii::t('app','l_service_box_3_text') ?></p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-bell-outline" style="color:#41cf2e;"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app','l_service_box_4_titre') ?></a></h4>
              <p class="description"><?= Yii::t('app','l_service_box_4_text') ?></p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-bell-outline" style="color: #d6ff22;"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app','l_service_box_5_titre') ?></a></h4>
              <p class="description"><?= Yii::t('app','l_service_box_5_text') ?></p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-navigate-outline" style="color: #4680ff;"></i></div>
              <h4 class="title"><a href="#"><?= Yii::t('app','l_service_box_6_titre') ?></a></h4>
              <p class="description"><?= Yii::t('app','l_service_box_6_text') ?></p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3><?= Yii::t('app','l_why_titre') ?></h3>
        </header>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-money"></i>
              <div class="card-body">
                <h5 class="card-title"><?= Yii::t('app','l_why_box_1_titre') ?></h5>
                <p class="card-text"><?= Yii::t('app','l_why_box_1_text') ?></p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fas fa-exchange-alt"></i>
              <div class="card-body">
                <h5 class="card-title"><?= Yii::t('app','l_why_box_2_titre') ?></h5>
                <p class="card-text"><?= Yii::t('app','l_why_box_2_text') ?></p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-handshake-o"></i>
              <div class="card-body">
                <h5 class="card-title"><?= Yii::t('app','l_why_box_3_titre') ?></h5>
                <p class="card-text"><?= Yii::t('app','l_why_box_3_text') ?></p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-mobile"></i>
              <div class="card-body">
                <h5 class="card-title"><?= Yii::t('app','l_why_box_4_titre') ?></h5>
                <p class="card-text"><?= Yii::t('app','l_why_box_4_text') ?></p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-truck"></i>
              <div class="card-body">
                <h5 class="card-title"><?= Yii::t('app','l_why_box_5_titre') ?></h5>
                <p class="card-text"><?= Yii::t('app','l_why_box_5_text') ?></p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div>
            
            <!-- <p style="text-align: center; color:white;">
              Ouvrez votre compte marchand, livreur, ou client dès aujourd'hui sans plus attendre C'est toujours 100% gratuit.
              Pas d'application à installer, tout se passe en écrivant à un numéro WhatsApp (ChatBot) <strong><a style="color: #000;" href="https://wa.me/22891000404" target="_blank" target="_blank" rel="noopener noreferrer">ABusiness</a></strong>, 1ère Market place sur WhatsApp. 
              
            </p> -->

              <div class="col-lg-4 mb-4" style="margin: 0 auto;">
                <div class="card wow bounceInUp">
                    <i class="fas fa-running"></i>
                  <div class="card-body">
                    <h5 class="card-title"><?= Yii::t('app','l_why_box_6_titre') ?></h5>
                    <p class="card-text"><?= Yii::t('app','l_why_box_6_text') ?></p>
                    <!-- <a href="#" class="readmore">Read more </a> -->
                  </div>
                </div>
              </div>

          </div>

        </div>

        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?= $counter['nbre_customer'] ?></span>
            <p><?= Yii::t('app','l_counter_customers') ?></p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?= $counter['nbre_marchand'] ?></span>
            <p><?= Yii::t('app','l_counter_sellers') ?></p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?= $counter['nbre_livreur'] ?></span>
            <p><?= Yii::t('app','l_counter_delivery_mens') ?></p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?= $counter['nbre_transaction'] ?></span>
            <p><?= Yii::t('app','l_counter_transactions') ?></p>
          </div>
  
        </div>

      </div>
    </section>

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="section-bg">

      <div class="container">

        <div class="section-header">
          <b><h3><?= Yii::t('app','l_partners') ?></h3></b>
        </div>

        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/tanko.jpg" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/lcvie.png" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/e-agribusiness.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/wassa_group.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <!-- <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/client-5.png" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/client-6.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/client-7.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/client-8.png" class="img-fluid" alt="">
            </div>
          </div> -->

        </div>

      </div>

    </section>

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3><?= Yii::t('app', 'contact'); ?></h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">
            <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="400px" id="gmap_canvas" src="https://maps.google.com/maps?q=Agbalepedogan&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:400px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:100%;}</style></div>
              <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe> -->
            </div>
          </div>

          <div class="col-lg-6">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p><?= Yii::t('app', 'l_our_adresse'); ?></p>
              </div>
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p><?= Yii::t('app', 'l_our_email'); ?></p>
              </div>
              <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p><?= Yii::t('app', 'l_our_telephone'); ?></p>
              </div>
            </div>

            <div class="form" id="form_contact">
              <!-- <div id="sendmessage">Your message has been sent. Thank you!</div> -->
              <div id="content_loading"></div>
              <div id="content_form">
				  <form action="" method="post" role="form" class="contactForm">
					<div class="form-row">
					  <div class="form-group col-lg-6">
						<input type="text" name="name" class="form-control" id="name" placeholder="<?= Yii::t('app', 'l_contact_name'); ?>" data-rule="minlen:4" data-msg="Entrer au moins quatre (4) caractères" />
						<div class="validation"></div>
					  </div>
					  <div class="form-group col-lg-6">
						<input type="email" class="form-control" name="email" id="email" placeholder="<?= Yii::t('app', 'l_contact_email'); ?>" data-rule="email" data-msg="Entrer un email valide" />
						<div class="validation"></div>
					  </div>
					</div>
					<div class="form-group">
					  <input type="text" class="form-control" name="phone" id="phone" placeholder="<?= Yii::t('app', 'l_contact_numero'); ?>" data-rule="minlen:4" data-msg="Entrer au moins huit (8) caractères" />
					  <div class="validation"></div>
					</div>
					<div class="form-group">
					  <input type="text" class="form-control" name="subject" id="subject" placeholder="<?= Yii::t('app', 'l_contact_sujet'); ?>" data-rule="minlen:4" data-msg="Entrer au moins huit (8) caractères" />
					  <div class="validation"></div>
					</div>
					<div class="form-group">
					  <textarea class="form-control" id="message" name="message" rows="5" data-rule="required" data-msg="Message" placeholder="<?= Yii::t('app', 'l_contact_message'); ?>"></textarea>
					  <div class="validation"></div>
					</div>
					<div class="text-center"><input type="button" onclick="sendContact()" value="<?= Yii::t('app', 'l_send'); ?>" title="<?= Yii::t('app', 'l_send'); ?>" class="btn btn-success" style="border-radius: 20px;"> </div>
					
				  </form>
				</div>
            </div>
          
		  </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>
  

</div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- <script src="//unpkg.com/jquery@3.4.1/dist/jquery.min.js"></script> -->
  <script src="//unpkg.com/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
  <script src="//unpkg.com/bootstrap-select@1.12.4/dist/js/bootstrap-select.min.js"></script>
  <script src="//unpkg.com/bootstrap-select-country@4.0.0/dist/js/bootstrap-select-country.min.js"></script>
  

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="abusiness/select2/js/select2.min.js"></script>

<script>
  function sendContact(){
    if($('#name').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: '<?= Yii::t('app', 'l_alert_name'); ?>',
      });
    }else if($('#email').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: '<?= Yii::t('app', 'l_alert_email'); ?>',
      });
    }else if($('#phone').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: '<?= Yii::t('app', 'l_alert_telephone'); ?>',
      });
    }else if($('#subject').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: '<?= Yii::t('app', 'l_alert_sujet'); ?>',
      });
    }else if($('[name=message]').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: '<?= Yii::t('app', 'l_alert_message'); ?>',
      });
    }else{
        var api = "http://abusiness.store/api/web/web_v1/datawises/send_email";
        var nom = ($('#name').val()).trim();
        var email = ($('#email').val()).trim();
        var phone = ($('#phone').val()).trim();
        var sujet = ($('#subject').val()).trim();
        var msg = ($('[name=message]').val()).trim();
        if (phone.charAt(0) == "+") {
          array = phone.split('+');
          phone = array[1].replace(/[\s]{2,}/g," ");
        }
        var url_image = '<div style="text-align:center;margin-top:20px"><?= Html::img(Yii::getAlias("@web/abusiness/spinner/ajax-loader.gif"), ["width" => "100px", "height" => "100px",]) ?></div>';
        $('#content_form').hide();
        $('#content_loading').show();
        $('#content_loading').html(url_image);
        $.ajax({
          type: "POST",
          url: api,
          data: {
            access_token: "sPdgD6GW6AVfAiO5uDSGeFvrmusPdgD6ytpozyidezius",
            nom_customer: nom,
            email_customer: email,
            sujet_contact: sujet,
            message_contact: msg,
            phone_customer: phone,
          },
          success: function (response) {
            $('#content_loading').html("");
            $('#content_loading').hide();
            $('#content_form').show();
			  
            console.log(response);
            $('#name').val("");
            $('#email').val("");
            $('#phone').val("");
            $('#subject').val("");
            $('#message').val("");
            Swal.fire({
              icon: 'success',
              title: '',
              text: 'Message envoyé !',
            });
          },
          error : function (err) {
            console.log(err);
          }
        });
    
    }
  }

	function getCountryChatbot(current_value){
		if(current_value == ""){
			$('#country_chatbot').html("");
		}else{
			var array = current_value.split('###');
			// $('#country_chatbot').html('<h3><?= Yii::t('app', 'l_target_country_chatbot'); ?>: <b>'+array[0]+'</b> <br> <?= Yii::t('app', 'l_target_country_chatbot_link'); ?>: <b><a href="'+array[1]+'" target="_blank" rel="noopener noreferrer"><?= Yii::t('app', 'l_target_link_click'); ?></a></b></h3>');
			var html_table = '<div align="center" style="margin-bottom:20px"><table style="text-align:center" width=60% border=1><tr><th>Logo</th><th>Réseau social</th><th>Accès</th></tr><tr><td><img src="abusiness/img/telegram.png" alt=""></td><td>Telegram</td><td><a href="https://t.me/Abtogo_bot" target="_blank">@Abtogo_bot</a></td></tr><tr><td><img src="abusiness/img/whatsapp.png" alt=""></td><td>WhatsApp</td><td><a href="https://wa.me/22891047373" target="_blank">https://wa.me/22891047373</a></td></tr><tr><td><img src="abusiness/img/android.png" alt=""></td><td>ABusiness sur Androïd</td><td><a href="https://abusiness.store/pwa" target="_blank">https://abusiness.store/pwa</a></td></tr><tr><td><img src="abusiness/img/windows.png" alt=""></td><td>ABusiness sur Windows</td><td><a href="https://abusiness.store/pwa" target="_blank">https://abusiness.store/pwa</a></td></tr></table></div>';
			$('#country_chatbot').html(html_table);
		}
	}

  function custom_template(obj){
      var data = $(obj.element).data();
      var text = $(obj.element).text();
      if(data && data['img_src']){
          img_src = data['img_src'];
          template = $("<div><img src=\"" + img_src + "\"/ style=\"margin-right:10px;\">" + text + "</div>");
          return template;
      }
  }

	var options = {
		'templateSelection': custom_template,
		'templateResult': custom_template,
	}

  $('#countries_flag').select2(options);
  // $('.select2-container--default .select2-selection--single').css({'height': '40px'});

  window.onload = function(){
    // $('#countries_flag').val('selected', true);
    // $('#countries_flag').val($('#countries_flag option:eq(1)').val()).trigger('change');
    var recup = $('#countries_flag option:eq(1)').val();
    // alert($('#countries_flag option:eq(1)').val());
    // $('option[value="'+recup+'"]').prop('selected', true);
    // $("#countries_flag").select2('val', recup);
    // $('#countries_flag').val($('#countries_flag option:eq(1)').val()).change();
    // $('#countries_flag').val(recup).trigger('change');
    var param = "";
    getCountryChatbot('+22891000404###https://wa.me/22891000404');
  }
</script>
