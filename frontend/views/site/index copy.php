<?php
use yii\helpers\Html;
$this->title = '1ère plateforme e-commerce via WhatsApp pour la résilience des marchands contre la COVID-19';
?>

<link rel="stylesheet" href="abusiness/select2/css/select2.min.css">
{{cvie_apiurl2}}api/web/web_v1/incomings/start_call?access_token=YqbwM42mVI4hhqR4N1e0IxmO9LrE&username=22891393958&code=2525


<div class="site-index">

     <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
        <img src="abusiness/img/la_maman.png" width="500px" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2><br>Solution WhatsApp<br></h2><h3 style="    color: #000;
          margin-bottom: 40px;
          font-size: 30px;
          font-weight: 700;">pour développer votre business et le rendre résilient face à une crise</h3>
        <div>
          <a href="#about" class="btn-get-started scrollto">Nos Fonctionnalités</a>
          <a href="#services" class="btn-services scrollto">Nos Services</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Le Concept <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 96px;"></h3>
          <p>Le monde entier traverse une crise pandémique (COVID-19) sans précédent. 
            Partout les consignes et recommandations sont de rester chez soi et de respecter les gestes barrières.</p>
        </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <p>
            De surcroît, 
            si aucun vaccin n'est vite trouvé pour ce virus qui fait rage, nous risquons de finir dans un confinement total. 
            Nous sommes tous d'avis que nos marchés sont le plus souvent des places publiques où le risque de contagion est très élevé à cause de la multitude des personnes qui s'y trouve. 
            </p>

            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-shopping-bag"></i></div>
              <h4 class="title"><a href="">Faciliter l'accès aux produits de 1ere nécessité</a></h4>
              <p class="description">Faciliter l'accès aux produits de première necessité aux ménages tout en proposant des prix raisonnables.
            </p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fas fa-shopping-cart"></i></div>
              <h4 class="title"><a href="">Approvisionnement de la diaspora à leur famille</a></h4>
              <p class="description">Faciliter à la diaspora elle-même en situation de confinement total de pouvoir continuer à soutenir leurs familles au pays.</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fas fa-shopping-basket"></i></div>
              <h4 class="title"><a href="">Soutenir et rendre résilient le marché local</a></h4>
              <p class="description">Permettre aux petits commerçants qui ont perdu leur revenu de passer au e-commerce afin de rendre leurs commerces disponibles.</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="abusiness/img/capture_ab1.png" class="img-fluid" alt="">
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="abusiness/img/capture_ab2.png" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
            <h4>Notre solution</h4>
            <p>
              Nous proposons une plateforme de e-commerce par <b>whatsApp</b> pour livrer les produits aux ménages. L'objectif est de souvenir les petits commerçants à mettre leurs activités sur internet via <b>whatsApp</b>.
            </p>
            <p>
              La boutique est créée sur whatsApp via notre automate. Les clients font les commandes et sont livrés. Les clients paient par un moyen de paiement électronique ( <b>Mobile money, Ecobank P, visa etc...</b> ­)
            </p>
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="abusiness/img/capture_ab3.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
            <h4>Sécurisé & Fiable</h4>
            <p>
              <ul>
                <li>Les paiements sont reçus chez nous à <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 36px;"> qui se positionne comme tier de confiance. A la livraison nous payons le marchand.</li>
                <li>Nous prélevons une commission de 5% chez les marchands et de 15 à 30% chez les livreurs.</li>
                <li>Les livreurs sont aussi payés par le même biais.</li>
                <li>Les livreurs et les marchands s'enrôlent depuis WhatsApp ou aussi via nos agents de terrain par tablettes.</li>
              </ul>
            </p>
            <p>
              Le choix du <b>chatbot WhatsApp</b> a été donc basé sur l'audience qu'il y'a sur ce canal. Ceci permet aux utilisateurs de pouvoir passer des commandes sans quitter WhatsApp ou installer une application supplémentaire. C'est donc dans une discussion avec un automate que vous accédez à des fonctionnalités jusqu'à la livraison des produits chez vous à la maison. 
            </p>
          </div>
          
        </div>

        <div class="row about-extra">
          <div class="col-lg-12 wow fadeInUp pt-5 pt-lg-0">
            <p align="justify">
              Ainsi, <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 36px;"> via un automate WhatsApp permet donc aux petits commerces de souscrire et de mettre à disposition des produits au grand public avec possibilité de livraison à domicile. Les livreurs aussi passent par le même canal pour s'enrôler et se mettre à disposition.
              L'utilisateur devra ajouter le numéro actif dans son pays.<b> Ex: pour le Togo +228 70 52 35 35</b> à son répertoire téléphonique, il écrit au numéro comme dans une discussion normale. L'automate guide le client. En tapant <b>« AB »</b>, le service démarre et guide le client jusqu'à la finalisation de sa commande.
            </p>
            <p>
            Notre solution est disponible en allant directement ici : 
            <ul>
              <li>Via WhatsApp : <b><a href="https://wa.me/22870523535" target="_blank" rel="noopener noreferrer">Cliquez ici</a></b></li>
              <li>Un centre d'appel en langues locales permet de prendre en charge les clients.</li>
            </ul>
            </p>
          </div>
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Accéder à <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 96px;"> par pays</h3>
          <div class="form-group" style="max-width: 300px;margin:auto;margin-bottom:50px;">
            <!-- <select class="selectpicker countrypicker" data-flag="true" ></select> -->
              <select class="form-control" id="countries_flag" onchange="getCountryChatbot(this.value)">
                <option value="">---Sélectionner votre pays---</option>
                <?php
                   // $img = '<img src="abusiness/flag/tg.gif">';
                    $size = sizeof($countries);
                    for ($i=0; $i < $size; $i++) {
                        echo '<option style="background: url("abusiness/img/logo.png");float:right;" id="'.$countries[$i]->pays.'" value="'.$countries[$i]->phone.'###'.$countries[$i]->lien.'">'. $countries[$i]->pays.'</option>';
                    }
                ?>
            </select>
          </div>
          <div id="country_chatbot"></div>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-plus-outline" style="color: #ff689b;"></i></div>
              <h4 class="title"><a href="">Enrôlement des marchands</a></h4>
              <p class="description">Les marchands s'inscrivent par WhatsApp ou via nos agents de terrains munis de tablettes gratuitement. Une fois leur compte validé, ils reçoivent des accès pour publier leurs produits depuis WhatsApp ou via une interface Web.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-plus-outline" style="color: #e9bf06;"></i></div>
              <h4 class="title"><a href="">Enrôlement des livreurs</a></h4>
              <p class="description">Les livreurs peuvent s'enregistrer par WhatsApp ou via nos agents de terrains munis de tablettes gratuitement. Nous procédons à la validation de leur compte avec une pièce d'identité. Ils sont donc mis à disposition des marchands.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-cart-outline" style="color: #3fcdc7;"></i></div>
              <h4 class="title"><a href="">Shopping des clients</a></h4>
              <p class="description">Les clients recherchent des produits par le nom ou des marchands par le code marchand. Une adresse unique permet à un client de se rendre directement dans le boutique d'un marchand. Ex: CF63391.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-bell-outline" style="color:#41cf2e;"></i></div>
              <h4 class="title"><a href="">Notification des marchands</a></h4>
              <p class="description">Après le paiement sur la boutique du marchand, il est notifié par Whatsapp afin de préparer la commande pour le livreur le plus proche de sa zone dans les rayons de 50 km maximum.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-bell-outline" style="color: #d6ff22;"></i></div>
              <h4 class="title"><a href="">Notification des livreurs</a></h4>
              <p class="description">Le livreur le plus proche se voit attribuer la prise en charge de la livraison de la commande au client. Chaque livreur est évalué pour son efficacité dans le système.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-navigate-outline" style="color: #4680ff;"></i></div>
              <h4 class="title"><a href="">Livraison des produits</a></h4>
              <p class="description">A la livraison des produits, le client confirme la bonne réception de la marchandise par WhatsApp. Le marchand est donc payé sur son porte-monnaie electronique.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3>Pourquoi <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 128px;"> est facile et très sécurisé ?</h3>
        </header>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-money"></i>
              <div class="card-body">
                <h5 class="card-title">Paiement sécurisé</h5>
                <p class="card-text">Vos paiements sont 100% sécurisés : Retour de votre paiement 100% garanti si votre produit n'est pas livré ou est de qualité douteuse.</p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fas fa-exchange-alt"></i>
              <div class="card-body">
                <h5 class="card-title">Votre garant</h5>
                <p class="card-text">ABusiness est votre tiers de confiance et vous pourrez réclamer votre argent en cas d'incident  au 108 rue Socrate, AGBALEPEDOGUAN près du lycée 2 Février, 13 BP197 LOME 13, Cel: (228) 92 92 92 85, Lomé-Togo.</p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-handshake-o"></i>
              <div class="card-body">
                <h5 class="card-title">Votre tiers de confiance</h5>
                <p class="card-text">Tous les paiements que vous effectuez sont sécurisés chez ABusiness jusqu'à ce que vous confirmiez la bonne réception du produit. </p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-mobile"></i>
              <div class="card-body">
                <h5 class="card-title">Paiement numérique</h5>
                <p class="card-text">Vous ne payez jamais de main en main, vous payez toujours à ABusiness qui paie en retour au marchand. </p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-truck"></i>
              <div class="card-body">
                <h5 class="card-title">Livreurs dédiés</h5>
                <p class="card-text">Des centaines de milliers de livreurs identifiés sont mis à disposition dans les rayons des marchands les plus prochent et livrent chez vous à la maison. </p>
                <!-- <a href="#" class="readmore">Read more </a> -->
              </div>
            </div>
          </div>

          <div>
            
            <!-- <p style="text-align: center; color:white;">
              Ouvrez votre compte marchand, livreur, ou client dès aujourd'hui sans plus attendre C'est toujours 100% gratuit.
              Pas d'application à installer, tout se passe en écrivant à un numéro WhatsApp (ChatBot) <strong><a style="color: #000;" href="https://wa.me/22870523535" target="_blank" rel="noopener noreferrer">ABusiness</a></strong>, 1ère Market place sur WhatsApp. 
              
            </p> -->

              <div class="col-lg-4 mb-4" style="margin: 0 auto;">
                <div class="card wow bounceInUp">
                    <i class="fas fa-running"></i>
                  <div class="card-body">
                    <h5 class="card-title">Qu'attendez-vous ?</h5>
                    <p class="card-text">Ouvrez votre compte marchand, livreur, ou client dès aujourd'hui sans plus attendre. C'est toujours 100% gratuit.
                  Pas d'application à installer, tout se passe en écrivant à un numéro WhatsApp (ChatBot) <strong><a style="color: #000;" href="https://wa.me/22870523535" target="_blank" rel="noopener noreferrer">ABusiness</a></strong>, 1ère Market place sur WhatsApp.  </p>
                    <!-- <a href="#" class="readmore">Read more </a> -->
                  </div>
                </div>
              </div>

          </div>

        </div>

        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?= $counter[0]['nbre_customer'] ?></span>
            <p>Clients</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?= $counter[0]['nbre_marchand'] ?></span>
            <p>Marchands</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?= $counter[0]['nbre_livreur'] ?></span>
            <p>Livreurs</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?= $counter[0]['nbre_transaction'] ?></span>
            <p>Transactions</p>
          </div>
  
        </div>

      </div>
    </section>

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="section-bg">

      <div class="container">

        <div class="section-header">
          <b><h3>ILS NOUS FONT CONFIANCE</h3></b>
        </div>

        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/tanko.jpg" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/lcvie.png" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/e-agribusiness.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/wassa_group.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <!-- <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/client-5.png" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/client-6.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/client-7.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="abusiness/img/clients/client-8.png" class="img-fluid" alt="">
            </div>
          </div> -->

        </div>

      </div>

    </section>

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3>Nous contacter</h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">
            <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="400px" id="gmap_canvas" src="https://maps.google.com/maps?q=Agbalepedogan&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:400px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:100%;}</style></div>
              <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe> -->
            </div>
          </div>

          <div class="col-lg-6">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p>108 Rue Socrate, AGBALEPEDOGAN</p>
              </div>
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p>info@abusiness.store</p>
              </div>
              <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p>+228 92 92 92 85</p>
              </div>
            </div>

            <div class="form" id="form_contact">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="content_loading"></div>
              <div id="content_form">
				  <form action="" method="post" role="form" class="contactForm">
					<div class="form-row">
					  <div class="form-group col-lg-6">
						<input type="text" name="name" class="form-control" id="name" placeholder="Votre nom" data-rule="minlen:4" data-msg="Entrer au moins quatre (4) caractères" />
						<div class="validation"></div>
					  </div>
					  <div class="form-group col-lg-6">
						<input type="email" class="form-control" name="email" id="email" placeholder="Votre email" data-rule="email" data-msg="Entrer un email valide" />
						<div class="validation"></div>
					  </div>
					</div>
					<div class="form-group">
					  <input type="text" class="form-control" name="phone" id="phone" placeholder="Votre numéro exemple : +228 90 XX XX XX" data-rule="minlen:4" data-msg="Entrer au moins huit (8) caractères" />
					  <div class="validation"></div>
					</div>
					<div class="form-group">
					  <input type="text" class="form-control" name="subject" id="subject" placeholder="Sujet" data-rule="minlen:4" data-msg="Entrer au moins huit (8) caractères" />
					  <div class="validation"></div>
					</div>
					<div class="form-group">
					  <textarea class="form-control" id="message" name="message" rows="5" data-rule="required" data-msg="Message" placeholder="Message"></textarea>
					  <div class="validation"></div>
					</div>
					<div class="text-center"><input type="button" onclick="sendContact()" value="Envoyer" title="Envoyer" class="btn btn-success" style="border-radius: 20px;"> </div>
					
				  </form>
				</div>
            </div>
          
		  </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>
  

</div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- <script src="//unpkg.com/jquery@3.4.1/dist/jquery.min.js"></script> -->
  <script src="//unpkg.com/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
  <script src="//unpkg.com/bootstrap-select@1.12.4/dist/js/bootstrap-select.min.js"></script>
  <script src="//unpkg.com/bootstrap-select-country@4.0.0/dist/js/bootstrap-select-country.min.js"></script>
  

<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/esm/popper.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="abusiness/select2/js/select2.min.js"></script>
<script>
  function sendContact(){
    if($('#name').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: 'Veuillez renseigner votre nom!',
      });
    }else if($('#email').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: 'Veuillez renseigner votre email!',
      });
    }else if($('#phone').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: 'Veuillez renseigner votre numéro de téléphone!',
      });
    }else if($('#subject').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: 'Veuillez renseigner le sujet!',
      });
    }else if($('[name=message]').val()==""){
      Swal.fire({
        icon: 'info',
        title: '',
        text: 'Veuillez renseigner le message!',
      });
    }else{
        var api = "http://abusiness.store/api/web/web_v1/datawises/send_email";
        var nom = ($('#name').val()).trim();
        var email = ($('#email').val()).trim();
        var phone = ($('#phone').val()).trim();
        var sujet = ($('#subject').val()).trim();
        var msg = ($('[name=message]').val()).trim();
        if (phone.charAt(0) == "+") {
          array = phone.split('+');
          phone = array[1].replace(/[\s]{2,}/g," ");
        }
        var url_image = '<div style="text-align:center;margin-top:20px"><?= Html::img(Yii::getAlias("@web/abusiness/spinner/ajax-loader.gif"), ["width" => "100px", "height" => "100px",]) ?></div>';
        $('#content_form').hide();
        $('#content_loading').show();
        $('#content_loading').html(url_image);
        $.ajax({
          type: "POST",
          url: api,
          data: {
            access_token: "sPdgD6GW6AVfAiO5uDSGeFvrmusPdgD6ytpozyidezius",
            nom_customer: nom,
            email_customer: email,
            sujet_contact: sujet,
            message_contact: msg,
            phone_customer: phone,
          },
          success: function (response) {
            $('#content_loading').html("");
            $('#content_loading').hide();
            $('#content_form').show();
			  
            console.log(response);
            $('#name').val("");
            $('#email').val("");
            $('#phone').val("");
            $('#subject').val("");
            $('#message').val("");
            Swal.fire({
              icon: 'success',
              title: '',
              text: 'Message envoyé !',
            });
          },
          error : function (err) {
            console.log(err);
          }
        });
    
    }
  }

  function getCountryChatbot(current_value){
    var array = current_value.split('###');
    $('#country_chatbot').html('<h3>Numéro du Chatbot: <b>'+array[0]+'</b> <br> Le lien WhatsApp: <b><a href="'+array[1]+'" target="_blank" rel="noopener noreferrer">Cliquez ici</a></b></h3>');
  }

  // $(document).ready(function() {
  //   $("#countries_flag").select2({
  //       templateResult: function(item) {
  //           return format(item, false);
  //       }
  //   });
  // });


  // function format(item, state) {
  //   if (!item.id) {
  //       return item.text;
  //   }
  //   var countryUrl = "https://lipis.github.io/flag-icon-css/flags/4x3/";
  //   var stateUrl = "https://oxguy3.github.io/flags/svg/us/";
  //   var url = state ? stateUrl : countryUrl;
  //   var img = $("<img>", {
  //       class: "img-flag",
  //       width: 20,
  //       src: url + item.element.value.toLowerCase() + ".svg"
  //   });
  //   var span = $("<span>", {
  //       text: " " + item.text
  //   });
  //   span.prepend(img);
  //   return span;
  // }


  window.onload = function(){
    $('#TOGO').attr('selected', true);
    getCountryChatbot('+22870523535###https://wa.me/22870523535');
  }
</script>

<script>
    $('.countrypicker').countrypicker();
</script>




