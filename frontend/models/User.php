<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $idP
 * @property integer $id_collecteur
 * @property integer $idProvenance
 * @property string $nom
 * @property string $prenoms
 * @property string $validation_code
 * @property integer $id_user_profil
 * @property string $photo
 * @property string $username
 * @property string $creation
 * @property string $canal
 * @property string $canal_key
 * @property string $web_key
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $raison_desactivation
 *
 * @property ActeurUser[] $acteurUsers
 * @property CommandeMarchand[] $commandeMarchands
 * @property CommandePayment[] $commandePayments
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idP', 'id_user_profil', 'username', 'auth_key', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['idP', 'id_collecteur', 'idProvenance', 'id_user_profil', 'role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['raison_desactivation'], 'string'],
            [['nom', 'prenoms', 'validation_code', 'photo', 'username', 'creation', 'canal_key', 'web_key', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['canal'], 'string', 'max' => 20],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idP' => 'Id P',
            'id_collecteur' => 'Id Collecteur',
            'idProvenance' => 'Id Provenance',
            'nom' => 'Nom',
            'prenoms' => 'Prenoms',
            'validation_code' => 'Validation Code',
            'id_user_profil' => 'Id User Profil',
            'photo' => 'Photo',
            'username' => 'Username',
            'creation' => 'Creation',
            'canal' => 'Canal',
            'canal_key' => 'Canal Key',
            'web_key' => 'Web Key',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'raison_desactivation' => 'Raison Desactivation',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActeurUsers()
    {
        return $this->hasMany(ActeurUser::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommandeMarchands()
    {
        return $this->hasMany(CommandeMarchand::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommandePayments()
    {
        return $this->hasMany(CommandePayment::className(), ['id_user' => 'id']);
    }
}
