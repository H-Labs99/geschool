<?php

/**
 * Translation map for nl-NL
 */
return array(

	# TITLE
	'l_title' => '1ère plateforme e-commerce via Réseaux sociaux pour la résilience des marchands contre la COVID-19',

	# HEADER
	'ab_c' => 'ABCrédit',
	'ab_desc' => 'Description du service',
	'abc_intro' => 'Choisissez la formule qui vous plaît.',
	'abc_slogan' => 'Avec ABCrédit, rendez vos fin du mois agréables.',
	'abc_inscription_title' => 'Souscription à ABCrédit',
	'abc_inscription' => "Pour souscrire à ABCrédit, veuillez vous inscrire via l'assistant automatique ABusiness en tant que client et ensuite vous rendre dans votre banque pour la finalisation.",
	'abc_get_now' => 'Souscrire',
	'abc_year' => '1 an',
	'abc_devise' => 'FCFA',
	'abc_f1' => 'Solde renouvelé de',
	'abc_f1_suite' => ' chaque mois.',
	'abc_f2' => 'Vous remboursez un interêt de',
	'abc_f2_suite' => 'uniquement que sur vos dépenses mensuelles à la fin du mois.',
	'abc_f3' => 'Un service de commande en ligne et de livraison à domicile avec votre carte de crédit chez les milliers de marchands.',
	'abc_d1' => "<b>ABCrédit</b> vous permet de disposer d'un portefeuille éléctronique de secours en cas de temps difficile.
		Ce portefeuille sert à effectuer des achats de produits de première nécessité auprès des marchands partenaires.
		Nous disposons actuellement d'un réseau de plus de 2000 marchands sur la ville de Lomé et de Kara qui ne cesse de grandir de jour en jour pour votre plus grand bonheur.
		Notre objectif est de vous assurer une serénité en tout temps pour vous et votre famille.",
	'abc_d2' => "Votre carte <b>ABCrédit</b> vous sert à valider vos achats auprès des marchands afin d'assurer les débits si vous n'avez pas votre téléphone sous la main.
	Vous pourrez aussi valider vos achats par les réseaux sociaux <b>WhatsApp</b> et <b>Telegram</b> ou via notre <b>assistant automatique</b>.",
	'abc_d3' => "Pour accéder à nos services vous devez avoir une domiciliation de salaire dans un établissement financier partenaire.",
	'abc_d4' => "Obtenez donc dès maintenant des facilités de crédits de consommation chaque mois en souscrivant à une de nos cartes :",

	'abusiness' => 'ABusiness',
	'functionalities' => 'Fonctionnalités',
	'services' => 'Accès',
	'la_foire' => 'La foire Adjafi 2020',
	'nos_produits' => 'Nos produits',
	'vente_live' => 'Vente live',
	'CGU' => 'CGU',
	'contact' => 'Nous contacter',
	'search_product' => 'Rechercher un produit',
	'search' => 'Rechercher',
	'contact_sc' => 'Contacter le service clientèle',
	'errora' => "Une erreur s'est produite, veuillez réessayer plus tard.",

	'l_details' => 'Détails sur le produit',
	'l_close' => 'Fermer',
	'l_details_btn' => 'Plus d\'informations',
	'l_buy_whatsapp' => '+ Acheter sur WhatsApp',
	'l_buy_telegram' => '+ Acheter sur Télégram',
	'l_product_img' => 'IMAGE',
	'l_product_ref' => 'RÉFÉRENCE',
	'l_product_name' => 'PRODUIT',
	'l_product_desc' => 'DESCRIPTION',
	'l_product_price' => 'PRIX',

	# INTRO
	'l_solution' => 'Solution Réseaux sociaux',
	'l_solution_suite' => 'pour développer votre business et le rendre résilient face à une crise',
	'l_functionalities' => 'Nos fonctionnalités',
	'l_services' => 'Nos services',
	'l_chatbot_access' => 'Accéder à l\'assistant automatique',

	# FONCTIONNALITES
	'l_concept' => 'Le Concept',
	'l_concept_intro' => 'Le monde entier traverse une crise pandémique (COVID-19) sans précédent. Partout les consignes et recommandations sont de rester chez soi et de respecter les gestes barrières.',
	'l_concept_intro_suite' => " De surcroît, si aucun vaccin n'est vite trouvé pour ce virus qui fait rage, nous risquons de finir dans un confinement total. Nous sommes tous d'avis que nos marchés sont le plus souvent des places publiques où le risque de contagion est très élevé à cause de la multitude des personnes qui s'y trouve.",
	'l_concept_target_1_titre' => "Faciliter l'accès aux produits de 1ere nécessité",
	'l_concept_target_1_text' => "Faciliter l'accès aux produits de première necessité aux ménages tout en proposant des prix raisonnables.",
	'l_concept_target_2_titre' => "Approvisionnement de la diaspora à leur famille",
	'l_concept_target_2_text' => "Faciliter à la diaspora elle-même en situation de confinement total de pouvoir continuer à soutenir leurs familles au pays.",
	'l_concept_target_3_titre' => "Soutenir et rendre résilient le marché local",
	'l_concept_target_3_text' => "Permettre aux petits commerçants qui ont perdu leur revenu de passer au e-commerce afin de rendre leurs commerces disponibles.",
	'l_concept_solution' => "Notre solution",
	'l_concept_solution_text_1' => "Nous proposons une plateforme de e-commerce par les <b>réseaux sociaux</b> pour livrer les produits aux ménages. L'objectif est de soutenir les petits commerçants à mettre leurs activités sur internet via les <b>réseaux sociaux</b>.",
	'l_concept_solution_text_2' => "La boutique est créée sur les réseaux sociaux via notre automate. Les clients font les commandes et sont livrés. Les clients paient par un moyen de paiement électronique ( <b>Mobile money, Ecobank P, visa etc...</b> ­)",
	'l_concept_securite' => "Sécurisé & Fiable",
	'l_concept_securite_li_1' => 'Les paiements sont reçus chez nous à <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 36px;"> qui se positionne comme tier de confiance. A la livraison nous payons le marchand.',
	'l_concept_securite_li_2' => "Nous prélevons une commission de 10 à 15% chez les livreurs.",
	'l_concept_securite_li_3' => "Les livreurs sont aussi payés par le même biais.",
	'l_concept_securite_li_4' => "Les livreurs et les marchands s'enrôlent par les réseaux sociaux ou aussi via nos agents de terrain par tablettes.",
	'l_concept_why_chatbot' => "Le choix du <b>chatbot Réseaux sociaux</b> a été donc basé sur l'audience qu'il y'a sur ce canal. Ceci permet aux utilisateurs de pouvoir passer des commandes sans quitter le réseau social ou installer une application supplémentaire. C'est donc dans une discussion avec un automate que vous accédez à des fonctionnalités jusqu'à la livraison des produits chez vous à la maison.",
	'l_concept_conclusion' => "Ainsi, <img src=\"abusiness/img/logo.png\" class=\"img-fluid\" style=\"max-height: 36px;\"> via un automate réseau social permet donc aux petits commerces de souscrire et de mettre à disposition des produits au grand public avec possibilité de livraison à domicile. Les livreurs aussi passent par le même canal pour s'enrôler et se mettre à disposition. L'utilisateur devra donc juste démarrer comme une discussion normale avec l'assitant automatique. L'automate guide le client. En tapant <b>« AB »</b>, le service démarre et guide le client jusqu'à la finalisation de sa commande.",
	'l_concept_dispo' => " Notre solution est disponible en allant directement ici : ",
	'l_concept_dispo_li_1' => 'Via les réseaux sociaux : <b><a href="#services" class="scrollto">Cliquez ici</a></b>',
	'l_concept_dispo_li_2' => "Un centre d'appel en langues locales permet de prendre en charge les clients.",

	# SERVICES
	'l_service' => 'Accéder à <img src="abusiness/img/logo.png" alt="" class="img-fluid"  style="max-height: 96px;"> par pays',
	'l_service_box_1' => 'Enrôlement des marchands',
	'l_service_box_1_titre' => 'Enrôlement des marchands',
	'l_service_box_1_text' => "Les marchands s'inscrivent par les réseaux sociaux ou via nos agents de terrains munis de tablettes gratuitement. Une fois leur compte validé, ils reçoivent des accès pour publier leurs produits depuis les réseaux sociaux ou via une interface Web.",
	'l_service_box_2_titre' => 'Enrôlement des livreurs',
	'l_service_box_2_text' => "Les livreurs peuvent s'enregistrer par les réseaux sociaux ou via nos agents de terrains munis de tablettes gratuitement. Nous procédons à la validation de leur compte avec une pièce d'identité. Ils sont donc mis à disposition des marchands.",
	'l_service_box_3_titre' => 'Shopping des clients',
	'l_service_box_3_text' => "Les clients recherchent des produits par le nom ou des marchands par le code marchand. Une adresse unique permet à un client de se rendre directement dans le boutique d'un marchand. Ex: CF63391.",
	'l_service_box_4_titre' => 'Notification des marchands',
	'l_service_box_4_text' => 'Après le paiement sur la boutique du marchand, il est notifié par les réseaux sociaux afin de préparer la commande pour le livreur le plus proche de sa zone dans les rayons de 50 km maximum.',
	'l_service_box_5_titre' => 'Notification des livreurs',
	'l_service_box_5_text' => 'Le livreur le plus proche se voit attribuer la prise en charge de la livraison de la commande au client. Chaque livreur est évalué pour son efficacité dans le système.',
	'l_service_box_6_titre' => 'Livraison des produits',
	'l_service_box_6_text' => 'A la livraison des produits, le client confirme la bonne réception de la marchandise par les réseaux sociaux. Le marchand est donc payé sur son porte-monnaie electronique.',

	# --- Why
	'l_why_titre' => 'Pourquoi <img src="abusiness/img/logo.png" alt="" class="img-fluid"  style="max-height: 128px;"> est facile et très sécurisé ?',
	'l_why_box_1_titre' => 'Paiement sécurisé',
	'l_why_box_1_text' => "Vos paiements sont 100% sécurisés : Retour de votre paiement 100% garanti si votre produit n'est pas livré ou est de qualité douteuse.",
	'l_why_box_2_titre' => 'Votre garant',
	'l_why_box_2_text' => "ABusiness est votre tiers de confiance et vous pourrez réclamer votre argent en cas d'incident  au 108 rue Socrate, AGBALEPEDOGUAN près du lycée 2 Février, 13 BP197 LOME 13, Cel: (228) 92 92 92 85, Lomé-Togo.",
	'l_why_box_3_titre' => 'Votre tiers de confiance',
	'l_why_box_3_text' => "Tous les paiements que vous effectuez sont sécurisés chez ABusiness jusqu'à ce que vous confirmiez la bonne réception du produit.",
	'l_why_box_4_titre' => 'Paiement numérique',
	'l_why_box_4_text' => 'Vous ne payez jamais de main en main, vous payez toujours à ABusiness qui paie en retour au marchand.',
	'l_why_box_5_titre' => 'Livreurs dédiés',
	'l_why_box_5_text' => 'Des centaines de milliers de livreurs identifiés sont mis à disposition dans les rayons des marchands les plus prochent et livrent chez vous à la maison.',
	'l_why_box_6_titre' => "Qu'attendez-vous ?",
	'l_why_box_6_text' => "Ouvrez votre compte marchand, livreur, ou client dès aujourd'hui sans plus attendre. C'est toujours 100% gratuit. Pas d'application à installer, tout se passe en écrivant à l'assitant automatique <strong><a style=\"color: #000;\" href=\"#services\" class=\"scrollto\">ABusiness</a></strong> via les réseaux sociaux, 1ère Market place sur les réseaux sociaux. ",

	'l_counter_customers' => "Clients",
	'l_counter_sellers' => "Marchands",
	'l_counter_delivery_mens' => "Livreurs",
	'l_counter_transactions' => "Transactions",

	'l_partners' => "ILS NOUS FONT CONFIANCE",
	'l_contact_name' => "Votre nom",
	'l_contact_email' => "Votre email",
	'l_contact_numero' => "Votre numéro exemple : +228 90 XX XX XX",
	'l_contact_sujet' => "Sujet",
	'l_contact_message' => "Message",

	# NOUS CONTACTER
	'l_our_adresse' => '108 Rue Socrate, AGBALEPEDOGAN',
	'l_our_email' => 'info@abusiness.store',
	'l_our_telephone' => '+228 92 92 92 85',
	'l_send' => 'Envoyer',
	'l_alert_name' => 'Veuillez renseigner votre nom!',
	'l_alert_email' => 'Veuillez renseigner votre email!',
	'l_alert_telephone' => 'Veuillez renseigner votre numéro de téléphone!',
	'l_alert_sujet' => 'Veuillez renseigner le sujet!',
	'l_alert_message' => 'Veuillez renseigner le message!',

	# FOOTER
	'l_footer_text' => "Une plateforme e-commerce pour les produits de première nécessité disponible pour les utilisateurs des réseaux sociaux. Les marchands, les livreurs et les clients l'utilisent sans installer la moindre application. Ils profitent de la puissance de la messagerie instantanée pour accéder à des fonctionnalités depuis un chatbot.",
	'l_useful_link' => 'Liens utiles',
	'l_footer_adress' => 'Notre adresse',
	'l_footer_cgu' => "Conditions générales d'utilisation",
	'l_footer_adress_postal' => '13 BP 197 LOME 13<br>Lomé - Togo <br>',
	'l_footer_customer_service' => 'Service clientèle Togo',
	'l_email' => 'Email',

	'l_footer_law' => 'Tous droits réservés',
	'l_footer_create_by' => 'Développée par',
	'l_clin' => 'CLIN',

	'l_cgu_titre' => "Termes & conditions générales d'utilisation",

	# SELECT COUNTRY CHATBOT
	'l_target_country_chatbot' => 'Numéro du Chatbot',
	'l_target_country_chatbot_link' => 'Le lien Réseaux sociaux',
	'l_target_link_click' => 'Cliquez ici',

	'l_chat_wellcome_msg' => "Bienvenue sur notre assistant automatique. La Plateforme <b>abusiness.store</b> est une plateforme de e-commerce par réseaux sociaux utilisant un assistant automatique interactif pour acheter  payer et livrer les produits de première nécessité aux ménages. 

	L'objectif est de soutenir les petits commerçants à mettre leurs activités sur internet afin d'être résilients même en situation de crise comme celle de la Covid 19 sans installer la moindre application.",

	'l_chat_start_word' => 'AB',

);
