<?php

/**
 * Translation map for nl-NL
 */
return array(

	# TITLE
	'l_title' => 'First e-commerce platform via social network for merchant resilience against COVID-19',

	# HEADER
	'ab_c' => 'ABCredit',
	'ab_desc' => 'Description of the service',
	'abc_intro' => 'Choose the formula you like.',
	'abc_slogan' => 'With ABCredit, make your end of the month pleasant.',
	'abc_inscription_title' => 'Subscription to ABCredit',
	'abc_inscription' => 'To subscribe to ABCredit, please register through the ABusiness automatic assistant as a customer and then go to your bank for finalization.',
	'abc_get_now' => 'Subscribe',
	'abc_year' => '1 year',
	'abc_devise' => 'FCFA',
	'abc_f1' => 'Each month you have a balance of',
	'abc_f1_suite' => ' par mois',
	'abc_f2' => 'Your bank reimburses us with a rate of',
	'abc_f2_suite' => 'according to your expenses.',
	'abc_f3' => 'Online ordering and home delivery with your credit card from thousands of merchants.',
	'abc_d1' => "<b> ABCredit </b> allows you to have a back-up electronic wallet in case of difficult times.
		This wallet is used to make purchases of essential products from partner merchants.
		We currently have a network of more than 2000 merchants in the city of Lomé and Kara which continues to grow day by day for your greatest happiness.
		Our goal is to ensure peace of mind at all times for you and your family.",
	'abc_d2' => "Your <b> ABCredit </b> card is used to validate your purchases with merchants to ensure debits if you do not have your phone handy.
		You can also validate your purchases via the <b> WhatsApp </b> and <b> Telegram </b> social networks or via our <b> automatic assistant</b>.",
	'abc_d3' => "To access our services you must have a salary domiciliation in a partner financial institution.",
	'abc_d4' => "So now obtain consumer credit facilities each month by subscribing to one of our cards :",

	'abusiness' => 'ABusiness',
	'functionalities' => 'Features',
	'services' => 'Services',
	'la_foire' => 'The Adjafi fair 2020',
	'nos_produits' => 'Our products',
	'vente_live' => 'Live sale',
	'CGU' => 'CGU',
	'contact' => 'Contact us',
	'search_product' => 'Search product',
	'search' => 'Search',
	'contact_sc' => 'Contact customer service',
	'errora' => "An error has occurred, please try again later.",

	'l_details' => 'Product details',
	'l_close' => 'Close',
	'l_details_btn' => 'Quick view',
	'l_buy_whatsapp' => '+ Buy with WhatsApp',
	'l_buy_telegram' => '+ Buy with Telegram',
	'l_product_img' => 'PICTURE',
	'l_product_ref' => 'REFERENCE',
	'l_product_name' => 'PRODUCT',
	'l_product_desc' => 'DESCRIPTION',
	'l_product_price' => 'PRICE',

	# INTRO
	'l_solution' => 'social network solution',
	'l_solution_suite' => 'to develop your business and make it resilient in the face of a crisis',
	'l_functionalities' => 'Our features',
	'l_services' => 'Our services',
	'l_chatbot_access' => 'Access the chatbot',

	# FONCTIONNALITES
	'l_concept' => 'The concept',
	'l_concept_intro' => 'The whole world is going through an unprecedented pandemic (COVID-19) crisis. The instructions and recommendations everywhere are to stay at home and to respect barrier gestures.',
	'l_concept_intro_suite' => "Moreover, if no vaccine is quickly found for this raging virus, we risk ending up in total containment. We are all of the opinion that our markets are most often public places where the risk of contagion is very high because of the multitude of people who are there.",
	'l_concept_target_1_titre' => "Facilitate access to basic necessities",
	'l_concept_target_1_text' => "Facilitate access to basic necessities for households while offering reasonable prices.",
	'l_concept_target_2_titre' => "Supplying the diaspora to their families",
	'l_concept_target_2_text' => "Make it easier for the diaspora itself in a situation of total containment to be able to continue to support their families in the country.",
	'l_concept_target_3_titre' => "Support and make the local market resilient",
	'l_concept_target_3_text' => "Allow small traders who have lost their income to switch to e-commerce in order to make their businesses available.",
	'l_concept_solution' => "Our solution",
	'l_concept_solution_text_1' => "We offer an e-commerce platform by <b> social network </b> to deliver the products to households. The objective is to support small traders to put their activities on the internet via <b> social network </b>.",
	'l_concept_solution_text_2' => "The store is created on social network via our machine. Customers place orders and are delivered. Customers pay by electronic means of payment (<b> Mobile money, Ecobank P, visa etc ... </b>)",
	'l_concept_securite' => "Secure & Reliable",
	'l_concept_securite_li_1' => 'Payments are received with us at <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 36px;"> which positions itself as a trusted tier. On delivery we pay the merchant.',
	'l_concept_securite_li_2' => "We take a commission of 10 to 15% from delivery people.",
	'l_concept_securite_li_3' => "Delivery people are also paid in the same way.",
	'l_concept_securite_li_4' => "Delivery people and merchants enroll from social network or also via our field agents via tablets.",
	'l_concept_why_chatbot' => "The choice of <b> social network chatbot </b> was therefore based on the audience that there is on this channel. This allows users to be able to place orders without leaving social network or installing an additional app. It is therefore in a discussion with a PLC that you access the functions until the products are delivered to your home..",
	'l_concept_conclusion' => "So, <img src=\"abusiness/img/logo.png\" class=\"img-fluid\" style=\"max-height: 36px;\"> Via a social network machine, therefore, small businesses can subscribe and make products available to the general public with the possibility of home delivery. Delivery people also go through the same channel to enlist and make themselves available. The user will therefore just have to start as a normal discussion with the automatic assistant. The automaton guides the customer. By typing <b> \"AB\" </b>, the service starts and guides the customer until the finalization of his order.",
	'l_concept_dispo' => " Our solution is available by going directly here: ",
	'l_concept_dispo_li_1' => 'Via social network : <b><a href="#services" class="scrollto">Click here</a></b>',
	'l_concept_dispo_li_2' => "A call center in local languages ​​can support customers.",

	# SERVICES
	'l_service' => 'Get to <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 96px;"> by country',
	'l_service_box_1' => 'Enrollment of merchants',
	'l_service_box_1_titre' => 'Enrollment of merchants',
	'l_service_box_1_text' => "Merchants register by social network or through our field agents with tablets for free. Once their account is validated, they receive access to publish their products from social network or via a web interface.",
	'l_service_box_2_titre' => 'Enrollment of delivery personnel',
	'l_service_box_2_text' => "Delivery people can check in by social network or through our field agents with tablets for free. We validate their account with an identity document. They are therefore made available to merchants.",
	'l_service_box_3_titre' => 'Customer shopping',
	'l_service_box_3_text' => "Customers search for products by name or merchants by merchant code. A unique address allows a customer to go directly to a merchant's store. Ex: CF63391.",
	'l_service_box_4_titre' => 'Merchant notification',
	'l_service_box_4_text' => "After payment on the merchant's store, he is notified by social network in order to prepare the order for the delivery person closest to his area within the radius of 50 km maximum.",
	'l_service_box_5_titre' => 'Notification of deliverers',
	'l_service_box_5_text' => 'The nearest delivery person is assigned to take charge of the delivery of the order to the customer. Each delivery person is evaluated for their efficiency in the system.',
	'l_service_box_6_titre' => 'Product delivery',
	'l_service_box_6_text' => 'Upon delivery of the products, the customer confirms receipt of the goods by social network. The merchant is therefore paid on his electronic wallet.',

	# --- Why
	'l_why_titre' => 'Why <img src="abusiness/img/logo.png" alt="" class="img-fluid" style="max-height: 128px;"> is easy and very secure ?',
	'l_why_box_1_titre' => 'Secure payment',
	'l_why_box_1_text' => "Your payments are 100% secure: Return of your payment 100% guaranteed if your product is not delivered or is of questionable quality.",
	'l_why_box_2_titre' => 'Your guarantor',
	'l_why_box_2_text' => "ABusiness is your trusted third party and you can claim your money in the event of an incident at 108 street Socrate, AGBALEPEDOGUAN near high school February 2, 13 BP 197 LOME 13, Cel: (228) 92 92 92 85, Lomé-Togo.",
	'l_why_box_3_titre' => 'Your trusted third party',
	'l_why_box_3_text' => "All payments you make are secure with ABusiness until you confirm receipt of the product..",
	'l_why_box_4_titre' => 'Digital payment',
	'l_why_box_4_text' => 'You never pay from hand to hand, you always pay to ABusiness who pays back to the merchant.',
	'l_why_box_5_titre' => 'Dedicated deliverers',
	'l_why_box_5_text' => 'Hundreds of thousands of identified deliverers are made available on the shelves of the closest merchants and deliver to you at home.',
	'l_why_box_6_titre' => "What are you waiting for?",
	'l_why_box_6_text' => "Open your merchant, delivery, or customer account today without further ado. It's always 100% free. No application to install, everything happens by writing to a social network number (ChatBot) <strong><a style=\"color: #000;\" href=\"#services\" class=\"scrollto\">ABusiness</a></strong>, first Market place on social network. ",

	'l_counter_customers' => "Clients",
	'l_counter_sellers' => "Merchants",
	'l_counter_delivery_mens' => "Delivery men",
	'l_counter_transactions' => "Transactions",

	'l_partners' => "THEY TRUST US",
	'l_contact_name' => "Your name",
	'l_contact_email' => "Your email",
	'l_contact_numero' => "Your number example : +228 90 XX XX XX",
	'l_contact_sujet' => "Subject",
	'l_contact_message' => "Message",

	# NOUS CONTACTER
	'l_our_adresse' => '108 Street Socrate, AGBALEPEDOGAN',
	'l_our_email' => 'info@abusiness.store',
	'l_our_telephone' => '+228 92 92 92 85',
	'l_send' => 'Send',
	'l_alert_name' => 'Please enter your name!',
	'l_alert_email' => 'Please enter your email!',
	'l_alert_telephone' => 'Please enter your phone number!',
	'l_alert_sujet' => 'Please fill in the subject!',
	'l_alert_message' => 'Please fill in the message!',

	# FOOTER
	'l_footer_text' => "An e-commerce platform for basic necessities available to users of social networks. Merchants, delivery people and customers use it without installing any apps. They take advantage of the power of instant messaging to access features from a chatbot.",
	'l_useful_link' => 'Useful links',
	'l_footer_adress' => 'Our adress',
	'l_footer_cgu' => "Terms of Service",
	'l_footer_adress_postal' => '13 BP 197 LOME 13<br>Lomé - Togo <br>',
	'l_footer_customer_service' => 'Togo Customer Service',
	'l_email' => 'Email',


	'l_footer_law' => 'All rights reserved',
	'l_footer_create_by' => 'Developed by',
	'l_clin' => 'CLIN',

	'l_cgu_titre' => "General terms & conditions of use",

	# SELECT COUNTRY CHATBOT
	'l_target_country_chatbot' => 'Chatbot number',
	'l_target_country_chatbot_link' => 'The social network link',
	'l_target_link_click' => 'Click here',

	'l_chat_wellcome_msg' => "Bienvenue sur notre assistant automatique. La Plateforme <b>abusiness.store</b> est une plateforme de e-commerce par réseaux sociaux utilisant un assistant automatique interactif pour acheter  payer et livrer les produits de première nécessité aux ménages. 

	L'objectif est de soutenir les petits commerçants à mettre leurs activités sur internet afin d'être résilients même en situation de crise comme celle de la Covid 19 sans installer la moindre application.",

	'l_chat_start_word' => 'AB',

);
