<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "categorieactivite".
 *
 * @property integer $id
 * @property string $libelle
 * @property string $keycategorieactivite
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Activite[] $activites
 */
class Categorieactivite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categorieactivite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelle', 'keycategorieactivite', 'created_at', 'created_by'], 'required'],
            [['status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['libelle'], 'string', 'max' => 255],
            [['keycategorieactivite'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'libelle' => 'Libelle',
            'keycategorieactivite' => 'Keycategorieactivite',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivites()
    {
        return $this->hasMany(Activite::className(), ['idcategorieactivite' => 'id']);
    }
}
