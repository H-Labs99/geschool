<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "anneescolaire".
 *
 * @property integer $id
 * @property string $libelleanneescolaire
 * @property string $keyanneescolaire
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Attribuer[] $attribuers
 * @property Centreannee[] $centreannees
 */
class Anneescolaire extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'anneescolaire';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelleanneescolaire', 'keyanneescolaire', 'created_at', 'create_by'], 'required'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['libelleanneescolaire'], 'string', 'max' => 12],
            [['keyanneescolaire'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'libelleanneescolaire' => 'Libelleanneescolaire',
            'keyanneescolaire' => 'Keyanneescolaire',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribuers()
    {
        return $this->hasMany(Attribuer::className(), ['idannee' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentreannees()
    {
        return $this->hasMany(Centreannee::className(), ['idannee' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDecoupages()
    {
        return $this->hasMany(Decoupage::className(), ['idannee' => 'id']);
    }
}
