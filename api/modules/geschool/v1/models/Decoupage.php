<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "decoupage".
 *
 * @property string $id
 * @property integer $idtypedecoupage
 * @property integer $idannee
 * @property string $keydecoupage
 * @property string $datedebutdecoupage
 * @property string $datefindecoupage
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 * @property integer $idcentre
 */
class Decoupage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'decoupage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idtypedecoupage', 'idannee', 'keydecoupage', 'datedebutdecoupage', 'datefindecoupage', 'created_at', 'create_by', 'idcentre'], 'required'],
            [['idtypedecoupage', 'idannee', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by', 'idcentre'], 'integer'],
            [['datedebutdecoupage', 'datefindecoupage'], 'safe'],
            [['keydecoupage'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idtypedecoupage' => 'Idtypedecoupage',
            'idannee' => 'Idannee',
            'keydecoupage' => 'Keydecoupage',
            'datedebutdecoupage' => 'Datedebutdecoupage',
            'datefindecoupage' => 'Datefindecoupage',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
            'idcentre' => 'Idcentre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnnee()
    {
        return $this->hasOne(Anneescolaire::className(), ['id' => 'idannee']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentre()
    {
        return $this->hasOne(SchoolCenter::className(), ['id_center' => 'idcentre']);
    }
}
