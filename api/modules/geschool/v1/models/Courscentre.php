<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "courscentre".
 *
 * @property integer $id
 * @property integer $idcentre
 * @property integer $idcours
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Courscentre extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courscentre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcentre', 'idcours', 'created_at', 'create_by'], 'required'],
            [['idcentre', 'idcours', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idcentre' => 'Idcentre',
            'idcours' => 'Idcours',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }
}
