<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "specialite".
 *
 * @property integer $id
 * @property string $keyspecialite
 * @property integer $idsection
 * @property string $libellespecialite
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Specialite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'specialite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keyspecialite', 'idsection', 'libellespecialite', 'created_at', 'create_by'], 'required'],
            [['idsection', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['keyspecialite'], 'string', 'max' => 32],
            [['libellespecialite'], 'string', 'max' => 254],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyspecialite' => 'Keyspecialite',
            'idsection' => 'Idsection',
            'libellespecialite' => 'Libellespecialite',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'idsection']);
    }
}
