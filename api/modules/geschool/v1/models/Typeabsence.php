<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "typeabsence".
 *
 * @property integer $id
 * @property string $designationtypeabsence
 * @property string $keytypeabsence
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Absence[] $absences
 */
class Typeabsence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typeabsence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['designationtypeabsence', 'keytypeabsence', 'created_at', 'create_by'], 'required'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['designationtypeabsence'], 'string', 'max' => 50],
            [['keytypeabsence'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'designationtypeabsence' => 'Designationtypeabsence',
            'keytypeabsence' => 'Keytypeabsence',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsences()
    {
        return $this->hasMany(Absence::className(), ['idtypeabsence' => 'id']);
    }
}
