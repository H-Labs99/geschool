<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "activite".
 *
 * @property integer $id
 * @property string $keyactivite
 * @property integer $idcategorieactivite
 * @property integer $idcentre
 * @property integer $idannee
 * @property string $iduserexec
 * @property string $idusersuivi
 * @property string $designationactivite
 * @property string $datedebutactivite
 * @property string $datefinactivite
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Anneescolaire $idannee0
 * @property SchoolCenter $idcentre0
 */
class Activite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keyactivite', 'idcategorieactivite', 'idcentre', 'idannee', 'iduserexec', 'idusersuivi', 'designationactivite', 'datedebutactivite', 'datefinactivite', 'created_at', 'create_by'], 'required'],
            [['idcategorieactivite', 'idcentre', 'idannee', 'iduserexec', 'idusersuivi', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['datedebutactivite', 'datefinactivite'], 'safe'],
            [['keyactivite'], 'string', 'max' => 32],
            [['designationactivite'], 'string', 'max' => 254],
            [['idannee'], 'exist', 'skipOnError' => true, 'targetClass' => Anneescolaire::className(), 'targetAttribute' => ['idannee' => 'id']],
            [['idcentre'], 'exist', 'skipOnError' => true, 'targetClass' => SchoolCenter::className(), 'targetAttribute' => ['idcentre' => 'id_center']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyactivite' => 'Keyactivite',
            'idcategorieactivite' => 'Idcategorieactivite',
            'idcentre' => 'Idcentre',
            'idannee' => 'Idannee',
            'iduserexec' => 'Iduserexec',
            'idusersuivi' => 'Idusersuivi',
            'designationactivite' => 'Designationactivite',
            'datedebutactivite' => 'Datedebutactivite',
            'datefinactivite' => 'Datefinactivite',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdannee0()
    {
        return $this->hasOne(Anneescolaire::className(), ['id' => 'idannee']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcentre0()
    {
        return $this->hasOne(SchoolCenter::className(), ['id_center' => 'idcentre']);
    }
}
