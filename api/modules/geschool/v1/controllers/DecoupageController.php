<?php

namespace api\modules\geschool\v1\controllers;
use api\modules\geschool\v1\models\Decoupage;
use api\modules\geschool\v1\models\Anneescolaire;
use api\modules\geschool\v1\models\Typedecoupage;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class DecoupageController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Decoupage';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev

    public function actionDecoupages()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();

                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_decoupage = Utils::have_accessapi("manage_decoupage", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_decoupage);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        //On vérifie l'année scolaire
                                        if(isset($_POST['annee_scolaire_key']) && $_POST['annee_scolaire_key'] != ''){
                                            $keyAnneescolaire = $_POST['annee_scolaire_key'];
                                            $anneeScolaire = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'keyanneescolaire'=>$keyAnneescolaire,'status' => [1, 2]])->one();

                                            if($anneeScolaire != '' or $anneeScolaire != null){
                                                $query = new Query();
                                                $data = $query
                                                    ->select(['d.*',
                                                        't.libelle','t.keytypedecoupage',
                                                    ])
                                                    ->from('decoupage d')
                                                    ->join('INNER JOIN','typedecoupage t','d.idtypedecoupage = t.id')
                                                    ->where(['d.status'=>1,
                                                        //'idcentre'=>$center->id_center,
                                                        'idannee'=>$anneeScolaire->id])
                                                    ->orderBy(['datedebutdecoupage' => SORT_ASC])
                                                    ->all()
                                                ;

                                                if (sizeof($data) > 0) {
                                                    return [
                                                        'status' => '000',
                                                        'message' => 'succes_get_decoupage',
                                                        'data' => $data
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'empty_decoupage',
                                                    ];
                                                }
                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_not_found',
                                                ];
                                            }
                                        }else{
                                            //Par défaut on prend l'année scolaire en cours du centre
                                            $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();
                                            if($anneeEncours != '' or $anneeEncours != null){

                                                $query = new Query();
                                                $data = $query
                                                    ->select(['d.*',
                                                        't.libelle','t.keytypedecoupage',
                                                    ])
                                                    ->from('decoupage d')
                                                    ->join('INNER JOIN','typedecoupage t','d.idtypedecoupage = t.id')
                                                    ->where(['d.status'=> 1,
                                                        //'idcentre'=>$center->id_center,
                                                        'idannee'=>$anneeEncours->id])
                                                    ->orderBy(['datedebutdecoupage' => SORT_ASC])
                                                    ->all()
                                                ;
                                                if (sizeof($data) > 0) {
                                                    return [
                                                        'status' => '000',
                                                        'message' => 'succes_get_decoupage',
                                                        'data' => $data
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'empty_decoupage',
                                                    ];
                                                }

                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_encour_invalid',
                                                ];
                                            }
                                        }

                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionAdddecoupage()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_decoupage = Utils::have_accessapi("manage_decoupage", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_decoupage);

                                    //On vérifie s'il y a une année scolaire en cours
                                    $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();
                                    if($anneeEncours != '' or $anneeEncours != null){
                                        //on vérifie si l'anné scolaire a été paramétré pour le centre
                                        /*$findCentreAnnee = Centreannee::find()->where(['idannee'=>$anneeEncours->id,'idcentre'=>$center->id_center,'status'=>1])->one();
                                        if($findCentreAnnee != '' or $findCentreAnnee != null){*/
                                        if (isset($_POST['key_type']) && $_POST['key_type'] != '') {
                                            $key_typedecoupage = $_POST['key_type'];
                                            $findType = Typedecoupage::find()->where(['idcentre'=>$center->id_center,'keytypedecoupage'=>$key_typedecoupage,'status' =>1])->one();

                                            if($findType != '' or $findType != null){

                                                if(isset($_POST['datedebut']) && $_POST['datedebut'] != ''){
                                                    $datedebut = $_POST['datedebut'];
                                                    if(Utils::isValidDate($datedebut)){
                                                        //Date début doit être dans la plage de  la date début et fin de l'année scolaire du centre
                                                        if( strtotime($anneeEncours->datedebutannee) <= strtotime($datedebut) && strtotime($anneeEncours->datefinannee) > strtotime($datedebut) ){

                                                            if(isset($_POST['datefin']) && $_POST['datefin'] != ''){
                                                                $datefin = $_POST['datefin'];
                                                                if(Utils::isValidDate($datefin)){
                                                                    //Date fin doit être dans la plage de  la date début et fin de l'année scolaire du centre et supérieure ou égale à la date début découpage
                                                                    if(strtotime($anneeEncours->datefinannee) >= strtotime($datefin) && strtotime($anneeEncours->datedebutannee) < strtotime($datefin) && strtotime($datedebut) <= strtotime($datefin)){
                                                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                                            $operation = $_POST['operation'];

                                                                            if ($operation == 1) { // AJOUT
                                                                                if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                                    //On verifie si la date début ne chevauche pas avec celui d'autre découpage
                                                                                    $datedebut_correct = 1;
                                                                                    $dernier_decoupage = Decoupage::find()->where([
                                                                                        'idcentre'=>$center->id_center,
                                                                                        'idannee'=>$anneeEncours->id,
                                                                                        'status' => 1
                                                                                    ])->orderBy(['datefindecoupage' => SORT_DESC])
                                                                                        ->limit(1)
                                                                                        ->one();
                                                                                    if($dernier_decoupage != '' or $dernier_decoupage != null){
                                                                                        //On compare
                                                                                        if( strtotime($dernier_decoupage->datefindecoupage) > strtotime($datedebut) ){
                                                                                            $datedebut_correct = 0;
                                                                                        }else{
                                                                                            $datedebut_correct = 1;
                                                                                        }
                                                                                    }else{
                                                                                        //Premier decoupage
                                                                                        $datedebut_correct = 1;
                                                                                    }
                                                                                    //Fin Verification

                                                                                    if($datedebut_correct ==1){
                                                                                        $exist = Decoupage::find()->where([
                                                                                            'idtypedecoupage'=>$findType->id,
                                                                                            'idcentre'=>$center->id_center,
                                                                                            'idannee'=>$anneeEncours->id,
                                                                                            'status' => 1
                                                                                        ])->one();
                                                                                        if ($exist == '' or $exist == null) {
                                                                                            $new_record = new Decoupage();
                                                                                            $new_record->idtypedecoupage = $findType->id;
                                                                                            $new_record->idcentre = $center->id_center;
                                                                                            $new_record->idannee = $anneeEncours->id;
                                                                                            $new_record->datedebutdecoupage = $datedebut;
                                                                                            $new_record->datefindecoupage = $datefin;
                                                                                            $new_record->keydecoupage = Yii::$app->security->generateRandomString(32);
                                                                                            $new_record->status = 1;
                                                                                            $new_record->created_at = time();
                                                                                            $new_record->create_by = $user_exist->id;
                                                                                            //print_r($new_record);exit;
                                                                                            if ($new_record->save()) {

                                                                                                //$data = Decoupage::find()->where(['idcentre'=>$center->id_center,'idannee'=>$anneeEncours->id,'status' => [1, 2]])->orderBy(['designationdecoupage' => SORT_DESC])->all();

                                                                                                $query = new Query();
                                                                                                $data = $query
                                                                                                    ->select(['d.*',
                                                                                                        't.libelle','t.keytypedecoupage',
                                                                                                    ])
                                                                                                    ->from('decoupage d')
                                                                                                    ->join('INNER JOIN','typedecoupage t','d.idtypedecoupage = t.id')
                                                                                                    ->where(['d.status'=> 1,
                                                                                                        //'idcentre'=>$center->id_center,
                                                                                                        'idannee'=>$anneeEncours->id])
                                                                                                    ->orderBy(['datedebutdecoupage' => SORT_ASC])
                                                                                                    ->all()
                                                                                                ;
                                                                                                return [
                                                                                                    'status' => '000',
                                                                                                    'message' => 'succes_creation_decoupage',
                                                                                                    'data' => $data,
                                                                                                ];
                                                                                            } else {
                                                                                                return [
                                                                                                    'status' => '002',
                                                                                                    'message' => 'update_error',
                                                                                                ];
                                                                                            }
                                                                                        } else {
                                                                                            return [
                                                                                                'status' => '003',
                                                                                                'message' => 'decoupage_used',
                                                                                            ];
                                                                                        }

                                                                                    }else{
                                                                                        return [
                                                                                            'status' => '001',
                                                                                            'message' => 'datedebut_incorrect',
                                                                                        ];
                                                                                    }


                                                                                } else {
                                                                                    return [
                                                                                        'status' => '002',
                                                                                        'message' => 'Accès non autorisé.',
                                                                                    ];
                                                                                }
                                                                            } else if ($operation == 2) { // MISE A JOUR
                                                                                if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                                    if (isset($_POST['decoupage_key']) && $_POST['decoupage_key'] != '') {
                                                                                        $decoupage_key = $_POST['decoupage_key'];
                                                                                        $old_record = Decoupage::find()->where(['idcentre'=>$center->id_center,'keydecoupage' => $decoupage_key, 'status' => 1])->one();
                                                                                        if ($old_record != '') {
                                                                                            //Pour vérifier s'il existe une autre occurence avec les mêmes informations
                                                                                            $exist = Decoupage::find()->where([
                                                                                                'idtypedecoupage'=>$findType->id,
                                                                                                'idcentre'=>$center->id_center,
                                                                                                'idannee'=>$anneeEncours->id,
                                                                                                'status' => 1
                                                                                            ])->one();

                                                                                            if ($exist == '' or $exist == null) {
                                                                                                $continue = true;
                                                                                            } else {
                                                                                                ($exist->keydecoupage != $decoupage_key) ? $continue = false : $continue = true;
                                                                                            }

                                                                                            if ($continue == true) {
                                                                                                //On verifie si la date début ne chevauche pas avec celui d'autre découpage
                                                                                                $datedebut_correct = 1;
                                                                                                $dernier_decoupage = Decoupage::find()->where([
                                                                                                    'idcentre'=>$center->id_center,
                                                                                                    'idannee'=>$anneeEncours->id,
                                                                                                    'status' => 1
                                                                                                ])
                                                                                                    ->andWhere(['not',['keydecoupage' => $decoupage_key]])
                                                                                                    ->orderBy(['datefindecoupage' => SORT_DESC])
                                                                                                    ->limit(1)
                                                                                                    ->one();
                                                                                                if($dernier_decoupage != '' or $dernier_decoupage != null){
                                                                                                    //On compare
                                                                                                    if( strtotime($dernier_decoupage->datefindecoupage) > strtotime($datedebut) ){
                                                                                                        $datedebut_correct = 0;
                                                                                                    }else{
                                                                                                        $datedebut_correct = 1;
                                                                                                    }
                                                                                                }else{
                                                                                                    //Premier decoupage
                                                                                                    $datedebut_correct = 1;
                                                                                                }
                                                                                                //Fin Verification

                                                                                                if($datedebut_correct ==1){
                                                                                                    $old_record->idtypedecoupage = $findType->id;
                                                                                                    $old_record->idcentre = $center->id_center;
                                                                                                    $old_record->idannee = $anneeEncours->id;
                                                                                                    $old_record->datedebutdecoupage = $datedebut;
                                                                                                    $old_record->datefindecoupage = $datefin;
                                                                                                    $old_record->updated_at = time();
                                                                                                    $old_record->updated_by = $user_exist->id;
                                                                                                    if ($old_record->save()) {

                                                                                                        $query = new Query();
                                                                                                        $data = $query
                                                                                                            ->select(['d.*',
                                                                                                                't.libelle','t.keytypedecoupage',
                                                                                                            ])
                                                                                                            ->from('decoupage d')
                                                                                                            ->join('INNER JOIN','typedecoupage t','d.idtypedecoupage = t.id')
                                                                                                            ->where(['d.status'=> 1,
                                                                                                                //'idcentre'=>$center->id_center,
                                                                                                                'idannee'=>$anneeEncours->id])
                                                                                                            ->orderBy(['datedebutdecoupage' => SORT_ASC])
                                                                                                            ->all()
                                                                                                        ;
                                                                                                        return [
                                                                                                            'status' => '000',
                                                                                                            'message' => 'succes_update_decoupage',
                                                                                                            'data' => $data,
                                                                                                        ];
                                                                                                    } else {
                                                                                                        return [
                                                                                                            'status' => '002',
                                                                                                            'message' => 'update_error',
                                                                                                        ];
                                                                                                    }
                                                                                                }else{
                                                                                                    return [
                                                                                                        'status' => '001',
                                                                                                        'message' => 'datedebut_incorrect',
                                                                                                    ];
                                                                                                }

                                                                                            } else {
                                                                                                return [
                                                                                                    'status' => '003',
                                                                                                    'message' => 'decoupage_used',
                                                                                                ];
                                                                                            }
                                                                                        } else {
                                                                                            return [
                                                                                                'status' => '001',
                                                                                                'message' => 'decoupage_not_found',
                                                                                            ];
                                                                                        }
                                                                                    } else {
                                                                                        return [
                                                                                            'status' => '001',
                                                                                            'message' => 'decoupage_key_empty',
                                                                                        ];
                                                                                    }
                                                                                } else {
                                                                                    return [
                                                                                        'status' => '002',
                                                                                        'message' => 'Accès non autorisé.',
                                                                                    ];
                                                                                }
                                                                            } else {
                                                                                return [
                                                                                    'status' => '002',
                                                                                    'message' => 'Type d\'opération non reconnu.',
                                                                                ];
                                                                            }
                                                                        } else {
                                                                            return [
                                                                                'status' => '002',
                                                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                                                            ];
                                                                        }
                                                                    }else{
                                                                        return [
                                                                            'status' => '001',
                                                                            'message' => 'decoupage_datefin_incorrect',
                                                                        ];
                                                                    }
                                                                }else{
                                                                    return [
                                                                        'status' => '001',
                                                                        'message' => 'decoupage_datefin_invalid',
                                                                    ];
                                                                }
                                                            }else{
                                                                return [
                                                                    'status' => '001',
                                                                    'message' => 'decoupage_datefin_empty',
                                                                ];
                                                            }

                                                        }else{
                                                            return [
                                                                'status' => '001',
                                                                'message' => 'decoupage_datedebut_incorrect',
                                                            ];
                                                        }

                                                    }else{
                                                        return [
                                                            'status' => '001',
                                                            'message' => 'decoupage_datedebut_invalid',
                                                        ];
                                                    }

                                                }else{
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'decoupage_datedebut_empty',
                                                    ];
                                                }


                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'decoupage_type_not_found',
                                                ];
                                            }

                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'decoupage_type_empty',
                                            ];
                                        }

                                        /*}else{
                                            return [
                                                'status' => '002',
                                                'message' => 'decoupage_error_param_centre',
                                            ];
                                        }*/

                                    }else{
                                        return [
                                            'status' => '002',
                                            'message' => 'decoupage_error_annee_scolaire',
                                        ];
                                    }

                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionDecoupage()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_decoupage = Utils::have_accessapi("manage_decoupage", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_decoupage);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        if (isset($_POST['decoupage_key']) && $_POST['decoupage_key'] != '') {
                                            $decoupage_key = $_POST['decoupage_key'];
                                            
                                            //$decoupage_exist = Decoupage::find()->where(['idcentre'=>$center->id_center,'keydecoupage' => $decoupage_key])->one();

                                            $query = new Query();
                                            $decoupage_exist = $query
                                                ->select(['d.*',
                                                    't.libelle','t.keytypedecoupage',
                                                ])
                                                ->from('decoupage d')
                                                ->join('INNER JOIN','typedecoupage t','d.idtypedecoupage = t.id')
                                                ->where(['d.status'=> 1,
                                                    'd.idcentre'=>$center->id_center,
                                                    'keydecoupage'=>$decoupage_key])
                                                ->orderBy(['datedebutdecoupage' => SORT_ASC])
                                                ->one()
                                            ;

                                            if ($decoupage_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'succes_get_decoupage',
                                                    'data' => $decoupage_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'decoupage_not_found.',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'decoupage_key_empty.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    public function actionDeletedecoupage()
    {
        $token = Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_decoupage = Utils::have_accessapi("manage_decoupage", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_decoupage);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();


                                        if (isset($_POST['decoupage_key']) && $_POST['decoupage_key'] != '') {
                                            $decoupage_key = $_POST['decoupage_key'];
                                            $old_record = Decoupage::find()->where(['idcentre'=>$center->id_center,'keydecoupage' => $decoupage_key])->one();
                                            if ($old_record != '') {
                                                $old_record->status = 3;
                                                $old_record->updated_at = time();
                                                $old_record->updated_by = $user_exist->id;
                                                if ($old_record->save()) {
                                                    //$data = Decoupage::find()->where(['idcentre'=>$center->id_center,'idannee'=>$anneeEncours->id,'status' => [1, 2]])->orderBy(['designationdecoupage' => SORT_DESC])->all();

                                                    $query = new Query();
                                                    $data = $query
                                                        ->select(['d.*',
                                                            't.libelle','t.keytypedecoupage',
                                                        ])
                                                        ->from('decoupage d')
                                                        ->join('INNER JOIN','typedecoupage t','d.idtypedecoupage = t.id')
                                                        ->where(['d.status'=> 1,
                                                            //'idcentre'=>$center->id_center,
                                                            'idannee'=>$anneeEncours->id])
                                                        ->orderBy(['datedebutdecoupage' => SORT_ASC])
                                                        ->all()
                                                    ;

                                                    return [
                                                        'status' => '000',
                                                        'message' =>  'delete_decoupage_success',
                                                        'data' => $data,
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'update_error',
                                                    ];
                                                }

                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'decoupage_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'decoupage_key_empty',
                                            ];
                                        }


                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

}
