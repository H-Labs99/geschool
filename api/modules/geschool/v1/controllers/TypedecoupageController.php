<?php

namespace api\modules\geschool\v1\controllers;
use Yii;
use api\modules\geschool\v1\models\Typedecoupage;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class TypedecoupageController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Typedecoupage';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev

    //Cette méthode nous permet de récupérer les types de découpages du système
    public function actionTypedecoupages()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_typedecoupage = Utils::have_accessapi("manage_typedecoupage", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_typedecoupage);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        $data = Typedecoupage::find()->where(['status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();

                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_type_decoupage',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_liste_type_decoupage',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet d'ajouter un type de découpage au système
    public function actionAddtypedecoupage()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_typedecoupage = Utils::have_accessapi("manage_typedecoupage", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_typedecoupage);

                                    if (isset($_POST['libelle']) && $_POST['libelle'] != '') {
                                        $libelle = $_POST['libelle'];
                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                            $operation = $_POST['operation'];
                                            if ($operation == 1) { // AJOUT
                                                if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                    $exist = Typedecoupage::find()->where(['libelle' => $libelle, 'status' => [1, 2]])->one();
                                                    if ($exist == '' or $exist == null) {
                                                        $new_record = new Typedecoupage();
                                                        $new_record->libelle = $libelle;
                                                        $new_record->keytypedecoupage = Yii::$app->security->generateRandomString(32);
                                                        $new_record->status = 1;
                                                        $new_record->created_at = time();
                                                        $new_record->updated_at = '';
                                                        $new_record->create_by = $user_exist->id;
                                                        $new_record->updated_by = '';
                                                        if ($new_record->save()) {
                                                            $data = Typedecoupage::find()->where(['status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();
                                                            return [
                                                                'status' => '000',
                                                                'message' => 'succes_creation_type_decoupage',
                                                                'data' => $data,
                                                            ];
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'update_error',
                                                            ];
                                                        }
                                                    } else {
                                                        return [
                                                            'status' => '003',
                                                            'message' => 'type_decoupage_used',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'Accès non autorisé.',
                                                    ];
                                                }
                                            } else if ($operation == 2) { // MISE A JOUR
                                                if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                    if (isset($_POST['type_decoupage_key']) && $_POST['type_decoupage_key'] != '') {
                                                        $type_decoupage_key = $_POST['type_decoupage_key'];
                                                        $old_record = Typedecoupage::find()->where(['keytypedecoupage' => $type_decoupage_key, 'status' => 1])->one();
                                                        if ($old_record != '') {
                                                            //Pour vérifier s'il existe une autre occurence avec les mêmes informations
                                                            $exist = Typedecoupage::find()->where(['libelle' => $libelle, 'status' => [1, 2]])->one();
                                                            if ($exist == '' or $exist == null) {
                                                                $continue = true;
                                                            } else {
                                                                ($exist->keytypedecoupage != $type_decoupage_key) ? $continue = false : $continue = true;
                                                            }

                                                            if ($continue == true) {
                                                                $old_record->libelle = $libelle;
                                                                $old_record->updated_at = time();
                                                                $old_record->updated_by = $user_exist->id;
                                                                if ($old_record->save()) {
                                                                    $data = Typedecoupage::find()->where(['status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();
                                                                    return [
                                                                        'status' => '000',
                                                                        'message' => 'succes_update_type_decoupage',
                                                                        'data' => $data,
                                                                    ];
                                                                } else {
                                                                    return [
                                                                        'status' => '002',
                                                                        'message' => 'update_error',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '003',
                                                                    'message' => 'type_decoupage_used',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '001',
                                                                'message' => 'type_decoupage_not_found',
                                                            ];
                                                        }
                                                    } else {
                                                        return [
                                                            'status' => '001',
                                                            'message' => 'type_decoupage_key_empty',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'Accès non autorisé.',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '002',
                                                    'message' => 'Type d\'opération non reconnu.',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '002',
                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '001',
                                            'message' => 'type_decoupage_libelle_empty',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '001',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de récuperer un type de découpage
    public function actionTypedecoupage()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_typedecoupage = Utils::have_accessapi("manage_typedecoupage", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_typedecoupage);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        if (isset($_POST['type_decoupage_key']) && $_POST['type_decoupage_key'] != '') {
                                            $type_decoupage_key = $_POST['type_decoupage_key'];
                                            $type_decoupage_exist = Typedecoupage::find()->where(['keytypedecoupage' => $type_decoupage_key])->one();
                                            if ($type_decoupage_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'succes_get_type_decoupage',
                                                    'data' => $type_decoupage_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'type_decoupage_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'type_decoupage_key_empty.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de supprimer d'activer et désactiver  un type de découpage
    public function actionDeletetypedecoupage()
    {
        $token = Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {


                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_typedecoupage = Utils::have_accessapi("manage_typedecoupage", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_typedecoupage);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                            $operation = $_POST['operation'];
                                            if (isset($_POST['type_decoupage_key']) && $_POST['type_decoupage_key'] != '') {
                                                $type_decoupage_key = $_POST['type_decoupage_key'];
                                                $old_record = Typedecoupage::find()->where(['keytypedecoupage' => $type_decoupage_key])->one();
                                                if ($old_record != '') {

                                                    if ($operation == 3) { // SUPPRESSION
                                                        $old_record->status = 3;
                                                        $msg = "delete_type_decoupage_success";
                                                    } else if ($operation == 2) { // DESACTIVATION
                                                        $old_record->status = 2;
                                                        $msg = "desactive_type_decoupage_success";
                                                    } else if ($operation == 1) { // ACTIVATION
                                                        $old_record->status = 1;
                                                        $msg = "active_type_decoupage_success";
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu',
                                                        ];
                                                    }

                                                    $old_record->updated_at = time();
                                                    $old_record->updated_by = $user_exist->id;
                                                    if ($old_record->save()) {
                                                        $data = Typedecoupage::find()->where(['status' => [1,2]])->orderBy(['libelle' => SORT_ASC])->all();
                                                        return [
                                                            'status' => '000',
                                                            'message' => $msg,
                                                            'data' => $data,
                                                        ];
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'update_error',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'type_decoupage_not_found',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'type_decoupage_key_empty',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '002',
                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

}
