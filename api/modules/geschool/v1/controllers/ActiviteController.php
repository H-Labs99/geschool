<?php

namespace api\modules\geschool\v1\controllers;
use api\modules\geschool\v1\models\Activite;
use api\modules\geschool\v1\models\Anneescolaire;
use api\modules\geschool\v1\models\Categorieactivite;
use api\modules\geschool\v1\models\Personnel;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class ActiviteController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Activite';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev

    public function actionCategorieactivites()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_activite = Utils::have_accessapi("manage_activite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_activite);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        $data = Categorieactivite::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();

                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_categorie_activite',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_liste_categorie_activite',
                                            ];
                                        }

                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionActivites()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();

                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_activite = Utils::have_accessapi("manage_activite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_activite);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        //On vérifie l'année scolaire
                                        if(isset($_POST['annee_scolaire_key']) && $_POST['annee_scolaire_key'] != ''){
                                            $keyAnneescolaire = $_POST['annee_scolaire_key'];
                                            $anneeScolaire = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'keyanneescolaire'=>$keyAnneescolaire,'status' => [1, 2]])->one();

                                            if($anneeScolaire != '' or $anneeScolaire != null){
                                                $query = new Query();
                                                $data = $query
                                                    ->select(['a.*',
                                                        'c.libelle','c.keycategorieactivite',
                                                        'iduserexec', 'e.nom nomexec', 'e.prenoms prenomexec',
                                                        'idusersuivi','s.nom nomsuivi', 's.prenoms prenomsuivi'
                                                    ])
                                                    ->from('activite a')
                                                    ->join('INNER JOIN','categorieactivite c','a.idcategorieactivite = c.id')
                                                    ->join('INNER JOIN','user e','a.iduserexec = e.id')
                                                    ->join('INNER JOIN','user s','a.idusersuivi = s.id')
                                                    ->where(['a.status'=> [1, 2],
                                                        //'idcentre'=>$center->id_center,
                                                        'idannee'=>$anneeScolaire->id])
                                                    ->orderBy(['datedebutactivite' => SORT_ASC])
                                                    ->all()
                                                ;

                                                if (sizeof($data) > 0) {
                                                    return [
                                                        'status' => '000',
                                                        'message' => 'succes_get_activite',
                                                        'data' => $data
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'empty_activite',
                                                    ];
                                                }
                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_not_found',
                                                ];
                                            }
                                        }else{
                                            //Par défaut on prend l'année scolaire en cours du centre
                                            $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();
                                            if($anneeEncours != '' or $anneeEncours != null){

                                                $query = new Query();
                                                $data = $query
                                                    ->select(['a.*',
                                                        'c.libelle','c.keycategorieactivite',
                                                        'iduserexec', 'e.nom nomexec', 'e.prenoms prenomexec',
                                                        'idusersuivi','s.nom nomsuivi', 's.prenoms prenomsuivi'
                                                    ])
                                                    ->from('activite a')
                                                    ->join('INNER JOIN','categorieactivite c','a.idcategorieactivite = c.id')
                                                    ->join('INNER JOIN','user e','a.iduserexec = e.id')
                                                    ->join('INNER JOIN','user s','a.idusersuivi = s.id')
                                                    ->where(['a.status'=> [1, 2],
                                                        //'idcentre'=>$center->id_center,
                                                        'idannee'=>$anneeEncours->id])
                                                    ->orderBy(['datedebutactivite' => SORT_ASC])
                                                    ->all()
                                                ;
                                                if (sizeof($data) > 0) {
                                                    return [
                                                        'status' => '000',
                                                        'message' => 'succes_get_activite',
                                                        'data' => $data
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'empty_activite',
                                                    ];
                                                }

                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_encour_invalid',
                                                ];
                                            }
                                        }


                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionAddactivite()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_activite = Utils::have_accessapi("manage_activite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_activite);

                                    //On vérifie s'il y a une année scolaire en cours
                                    $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();
                                    if($anneeEncours != '' or $anneeEncours != null){
                                        //on vérifie si l'anné scolaire a été paramétré pour le centre
                                        /*$findCentreAnnee = Centreannee::find()->where(['idannee'=>$anneeEncours->id,'idcentre'=>$center->id_center,'status'=>1])->one();
                                        if($findCentreAnnee != '' or $findCentreAnnee != null){*/
                                        if (isset($_POST['idcategorie']) && $_POST['idcategorie'] != '') {
                                            $idcategorieactivite = $_POST['idcategorie'];
                                            $findCategorie = Categorieactivite::findOne($idcategorieactivite);
                                            if($findCategorie != '' or $findCategorie != null){
                                                if(isset($_POST['designation']) && $_POST['designation'] != ''){
                                                    $designationactivite = $_POST['designation'];
                                                    if(isset($_POST['iduserexec']) && $_POST['iduserexec'] != ''){
                                                        $iduserexec = $_POST['iduserexec'];
                                                        $findExecutant = Personnel::findOne($iduserexec);
                                                        if($findExecutant != '' or $findExecutant != null){
                                                            if(isset($_POST['idusersuivi']) && $_POST['idusersuivi'] != ''){
                                                                $idusersuivi = $_POST['idusersuivi'];
                                                                $findSuivi = Personnel::findOne($idusersuivi);
                                                                if($findSuivi != '' or $findSuivi != null){
                                                                    if(isset($_POST['datedebut']) && $_POST['datedebut'] != ''){
                                                                        $datedebut = $_POST['datedebut'];
                                                                        if(Utils::isValidDate($datedebut)){
                                                                            //Date début doit être supérieure à la date début année scolaire du centre
                                                                            if( strtotime($anneeEncours->datedebutannee) <= strtotime($datedebut)  ){
                                                                                if(isset($_POST['datefin']) && $_POST['datefin'] != ''){
                                                                                    $datefin = $_POST['datefin'];
                                                                                    if(Utils::isValidDate($datefin)){
                                                                                        //Date fin doit être inférieure ou égale à la date fin année scolaire du centre
                                                                                        if(strtotime($anneeEncours->datefinannee) >= strtotime($datefin)){
                                                                                            if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                                                                $operation = $_POST['operation'];
                                                                                                if ($operation == 1) { // AJOUT
                                                                                                    if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                                                        $exist = Activite::find()->where([
                                                                                                            'designationactivite' => $designationactivite,
                                                                                                            'idcategorieactivite'=>$idcategorieactivite,
                                                                                                            'idcentre'=>$center->id_center,
                                                                                                            'idannee'=>$anneeEncours->id,
                                                                                                            'status' => [1, 2]
                                                                                                        ])->one();
                                                                                                        if ($exist == '' or $exist == null) {
                                                                                                            $new_record = new Activite();
                                                                                                            $new_record->designationactivite = $designationactivite;
                                                                                                            $new_record->idcategorieactivite = $idcategorieactivite;
                                                                                                            $new_record->idcentre = $center->id_center;
                                                                                                            $new_record->idannee = $anneeEncours->id;
                                                                                                            $new_record->iduserexec = $iduserexec;
                                                                                                            $new_record->idusersuivi = $idusersuivi;
                                                                                                            $new_record->datedebutactivite = $datedebut;
                                                                                                            $new_record->datefinactivite = $datefin;
                                                                                                            $new_record->keyactivite = Yii::$app->security->generateRandomString(32);
                                                                                                            $new_record->status = 1;
                                                                                                            $new_record->created_at = time();
                                                                                                            $new_record->updated_at = '';
                                                                                                            $new_record->create_by = $user_exist->id;
                                                                                                            $new_record->updated_by = '';
                                                                                                            //print_r($new_record);exit;
                                                                                                            if ($new_record->save()) {

                                                                                                                //$data = Activite::find()->where(['idcentre'=>$center->id_center,'idannee'=>$anneeEncours->id,'status' => [1, 2]])->orderBy(['designationactivite' => SORT_DESC])->all();

                                                                                                                $query = new Query();
                                                                                                                $data = $query
                                                                                                                    ->select(['a.*',
                                                                                                                        'c.libelle','c.keycategorieactivite',
                                                                                                                        'iduserexec', 'e.nom nomexec', 'e.prenoms prenomexec',
                                                                                                                        'idusersuivi','s.nom nomsuivi', 's.prenoms prenomsuivi'
                                                                                                                    ])
                                                                                                                    ->from('activite a')
                                                                                                                    ->join('INNER JOIN','categorieactivite c','a.idcategorieactivite = c.id')
                                                                                                                    ->join('INNER JOIN','user e','a.iduserexec = e.id')
                                                                                                                    ->join('INNER JOIN','user s','a.idusersuivi = s.id')
                                                                                                                    ->where(['a.status'=> [1, 2],
                                                                                                                        //'idcentre'=>$center->id_center,
                                                                                                                        'idannee'=>$anneeEncours->id])
                                                                                                                    ->orderBy(['datedebutactivite' => SORT_ASC])
                                                                                                                    ->all()
                                                                                                                ;
                                                                                                                return [
                                                                                                                    'status' => '000',
                                                                                                                    'message' => 'succes_creation_activite',
                                                                                                                    'data' => $data,
                                                                                                                ];
                                                                                                            } else {
                                                                                                                return [
                                                                                                                    'status' => '002',
                                                                                                                    'message' => 'update_error',
                                                                                                                ];
                                                                                                            }
                                                                                                        } else {
                                                                                                            return [
                                                                                                                'status' => '003',
                                                                                                                'message' => 'activite_used',
                                                                                                            ];
                                                                                                        }
                                                                                                    } else {
                                                                                                        return [
                                                                                                            'status' => '002',
                                                                                                            'message' => 'Accès non autorisé.',
                                                                                                        ];
                                                                                                    }
                                                                                                } else if ($operation == 2) { // MISE A JOUR
                                                                                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                                                        if (isset($_POST['activite_key']) && $_POST['activite_key'] != '') {
                                                                                                            $activite_key = $_POST['activite_key'];
                                                                                                            $old_record = Activite::find()->where(['idcentre'=>$center->id_center,'keyactivite' => $activite_key, 'status' => [1,2]])->one();
                                                                                                            if ($old_record != '') {
                                                                                                                //Pour vérifier s'il existe une autre occurence avec les mêmes informations
                                                                                                                $exist = Activite::find()->where([
                                                                                                                    'designationactivite' => $designationactivite,
                                                                                                                    'idcategorieactivite'=>$idcategorieactivite,
                                                                                                                    'idcentre'=>$center->id_center,
                                                                                                                    'idannee'=>$anneeEncours->id,
                                                                                                                    'status' => [1, 2]
                                                                                                                ])->one();

                                                                                                                if ($exist == '' or $exist == null) {
                                                                                                                    $continue = true;
                                                                                                                } else {
                                                                                                                    ($exist->keyactivite != $activite_key) ? $continue = false : $continue = true;
                                                                                                                }

                                                                                                                if ($continue == true) {
                                                                                                                    $old_record->designationactivite = $designationactivite;
                                                                                                                    $old_record->idcategorieactivite = $idcategorieactivite;
                                                                                                                    $old_record->idcentre = $center->id_center;
                                                                                                                    $old_record->idannee = $anneeEncours->id;
                                                                                                                    $old_record->iduserexec = $iduserexec;
                                                                                                                    $old_record->idusersuivi = $idusersuivi;
                                                                                                                    $old_record->datedebutactivite = $datedebut;
                                                                                                                    $old_record->datefinactivite = $datefin;
                                                                                                                    $old_record->updated_at = time();
                                                                                                                    $old_record->updated_by = $user_exist->id;
                                                                                                                    if ($old_record->save()) {

                                                                                                                        $query = new Query();
                                                                                                                        $data = $query
                                                                                                                            ->select(['a.*',
                                                                                                                                'c.libelle','c.keycategorieactivite',
                                                                                                                                'iduserexec', 'e.nom nomexec', 'e.prenoms prenomexec',
                                                                                                                                'idusersuivi','s.nom nomsuivi', 's.prenoms prenomsuivi'
                                                                                                                            ])
                                                                                                                            ->from('activite a')
                                                                                                                            ->join('INNER JOIN','categorieactivite c','a.idcategorieactivite = c.id')
                                                                                                                            ->join('INNER JOIN','user e','a.iduserexec = e.id')
                                                                                                                            ->join('INNER JOIN','user s','a.idusersuivi = s.id')
                                                                                                                            ->where(['a.status'=> [1, 2],
                                                                                                                                //'idcentre'=>$center->id_center,
                                                                                                                                'idannee'=>$anneeEncours->id])
                                                                                                                            ->orderBy(['datedebutactivite' => SORT_ASC])
                                                                                                                            ->all()
                                                                                                                        ;
                                                                                                                        return [
                                                                                                                            'status' => '000',
                                                                                                                            'message' => 'succes_update_activite',
                                                                                                                            'data' => $data,
                                                                                                                        ];
                                                                                                                    } else {
                                                                                                                        return [
                                                                                                                            'status' => '002',
                                                                                                                            'message' => 'update_error',
                                                                                                                        ];
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    return [
                                                                                                                        'status' => '003',
                                                                                                                        'message' => 'activite_used',
                                                                                                                    ];
                                                                                                                }
                                                                                                            } else {
                                                                                                                return [
                                                                                                                    'status' => '001',
                                                                                                                    'message' => 'activite_not_found',
                                                                                                                ];
                                                                                                            }
                                                                                                        } else {
                                                                                                            return [
                                                                                                                'status' => '001',
                                                                                                                'message' => 'activite_key ne doit pas être vide.',
                                                                                                            ];
                                                                                                        }
                                                                                                    } else {
                                                                                                        return [
                                                                                                            'status' => '002',
                                                                                                            'message' => 'Accès non autorisé.',
                                                                                                        ];
                                                                                                    }
                                                                                                } else {
                                                                                                    return [
                                                                                                        'status' => '002',
                                                                                                        'message' => 'Type d\'opération non reconnu.',
                                                                                                    ];
                                                                                                }
                                                                                            } else {
                                                                                                return [
                                                                                                    'status' => '002',
                                                                                                    'message' => 'Le type d\'opération ne doit pas être vide.',
                                                                                                ];
                                                                                            }
                                                                                        }else{
                                                                                            return [
                                                                                                'status' => '001',
                                                                                                'message' => 'activite_datefin_incorrect',
                                                                                            ];
                                                                                        }
                                                                                    }else{
                                                                                        return [
                                                                                            'status' => '001',
                                                                                            'message' => 'activite_datefin_invalid',
                                                                                        ];
                                                                                    }
                                                                                }else{
                                                                                    return [
                                                                                        'status' => '001',
                                                                                        'message' => 'activite_datefin_empty',
                                                                                    ];
                                                                                }
                                                                            }else{
                                                                                return [
                                                                                    'status' => '001',
                                                                                    'message' => 'activite_datedebut_incorrect',
                                                                                ];
                                                                            }

                                                                        }else{
                                                                            return [
                                                                                'status' => '001',
                                                                                'message' => 'activite_datedebut_invalid',
                                                                            ];
                                                                        }

                                                                    }else{
                                                                        return [
                                                                            'status' => '001',
                                                                            'message' => 'activite_datedebut_empty',
                                                                        ];
                                                                    }
                                                                }else{
                                                                    return [
                                                                        'status' => '001',
                                                                        'message' => 'Ce personnel n\'existe.',
                                                                    ];
                                                                }

                                                            }else{
                                                                return [
                                                                    'status' => '001',
                                                                    'message' => 'activite_suivi_empty',
                                                                ];
                                                            }
                                                        }else{
                                                            return [
                                                                'status' => '001',
                                                                'message' => 'Ce personnel n\'existe.',
                                                            ];
                                                        }


                                                    }else{
                                                        return [
                                                            'status' => '001',
                                                            'message' => 'activite_executant_empty',
                                                        ];
                                                    }

                                                }else{
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'activite_designation_empty',
                                                    ];
                                                }

                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'activite_categorie_not_found',
                                                ];
                                            }

                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'activite_categorie_empty',
                                            ];
                                        }

                                        /*}else{
                                            return [
                                                'status' => '002',
                                                'message' => 'activite_error_param_centre',
                                            ];
                                        }*/

                                    }else{
                                        return [
                                            'status' => '002',
                                            'message' => 'activite_error_annee_scolaire',
                                        ];
                                    }

                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionActivite()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_activite = Utils::have_accessapi("manage_activite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_activite);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        if (isset($_POST['activite_key']) && $_POST['activite_key'] != '') {
                                            $activite_key = $_POST['activite_key'];
                                            
                                            //$activite_exist = Activite::find()->where(['idcentre'=>$center->id_center,'keyactivite' => $activite_key])->one();

                                            $query = new Query();
                                            $activite_exist = $query
                                                ->select(['a.*',
                                                    'c.libelle','c.keycategorieactivite',
                                                    'iduserexec', 'e.nom nomexec', 'e.prenoms prenomexec',
                                                    'idusersuivi','s.nom nomsuivi', 's.prenoms prenomsuivi'
                                                ])
                                                ->from('activite a')
                                                ->join('INNER JOIN','categorieactivite c','a.idcategorieactivite = c.id')
                                                ->join('INNER JOIN','user e','a.iduserexec = e.id')
                                                ->join('INNER JOIN','user s','a.idusersuivi = s.id')
                                                ->where(['a.status'=> [1, 2],
                                                    'a.idcentre'=>$center->id_center,
                                                    'keyactivite'=>$activite_key])
                                                ->orderBy(['datedebutactivite' => SORT_ASC])
                                                ->one()
                                            ;

                                            if ($activite_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'succes_get_jour_ferie',
                                                    'data' => $activite_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'activite_not_found.',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'activite_key_empty.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    public function actionDeleteactivite()
    {
        $token = Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_activite = Utils::have_accessapi("manage_activite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_activite);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();

                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                            $operation = $_POST['operation'];
                                            if (isset($_POST['activite_key']) && $_POST['activite_key'] != '') {
                                                $activite_key = $_POST['activite_key'];
                                                $old_record = Activite::find()->where(['idcentre'=>$center->id_center,'keyactivite' => $activite_key])->one();
                                                if ($old_record != '') {

                                                    if ($operation == 3) { // SUPPRESSION
                                                        $old_record->status = 3;
                                                        $msg = "delete_activite_success";
                                                    } else if ($operation == 2) { // DESACTIVATION
                                                        $old_record->status = 2;
                                                        $msg = "desactive_activite_success";
                                                    } else if ($operation == 1) { // ACTIVATION
                                                        $old_record->status = 1;
                                                        $msg = "active_activite_success";
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu',
                                                        ];
                                                    }

                                                    $old_record->updated_at = time();
                                                    $old_record->updated_by = $user_exist->id;
                                                    if ($old_record->save()) {
                                                        //$data = Activite::find()->where(['idcentre'=>$center->id_center,'idannee'=>$anneeEncours->id,'status' => [1, 2]])->orderBy(['designationactivite' => SORT_DESC])->all();

                                                        $query = new Query();
                                                        $data = $query
                                                            ->select(['a.*',
                                                                'c.libelle','c.keycategorieactivite',
                                                                'iduserexec', 'e.nom nomexec', 'e.prenoms prenomexec',
                                                                'idusersuivi','s.nom nomsuivi', 's.prenoms prenomsuivi'
                                                            ])
                                                            ->from('activite a')
                                                            ->join('INNER JOIN','categorieactivite c','a.idcategorieactivite = c.id')
                                                            ->join('INNER JOIN','user e','a.iduserexec = e.id')
                                                            ->join('INNER JOIN','user s','a.idusersuivi = s.id')
                                                            ->where(['a.status'=> [1, 2],
                                                                //'idcentre'=>$center->id_center,
                                                                'idannee'=>$anneeEncours->id])
                                                            ->orderBy(['datedebutactivite' => SORT_ASC])
                                                            ->all()
                                                        ;

                                                        return [
                                                            'status' => '000',
                                                            'message' =>  $msg,
                                                            'data' => $data,
                                                        ];
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'update_error',
                                                        ];
                                                    }

                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'activite_not_found',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'activite_key ne doit pas être vide.',
                                                ];
                                            }

                                        } else {
                                            return [
                                                'status' => '002',
                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

}
