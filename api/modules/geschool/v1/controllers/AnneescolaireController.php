<?php

namespace api\modules\geschool\v1\controllers;

use api\modules\geschool\v1\models\Anneescolaire;
use api\modules\geschool\v1\models\Jourferie;
use Yii;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class AnneescolaireController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Anneescolaire';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev


    //Cette méthode nous permet de récupérer les années scolaires d'un centre
    public function actionAnneescolaires()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_anneescolaire = Utils::have_accessapi("manage_anneescolaire", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_anneescolaire);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        $data = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'status' => [1, 2]])->orderBy(['libelleanneescolaire' => SORT_ASC])->all();

                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_anneescolaire',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_anneescolaire',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet d'ajouter une année scolaire à un centre
    public function actionAddanneescolaire()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_anneescolaire = Utils::have_accessapi("manage_anneescolaire", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_anneescolaire);

                                    if (isset($_POST['datedebut']) && $_POST['datedebut'] != '') {
                                        $date_jour = date('Y-m-d H:i:s');
                                        $datedebut = $_POST['datedebut'];

                                        //On vérifie la validité de la date début > date du jour
                                        if (strtotime($datedebut) > strtotime($date_jour)) {
                                            if (isset($_POST['datefin']) && $_POST['datefin'] != '') {
                                                $datefin = $_POST['datefin'];
                                                //On vérifie la validité de la date fin > date début
                                                if (strtotime($datefin) > strtotime($datedebut)) {
                                                    $anneedebut = substr($datedebut, 0, 4);
                                                    $anneefin = substr($datefin, 0, 4);
                                                    $libelle = $anneedebut . '-' . $anneefin;
                                                    if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                        $operation = $_POST['operation'];

                                                        if ($operation == 1) { // AJOUT
                                                            if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
                                                                $anneeEncours = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'status' => 1])->one();
                                                                //On vérifie s'il y a une année en cours pour le centre
                                                                if ($anneeEncours == '' or $anneeEncours == null) {

                                                                    $exist = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'libelleanneescolaire' => $libelle, 'status' => [1, 2]])->one();
                                                                    //On vérifie s'il y a déjà une année scolaire pour la période  pour le centre
                                                                    if ($exist == '' or $exist == null) {
                                                                        $new_record = new Anneescolaire();
                                                                        $new_record->datedebutannee = $datedebut;
                                                                        $new_record->datefinannee = $datefin;
                                                                        $new_record->libelleanneescolaire = $libelle;
                                                                        $new_record->keyanneescolaire = Yii::$app->security->generateRandomString(32);
                                                                        $new_record->status = 1;
                                                                        $new_record->idcentre = $center->id_center;
                                                                        $new_record->created_at = time();
                                                                        $new_record->updated_at = '';
                                                                        $new_record->create_by = $user_exist->id;
                                                                        $new_record->updated_by = '';
                                                                        if ($new_record->save()) {
                                                                            $data = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'status' => [1, 2]])->orderBy(['libelleanneescolaire' => SORT_ASC])->all();
                                                                            return [
                                                                                'status' => '000',
                                                                                'message' => 'succes_creation_annee_scolaire',
                                                                                'data' => $data,
                                                                            ];
                                                                        } else {
                                                                            return [
                                                                                'status' => '002',
                                                                                'message' => 'update_error',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '003',
                                                                            'message' => 'annee_scolaire_used',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '001',
                                                                        'message' => 'annee_scolaire_deja_en_cour',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '002',
                                                                    'message' => 'Accès non autorisé.',
                                                                ];
                                                            }
                                                        } elseif ($operation == 2) { //Modification

                                                            if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                if (isset($_POST['annee_scolaire_key']) && $_POST['annee_scolaire_key'] != '') {
                                                                    $annee_scoliare_key = $_POST['annee_scolaire_key'];

                                                                    $old_record = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'keyanneescolaire' => $annee_scoliare_key, 'status' => 1])->one();
                                                                    if ($old_record !== null) {
                                                                        $old_record->datedebutannee = $datedebut;
                                                                        $old_record->datefinannee = $datefin;
                                                                        $old_record->libelleanneescolaire = $libelle;
                                                                        $old_record->updated_at = time();
                                                                        $old_record->updated_by = $user_exist->id;
                                                                        if ($old_record->save()) {

                                                                            $data = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'status' => [1, 2]])->orderBy(['libelleanneescolaire' => SORT_ASC])->all();
                                                                            return [
                                                                                'status' => '000',
                                                                                'message' => 'succes_update_annee_scolaire',
                                                                                'data' => $data,
                                                                            ];
                                                                        } else {
                                                                            return [
                                                                                'status' => '002',
                                                                                'message' => 'update_error',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '001',
                                                                            'message' => 'annee_scolaire_not_found',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '001',
                                                                        'message' => 'annee_scolaire_key_empty',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '002',
                                                                    'message' => 'Accès non autorisé.',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Type d\'opération non reconnu.',
                                                            ];
                                                        }
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Le type d\'opération ne doit pas être vide.',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'annee_scolaire_date_fin_inf_date_debut',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_scolaire_date_fin_empty',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'annee_scolaire_date_debut_inf_date_jour',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '001',
                                            'message' => 'annee_scolaire_date_debut_empty',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de lire l'année scolaire en cours d'un centre
    public function actionAnneescolaire()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_anneescolaire = Utils::have_accessapi("manage_anneescolaire", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_anneescolaire);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        // if (isset($_POST['annee_scolaire_key']) && $_POST['annee_scolaire_key'] != '') {
                                        // $annee_scolaire_key = $_POST['annee_scolaire_key'];
                                        $annee_scolaire_exist = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'status' => 1])->one();
                                        if ($annee_scolaire_exist != '') {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_anneescolaire',
                                                'data' => $annee_scolaire_exist,
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'annee_scolaire_not_found',
                                            ];
                                        }
                                        /* } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'annee_scolaire_key ne doit pas être vide.',
                                            ];
                                        }*/
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de supprimer une année scolaire d'un centre
    public function actionDeleteanneescolaire()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {


                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_anneescolaire = Utils::have_accessapi("manage_anneescolaire", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_anneescolaire);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['annee_scolaire_key']) && $_POST['annee_scolaire_key'] != '') {
                                            $annee_scolaire_key = $_POST['annee_scolaire_key'];

                                            $old_record = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'keyanneescolaire' => $annee_scolaire_key])->one();
                                            if ($old_record != '') {
                                                //On verifie si cette année scolaire n'est pas déjà réferencé
                                                /*$findUsage = Jourferie::find()->where(['idannee'=>$old_record->id,'status'=>1])->all();
                                            if ($findUsage == '' or $findUsage == null) {*/
                                                $old_record->status = 3;
                                                $old_record->updated_at = time();
                                                $old_record->updated_by = $user_exist->id;
                                                if ($old_record->save()) {
                                                    $data = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'status' => [1, 2]])->orderBy(['libelleanneescolaire' => SORT_ASC])->all();
                                                    return [
                                                        'status' => '000',
                                                        'message' => 'delete_annee_scolaire_success',
                                                        'data' => $data,
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'update_error',
                                                    ];
                                                }
                                                /*}else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_scolaire_already_used',
                                                ];
                                            }*/
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_scolaire_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'annee_scolaire_key_empty',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de clôturer une année scolaire d'un centre
    public function actionClotureranneescolaire()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {


                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_anneescolaire = Utils::have_accessapi("manage_anneescolaire", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_anneescolaire);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['annee_scolaire_key']) && $_POST['annee_scolaire_key'] != '') {
                                            $annee_scolaire_key = $_POST['annee_scolaire_key'];
                                            $old_record = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'keyanneescolaire' => $annee_scolaire_key])->one();

                                            if ($old_record != '') {

                                                $old_record->status = 2;
                                                $old_record->updated_at = time();
                                                $old_record->updated_by = $user_exist->id;
                                                if ($old_record->save()) {
                                                    $data = Anneescolaire::find()->where(['idcentre' => $center->id_center, 'status' => [1, 2]])->orderBy(['libelleanneescolaire' => SORT_ASC])->all();
                                                    return [
                                                        'status' => '000',
                                                        'message' => 'cloturer_annee_scolaire_success',
                                                        'data' => $data,
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'update_error',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_scolaire_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'annee_scolaire_key_empty',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }
}
