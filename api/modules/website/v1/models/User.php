<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $idP
 * @property integer $idProvenance
 * @property string $nom
 * @property string $prenoms
 * @property integer $id_user_profil
 * @property string $photo
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $raison_desactivation
 *
 * @property ActeurUser[] $acteurUsers
 * @property CommandeMarchand[] $commandeMarchands
 * @property HistoriqueApplication[] $historiqueApplications
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idP', 'id_user_profil', 'username', 'auth_key', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['idP', 'idProvenance', 'id_user_profil', 'role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['raison_desactivation'], 'string'],
            [['nom', 'prenoms', 'photo', 'username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idP' => 'Id P',
            'idProvenance' => 'Id Provenance',
            'nom' => 'Nom',
            'prenoms' => 'Prenoms',
            'id_user_profil' => 'Id User Profil',
            'photo' => 'Photo',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'raison_desactivation' => 'Raison Desactivation',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActeurUsers()
    {
        return $this->hasMany(ActeurUser::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommandeMarchands()
    {
        return $this->hasMany(CommandeMarchand::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistoriqueApplications()
    {
        return $this->hasMany(HistoriqueApplication::className(), ['idUser' => 'id']);
    }
}
