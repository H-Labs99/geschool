<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "email_customer".
 *
 * @property integer $id_email_customer
 * @property string $nom_customer
 * @property string $phone_customer
 * @property string $email_customer
 * @property string $sujet_contact
 * @property string $message_contact
 * @property string $date_send
 */
class EmailCustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom_customer', 'phone_customer', 'email_customer', 'sujet_contact', 'message_contact', 'date_send'], 'required'],
            [['message_contact'], 'string'],
            [['date_send'], 'safe'],
            [['nom_customer', 'sujet_contact'], 'string', 'max' => 255],
            [['phone_customer'], 'string', 'max' => 50],
            [['email_customer'], 'string', 'max' => 70],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_email_customer' => 'Id Email Customer',
            'nom_customer' => 'Nom Customer',
            'phone_customer' => 'Phone Customer',
            'email_customer' => 'Email Customer',
            'sujet_contact' => 'Sujet Contact',
            'message_contact' => 'Message Contact',
            'date_send' => 'Date Send',
        ];
    }
}
