<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "package_info".
 *
 * @property integer $idPackageInfo
 * @property string $key_package_info
 * @property integer $idProduit
 * @property integer $idPackage
 * @property double $qte
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 *
 * @property Package $idPackage0
 * @property Produits $idProduit0
 */
class PackageInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_package_info', 'idProduit', 'idPackage', 'qte', 'etat', 'created_by'], 'required'],
            [['idProduit', 'idPackage', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['qte'], 'number'],
            [['date_create', 'date_update'], 'safe'],
            [['key_package_info'], 'string', 'max' => 255],
            [['idPackage'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['idPackage' => 'idPackage']],
            [['idProduit'], 'exist', 'skipOnError' => true, 'targetClass' => Produits::className(), 'targetAttribute' => ['idProduit' => 'idProduit']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPackageInfo' => 'Id Package Info',
            'key_package_info' => 'Key Package Info',
            'idProduit' => 'Id Produit',
            'idPackage' => 'Id Package',
            'qte' => 'Qte',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPackage0()
    {
        return $this->hasOne(Package::className(), ['idPackage' => 'idPackage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduit0()
    {
        return $this->hasOne(Produits::className(), ['idProduit' => 'idProduit']);
    }
}
