<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "package".
 *
 * @property integer $idPackage
 * @property string $key_package
 * @property string $denomination
 * @property integer $prix_package
 * @property string $photo_package
 * @property integer $position_menu_ussd
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 *
 * @property PackageInfo[] $packageInfos
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_package', 'prix_package', 'photo_package', 'position_menu_ussd', 'etat', 'created_by'], 'required'],
            [['prix_package', 'position_menu_ussd', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['key_package', 'denomination', 'photo_package'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPackage' => 'Id Package',
            'key_package' => 'Key Package',
            'denomination' => 'Denomination',
            'prix_package' => 'Prix Package',
            'photo_package' => 'Photo Package',
            'position_menu_ussd' => 'Position Menu Ussd',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackageInfos()
    {
        return $this->hasMany(PackageInfo::className(), ['idPackage' => 'idPackage']);
    }
}
