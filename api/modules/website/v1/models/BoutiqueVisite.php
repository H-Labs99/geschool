<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "boutique_visite".
 *
 * @property integer $idboutique_visite
 * @property integer $id_acteur
 * @property integer $id_user
 * @property integer $id_transaction
 * @property string $date_visite
 * @property string $date_create
 */
class BoutiqueVisite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'boutique_visite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_acteur', 'id_user', 'id_transaction', 'date_visite', 'date_create'], 'required'],
            [['id_acteur', 'id_user', 'id_transaction'], 'integer'],
            [['date_visite', 'date_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idboutique_visite' => 'Idboutique Visite',
            'id_acteur' => 'Id Acteur',
            'id_user' => 'Id User',
            'id_transaction' => 'Id Transaction',
            'date_visite' => 'Date Visite',
            'date_create' => 'Date Create',
        ];
    }
}
