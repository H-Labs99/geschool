<?php
namespace api\modules\website\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use yii\helpers\Json;
use api\modules\website\v1\controllers\Common;
use api\modules\website\v1\models\CommandeMarchand;
use api\modules\website\v1\models\CommandePayment;

class PaiementController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\TypeProduit';  

    public $api_passe = 'PL7RBSPzoiz42plJgJ6sYbRHOFle1TrJGW4MHrTo8Cf8et';
	
	public function actionGet_qrcode() {
		$return_id=0;
		date_default_timezone_set('UTC');
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
		}else{
			$information=file_get_contents('php://input');	
			$all_post=json_decode($information, TRUE);
		}
			
		if(isset($all_post["access_token"]) && $this->api_passe==$all_post["access_token"]){
			
			if(isset($all_post["commande_key"])){		
				
				if(trim($all_post["commande_key"])){	
				
					//recuperer le token de la commande
					$find_command = CommandeMarchand::find()->where(['commande_key'=>$commande_key,'etat'=>0])->one();
					if($find_command!==null){
						$id_commande_marchand=$find_command->id_commande_marchand;
						$id_user=$find_command->id_user;
						
						$continue_process=true;
						//verifier si ce paiement a ete deja enregistrer
						$new_paiement = CommandePayment::find()->where(['id_commande_marchand'=>$id_commande_marchand,'id_user'=>$id_user,etat=>[0,1,2]])->one();
						
						if($new_paiement==null || $new_paiement->etat==0){
							
							if($new_paiement==null){
								//enregistrer le paiement dans la base local
								  $new_paiement=new CommandePayment;
								  $new_paiement->paiement_key=Yii::$app->security->generateRandomString(32);
								  $new_paiement->num_paiement=$id_user.time();
								  $new_paiement->id_commande_marchand=$id_commande_marchand;
								  $new_paiement->id_user=$id_user;
								  $new_paiement->type_paiement=1;
								  $new_paiement->etat=0;
								  $new_paiement->date_create=date("Y-m-d H:i:s");
								  $new_paiement->date_update=date("Y-m-d H:i:s");
								  $new_paiement->save();
							}else{
								  $new_paiement->date_update=date("Y-m-d H:i:s");		
								  $new_paiement->save();
						    }
						  
						}else{
							if($new_paiement->etat==2){
								$continue_process=false;
							}else{
								$new_paiement->date_update=date("Y-m-d H:i:s");		
								$new_paiement->save();
							}							
						}
						
						if($continue_process){
							
							if($new_paiement->etat==0){
								
								
								
									//generation du token
									
									$tab_token=array();
									$tab_token["userId"]= Common::user_id;
									$tab_token["password"]= Common::user_pass;
									
									$curl = curl_init();

									curl_setopt_array($curl, array(
									  CURLOPT_URL => Common::token_api,
									  CURLOPT_RETURNTRANSFER => true,
									  CURLOPT_ENCODING => "",
									  CURLOPT_MAXREDIRS => 10,
									  CURLOPT_TIMEOUT => 0,
									  CURLOPT_FOLLOWLOCATION => true,
									  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
									  CURLOPT_CUSTOMREQUEST => "POST",
									  CURLOPT_POSTFIELDS =>json_encode($tab_token),
									  CURLOPT_HTTPHEADER => array(
										"Content-Type: application/json",
										"Accept: application/json",
										"Origin: developer.ecobank.com"
									  ),
									));

									$response = curl_exec($curl);
									if($errno = curl_errno($curl)) {
										$error_message = curl_strerror($errno);
										$response= "cURL error ({$errno}):\n {$error_message}";
										curl_close($curl);
									}else{
										curl_close($curl);
										$request_token=json_decode($response);
										if(isset($request_token->token)){						
												
												
												$securesecret=$request_token->token;
												
												$tab_request=array();
												$tab_request["ec_terminal_id"]=Common::terminal_id;
												$tab_request["ec_transaction_id"]="we009";
												$tab_request["ec_amount"]="200";
												$tab_request["ec_charges"]="0";
												$tab_request["ec_fees_type"]="P";
												$tab_request["ec_ccy"]="KES";
												$tab_request["ec_payment_method"]="QR";
												$tab_request["ec_customer_id"]="OK1337/09";
												$tab_request["ec_customer_name"]="GABIAM FOLLY";
												$tab_request["ec_mobile_no"]="22891393958";
												$tab_request["ec_email"]="folly.bernard.gabiam@gmail.com";
												$tab_request["ec_payment_description"]="paiement chez go daddy";
												$tab_request["ec_product_code"]="ehjgjs";
												$tab_request["ec_product_name"]="Mangue";
												$tab_request["ec_transaction_date"]=date("Y-m-d");
												$tab_request["ec_affiliate"]="qwe123QE";
												$tab_request["ec_country_code"]="123";
												
												/*
												$hashinput="";
												foreach($tab_request as $key=>$value){			
													if(trim($value)!=""){
														$hashinput.=$key."".$value;
													}		
													 if (strlen($value) > 0) {
														$hashinput .= $key . "=" . $value . "&";
													}
												}
												$hashinput = rtrim($hashinput, "&");
												
												
												
												//$tab_request["secure_hash"]=hash_hmac('SHA256', $hashinput, $request_token->token);
												/$tab_request["secure_hash"]=(hash_hmac('SHA256', $hashinput, pack('H*',$securesecret)));
												*/
												
												$tab_request["secure_hash"]="7f137705f4caa39dd691e771403430dd23d27aa53cefcb97217927312e77847bca6b8764f487ce5d1f6520fd7227e4d4c470c5d1e7455822c8ee95b10a0e9855";
												
												
												
												$curl = curl_init();

												curl_setopt_array($curl, array(
												CURLOPT_URL => Common::url_api,
												CURLOPT_RETURNTRANSFER => true,
												CURLOPT_ENCODING => "",
												CURLOPT_MAXREDIRS => 10,
												CURLOPT_TIMEOUT => 0,
												CURLOPT_FOLLOWLOCATION => true,
												CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
												CURLOPT_CUSTOMREQUEST => "POST",
												CURLOPT_POSTFIELDS =>json_encode($tab_request),
												CURLOPT_HTTPHEADER => array(
												"Authorization: Bearer ".$request_token->token."",
												"Content-Type: application/json",
												"Accept: application/json",
												"Origin: developer.ecobank.com"
												),
												));

												$response = curl_exec($curl);
												if($errno = curl_errno($curl)) {
													$error_message = curl_strerror($errno);
													$response= "cURL error ({$errno}):\n {$error_message}";
												}else{
													$response=json_decode($response);
												}
												curl_close($curl);
												
												$request_response=$response;
									
									
									
										}else{
											$request_response["status"]="002";
											$request_response["message"]="Une erreur s'estproduite, Veuillez reesayer plus tard";
										}
									}
								
								
							}
							
							//envoie des informations a lutilisateur pour paiement
							
						}
					
					}else{
						$request_response["status"]="001";
						$request_response["message"]="Erreur d'authentification de la commande";
					}
				
				}else{
														
					$not_enter="Informations manquées : ";
					if(trim($all_post['commande_key'])=="")$not_enter.=", commande_key";
			
					$request_response["status"]="003";
					$request_response["message"]="Toutes les informations ne sont pas renseignées. ".$not_enter;
				}
				
			}else{
					
				$not_enter="Champ raté : ";
				if(!isset($all_post['commande_key']))$not_enter.=", commande_key";
		
				$request_response["status"]="003";
				$request_response["message"]="Tous les champs ne sont pas définis . ".$not_enter ;
			}		
		}else{
			$request_response["status"]="001";
			$request_response["message"]="Erreur d'authentification de l'API";
		}	
		
		echo Json::encode($request_response) ;
		exit();
	}
	
}