<?php
namespace api\modules\website\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use yii\helpers\Json;
use api\modules\website\v1\controllers\Common;
use api\modules\website\v1\models\CommandeMarchand;
use api\modules\website\v1\models\CommandePayment;
use api\modules\website\v1\models\TamponEcobank;
use api\modules\website\v1\models\AbcRetraitmobile;
use backend\controllers\Utils;

class PaiementController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\TypeProduit';  

   
    public $api_success = 'PL7RBSPzoivyp_y4U8FbTvk6_iUTeyHqlyl0Dp453N4MHrL7RmcB';
    public $api_reject =  'PL7RBSPzb_B5KTkKiFRe1L7RmcBCoYSKlWFS3MHrTo8Cf8et';
	
	public function actionGet_qrcode() {
		date_default_timezone_set('UTC');
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
		}else{
			$information=file_get_contents('php://input');	
			$all_post=json_decode($information, TRUE);
		}
		
		
	
	
			
		if(isset($all_post["access_token"]) && Common::api_passe==$all_post["access_token"]){
			
			if(isset($all_post["commande_key"])){		
				
				if(trim($all_post["commande_key"])!=""){	
				
					$commande_key=trim($all_post["commande_key"]);
					//recuperer le token de la commande
					$find_command = CommandeMarchand::find()->where(['commande_key'=>$commande_key,'etat'=>0])->one();
					if($find_command!==null){
						
						$id_commande_marchand=$find_command->id_commande_marchand;
						$info_user=$find_command->idUser;
						$id_user=$find_command->id_user;
						$charge_paiement=$find_command->prix_service;
						$montant_paiement=($find_command->prix_produits+$find_command->prix_livraison);
						
						$email=$info_user->email;
						$phone_number=$info_user->username;
						$canal_key=$info_user->canal_key;
						$full_name=$info_user->nom." ".$info_user->prenoms;
						
						if(trim($email)==""){
							$email="folly.bernard.gabiam@gmail.com";
						}
						
						$continue_process=true;
						//verifier si ce paiement a ete deja enregistrer
						$new_paiement = CommandePayment::find()->where(['montant_paiement'=>$montant_paiement,'id_commande_marchand'=>$id_commande_marchand,'id_user'=>$id_user,'etat'=>[0,1,2]])->one();
						
						if($new_paiement==null || $new_paiement->etat==0){
							
							if($new_paiement==null){
								//enregistrer le paiement dans la base local
								  $new_paiement=new CommandePayment;
								  $new_paiement->paiement_key=Yii::$app->security->generateRandomString(32);
								  $new_paiement->num_paiement=$id_user.time();
								  $new_paiement->id_commande_marchand=$id_commande_marchand;
								  $new_paiement->id_user=$id_user;
								  $new_paiement->type_paiement=1;
								  $new_paiement->montant_paiement=$montant_paiement;
								  $new_paiement->charge_paiement=$charge_paiement;
								  $new_paiement->etat=0;
								  $new_paiement->date_create=date("Y-m-d H:i:s");
								  $new_paiement->date_update=date("Y-m-d H:i:s");
								  $new_paiement->save();
							}else{
								  $new_paiement->date_update=date("Y-m-d H:i:s");		
								  $new_paiement->save();
						    }
						  
						}else{
							if($new_paiement->etat==2){
								$continue_process=false;
							}else{
								$new_paiement->date_update=date("Y-m-d H:i:s");		
								$new_paiement->save();
							}							
						}
						
						if($continue_process){
							
							if($new_paiement->etat==0){
								
								
												
								//$securesecret=$request_token->token;
								$terminalID=Common::terminal_id;
								$uniq=$new_paiement->num_paiement;
								$price=(int)($new_paiement->montant_paiement+$new_paiement->charge_paiement);
								//$price=50;
								$key=Common::secret_key; 
								$hashField = $terminalID.$key.strval($price).$uniq;
								
								$tab_request=array();
								$tab_request["ec_version"]="1.0";
								$tab_request['ec_lang'] = "fr";
								$tab_request["ec_terminal_id"]=$terminalID;
								$tab_request["ec_transaction_id"]=$uniq;
								$tab_request["ec_hash"] = hash('SHA512', $hashField);
								$tab_request["ec_amount"]=$price;
								$tab_request['ec_charges'] = "0";
								$tab_request['ec_fees_type'] = "F";
								$tab_request['ec_ccy'] = "XOF";
								$tab_request['ec_payment_method'] = "QR";
								$tab_request['ec_customer_id'] = "AB/".$phone_number;
								$tab_request['ec_customer_name'] = $full_name;
								$tab_request['ec_mobile_no'] = $phone_number;
								$tab_request['ec_email'] = $email;
								$tab_request['ec_payment_description'] ="Paiement ".$find_command->num_commande;
								$tab_request['ec_product_code'] = "PPWA";
								$tab_request['ec_product_name'] = "Vivre";
								$tab_request['ec_udf1'] = "";
								$tab_request['ec_udf2'] = "";
								$tab_request['ec_udf3'] = "";
								$tab_request['ec_udf4'] = "";
								$tab_request['ec_udf5'] = "";		
								
								
								$curl = curl_init();

								curl_setopt_array($curl, array(
								CURLOPT_URL => Common::url_api,
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => "",
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 0,
								CURLOPT_FOLLOWLOCATION => true,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => "POST",
								CURLOPT_POSTFIELDS =>json_encode($tab_request),
								CURLOPT_HTTPHEADER => array(
								"Authorization: Bearer ".Base64_encode(hash('sha512',$hashField))."",
								"Content-Type: application/json",
								"Accept: application/json",
								"Origin: rafiki.ecobank.com"
								),
								));

								$response = curl_exec($curl);
								
								if($errno = curl_errno($curl)) {
									$error_message = curl_strerror($errno);
									$response= "cURL error ({$errno}):\n {$error_message}";
									
									
									$new_paiement->etat=3;		
									$new_paiement->response_gateway=$response;		
									$new_paiement->date_sendgateway=date("Y-m-d H:i:s");		
									$new_paiement->save();
									
								}else{
									$recup_info=json_decode($response);
									
									if(isset($recup_info->responseCode) && $recup_info->responseCode=="000"){
										
										$my_blob= base64_decode($recup_info->dynamicQRBase64);
										
										$photo_name = "qr_".$new_paiement->idcommande_paiement."_".time().".jpg";
										$url=Yii::$app->basePath;
										$recup=explode(DIRECTORY_SEPARATOR."api",$url);
										$target_path= $recup[0].DIRECTORY_SEPARATOR."backend".DIRECTORY_SEPARATOR."web".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."paiement".DIRECTORY_SEPARATOR;	
										$source_url = $target_path.$photo_name;
										file_put_contents($source_url, $my_blob);
				
				
										$new_paiement->etat=1;	
										$new_paiement->photo_qrcode=$photo_name;	
										$new_paiement->identifiant_paiement=$recup_info->dynamicQR;	
									}else{
										$new_paiement->etat=3;	
									}													
									$new_paiement->response_gateway=$response;		
									$new_paiement->date_sendgateway=date("Y-m-d H:i:s");		
									$new_paiement->save();		
																					
								}
								curl_close($curl);
								
								
							}
							
							
							if($new_paiement->etat==1){
								
								$content="Cher Client <b>".$full_name."</b>";
								$content.="\nNous avons bien reçu votre commande.";
								$content.="\nLe récapitulatif est le suivant :"; 
								$content.="\n\nCommande : N° <b>".$find_command->num_commande."</b>"; 
								$content.="\nMontant des produits : <b>".Utils::show_nbre($find_command->prix_produits)." FCFA</b>";
								$content.="\nFrais de livraison : <b>".Utils::show_nbre($find_command->prix_livraison)." FCFA</b>";
								$content.="\nAutres frais ABusiness : <b>".Utils::show_nbre($new_paiement->charge_paiement)." FCFA</b>";
								$content.="\nTotal à payer : <b>".Utils::show_nbre($new_paiement->montant_paiement+$new_paiement->charge_paiement)." FCFA</b>";
								$content.="\n\nPour déclencher le processus de livraison vous devez payer votre commande : ";
								$content.="\n\n- Par T-money, Flooz en composant <b>*XXXX#</b>";
								$content.="\n- Par Ecobank Pay en scannant le code QR joint au présent message ou en utilisant le terminal ID : <b>".Common::terminal_id."</b> avec l’application mobile Ecobank";
								$content.="\n- Par carte visa ici : <b>URL</b>";
								$content.="\nMerci pour la confiance à ABusiness !!!";
								
								$photo_qr=$new_paiement->photo_qrcode;		
								$url_photo=Utils::photoqr.$photo_qr;
								
								
								
								if($find_command->type_envoiepaiement=="1"){
									$content=str_replace("<b>","*",$content);
									$content=str_replace("</b>","*",$content);
									Utils::send_pinformation($phone_number,$url_photo,$photo_qr,$content,"WHATSAPP");
								}else if($find_command->type_envoiepaiement=="2"){
									
									Utils::send_pinformation($canal_key,$url_photo,$photo_qr,$content,"TELEGRAM");
								}else if($find_command->type_envoiepaiement=="3"){
									
									$sms="Cher Client ".$full_name;
									$sms.=".Nous avons bien recu votre commande.";
									$sms.="Pour declencher le processus de livraison vous devez payer votre commande:";
									$sms.="Par T-money, Flooz en composant *XXXX#";
									Common::hit_sms($sms,$phone_number);
								}
							
							
								$request_response["status"]="000";
								$request_response["message"]="Paiement envoyé au client avec succès.";
								$request_response["lien_photo"]=$url_photo;
		
								
							}else{
								$request_response["status"]="003";
								$request_response["message"]=$new_paiement->response_gateway;
							}
							
						}else{
							$request_response["status"]="002";
							$request_response["message"]="Erreur, paiement deja effectue";
						}
					
					}else{
						$request_response["status"]="001";
						$request_response["message"]="Erreur d'authentification de la commande";
					}
				
				}else{
														
					$not_enter="Informations manquées : ";
					if(trim($all_post['commande_key'])=="")$not_enter.=", commande_key";
			
					$request_response["status"]="003";
					$request_response["message"]="Toutes les informations ne sont pas renseignées. ".$not_enter;
				}
				
			}else{
					
				$not_enter="Champ raté : ";
				if(!isset($all_post['commande_key']))$not_enter.=", commande_key";
		
				$request_response["status"]="003";
				$request_response["message"]="Tous les champs ne sont pas définis . ".$not_enter ;
			}		
		}else{
			$request_response["status"]="001";
			$request_response["message"]="Erreur d'authentification de l'API";
		}	
		
		echo Json::encode($request_response) ;
		exit();
	}
	
	public function actionFinalise_paiement() {
		
		date_default_timezone_set('UTC');
		$all_post=array();
		
		$information="";
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
			$information=json_encode($all_post);
		}else if (Yii::$app->request->get()) {
			$all_post=Yii::$app->request->get();
			$information=json_encode($all_post);
		}else{
			$information=file_get_contents('php://input');	
		}
		$user_ip = $_SERVER['REMOTE_ADDR'];
		$user_header = $this->getAuthorizationHeader();
		
		$create_tampon=new TamponEcobank;
		$create_tampon->user_ip=$user_ip;
		$create_tampon->etat_tampon=1;
		$create_tampon->user_header=$user_header;
		$create_tampon->user_response=trim($information);
		$create_tampon->date_create=date("Y-m-d H:i:s");
		
		
		
		if($create_tampon->save()){
		
		
			$new_tampon = TamponEcobank::find()->where(['idtampon_ecobank'=>$create_tampon->idtampon_ecobank])->one();
			
			//verifiez si lip est authorise
			$authorise_ip=array("160.119.246.139");
		
			$terminalID=Common::terminal_id;
			$key=Common::secret_key; 
			
			
			$all_post = json_decode($new_tampon->user_response,true);	
			
			
			if(in_array($user_ip,$authorise_ip) ) {
				
				if(isset($all_post["ec_termianl_id"])  && isset($all_post["ec_transaction_id"]) && isset($all_post["responseCode"]) && isset($all_post["paymentStatus"])  ){
						
					if(trim($all_post["ec_termianl_id"])!="" && trim($all_post["ec_transaction_id"])!="" && trim($all_post["responseCode"])!="" && trim($all_post["paymentStatus"])!=""){
						
						//verifier si ca provient de notre terminal					
						if($terminalID==trim($all_post["ec_termianl_id"])){
							
							$uniq=trim($all_post["ec_transaction_id"]);
							$hashField = $terminalID.$key.$uniq;
							$hash_verify= hash('SHA512', $hashField);
							
							//verification du hash
							if($hash_verify==$user_header || 1==1){
							
								$num_paiement=str_replace("ECDY","",trim($all_post["ec_transaction_id"]));
								$new_paiement = CommandePayment::find()->where(['num_paiement'=>$num_paiement,'etat'=>1])->one();
								if($new_paiement!=null){
											
									$etat_paiement=3;
									if(trim($all_post["responseCode"])=="000" && trim($all_post["paymentStatus"])=="000"){
										$etat_paiement=2;									
									}
									
									$date_paiement=date("Y-m-d H:i:s");
									$new_paiement->etat=$etat_paiement;
									$new_paiement->date_paiement=$date_paiement;
									$new_paiement->response_paiement=json_encode($all_post);
									$new_paiement->save();
									
									if($etat_paiement==2){
										$find_command=$new_paiement->idCommandeMarchand;
										if($find_command!=null){									
												
											$find_command->idcommande_paiement=$new_paiement->idcommande_paiement;
											$find_command->date_paiement=$date_paiement;
											if($find_command->save()){
												
												
												$info_user=$find_command->idUser;								
												$full_name=$info_user->nom." ".$info_user->prenoms;
												
												$content="Cher Client <b>".$full_name."</b>";
												$content.="\nNous avons bien reçu votre paiement. Vous serez livrer dans les heures qui suivent.";
												$content.="\nLe récapitulatif est le suivant :"; 
												$content.="\n\nCommande : N° <b>".$find_command->num_commande."</b>"; 
												$content.="\nMontant des produits : <b>".Utils::show_nbre($find_command->prix_produits)." FCFA</b>";
												$content.="\nFrais de livraison : <b>".Utils::show_nbre($find_command->prix_livraison)." FCFA</b>";
												$content.="\nAutres frais ABusiness : <b>".Utils::show_nbre($new_paiement->charge_paiement)." FCFA</b>";						
												$content.="\n\nPaiement : N° <b>".$new_paiement->reference_paiement."</b>"; 
												$content.="\nTotal payé : : <b>".Utils::show_nbre($new_paiement->montant_paiement+$new_paiement->charge_paiement)." FCFA</b>";
												$content.="\n\nMerci pour la confiance à ABusiness !!!";
												
																			
												if($find_command->type_envoiepaiement=="1" || $info_user->canal=="WHATSAPP"){
													$content=str_replace("<b>","*",$content);
													$content=str_replace("</b>","*",$content);
													$phone_number=$info_user->username;
													Utils::send_information($phone_number,$content,"WHATSAPP");
												}else if($find_command->type_envoiepaiement=="2" || $info_user->canal=="TELEGRAM"){
													
													$canal_key=$info_user->canal_key;
													Utils::send_information($canal_key,$content,"TELEGRAM");
												}else if($find_command->type_envoiepaiement=="3"){
													
													$sms="Cher Client ".$full_name;
													$sms.=". Nous avons bien reçu votre paiement. Vous serez livrer dans les heures qui suivent.";
													$sms.=" Commande: N°".$find_command->num_commande;
													$sms.=". Paiement: N°".$new_paiement->reference_paiement; 
														
													$phone_number=$info_user->username;
													Common::hit_sms($sms,$phone_number);											
												}
												
												//flash sms Amen pour signaler larriver dune commande provenant de adjafi
												
												$tab_numero=array("22891393958","22891886249","22893642204");
												$plus=$info_user->nom." ".$info_user->prenoms .". ";
												$plus.="\nLien: https://wa.me/".$info_user->username;	
												$alert_sms="Un paiement provenant de Ecobank vient d'être effectuer. Veuillez prendre sa commande en charge dans les plus brefs delais.";
												$alert_sms.="\nCommande num: *".$find_command->num_commande."*";
												$alert_sms.="\nTotal paiement: *".Utils::show_nbre($new_paiement->montant_paiement+$new_paiement->charge_paiement)." FCFA*";
												$alert_sms.="\nPaiement effectuée par:\n".$plus;
												
												foreach($tab_numero as $numero){
													Utils::send_information($numero,$alert_sms,"WHATSAPP");
												}
												
												
											}else{
												$new_tampon->etat_tampon=10;
												$new_tampon->save();
											}
										
										}else{
											$new_tampon->etat_tampon=9;
											$new_tampon->save();
										}
									}else{
										$new_tampon->etat_tampon=8;
										$new_tampon->save();
									}
															
								}else{
									$new_tampon->etat_tampon=7;
									$new_tampon->save();
								}
								
							}else{
								$new_tampon->etat_tampon=6;
								$new_tampon->user_response.=" - ".json_encode($all_post);
								$new_tampon->save();
							}
						}else{
							$new_tampon->etat_tampon=5;
							$new_tampon->user_response.=" - ".json_encode($all_post);
							$new_tampon->save();
						}
					
					}else{
						$new_tampon->etat_tampon=4;
						$new_tampon->user_response.=" - ".json_encode($all_post);
						$new_tampon->save();
					}
					
				}else{
					$new_tampon->etat_tampon=3;
					$new_tampon->user_response.=" - ".json_encode($all_post);
					$new_tampon->save();
				}
				
			}else{
				$new_tampon->etat_tampon=2;
				$new_tampon->user_response.=" - ".json_encode($all_post);
				$new_tampon->save();
			}
			
		}
		exit();			
	}
	
	public function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        if (!empty($headers)) {
			if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
				return $matches[1];
			}
		}
		return null;
    }
	
	public function actionFinalise_paygate() {
		
		date_default_timezone_set('UTC');
				
		if (Yii::$app->request->post()) {
			$post=Yii::$app->request->post();
		}else{		
			$information=file_get_contents('php://input');
			$post=(array)json_decode($information);
		}
	
		if( isset($post['tx_reference']) && isset($post['payment_reference']) && isset($post['amount']) && isset($post['identifier'])){
			
			if(trim($post["identifier"])!=""){
					
				$num_paiement=trim($post["identifier"]);
				$new_paiement = CommandePayment::find()->where(['num_paiement'=>$num_paiement,'etat'=>0])->one();
				if($new_paiement!=null){
					
					$date_paiement=date("Y-m-d H:i:s");
					$new_paiement->etat=2;
					$new_paiement->date_paiement=$date_paiement;
					$new_paiement->response_paiement=json_encode($post);
					$new_paiement->save();
					
					$find_command=$new_paiement->idCommandeMarchand;
					if($find_command!=null){
						
						
							
						$find_command->idcommande_paiement=$new_paiement->idcommande_paiement;
						$find_command->date_paiement=$date_paiement;
						if($find_command->save()){
							
							
							$info_user=$find_command->idUser;								
							$full_name=$info_user->nom." ".$info_user->prenoms;
							
							$content="Cher Client <b>".$full_name."</b>";
							$content.="\nNous avons bien reçu votre paiement. Vous serez livrer dans les heures qui suivent.";
							$content.="\nLe récapitulatif est le suivant :"; 
							$content.="\n\nCommande : N° <b>".$find_command->num_commande."</b>"; 
							$content.="\nMontant des produits : <b>".Utils::show_nbre($find_command->prix_produits)." FCFA</b>";
							$content.="\nFrais de livraison : <b>".Utils::show_nbre($find_command->prix_livraison)." FCFA</b>";
							$content.="\nAutres frais ABusiness : <b>".Utils::show_nbre($new_paiement->charge_paiement)." FCFA</b>";						
							$content.="\n\nPaiement : N° <b>".$new_paiement->reference_paiement."</b>"; 
							$content.="\nTotal payé : : <b>".Utils::show_nbre($new_paiement->montant_paiement+$new_paiement->charge_paiement)." FCFA</b>";
							$content.="\n\nMerci pour la confiance à ABusiness !!!";
							
														
							if($find_command->type_envoiepaiement=="1"){
								$content=str_replace("<b>","*",$content);
								$content=str_replace("</b>","*",$content);
								$phone_number=$info_user->username;
								Utils::send_information($phone_number,$content,"WHATSAPP");
							}else if($find_command->type_envoiepaiement=="2"){
								
								$canal_key=$info_user->canal_key;
								Utils::send_information($canal_key,$content,"TELEGRAM");
							}else if($find_command->type_envoiepaiement=="3"){
								
								$sms="Cher Client ".$full_name;
								$sms.=". Nous avons bien reçu votre paiement. Vous serez livrer dans les heures qui suivent.";
								$sms.=" Commande: N°".$find_command->num_commande;
								$sms.=". Paiement: N°".$new_paiement->reference_paiement; 
									
									$phone_number=$info_user->username;
									Common::hit_sms($sms,$phone_number);
								
							}
							
							
							//flash sms Amen pour signaler larriver dune commande provenant de adjafi
							
							$tab_numero=array("22891393958","22891886249","22893642204");
							$plus=$info_user->nom." ".$info_user->prenoms .". ";
							$plus.="\nLien: https://wa.me/".$info_user->username;	
							$alert_sms="Un paiement provenant de PayGate vient d'être effectuer. Veuillez prendre sa commande en charge dans les plus brefs delais.";
							$alert_sms.="\nCommande num: *".$find_command->num_commande."*";
							$alert_sms.="\nTotal paiement: *".Utils::show_nbre($new_paiement->montant_paiement+$new_paiement->charge_paiement)." FCFA*";
							$alert_sms.="\nPaiement effectuée par:\n".$plus;
							
							foreach($tab_numero as $numero){
								Utils::send_information($numero,$alert_sms,"WHATSAPP");
							}
							
							
						}
					
					}							
				}	
				
				
			}		
		}
		exit();		
	}
	
	public function actionGet_tokencinet(){
		
		
			$tab_request=array();
			$tab_request["apikey"]=Common::apikey_cinet;
			$tab_request["password"]=Common::password_cinet;
			
			
			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => Common::api_cinet."?lang=fr",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS =>json_encode($tab_request),			
			));

			$response = curl_exec($curl);
			
			if($errno = curl_errno($curl)) {
				$error_message = curl_strerror($errno);
				echo $response= "cURL error ({$errno}):\n {$error_message}";
				
			}else{
						
				echo $response ;												
			}
			curl_close($curl);
	}
	
	public function actionPaiement_cinet(){
		
		//verifier s'il ya des demandes de retrait en attente
		$allretrait = AbcRetraitmobile::find()->where(['etat'=>0])->limit(1)->all();
		if(sizeof($allretrait)>0){
			
			
			//recuperation du token de demande de paiement
			$tab_request=array();
			$tab_request["apikey"]=Common::apikey_cinet;
			$tab_request["password"]=Common::password_cinet;
			
			$fullInfo="";
			foreach($tab_request as $key=>$value){
				if($fullInfo!="")$fullInfo.="&";
				$fullInfo.=$key."=".$value;				
			}
			
			
			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => Common::api_cinet."?lang=fr",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS =>$fullInfo,			
			));

			$response = curl_exec($curl);
			
			$continue=true;
			if($errno = curl_errno($curl)) {
				$error_message = curl_strerror($errno);
				$response= "cURL error ({$errno}):\n {$error_message}";
				$continue=false;
			}
			
			curl_close($curl);
			
			
			
			$continue=true;
			if($continue==true){
				
				$info=json_decode($response);
				if($info->code==0){
					
					$token=$info->data->token;
					
					foreach($allretrait as $inforetrait){
						
						//enregistrement du contact sur cnetpay
						$userInfo=$inforetrait->idclient0->idUser;
						$email="folly.bernard.gabiam@gmail.com";
						if(trim($userInfo->email)!=""){
							$email=trim($userInfo->email);
						}
						
						$tab_request=array();
						$tab_request[0]["prefix"]="228";
						$tab_request[0]["phone"]=$inforetrait->numero_paiement;
						$tab_request[0]["name"]=$userInfo->nom;
						$tab_request[0]["surname"]=$userInfo->prenoms;
						$tab_request[0]["email"]=$email;
						
						
						$inforequest="data=".json_encode($tab_request);
						
						
						$curl = curl_init();

						curl_setopt_array($curl, array(
						CURLOPT_URL => Common::apicontact_cinet."?lang=fr&token=".$token,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS =>$inforequest,			
						));

						$responselot = curl_exec($curl);
						
						$continue=true;
						if($errno = curl_errno($curl)) {
							$error_message = curl_strerror($errno);
							$responselot= "cURL error ({$errno}):\n {$error_message}";
							$continue=false;
						}
						
						$inforetrait->gateway_lot=$responselot;
						$inforetrait->date_update=date("Y-m-d H:i:s");
						$inforetrait->save();					
						
						curl_close($curl);
						
						if($continue==true){
							
							$info=json_decode($responselot);
							if($info->code==0){
								
								
								$lotInfo=$info->data[0][0]->lot;
								
								$return_url=Common::apireturn_cinet."?keyverify=".$inforetrait->key_retraitmobile;
								
								$tab_request=array();
								$tab_request[0]["prefix"]="228";
								$tab_request[0]["phone"]=$inforetrait->numero_paiement;
								$tab_request[0]["amount"]=$inforetrait->montant;
								$tab_request[0]["notify_url"]=$return_url;
								$tab_request[0]["client_transaction_id"]=$inforetrait->ref_transaction;
								
								$inforequest="data=".json_encode($tab_request);
								$curl = curl_init();

								curl_setopt_array($curl, array(
								CURLOPT_URL => Common::apimoney_cinet."?lang=fr&token=".$token."&lot=".$lotInfo,
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => "",
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 0,
								CURLOPT_FOLLOWLOCATION => true,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => "POST",
								CURLOPT_POSTFIELDS =>$inforequest,			
								));

								$responsepaie = curl_exec($curl);
								
								$continue=true;
								if($errno = curl_errno($curl)) {
									$error_message = curl_strerror($errno);
									$responsepaie= "cURL error ({$errno}):\n {$error_message}";
									$continue=false;
								}
								
								$inforetrait->gateway_response=$responsepaie;
								if($continue==true){
									$paieInfo=json_decode($responsepaie);
									if($paieInfo->code==0){
										$transaction_id=$info->data[0]->transaction_id;
									
										$inforetrait->etat=1;
										$inforetrait->paiement_response=$transaction_id;
										$inforetrait->date_paiement=date("Y-m-d H:i:s");									
									}
								}
								$inforetrait->save();					
								
								curl_close($curl);								
								
							}
							
						}							
					}
					
				}					
			}
		}
		
		exit();	
	}
	
	public function actionFinalise_cinet() {
		
		date_default_timezone_set('UTC');
		$all_post=array();
		
		$keyverify="---";
		if(isset($_GET['keyverify']))$keyverify=trim($_GET['keyverify']);
		
		$information="";
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
			$information=json_encode($all_post);
		}else{
			$information=file_get_contents('php://input');	
		}
		
		
		$all_post = json_decode($information,true);	
	
		if(isset($all_post["transaction_id"])  && isset($all_post["client_transaction_id"]) && isset($all_post["lot"]) && isset($all_post["amount"]) && isset($all_post["receiver"]) && isset($all_post["operator"]) && isset($all_post["treatment_status"])  ){
				
			if(trim($all_post["transaction_id"])!="" && trim($all_post["client_transaction_id"])!="" && trim($all_post["lot"])!="" && trim($all_post["amount"])!="" && trim($all_post["receiver"])!="" && trim($all_post["operator"])!="" && trim($all_post["treatment_status"])!=""){
				
				
				$transaction_id=trim($all_post["transaction_id"]);
				$receiver=trim($all_post["receiver"]);
				$amount=trim($all_post["amount"]);
				$client_transaction_id=trim($all_post["client_transaction_id"]);
				
				
				//verification de l'existance de la transaction_id
				$testRetrait = AbcRetraitmobile::find()->where(['etat'=>1,'key_retraitmobile'=>$keyverify,'paiement_response'=>$transaction_id,'ref_transaction'=>$client_transaction_id,'montant'=>$amount,'numero_paiement'=>$receiver])->one();
				if($testRetrait!=null){
					
					$testRetrait->etat=2;
					$testRetrait->paiement_response=$information;
					$testRetrait->date_paiement=date("Y-m-d H:i:s");
					$testRetrait->save();
					
					//envoyer un message au client pour lui signaler que la transaction est ready
					$message="Votre demande de retrait vers le numéro ".$receiver." est effective";
					$message.="\nSolde restant: <b>".Utils::show_nbre($testRetrait->idclient0->solde_disponible)." FCFA</b>";
					$message.="\nReference: ".$refcredit;
					
					$phone_number=$testRetrait->idclient0->idUser->username;
					Common::hit_sms($message,$phone_number,"ABCrédit");					
					
				}				
				
				
			}
			
		}
	
	
		exit();			
	}
	
}