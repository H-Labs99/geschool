<?php
namespace api\modules\website\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use api\modules\website\v1\models\Collecteur;
use api\modules\website\v1\models\Information;
use api\modules\website\v1\models\User;
use api\modules\website\v1\models\ActeurUser;
use api\modules\website\v1\models\Ussdtransation;
use api\modules\website\v1\models\EmailCustomer;
use api\modules\website\v1\models\BoutiqueVisite;
use api\modules\website\v1\models\CommandeMarchand;
use api\modules\website\v1\models\PubliciteContact;
use api\modules\website\v1\models\PubliciteInformation;
use api\modules\website\v1\models\HistoriquePubMessage;
use api\modules\website\v1\models\PublicitePartenaire;
use api\modules\website\v1\models\Mproduits;
use console\controllers\Cgu_content_fr;
use console\controllers\Cgu_content_en;
use backend\controllers\Utils;
use yii\helpers\Json;

class DatawiseController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\TypeProduit';  

    public $api_passe = 'sPdgD6GW6AVfAiO5uDSGeFvrmusPdgD6ytpozyidezius';
    //public $server_url = 'http://collectekf.datawise.local/assets/';
    public $server_url = 'https://ecoform.datawise.site/assets/';
    //public $username = 'clincollecte';
    //public $password = '@dmin@2020';
	
	public $username="ecobankadmin";
	public $password="ecobank@2020";
    public $nbre_show = 500;    
    public $form_name = "ENRÔLEMENT DES ACTEURS A-BUSINESS";    
    public $url_vphoto = "https://abusiness.store/admin/images/produit";    
   		
	
	public function actionGet_produit() {
			
		set_time_limit(0);
		date_default_timezone_set('UTC');
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
		}else{
			$information=file_get_contents('php://input');	
			$all_post=json_decode($information, TRUE);
		}
		
		if(isset($all_post["access_token"]) && $this->api_passe==$all_post["access_token"]){
			
			if(isset($all_post["produit_name"])){
				
				$produit_name=strtolower(trim($all_post["produit_name"]));
				if($produit_name==""){
					$liste=Mproduits::find()->select(['id_acteur','position_menu_ussd','denomination','prix_vente','CONCAT(\''.$this->url_vphoto.'\',\'/\',photo_produit) AS photo_produit','qte','unite','CONCAT(\'AB-P00\',\'\',idProduit) AS poids_unitaire','descriptionProduit'])->where(['etat'=>1,'visible_vitrine'=>1])->orderBy(['idProduit'=>SORT_DESC])->all();
				}else{
					
					$all_produit=explode(" ",$produit_name);
					$tab_produit=array();
					if(sizeof($all_produit)>0){
						foreach($all_produit as $info_produit){
							if(strlen($info_produit)>2){
								$tab_produit[]=$info_produit;
							}
						}
					}
					
					$liste=Mproduits::find()->select(['id_acteur','position_menu_ussd','denomination','prix_vente','CONCAT(\''.$this->url_vphoto.'\',\'/\',photo_produit) AS photo_produit','qte','unite','CONCAT(\'AB-P00\',\'\',idProduit) AS poids_unitaire','descriptionProduit'])->where(['etat'=>1,'visible_vitrine'=>1])->andWhere(['or like','denomination',$tab_produit])->orderBy(['idProduit'=>SORT_DESC])->all();						
				}
				
															
															
				if(sizeof($liste)==0){
					$request_response["status"]="001";
					$request_response["message"]="Désolé le resultat de votre recherche est vide";
					if($produit_name=="")$request_response["message"]="Désolé la liste des produits est vide" ;
				}else{
					
					$tab_produits=[];
					
					foreach($liste as $info){
						
						$code_produit="TTB".$info->id_acteur."U".$info->position_menu_ussd."TTBCLIN";
						$information=Json::decode(Json::encode($info)) ;
						$information['referenceProduit']="ABSELLER".$info->idActeur->code_reference.$code_produit."ABSELLER";
						$tab_produits[]=$information;
					}
					
					$request_response["status"]="000";
					$request_response["message"]="Opération complétée avec succès";
					$request_response["information"]=$tab_produits;
				}
				
			
			}else{
				
				$not_enter="Champ raté : ";
				if(!isset($all_post['produit_name']))$not_enter.=", produit_name";	
		
				$request_response["status"]="003";
				$request_response["message"]="Tous les champs ne sont pas définis . ".$not_enter ;
			}
		
		}else{	
			$request_response["status"]="002";
			$request_response["message"]="Erreur d'authentification de l'API";
		}
		
		echo Json::encode($request_response) ;
		exit();
	}


	public function actionRefresh_tokenx() {
		$return_id=0;
		date_default_timezone_set('UTC');
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
		}else{
			$information=file_get_contents('php://input');	
			$all_post=json_decode($information, TRUE);
		}
			
		$access_token=$all_post["access_token"];
		if($this->api_passe==$access_token){
			
			$username=$all_post["username"];
			$userId=str_replace('"','',$all_post["userId"]);
			$registration=$all_post["registration"];
			
			$info_collecteur=Collecteur::find()->where(['username_collecteur'=>$username,'collecteur_key'=>$userId])->one();
			if($info_collecteur!=null){
				$info_collecteur->registration_id= $registration;
				$info_collecteur->save();
				$return_id=$info_collecteur->collecteur_key;
			}else{
			
				$model = new Collecteur;
				$model->collecteur_key= Yii::$app->security->generateRandomString(32);
				$model->nom_collecteur= "-";
				$model->username_collecteur= $username;
				$model->phone_collecteur= "-";
				$model->registration_id= $registration;
				$model->etat= 1;
				$model->created_by= 0;						
				if($model->save()){
					$return_id=$model->collecteur_key;
				}
			}
			
		}
	
		return $return_id;	
		exit();
	}
				
	public function actionGet_souscription() {
		
		set_time_limit(0);
		date_default_timezone_set('UTC');		
		$type_request=10;
		
		$exit_info=Information::find()->select(["count(idInformation) as idInformation"])->where(['type'=>$type_request])->one();
		$begin=$exit_info->idInformation;
		
		$formId="ayug2D2k9fKG5zCnF5JJVz";
		$url_api=$this->server_url.$formId."/submissions.json?start=".$begin."&limit=".$this->nbre_show;
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url_api,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_USERPWD => $this->username.":".$this->password,
		  CURLOPT_USERAGENT=>'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17',
		  CURLOPT_AUTOREFERER=> true, 
		  CURLOPT_RETURNTRANSFER=> 1,
		  CURLOPT_FOLLOWLOCATION=> 1,
		  CURLOPT_VERBOSE=> 1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);

		if ($err) {
		   echo "cURL Error #:" . $err;
		} else {
		
			
			$all_information=json_decode($response);
			
			if(sizeof($all_information)>0){
				echo sizeof($all_information);
				foreach ($all_information as $information){
					$model = new Information;
					$model->extract= 0;
					$model->type = (int)$type_request;
					$model->content = json_encode($information);
					$model->dateRegistration = date("Y-m-d H:i:s");
					if(!$model->save()){
						print_r($model->getErrors());
						exit();
					}
				}
			}else{
				echo "END INFO";
			}			
		}

		exit();
	}
				
	public function actionGet_info() {
		
		set_time_limit(0);
		date_default_timezone_set('UTC');
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
		}else{
			$information=file_get_contents('php://input');	
			$all_post=json_decode($information, TRUE);
		}
			
		if(isset($all_post["access_token"]) && $this->api_passe==$all_post["access_token"]){
			 //recuperer les informations des utilisateurs			 
			$tab_client['status']=['40','30'];
			$tab_client['id_user_profil']=[3];
			$info_client=User::find()->select(['count(id) as id'])->where($tab_client)->one();
			
			
			 //recuperer les informations des livreurs			 
			$tab_livreur['status']=['10'];
			$tab_livreur['id_user_profil']=[3];
			$info_livreur=User::find()->select(['count(id) as id'])->where($tab_livreur)->one();			
			$info_allattentelivreur=ActeurUser::find()->select(['count(id_acteur_user) as id_acteur_user'])->where(['etat'=>0,'type_acteur'=>2])->one();
		
			 //recuperer les informations des marchands
			$tab_marchand['status']=['10'];
			$tab_marchand['id_user_profil']=[4];
			$info_marchand=User::find()->select(['count(id) as id'])->where($tab_marchand)->one();
			$info_allattentemarchand=ActeurUser::find()->select(['count(id_acteur_user) as id_acteur_user'])->where(['etat'=>0,'type_acteur'=>1])->one();			
			
			//recuperer les informations sur la transaction
			$info_transaction=Ussdtransation::find()->select(["count(id) id"])->one();	


			$tab_pays[]=array("pays"=>"TOGO","phone"=>"+22891000404","lien"=>"https://wa.me/22891047373");
			//$tab_pays[]=array("pays"=>"TOGO","phone"=>"+22891000404","lien"=>"https://wa.me/22891000404");
			//$tab_pays[]=array("pays"=>"BENIN","phone"=>"+22870523434","lien"=>"https://wa.me/22870523434");
			
			$request_response["status"]="000";
			$request_response['nbre_customer']=(int)$info_client->id;
			$request_response['nbre_livreur']=(int)$info_livreur->id+(int)$info_allattentelivreur->id_acteur_user;
			$request_response['nbre_marchand']=(int)$info_marchand->id+(int)$info_allattentemarchand->id_acteur_user;
			$request_response['nbre_transaction']=(int)$info_transaction->id;
			$request_response['country_chatbox']=$tab_pays;
			
			
		}else{	
			$request_response["status"]="002";
			$request_response["message"]="Erreur d'authentification de l'API . Field access_token";
		}		
		echo Json::encode($request_response) ;
		exit();
	}
				
	public function actionSend_email() {
		
		set_time_limit(0);
		date_default_timezone_set('UTC');
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
		}else{
			$information=file_get_contents('php://input');	
			$all_post=json_decode($information, TRUE);
		}
			
		if(isset($all_post["access_token"]) && $this->api_passe==$all_post["access_token"]){
			
			if(isset($all_post["nom_customer"]) && isset($all_post["email_customer"]) && isset($all_post["phone_customer"]) && isset($all_post["sujet_contact"]) && isset($all_post["message_contact"]) ){
										
						
						if(trim($all_post["nom_customer"])!="" && trim($all_post["email_customer"])!="" && trim($all_post["phone_customer"])!="" && trim($all_post["sujet_contact"])!=""  && trim($all_post["message_contact"])!=""){
							
							
								$phone_customer=str_replace(" ","",trim($all_post["phone_customer"]));
								//enregistrement dans la base de donnee local
								
								$format_email ="Un utilisateur cherche à nous contacter\n\n*Nom & Prénoms* : ".trim($all_post["nom_customer"]);
								$format_email.="\n*Phone* : ".$phone_customer;
								$format_email.="\n*Email* : ".trim($all_post["email_customer"]);
								$format_email.="\n*Sujet* : ".trim($all_post["sujet_contact"]);
								$format_email.="\n\n*Contenu*\n".trim($all_post["message_contact"]);
								$format_email.="\n\n*Lien whatsapp* : https://wa.me/".$phone_customer;
								
								
								$model = new EmailCustomer;
								$model->phone_customer= $phone_customer;
								$model->nom_customer= trim($all_post["nom_customer"]);
								$model->email_customer= trim($all_post["email_customer"]);
								$model->sujet_contact= trim($all_post["sujet_contact"]);
								$model->message_contact= trim($all_post["message_contact"]);
								$model->date_send = date("Y-m-d H:i:s");
								if($model->save()){									
									$phone_number="22892929285";								
									Utils::send_information($phone_number,$format_email);	
									$request_response["status"]="000";
								}else{
									$request_response["status"]="001";
									$request_response["message"]="Une erreur s'est produite. Veuillez reesayer plus tard";
								}
					

						}else{
													
							$not_enter="Informations manquées : ";
							if(trim($all_post['phone_customer'])=="")$not_enter.=", phone_customer";
							if(trim($all_post['nom_customer'])=="")$not_enter.=", nom_customer";
							if(trim($all_post['email_customer'])=="")$not_enter.=", email_customer";
							if(trim($all_post['sujet_contact'])=="")$not_enter.=", sujet_contact";
							if(trim($all_post['message_contact'])=="")$not_enter.=", message_contact";
					
							$request_response["status"]="003";
							$request_response["message"]="Toutes les informations ne sont pas renseignées. ".$not_enter;
						}
				
			}else{
					
					$not_enter="Champ raté : ";
					if(!isset($all_post['phone_customer']))$not_enter.=", phone_customer";
					if(!isset($all_post['nom_customer']))$not_enter.=", nom_customer";
					if(!isset($all_post['email_customer']))$not_enter.=", email_customer";
					if(!isset($all_post['sujet_contact']))$not_enter.=", sujet_contact";
					if(!isset($all_post['message_contact']))$not_enter.=", message_contact";
			
					$request_response["status"]="003";
					$request_response["message"]="Tous les champs ne sont pas définis . ".$not_enter ;
			}			
			
		}else{	
			$request_response["status"]="002";
			$request_response["message"]="Erreur d'authentification de l'API . Field access_token";
		}		
		echo Json::encode($request_response) ;
		exit();
	}			
	
	public function actionGet_cgu() {
		
		set_time_limit(0);
		date_default_timezone_set('UTC');
		if (Yii::$app->request->post()) {
			$all_post=Yii::$app->request->post();
		}else{
			$information=file_get_contents('php://input');	
			$all_post=json_decode($information, TRUE);
		}
			
		if(isset($all_post["access_token"]) && $this->api_passe==$all_post["access_token"]){
			
			$langue="fr";
			if(isset($all_post["langue"]))$langue=trim($all_post["langue"]);
			
			if($langue!="en"){
				$all_information =Cgu_content_fr::cgu_entete."<br/><br/>";
				$all_information.=Cgu_content_fr::cgu_content1."<br/><br/>";
				$all_information.=Cgu_content_fr::cgu_content2."<br/><br/>";
				$all_information.=Cgu_content_fr::cgu_content3."<br/><br/>";
				$all_information.=Cgu_content_fr::cgu_bas;
			}else{
				$all_information =Cgu_content_en::cgu_entete."<br/><br/>";
				$all_information.=Cgu_content_en::cgu_content1."<br/><br/>";
				$all_information.=Cgu_content_en::cgu_content2."<br/><br/>";
				$all_information.=Cgu_content_en::cgu_content3."<br/><br/>";
				$all_information.=Cgu_content_en::cgu_bas;
			}
			
			$all_information=str_replace("\n","<br/>",$all_information);
			$all_information=str_replace("**","</b>",$all_information);
			$all_information=str_replace("*","<b>",$all_information);
		
			$request_response["status"]="000";
			$request_response['content_cgu']=$all_information;			
			
		}else{	
			$request_response["status"]="002";
			$request_response["message"]="Erreur d'authentification de l'API . Field access_token";
		}		
		echo Json::encode($request_response) ;
		exit();
	}
				
	//fonction pour relancer les marchands qui nont pas finalisE leur compte
	public function actionSend_remember(){
		
			/*
			$nullrequest=new \yii\db\Expression('null');
			$date_request=date("Y-m-d");
			$next_date=date("Y-m-d",strtotime("+1 days"));
			
			$query= ActeurUser::find()->andWhere(['is','code_reference',$nullrequest]);
			$query->orderBy(['id_acteur_user'=>SORT_ASC])->limit(10);
			
			$all_marchand=$query->all();
			if(sizeof($all_marchand)>0){
				foreach($all_marchand as $info_marchand){
					
					$continue=true;
					$reference="CF".rand(10000,99999);
					while($continue){
						$test_marchand = ActeurUser::find()->where(['code_reference'=>$reference])->one();
						if($test_marchand!=null){
							$reference="CF".rand(10000,99999);
						}else{
							$continue=false;
						}
					}
					
					$info_marchand->code_reference=$reference;
					$info_marchand->save();
				}
			}*/
			
		exit();
		date_default_timezone_set('UTC');
		if(date("H")>=7){
			
			Set_time_limit(0);
			
			//recuperer la liste des marchand en attentes de validation. les marchands sans gps et photo
			$nullrequest=new \yii\db\Expression('null');
			$date_request=date("Y-m-d");
			$next_date=date("Y-m-d",strtotime("+1 days"));
			
			$query= ActeurUser::find()->where(['etat'=>0,'type_acteur'=>[1,2],'dateremember_acteur'=>['0000-00-00',$date_request]]);
			$query->andFilterWhere(['or',['is','photo_acteur',$nullrequest],['=','LENGTH(photo_acteur)',0],['=','lat_acteur',0],['=','long_acteur',0]]);
			$query->orderBy(['id_acteur_user'=>SORT_ASC])->limit(100);
			
			$all_marchand=$query->all();
			if(sizeof($all_marchand)>0){
				foreach($all_marchand as $info_marchand){
					
					$info_user=$info_marchand->idUser; 
					$code_user="TTR".$info_user->id."TTR".$info_marchand->type_acteur."TTR".rand(100,999)."CLIN";
					$lien_user="https://wa.me/22891000404?text=ABSELLER".$info_marchand->id_acteur_user.$code_user."ABSELLER";
					$phone_collecteur=$info_user->username;				
						
					if($info_marchand->type_acteur==1){
						
						$content="Cher(e) Client(e) ".$info_user->nom." ".$info_user->prenoms;
						$content.="\n\nVous avez récemment créé un *compte marchand* sur *ABusiness* (*".$info_marchand->denomination."*) 📱📱📱. Pour rendre votre compte opérationnel ✅✅✅ et gagner des milliers de clients 🧕🏾👩🏽‍💼👲🏽 qui vous attendent, nous vous prions de valider votre compte en ajoutant *la position Géo*  📍📍📍 de votre commerce ainsi que *la photo* 📷📷📷 en cliquant sur le lien ci-dessous et en envoyant automatiquement le code contenu dans votre champ de saisie.\nPour le reste laissez-vous guider par notre assistant automatique.\n\n*Lien de validation:* 👇🏽\n".$lien_user;					
	  
						Utils::send_information($phone_collecteur,$content); 
						
						
					 }else if($info_marchand->type_acteur==2){
						 
						$content="Cher(e) Client(e) ".$info_user->nom." ".$info_user->prenoms;
						$content.="\n\nVous avez récemment créé un *compte livreur* 🛵🚚🚕 sur *ABusiness* 📱📱. Pour rendre votre compte opérationnel ✅ ✅✅ et recevoir des milliers de livraisons qui vous attendent, nous vous prions de valider votre compte en ajoutant *la position Géo* 📍📍 de votre *lieu de résidence ou de stationnement* ainsi que *votre photo* 📷 en cliquant sur le lien ci-dessous et en envoyant automatiquement le code contenu dans votre champ de saisie.\nPour le reste laissez-vous guider par notre assistant automatique.\n\n*Lien de validation:* 👇🏽\n".$lien_user;					
	  
						Utils::send_information($phone_collecteur,$content); 
						 
					 }
					
					$info_marchand->dateremember_acteur=$next_date;
					$info_marchand->save();
				}			
			}
			
		}
		exit();
		
		
	}
				
	//fonction pour envoyer des recap aux marchands
	public function actionSend_recap(){
		
		exit();
		date_default_timezone_set('UTC');
		if(date("H")>=20 && date("H")<=23){
			
			Set_time_limit(0);
			
			
			$nullrequest=new \yii\db\Expression('null');
			$date_request=date("Y-m-d");
			$next_date=date("Y-m-d",strtotime("+1 days"));
			
			$query= ActeurUser::find()->where(['etat'=>1,'type_acteur'=>1]);
			$query->andFilterWhere(['or',['is','daterecap_acteur',$nullrequest],['in','daterecap_acteur',['0000-00-00',$date_request,$nullrequest]]]);
			$query->orderBy(['id_acteur_user'=>SORT_ASC])->limit(1);			
			$all_marchand=$query->all();
			
			if(sizeof($all_marchand)>0){
				
				$total_visite=BoutiqueVisite::find()->select(["count(idboutique_visite) as idboutique_visite"])->where(['date_visite'=>$date_request])->one();
					
					
				foreach($all_marchand as $info_marchand){
					
					$code_reference=$info_marchand->code_reference;
					$url="https://wa.me/22891000404?text=ABSELLER".$code_reference."ABSELLER";
					
					$info_user=$info_marchand->idUser; 
					$recup_visite=BoutiqueVisite::find()->select(["count(idboutique_visite) as idboutique_visite"])->where(['id_acteur'=>$info_marchand->id_acteur_user,'date_visite'=>$date_request])->one();
					
					$recup_commande=CommandeMarchand::find()->select(["count(id_commande_marchand) as id_commande_marchand","GROUP_CONCAT( produit_livraison SEPARATOR '-' ) as produit_livraison"])->where(['id_marchand'=>$info_marchand->id_acteur_user,'etat'=>[1,2]])->andWhere(['<=','date_create',$date_request." 23:59:59"])->andWhere(['>=','date_create',$date_request." 00:00:00"])->one();
						
					$nbre_produit=explode("-",$recup_commande->produit_livraison);
					
					$content="Cher(e) Client(e) ".$info_user->nom." ".$info_user->prenoms;					
					$content.="\n\n💼Votre activité journalière sur ABusiness"; 
					$content.="\n\n📱Total visite sur votre boutique : ".$recup_visite->idboutique_visite;
					$content.="\n🛒Total commande reçue: ".$recup_commande->id_commande_marchand; 
					$content.="\n💰Total produits vendus: ".sizeof($nbre_produit); 
					$content.="\n👁️Total visite global sur ABusiness: ".$total_visite->idboutique_visite;
					$content.="\n\n💪🏽💪🏽💪🏽Voici quelques conseils pour augmenter votre visibilité, votre chiffre d'affaire et gagner de nouveaux clients : ";
					$content.="\n-🏩 Partagez régulièrement L’URL de votre boutique ( *".$url."* ) ou de vos produits sur votre statut, sur les réseaux sociaux et avec vos clients et amis.\n-📺 Communiquez autour de votre code marchand ( *".$code_reference."* ).\n-📧 Publiez des offres défiant toute concurrence, qualité et prix, et partagez l'URL après publication pour attirer de nouveaux clients.";
					$content.="\n💰💰💰Si vous avez des offres personnalisées en promotion vous pourrez utiliser notre canal publicitaire pour communiquer vers plus de 100 000 clients utilisant WhatsApp";
					$content.="\n\n📥Pour toutes informations contactez notre service clientèle au https://wa.me/22892929285";
					
										

					$phone_collecteur=$info_user->username;	
					Utils::send_information($phone_collecteur,$content); 
											 
					
					$info_marchand->daterecap_acteur=$next_date;
					$info_marchand->save();
				}			
			}
			
		}
		exit();
		
		
	}
	
	public function actionExtraction_contact(){
		
		
		date_default_timezone_set('UTC');
			
		Set_time_limit(0);
		
		//recuperer la liste des marchand en attentes de validation. les marchands sans gps et photo
		$date_request=date("Y-m-d");
		$info_pub=PubliciteInformation::find()->where(['etat_publicite'=>0])->one();
		if($info_pub!=null){
			$info_pub->etat_publicite=10;
			$info_pub->save();
			
			$name_file=	$info_pub->fichier_publicite;	
			
			
			$url=Yii::$app->basePath;
			$recup=explode(DIRECTORY_SEPARATOR."api",$url);
			$target_path= $recup[0].DIRECTORY_SEPARATOR."backend".DIRECTORY_SEPARATOR."web".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."fichier_pub".DIRECTORY_SEPARATOR;	
			$fichier = $target_path.$name_file;
													
			$text="";
		  
			if (file_exists($fichier)){ 
		  
						
				$fp = fopen($fichier, "r"); 
				
				while (!feof($fp)) {
						
					$ligne = fgets($fp,4096);
					$liste = explode( ";",$ligne);
					$numero=trim($liste[0]);
					if($numero!=""){
						//verifier si ce numero existe de ja dans notre systeme
						$find_user = User::find()->where(['username'=>$numero])->all();
						$find_numero = PubliciteContact::find()->select(['count(idpublicite_contact) as idpublicite_contact'])->where(['numero'=>$numero])->one();
						
						//verifier si cest deja enregistrE
						$find_pcontact = PubliciteContact::find()->where(['id_publicite'=>$info_pub->id_publicite,'numero'=>$numero])->one();
						if($find_pcontact==null){					
							$model=new PubliciteContact;						
							$model->id_publicite=$info_pub->id_publicite;
							$model->numero=$numero;
							$model->send=0;
							$model->exist=sizeof($find_user);
							$model->flash=$find_numero->idpublicite_contact;
							$model->date_create=date("Y-m-d H:i:s");
							$model->save();						
						}					
						
					}
				}
				
				$info_pub->etat_publicite=1;
				$info_pub->save();
				
			}else{											
				$info_pub->etat_publicite=1;
				$info_pub->save();
			}									 
  
		}
		
		exit();		
		
	}
	
	public function actionMessage_contact(){
		
		
		date_default_timezone_set('UTC');
			
		Set_time_limit(0);
		
		//recuperer la liste des marchand en attentes de validation. les marchands sans gps et photo
		$find_pcontact = PubliciteContact::find()->where(['send'=>0])->limit(100)->all();
		if(sizeof($find_pcontact)>0){
			foreach($find_pcontact as $info_contact){
				$publicite=$info_contact->idPublicite;
				$type_publicite=$publicite->type_publicite;
				$send=3;
				$info_contact->send=4;
				$info_contact->save();	
				
				if($type_publicite==1 || ($type_publicite==2 && $info_contact->exist==0) || ($type_publicite==3 && $info_contact->exist==0 && $info_contact->flash==0) ){
					$send=1;
				
					$phone_number=$info_contact->numero;
					$message=$publicite->content_publicite;
					$photo_name=(string)$publicite->photo_publicite;
					
					//$message.=$phone_number;					
					//$phone_number="22891393958";
					
					$url_photo=Utils::photopublicite.$photo_name;
					if($photo_name==""){
						Utils::send_information($phone_number,$message);
					}else {											
						Utils::send_pinformation($phone_number,$url_photo,$photo_name,$message);
					}
				}
				
				$info_contact->send=$send;
				$info_contact->date_send=date("Y-m-d H:i:s");
				$info_contact->save();	
			}
		}
		
		exit();		
		
	}
	
	
	public function actionFirst_inactivite(){
		
		
		date_default_timezone_set('UTC');
			
		Set_time_limit(0);
		
		$last_update=time()-(60*5);
		$day=time()-(60*30);
		//recuperer la liste des marchand en attentes de validation. les marchands sans gps et photo
	    $find_transaction = Ussdtransation::find()->where(['etat_transaction'=>0,'relance'=>0])->andWhere(['<=','last_update',$last_update])->andWhere(['>=','last_update',$day])->limit(10)->all();
		echo sizeof($find_transaction) ;
		if(sizeof($find_transaction)>0){
			foreach($find_transaction as $info_trans){
				$this->send_relance($info_trans,1);
			}
		}
		
		exit();		
		
	}
	
	public function actionSecond_inactivite(){
		
		
		date_default_timezone_set('UTC');
			
		Set_time_limit(0);
		
		$last_update=time()-(60*30);
		$day=time()-(60*60*24);
		//recuperer la liste des marchand en attentes de validation. les marchands sans gps et photo
	    $find_transaction = Ussdtransation::find()->where(['etat_transaction'=>0,'relance'=>1])->andWhere(['<=','last_update',$last_update])->andWhere(['>=','last_update',$day])->limit(10)->all();
		
		if(sizeof($find_transaction)>0){
			foreach($find_transaction as $info_trans){		
				$this->send_relance($info_trans,2);
			}
		}
		
		exit();		
		
	}
	
	public function send_relance($info_trans,$type){
		
		$phone_number=$info_trans->username;
		$provenance=$info_trans->reference;
		$id_user=$info_trans->iduser;
		
		if($phone_number=="22891393958" || $phone_number=="1093179373" || $phone_number=="1600952859494"){	
			
			$query_get=HistoriquePubMessage::find()->select(['id_partenaire'])->where(['id_user'=>$id_user,'date_send'=>date("Y-m-d")]);		
			$get_publicite=PublicitePartenaire::find()->where(['etat_publicite'=>1,'type_support'=>[1,2,3,4]])->andWhere(['like','niveau_affichage','INACTIVITE'])->andWhere(['not in','id_publicite',$query_get])->one();
			if($get_publicite!=null && trim($get_publicite->support_publicite)){
				
				$publicite = new HistoriquePubMessage();
				$publicite->id_partenaire = $get_publicite->id_publicite;
				$publicite->id_user =$id_user;
				$publicite->date_send = date("Y-m-d");
				$publicite->date_create = date("Y-m-d H:i:s");
				$publicite->save();
				
				$fullmessage=trim($get_publicite->content_publicite);
				if($fullmessage!="")$fullmessage.="\n\n";
				
				if($type==1){
					$fullmessage.="L'assistant automatique attends toujours votre réponse. si vous désirez recommencer veuillez taper <b>AB</b> ou entrez la réponse à la question précédente pour continuer.";
				}else if($type==1){
					$fullmessage.="Votre session vient d'expirer. Recommencer une nouvelle expérience avec notre assistant automatique en tapant <b>AB</b>.";
				}
				
				if($get_publicite->type_support=="1"){
					
					Utils::send_information($phone_number,$fullmessage,$provenance);
					
				}else if($get_publicite->type_support=="2"){
					
					$photo_name=$get_publicite->support_publicite;
					$url_photo=Utils::photobase_url."partenaire/".$photo_name;
					Utils::send_pinformation($phone_number,$url_photo,$photo_name,$fullmessage,$provenance);
					
				}else if($get_publicite->type_support=="3"){
					
					
					$photo_name=$get_publicite->support_publicite;
					$url_photo=Utils::photobase_url."partenaire/".$photo_name;
					Utils::send_ainformation($phone_number,$url_photo,$fullmessage,$provenance);
					
				}else if($get_publicite->type_support=="4"){
					
					$photo_name=$get_publicite->support_publicite;
					$url_photo=Utils::photobase_url."partenaire/".$photo_name;
					Utils::send_vinformation($phone_number,$url_photo,$photo_name,$fullmessage,$provenance);
					
				}
			
			}
		}
		$info_trans->relance=$type;
		$info_trans->save();
	}
	
	
	
}