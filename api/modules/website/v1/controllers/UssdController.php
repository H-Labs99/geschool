<?php
namespace api\modules\website\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use console\models\Ussdtransation;
use api\modules\website\v1\models\HistoriqueWhatsapp;
use api\modules\website\v1\models\MessageHistorique;
use api\modules\website\v1\models\HistoriqueWhatsappMessage;
use api\modules\website\v1\controllers\Common;

class UssdController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\HistoriqueWhatsappMessage';  


public static function actionSend_sms(){
	$params = Yii::$app->request->get();
    if(isset($params['receiver']) && isset($params['content']) && isset($params['sender'])) {
	   
		$from= "Abusiness";
		$from= $params['sender'];
		$receveid= $params['receiver'];
		$message= $params['content'];	
				
		$content=urlencode($message);		
		
		date_default_timezone_set('UTC');
		
		$nbre_caractere=strlen($message);
		$nbre_page=(int)($nbre_caractere/160)+1;
		
		$new_message=new MessageHistorique ;
		$new_message->destinataire=$receveid;
		$new_message->content=$message;
		$new_message->nbre_page=$nbre_page;
		$new_message->datemessage=date("Y-m-d H:i:s");
		$new_message->response_message="";
		$new_message->operateur="Wassa";
		$new_message->save();
		
		
		$live_url=Common::smsbase_url.$from."&receiver=".$receveid."&text=".$content ;	
		
		$ch = curl_init($live_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$resultat=curl_exec($ch);
		
		if($errno = curl_errno($ch)) {
			$error_message = curl_strerror($errno);
			 "\n\ncURL error ({$errno}):\n {$error_message}";			 
		}else{
			$error_message =$resultat;
		}
        curl_close($ch);
		
		$new_message->response_message=$error_message;
		$new_message->save();		
	}
}
	
public function actionExecweb($cmd) {
	  
		//exit();
	    $get_url=yii::$app->basePath;   
        $base_url=explode(DIRECTORY_SEPARATOR."api",$get_url)[0];  
        chdir($base_url);
		
        $command = 'yii consoleappinit/process '.$cmd;
		
		$handler = popen($command, 'r');
		
        $output = '';
        while (!feof($handler)) {
            $output .= fgets($handler);
        }
		$output = trim($output);
        $status = pclose($handler);
        return $output;	
		
	
}

public function actionGetdata_testweb() {
   
   
   $params = Yii::$app->request->post();
   
   if( isset($params['type']) and isset($params['transaction_id']) and isset($params['response']) and 
   isset($params['user_mobile']) and isset($params['user_first_name']) and isset($params['user_last_name'])   ) {
    
		$transaction_id=(string)trim($params['transaction_id']);
		$type=(string)trim($params['type']);
		$response=(string)trim($params['response']);
		$numero=(string)trim($params['user_mobile']);
		$name=(string)trim($params['user_first_name']);
		$firstname=(string)trim($params['user_last_name']);

		if($type=="PREAUTH"){
                      
				//Initialisation de l'operation		
				$command='-t="'.$numero.'" -r="GOAGRI"  -n="'.$name.'" -f="'.$firstname.'" -s="0" -i="'.$transaction_id.'" ';
				$message= $this->actionExecweb($command);	
					
		}else if($type=="TRANSACTION"){                     
						
				$command='-t="'.$numero.'" -r="'.$response.'" -i="'.$transaction_id.'" -s="1" ';
				$message= $this->actionExecweb($command);
						
		}             
              
    }else{
         $message= \Yii::t('app', 'NO_INFORMATION_RECEIVED');
    }
	
	echo  $message ;	
	exit();
}


public function actionExewhatsapp($cmd_debut) {
    
	$cmd = 'cd ../.. && /usr/bin/php yii consoleappinit/process '.$cmd_debut;	
	return $result = shell_exec($cmd);	
}

public function send_cmessage($format_url,$phone,$content) {

	
	$ch = curl_init();
	$curlConfig = array(
	  CURLOPT_URL            => $format_url,
	  CURLOPT_POST           => true,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_CONNECTTIMEOUT => 5,
	  CURLOPT_POSTFIELDS     => array(
		'chatId' => $phone."@c.us",
		'phone' => $phone,
		'body' => $content,
	  ),
	);
	curl_setopt_array($ch, $curlConfig);
	$result = curl_exec($ch);
	curl_close($ch);	
}

public function send_cfmessage($format_url,$phone,$url_photo,$file_name,$content) {
	
	$ch = curl_init();
	$curlConfig = array(
	  CURLOPT_URL            => $format_url,
	  CURLOPT_POST           => true,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_CONNECTTIMEOUT => 5,
	  CURLOPT_POSTFIELDS     => array(
		'chatId' => $phone."@c.us",
		'phone' => $phone,
		'body' => $url_photo,
		'filename' => $file_name,
		'caption' => $content,
	  ),
	);
	curl_setopt_array($ch, $curlConfig);
	$result = curl_exec($ch);
	curl_close($ch);

  
}

public function actionFlash_nsia() {
	
		$test_whatsapp=HistoriqueWhatsapp::find()->where(['name_param'=>'nsia_api'])->one();
		if($test_whatsapp!=null){
			
			/*
			$default_message="Bienvenue sur GO NSIA de NSIA ASSURANCE, le chatBot de sensibilisation en riposte au COVID-19\n\nPour démarrer une discussion *Tapez GO NSIA*";
			$default_message="Bienvenue sur GO ORABANK de ORABANK, le chatBot de sensibilisation en riposte au COVID-19\n\nPour démarrer une discussion *Tapez GO ORABANK*";
			$default_message="Bienvenue sur GO GTA de GTA ASSURANCE, le chatBot de sensibilisation en riposte au COVID-19\n\nPour démarrer une discussion *Tapez GO GTA*";
			$default_message="Bienvenue sur GO SAHAM de SAHAM ASSURANCE, le chatBot de sensibilisation en riposte au COVID-19\n\nPour démarrer une discussion *Tapez GO SAHAM*";
			$default_message="Bienvenue sur GO FIDELIA de FIDELIA ASSURANCE, le chatBot de sensibilisation en riposte au COVID-19\n\nPour démarrer une discussion *Tapez GO FIDELIA*";
			*/
			$default_message="Bienvenue sur GO PSA de LA PROTECTRICE SA, le chatBot de sensibilisation en riposte au COVID-19\n\nPour démarrer une discussion *Tapez GO PSA*";
			$formatstar_content=array("gonsia"=>"NSIA","goorabank"=>"ORABANK","gogta"=>"GTA","gosaham"=>"SAHAM","gofidelia"=>"FIDELIA","gopsa"=>"PSA");
			
			
			$tab_info=array();
			$star_content=array();
			foreach($formatstar_content as $key=>$recup){
				$tab_info[]=$recup;
				$star_content[]=$key;
			}
			$recup_response=str_ireplace("[","(",json_encode($tab_info));
			$recup_response=str_ireplace("]",")",$recup_response);
			
			
			$information = file_get_contents('php://input');
			$decoded = json_decode($information,true);	
			
			if(isset($decoded['messages']) && isset($decoded['instanceId'])){
				
				$instance="instance".$decoded['instanceId'];
				if($instance==$test_whatsapp->instance_chat){
			
					$all_boitemessage=$decoded['messages'];						
					if(sizeof($all_boitemessage)>0){
						
						$test_whatsapp->date_operation=date("Y-m-d H:i:s");
						$test_whatsapp->total_message_chat=$test_whatsapp->total_message_chat+sizeof($all_boitemessage);
						$test_whatsapp->save();	
						
						foreach($all_boitemessage as $recup_message){								
							
							if($recup_message['fromMe']==false){
								
								//verifier si ce message a ete deja recu et traite 
								$sender=explode("@",$recup_message['author'])[0];
								$content=trim($recup_message['body']);
								$tps_send=trim($recup_message['time']);
								$type=2;
								
								$test_contentwhatsapp=HistoriqueWhatsappMessage::find()->where(['sender'=>$sender,'datesend'=>$tps_send,'body'=>$content,'type'=>$type])->one();
								
								if($test_contentwhatsapp==null){
									
										$new_cmessage=new HistoriqueWhatsappMessage ;
										$new_cmessage->sender=$sender;
										$new_cmessage->datesend=$tps_send;
										$new_cmessage->body=json_encode($recup_message);
										$new_cmessage->type=$type;
										if($new_cmessage->save()){
								
											$convert_content=strtolower(str_replace(" ","",$content));
											$convert_content=str_replace("*","",$convert_content);
											$message="";
											
											if(in_array($convert_content,$star_content)){
												
												//fermer toutes les sessions ouvertes
												Ussdtransation::updateAll(['etat_transaction' => 3], 'username= '.$sender.' and reference in '.$recup_response.' and etat_transaction!=1');
												
												//demarrer un nouveau dialogue
												$sessionid=$tps_send."_".$sender;
												$nom="";
												$prenom="";
												$response=$formatstar_content[$convert_content];
												
												$command='-t="'.$sender.'" -r="'.$response.'" -n="'.$nom.'" -f="'.$prenom.'" -s="0" -i="'.$sessionid.'" ';
												$message= $this->actionExewhatsapp($command,$response);													
												
											}else{
												
												//verifier si cet user a une session ouverte
												$find_transaction = Ussdtransation::findOne(['username'=>$sender,'reference'=>$tab_info,'etat_transaction'=>0]);
												if($find_transaction!=null){
													
														$response=$find_transaction->reference;	
														
														if($tps_send-$find_transaction->last_update < (60*30)){
														
															$find_transaction->last_update=(int)$tps_send;
															$find_transaction->save();
															
															$sessionid=$find_transaction->idtransaction;
															$command='-t="'.$sender.'" -r="'.$content.'" -i="'.$sessionid.'" -s="1" ';
															
															$message= $this->actionExewhatsapp($command,$response);	
															
														}else{															
															$message="Votre dernière session a expiré.\n\nPour demarrer une discussion tapez: *Go ".$response."* ";														
														}
																										
												}else{								
													$message=$default_message;
												}
											
											}
										
											//sleep(10);
											if($message!=""){
								
												//$access_user=array("22891393958","22893649619","22892363533","22898996088","22893604510","22890080088","22899595353","22891886249","22891121670","22891749741");
												
												$access_user=array();
												if(in_array($sender,$access_user)){												
													$message="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";
													$url =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendMessage?token=".$test_whatsapp->token_chat;
													$this->send_cmessage($url,$sender,$message);
												}else{
													
													$message=str_replace("Erreur:","*Erreur:* \n",$message);
													$agent_message="";
													$contact_message="";
													
													$recup_message=explode("#####",$message);
													if(sizeof($recup_message)==3){
														
														$message=$recup_message[0]."".$recup_message[2];
														$agent_message=$recup_message[1];
													}
													
													$recup_cmessage=explode("CONT_ACT",$message);
													if(sizeof($recup_cmessage)==3){
														
														$message=$recup_cmessage[0]."".$recup_cmessage[2];
														$contact_message=$recup_cmessage[1];
													}
													
													$url =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendMessage?token=".$test_whatsapp->token_chat;
													
													//verifier si cest un message avec image ou si cest un simple message
													$recup_message=explode("W_IMAGE",$message);
													if(sizeof($recup_message)==3){
														
														$urlphoto =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendFile?token=".$test_whatsapp->token_chat;
														$message=$recup_message[0]."".$recup_message[2];
														$file_url=Common::photobase_url.$recup_message[1];
														$file_name=$recup_message[1];
														//$this->send_cfmessage($urlphoto,$sender,$file_url,$file_name,$message);
														
														$message.="\n".$file_url;
														$this->send_cmessage($url,$sender,$message);
													}else{
														$recup_usermessage=explode("GOOD_DIAGNOSTIC",$message);
														
														$share_message="";
														if(sizeof($recup_usermessage)==3){													
															$message=$recup_usermessage[0]." ".$recup_usermessage[2];
															$share_message=$recup_usermessage[1];
														}
														$this->send_cmessage($url,$sender,$message);																
														if(trim($share_message)!=""){
															$this->send_cmessage($url,$sender,$share_message);																
														}
													
													
													}
												}
																								
												if($agent_message!=""){
													
													//$sender_agent=array("22892929285","22899595353","22891749741","22898996088");
													$sender_agent=array("22892929285","22899595353","22891749741");
													foreach($sender_agent as $numero){
														$this->send_cmessage($url,$numero,$agent_message);
													}
												}
												
												if($contact_message!=""){												
													$this->send_cmessage($url,"22892929285",$contact_message);
												}
												
											}
										}
								}
								
							}							
						}
					
					}				
			
				}
			}
		}
		
		exit();
}

public function actionFlash_auto() {
	
		
		
		/*
		$information = file_get_contents('php://input');
		$decoded = json_decode($information,true);	
		*/
		
		
		$star_content=array("ab");
		if (Yii::$app->request->post()) {
			$decoded=Yii::$app->request->post();
		}else{
			$information=file_get_contents('php://input');	
			$decoded=json_decode($information, TRUE);
		}
		$authorize_app=array("WhatsAuto","WhatsApp","Wa Business");
		
		/*
		
		$tab_response["reply"]="Service Temporairement indisponible";
		echo json_encode($tab_response);
		exit();	
*/		
		

		if(isset($decoded['app']) && isset($decoded['sender']) && isset($decoded['message'])){
			
			$sender=$decoded['sender'];
			$provenance=$decoded['app'];
			$rmessage=$decoded['message'];
			$content=json_encode($decoded);
			
			if(in_array($provenance,$authorize_app)){
		
				if($content!=""){
					//verifier si ce message a ete deja recu et traite 
					$tps_send=time();
					$type=2;
					
					$test_contentwhatsapp=HistoriqueWhatsappMessage::find()->where(['sender'=>$sender,'datesend'=>$tps_send,'body'=>$content,'type'=>$type])->one();
					
					if($test_contentwhatsapp==null){
						
							$new_cmessage=new HistoriqueWhatsappMessage ;
							$new_cmessage->sender=$sender;
							$new_cmessage->datesend=$tps_send;
							$new_cmessage->body=$content;
							$new_cmessage->type=$type;
							if($new_cmessage->save()){
					
								$convert_content=strtolower(str_replace(" ","",$rmessage));
								$convert_content=str_replace("*","",$convert_content);
								$message="";
								if(in_array($convert_content,$star_content)){
									
									//fermer toutes les sessions ouvertes
									Ussdtransation::updateAll(['etat_transaction' => 3], 'username= '.$sender.' and reference="GOAGRI" and etat_transaction!=1');
									
									//demarrer un nouveau dialogue
									$sessionid=$tps_send."_".$sender;
									$nom="";
									$prenom="";
									$response="AB";
									$command='-t="'.$sender.'" -r="'.$response.'" -n="'.$nom.'" -f="'.$prenom.'" -s="0" -i="'.$sessionid.'" ';
									$message= $this->actionExewhatsapp($command,"GOAGRI");
									
								}else{
									//verifier si cet user a une session ouverte
									$find_transaction = Ussdtransation::findOne(['username'=>$sender,'reference'=>'GOAGRI','etat_transaction'=>0]);
									if($find_transaction!=null){
										if($tps_send-$find_transaction->last_update < (600*3)){
											
											$find_transaction->last_update=(int)$tps_send;
											$find_transaction->save();
											
											$sessionid=$find_transaction->idtransaction;
											$command='-t="'.$sender.'" -r="'.$content.'" -i="'.$sessionid.'" -s="1" ';
											$message= $this->actionExewhatsapp($command,"GOAGRI");
										}else{
											
											$message="Votre dernière session a expiré.\n\nPour demarrer une discussion tapez: *AB* ";
											
										}
									}else{								
										$message="Bienvenue sur *ABusiness*, votre chatBot e-commerce de produits de première nécessité\n\nPour démarrer une discussion tapez: *AB*";
									}
								}
							
								//sleep(10);
								if($message=="Bernard"){
					
									//$access_user=array("22891393958","22893649619","22892363533","22898996088","22893604510","22890080088","22899595353","22891886249","22891121670","22891749741");
									
									$access_user=array();
									if(in_array($sender,$access_user)){												
										$message="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";
										$url =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendMessage?token=".$test_whatsapp->token_chat;
										$this->send_cmessage($url,$sender,$message);
									}else{
										
										$message=str_replace("Erreur:","*Erreur:* \n",$message);
										$agent_message="";
										$contact_message="";
										$contact_fmessage="";
										
										$recup_message=explode("#####",$message);
										if(sizeof($recup_message)==3){
											
											$message=$recup_message[0]."".$recup_message[2];
											$agent_message=$recup_message[1];
										}
										
										$recup_cmessage=explode("CONT_ACT",$message);
										if(sizeof($recup_cmessage)==3){
											
											$message=$recup_cmessage[0]."".$recup_cmessage[2];
											$contact_message=$recup_cmessage[1];
										}
										
										$recup_fmessage=explode("LIVRA_ISON",$message);
										if(sizeof($recup_fmessage)==3){
											
											$message=$recup_fmessage[0]."".$recup_fmessage[2];
											$contact_fmessage=$recup_fmessage[1];
										}
										
										$url =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendMessage?token=".$test_whatsapp->token_chat;
										
										//verifier si cest un message avec image ou si cest un simple message
										$recup_message=explode("WA_IMAGE",$message);
										if(sizeof($recup_message)==3){
											
											$urlphoto =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendFile?token=".$test_whatsapp->token_chat;
											$message=$recup_message[0]."".$recup_message[2];
											$file_url=Common::photobase_url.$recup_message[1];
											$file_name=$recup_message[1];
											$recup_name=explode("/",$recup_message[1]);
											if(sizeof($recup_name)>1){
												$file_name=$recup_name[1];															
											}
											$this->send_cfmessage($urlphoto,$sender,$file_url,$file_name,$message);
										}else{
											$recup_usermessage=explode("GOOD_DIAGNOSTIC",$message);
												
											$share_message="";
											if(sizeof($recup_usermessage)==3){													
												$message=$recup_usermessage[0]." ".$recup_usermessage[2];
												$share_message=$recup_usermessage[1];
											}
											$this->send_cmessage($url,$sender,$message);																
											if(trim($share_message)!=""){
												$this->send_cmessage($url,$sender,$share_message);																
											}
										
										}
									}
									
									
									
									if($agent_message!=""){
										
										//$sender_agent=array("22892929285","22899595353","22891749741","22898996088");
										$sender_agent=array("22892929285","22899595353","22891749741");
										foreach($sender_agent as $numero){
											$this->send_cmessage($url,$numero,$agent_message);
										}
									}
									if($contact_fmessage!=""){
										
										//$sender_agent=array("22892929285","22899595353","22898996088");
										//$sender_agent=array("22892929285","22898996088");
										$sender_agent=array("22892929285","22893642204");
										foreach($sender_agent as $numero){
											$this->send_cmessage($url,$numero,$contact_fmessage);
										}
									}
									if($contact_message!=""){												
										$this->send_cmessage($url,"22892929285",$contact_message);
									}
									
								}else{
									
									$tab_response["reply"]="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";
									echo json_encode($tab_response);
								}
							}
					}			
				
				}			
		
			}
		} 		
	
		exit();
}

public function actionFlash_agri() {
	
	
		$information = file_get_contents('php://input');
		
	
		$test_whatsapp=HistoriqueWhatsapp::find()->where(['name_param'=>'info_api'])->one();
		if($test_whatsapp!=null){
			
			$star_content=array("ab");
			$decoded = json_decode($information,true);	
			
			if(isset($decoded['messages']) && isset($decoded['instanceId'])){
				
				$instance="instance".$decoded['instanceId'];
				if($instance==$test_whatsapp->instance_chat){
			
					$all_boitemessage=$decoded['messages'];						
					if(sizeof($all_boitemessage)>0){
						
						$test_whatsapp->date_operation=date("Y-m-d H:i:s");
						$test_whatsapp->total_message_chat=$test_whatsapp->total_message_chat+sizeof($all_boitemessage);
						$test_whatsapp->save();	
						
						foreach($all_boitemessage as $recup_message){								
							
							if($recup_message['fromMe']==false){
								
								
								//verifier si ce message a ete deja recu et traite 
								$sender=explode("@",$recup_message['author'])[0];
								$content=trim($recup_message['body']);
								$tps_send=trim($recup_message['time']);
								$type=2;
								
								$test_contentwhatsapp=HistoriqueWhatsappMessage::find()->where(['sender'=>$sender,'datesend'=>$tps_send,'body'=>$content,'type'=>$type])->one();
								
								if($test_contentwhatsapp==null){
									
										$new_cmessage=new HistoriqueWhatsappMessage ;
										$new_cmessage->sender=$sender;
										$new_cmessage->datesend=$tps_send;
										$new_cmessage->body=json_encode($recup_message);
										$new_cmessage->type=$type;
										if($new_cmessage->save()){
								
											$convert_content=strtolower(str_replace(" ","",$content));
											$convert_content=str_replace("*","",$convert_content);
											
											
											$content=str_replace("\n\r", '. ', $content);
											$content=str_replace("\n", '. ', $content);
											$content=str_replace("\r", '. ', $content);
											
											$direct=false;
											$test_agent=explode("ABCOLLECTOR",$content);
											if(sizeof($test_agent)==3) $direct=true;
											
									
											$message="";
											if(in_array($convert_content,$star_content) || $direct==true){
												
												//fermer toutes les sessions ouvertes
												Ussdtransation::updateAll(['etat_transaction' => 3], 'username= '.$sender.' and reference="'.Common::whatsapp_ref.'" and etat_transaction!=1');
												
												//demarrer un nouveau dialogue
												$sessionid=$tps_send."_".$sender;
												$nom="";
												$prenom="";
												$response="1";
												if($direct==true)$response=(int)$test_agent[1];
												$command='-t="'.$sender.'" -r="'.$response.'" -n="'.$nom.'" -f="'.$prenom.'" -s="0" -i="'.$sessionid.'" -p="'.Common::whatsapp_ref.'"';
												$message= $this->actionExewhatsapp($command);
												
											}else{
												
												$run=false;
												$test_direct=explode("ABSELLER",$content);
												if(sizeof($test_direct)==3) $run=true;
													
								
												//verifier si cet user a une session ouverte
												//$find_transaction = Ussdtransation::findOne(['username'=>$sender,'reference'=>Common::whatsapp_ref,'etat_transaction'=>0]);
												$find_transaction = Ussdtransation::find()->where(['username'=>$sender,'reference'=>Common::whatsapp_ref,'etat_transaction'=>[0,2]])->orderby(['id'=>SORT_DESC])->one();
												if($find_transaction!=null || $run==true){
													
													if($run==true){
														Ussdtransation::updateAll(['etat_transaction' => 3], 'username= '.$sender.' and reference="'.Common::whatsapp_ref.'" and etat_transaction!=1');
														$sessionid=$tps_send."_".$sender;
														if($find_transaction!=null){
															$sessionid=$find_transaction->idtransaction;
														}
														
														$command='-t="'.$sender.'" -r="'.$content.'" -i="'.$sessionid.'" -s="0" -p="'.Common::whatsapp_ref.'"';
														$message= $this->actionExewhatsapp($command);
													}else if($tps_send-$find_transaction->last_update < (600*3)){
														
														$find_transaction->last_update=(int)$tps_send;
														$find_transaction->relance=0;
														$find_transaction->save();
														
														$sessionid=$find_transaction->idtransaction;
														$command='-t="'.$sender.'" -r="'.$content.'" -i="'.$sessionid.'" -s="1" -p="'.Common::whatsapp_ref.'"';
														$message= $this->actionExewhatsapp($command);
													}else{
														
														$message="Votre dernière session a expiré.\n\nPour demarrer une discussion tapez: *AB* ";
														
													}
												}else{								
													$message="Bienvenue sur *ABusiness*, votre chatBot e-commerce de produits de première nécessité\n\nPour démarrer une discussion tapez: *AB*";
												}
											}
										
											//sleep(10);
											if($message!=""){
								
												//$access_user=array("22891393958","22893649619","22892363533","22898996088","22893604510","22890080088","22899595353","22891886249","22891121670","22891749741");
												
												$access_user=array();
												if(in_array($sender,$access_user)){												
													$message="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";
													$url =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendMessage?token=".$test_whatsapp->token_chat;
													$this->send_cmessage($url,$sender,$message);
												}else{
													
													$message=str_replace("Erreur:","*Erreur:* \n",$message);
													$agent_message="";
													$contact_message="";
													$contact_fmessage="";
													
													$recup_message=explode("#####",$message);
													if(sizeof($recup_message)==3){
														
														$message=$recup_message[0]."".$recup_message[2];
														$agent_message=$recup_message[1];
													}
													
													$recup_cmessage=explode("CONT_ACT",$message);
													if(sizeof($recup_cmessage)==3){
														
														$message=$recup_cmessage[0]."".$recup_cmessage[2];
														$contact_message=$recup_cmessage[1];
													}
													
													$recup_fmessage=explode("LIVRA_ISON",$message);
													if(sizeof($recup_fmessage)==3){
														
														$message=$recup_fmessage[0]."".$recup_fmessage[2];
														$contact_fmessage=$recup_fmessage[1];
													}
													
													$url =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendMessage?token=".$test_whatsapp->token_chat;
													
													//verifier si cest un message avec image ou si cest un simple message
													$recup_message0=explode("WA_IMAGE",$message);
													$recup_message1=explode("WA_VIDEO",$message);
													if(sizeof($recup_message0)==3 || sizeof($recup_message1)==3){
														
														if(sizeof($recup_message0)==3)$recup_message=$recup_message0;
														if(sizeof($recup_message1)==3)$recup_message=$recup_message1;
															
														$urlphoto =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendFile?token=".$test_whatsapp->token_chat;
														$message=$recup_message[0]."".$recup_message[2];
														$file_url=Common::photobase_url.$recup_message[1];
														$file_name=$recup_message[1];
														$recup_name=explode("/",$recup_message[1]);
														if(sizeof($recup_name)>1){
															$file_name=$recup_name[1];															
														}
														$this->send_cfmessage($urlphoto,$sender,$file_url,$file_name,$message);
														
													}else{
														
														
														
														$recup_usermessage=explode("GOOD_DIAGNOSTIC",$message);
															
														$share_message="";
														if(sizeof($recup_usermessage)==3){													
															$message=$recup_usermessage[0]." ".$recup_usermessage[2];
															$share_message=$recup_usermessage[1];
														}
														$this->send_cmessage($url,$sender,$message);																
														if(trim($share_message)!=""){
															$this->send_cmessage($url,$sender,$share_message);																
														}
													
													}
												}
												
												
												if($agent_message!=""){
													
													//$sender_agent=array("22892929285","22899595353","22891749741","22898996088");
													$sender_agent=array("22892929285","22899595353","22891749741");
													foreach($sender_agent as $numero){
														$this->send_cmessage($url,$numero,$agent_message);
													}
												}
												if($contact_fmessage!=""){
													
													//$sender_agent=array("22892929285","22899595353","22898996088");
													//$sender_agent=array("22892929285","22898996088");
													$sender_agent=array("22892929285","22893642204");
													foreach($sender_agent as $numero){
														$this->send_cmessage($url,$numero,$contact_fmessage);
													}
												}
												if($contact_message!=""){												
													$this->send_cmessage($url,"22892929285",$contact_message);
												}
												
											}
										}
								}
								
							}							
						}
					
					}				
			
				}
			}
		}
		
		exit();
}




}