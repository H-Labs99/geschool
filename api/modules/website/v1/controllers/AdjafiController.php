<?php

namespace api\modules\website\v1\controllers;

header("Access-Control-Allow-Origin: *");

use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use console\models\Ussdtransation;

use api\modules\website\v1\models\Information;
use api\modules\website\v1\models\User;
use api\modules\website\v1\models\TamponAdjafi;
use api\modules\website\v1\models\ActeurUser;
use api\modules\website\v1\models\Mproduits;
use api\modules\website\v1\models\CommandeMarchand;
use api\modules\website\v1\controllers\Common;
use backend\controllers\Utils;
use yii\helpers\Json;

class AdjafiController extends ActiveController
{
	//https://abusiness.store/api/web/web_v1/adjafis/get_souscription  Chaque 15 min
	//https://abusiness.store/api/web/web_v1/adjafis/register_seller Chaque 5 min
	//https://abusiness.store/api/web/web_v1/adjafis/get_produit Chaque 5 min
	//https://abusiness.store/api/web/web_v1/adjafis/register_produit Chaque 5 min
	//https://abusiness.store/api/web/web_v1/adjafis/get_commande Chaque 5 min
	//https://abusiness.store/api/web/web_v1/adjafis/register_commande Chaque 5 min
	//REMMEBER TO CHANGE THE SELLER NUMBER AFTER CREATION


	public $modelClass = 'api\modules\website\v1\models\HistoriqueWhatsappMessage';
	public $nbre_show = 500;
	public $nbrelimit = 100;
	public $lien_whatsapp = "https://wa.me/22891047373?text=";
	public $username = "admin";
	public $password = "WarmingABC@@2020#";

	public $username_wordpress = "admin";
	public $password_wordpress = "WarmingABC@@2020#";



	public function actionGet_souscription()
	{


		set_time_limit(0);
		date_default_timezone_set('UTC');
		$type_request = 20;

		$exit_info = Information::find()->select(["count(idInformation) as idInformation"])->where(['type' => $type_request])->one();
		$begin = $exit_info->idInformation;

		$formId = "6fa06f70-2faa-4596-bcb6-1ba1baa4b5b3";
		$url_api = "https://sheet.best/api/sheets/" . $formId . "?_limit=" . $this->nbre_show . "&_offset=" . $begin;


		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url_api,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$all_information = json_decode($response);
			if (sizeof($all_information) > 0) {
				foreach ($all_information as $information) {

					if (isset($information->Horodateur) && (string)$information->Horodateur != "" && (string)$information->Horodateur != null) {
						$model = new Information;
						$model->extract = 0;
						$model->type = (int)$type_request;
						$model->content = json_encode($information);
						$model->dateRegistration = date("Y-m-d H:i:s");
						$model->save();
					}
				}
			} else {
				echo "END INFO";
			}
		}


		exit();
	}

	public function actionRegister_seller()
	{



		$all_information = Information::find()->where(['type' => '20', 'extract' => '0'])->limit($this->nbrelimit)->all();
		$tab_field = array(
			"nom" => "Nom du Responsable",
			"prenoms" => "Prénoms",
			"email" => "E-mail",
			"username" => "Contact whatsapp",
			"nom_boutique" => "Raison sociale (Nom de l'entreprise)",
			"sexe" => "Sexe",
			"age" => "Age",
			"activite" => "Secteur d'activité",
			"adresse1" => "Dans quelle ville se trouve votre entreprise?",
			"adresse2" => "Dans quel quartier se trouve votre entreprise?"
		);


		if (sizeof($all_information) > 0) {
			foreach ($all_information as $info_recup) {
				$all_content = (array) json_decode($info_recup->content);

				$info_recup->extract = "2";
				$info_recup->save();

				$all_post = array();
				foreach ($tab_field as $key => $field_name) {
					if (isset($all_content[$field_name])) {
						$all_post[$key] = $all_content[$field_name];
					} else {
						$all_post[$key] = "";
					}
				}

				$idprovenance = 100;
				if (trim($all_post['username']) != ""  && trim($all_post['nom_boutique']) != "") {

					$numero = preg_replace('~\D~', '', $all_post['username']);

					if (strlen($numero) == 8) {
						$numero = "228" . $numero;
					} else {
						if (substr($numero, 0, 2) == "00") {
							$numero = substr($numero, 2);
						}
					}

					$password = "FA#" . rand(123456, 999999) . "!20";
					$info_user = User::find()->where(['username' => $numero])->one();
					if ($info_user == null) {

						$info_user = new User();
						$info_user->username = $numero;
						$info_user->nom = $all_post['nom'];
						$info_user->prenoms = $all_post['prenoms'];
						$info_user->email = $all_post['email'];
						$info_user->creation = "WHATSAPP";
						$info_user->canal = "WHATSAPP";
						$info_user->id_collecteur = 1;
						$info_user->idP = 1;
						$info_user->idProvenance = $idprovenance;
						$info_user->id_user_profil = 4;
						$info_user->status = 40;
						$info_user->role = 10;
						$info_user->created_at = time();
						$info_user->updated_at = time();
						$info_user->auth_key = Yii::$app->security->generateRandomString(32);
						$info_user->password_hash = Yii::$app->security->generatePasswordHash($password);
						$info_user->save();
					} else {
						$info_user->nom = $all_post['nom'];
						$info_user->prenoms = $all_post['prenoms'];
						$info_user->email = $all_post['email'];
						$info_user->updated_at = time();
						$info_user->save();
					}


					$type = true;

					//verifier les info du marchand
					$acteur_chaine = ActeurUser::find()->where(['id_user' => $info_user->id, 'type_acteur' => 1])->one();
					if ($acteur_chaine == null) {

						$continue = true;
						$reference = "CF" . rand(10000, 99999);
						while ($continue) {
							$test_marchand = ActeurUser::find()->where(['code_reference' => $reference])->one();
							if ($test_marchand != null) {
								$reference = "CF" . rand(10000, 99999);
							} else {
								$continue = false;
							}
						}


						$acteur_chaine = new ActeurUser();
						$acteur_chaine->id_user = $info_user->id;
						$acteur_chaine->idtransaction = 0;
						$acteur_chaine->code_reference = $reference;
						$acteur_chaine->type_acteur = 1;
						$acteur_chaine->pourcentage = 0;
						$acteur_chaine->denomination = trim($all_post['nom_boutique']);
						$acteur_chaine->address_acteur = trim($all_post['adresse1']) . "-" . trim($all_post['adresse2']);
						$acteur_chaine->description_activite = trim($all_post['activite']);
						$acteur_chaine->lat_acteur = "0";
						$acteur_chaine->long_acteur = "0";
						$acteur_chaine->sexe_acteur = $this->getSexe(trim($all_post['sexe']));
						$acteur_chaine->naissance_acteur = $this->getNaissance(trim($all_post['age'])) . "-01-01";
						$acteur_chaine->provenance_acteur = $idprovenance;
						$acteur_chaine->other_number = "";
						$acteur_chaine->id_collecteur = 1;
						$acteur_chaine->etat = 0;
						$acteur_chaine->acteur_key = Yii::$app->security->generateRandomString(32);
						$acteur_chaine->save();
					} else {
						$type = false;
						$acteur_chaine->sexe_acteur = $this->getSexe(trim($all_post['sexe']));
						$acteur_chaine->naissance_acteur = $this->getNaissance(trim($all_post['age'])) . "-01-01";
						$acteur_chaine->denomination = trim($all_post['nom_boutique']);
						$acteur_chaine->address_acteur = trim($all_post['adresse1']) . "-" . trim($all_post['adresse2']);
						$acteur_chaine->description_activite = trim($all_post['activite']);
						$acteur_chaine->save();
					}

					$reference = $acteur_chaine->code_reference;

					//ENREGISTREMENT VERS ADJAFI ET ENVOIE DE SMS
					$un_prenom = explode(' ', $info_user->prenoms)[0];
					$un_nom = explode(' ', $info_user->nom)[0];
					$adj_username = $this->content_remove_accents(strtolower(trim($un_nom)) . '-' . strtolower(trim($un_prenom)));

					$adj_email = trim($info_user->email);

					if ($adj_email == "" || !filter_var($adj_email, FILTER_VALIDATE_EMAIL)) {
						$adj_email = "defaultab" . $acteur_chaine->id_acteur_user . "@abusiness.store";
					}

					$infos = array('username' => trim($adj_username), 'first_name' => trim($info_user->nom), 'last_name' => trim($info_user->prenoms), 'email' =>  trim($adj_email), 'roles' => 'wcfm_vendor', 'password' => $password, 'nickname' => $reference);

					$response_vendeur = $this->todatabaseAdjafi_user($infos);


					$un_vendeur = json_decode($response_vendeur, true);
					if (isset($un_vendeur['id'])) {

						$acteur_chaine->provenance_idacteur = $un_vendeur['id'];
						$acteur_chaine->save();



						//ENVOIE DE SMS DE LOGIN SUR ADJAFI
						$msg = "Inscription bien prise en compte.Connectez-vous maintenant avec\nIdentifiant: " . trim($adj_username) . "\nMot de passe: " . $password . "\nAcces sur: http://adjafi-9.com/store-manager";
						Common::hit_sms($msg, $numero, "FoireAdjafi");


						if ($type == true) {
							//ENVOIE MESSAGE PAR WHATSAPP
							$code_user = "TTR" . $info_user->id . "TTR1TTR" . rand(100, 999) . "CLIN";
							$lien_user = $this->lien_whatsapp . "ABSELLER" . $acteur_chaine->id_acteur_user . $code_user . "ABSELLER";
							$sms = "Cher(e) Marchand " . $info_user->nom . " " . $info_user->prenoms . "\n";
							$sms .= "Votre demande de <b>compte marchand</b> sur <b>ABusiness</b> a été acceptée.\n\n";
							$sms .= "<b>Attention ce n'est pas fini!!!</b>\n\n";
							$sms .= "Pour obtenir le statut validé et être visible à des clients qui recherchent des produits dans votre entourage,vous devez ajouter la position de géolocalisation, une photo de votre commerce et publier quelques produits disponibles en accédant au <b>menu 5</b> ou directement en cliquant sur le lien ci-dessous et en envoyant automatiquement le code contenu dans votre champ de saisie\n" . $lien_user;
							$sms .= "\n\n<b>Denomination:</b> " . $acteur_chaine->denomination;
							$sms .= "\n" . $acteur_chaine->description_activite;
							$sms .= "\n<b>Adresse lieu:</b> " . $acteur_chaine->address_acteur;
							Utils::send_information($numero, $sms, "WHATSAPP");
						}
						Utils::send_information($numero, $msg, "WHATSAPP");


						$info_recup->extract = "1";
						$info_recup->content_adjafi = $response_vendeur;
						$info_recup->save();
					} else {


						$msg = "Un souci s'est produit lors de votre enregistrement. veuillez prendre attache avec notre service pour debloquer la situation. Contact: 22892929285";
						Common::hit_sms($msg, $numero, "FoireAdjafi");
						Utils::send_information($numero, $msg, "WHATSAPP");


						$info_recup->extract = "3";
						$info_recup->content_adjafi = $adj_username . " " . $response_vendeur;
						$info_recup->save();
					}
				}
			}
		}
	}

	public function todatabaseAdjafi_user($infos)
	{
		set_time_limit(0);
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://adjafi-9.com/wp-json/wp/v2/users",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->username_wordpress . ":" . $this->password_wordpress,
			CURLOPT_POSTFIELDS => $infos,

		));

		$response = curl_exec($curl);
		if ($response === false)
			echo 'Erreur Curl : ' . curl_error($curl);
		curl_close($curl);
		return $response;
	}

	public function todatabaseAdjafi_products($username, $password, $infos)
	{
		set_time_limit(0);
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://adjafi-9.com/wp-json/wc/v3/products",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $username . ":" . $password,
			CURLOPT_POSTFIELDS => http_build_query($infos),
		));

		$response = curl_exec($curl);
		if ($response === false)
			echo 'Erreur Curl : ' . curl_error($curl);
		curl_close($curl);
		return $response;
	}


	public function actionGet_produit()
	{

		set_time_limit(0);
		date_default_timezone_set('UTC');

		$type_info = TamponAdjafi::find()->where(['field_name' => 'produit_liste'])->one();
		if ($type_info != null) {

			$last_update = str_replace("T", " ", $type_info->last_update);
			$url_api = "http://adjafi-9.com/wp-json/wc/v3/products?after=" . $type_info->last_update . "Z&order=asc&per_page=100";

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url_api,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_USERPWD => $this->username . ":" . $this->password,
				CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17',
				CURLOPT_AUTOREFERER => true,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_VERBOSE => 1,
				CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);


			curl_close($curl);

			if ($err) {
				echo "cURL Error #:" . $err;
			} else {

				$all_information = json_decode($response);

				if (sizeof($all_information) > 0) {
					$i = 0;
					foreach ($all_information as $information) {
						if (isset($information->date_created) && (string)$information->date_created != "" && (string)$information->date_created != null) {
							$model = new Information;
							$model->extract = 0;
							$model->type = 30;
							$model->content = json_encode($information);
							$model->dateRegistration = date("Y-m-d H:i:s");
							$model->save();

							$new_update = str_replace("T", " ", $information->date_created);
							if ($last_update < $new_update) {
								$last_update = $new_update;
							}
						}
					}
					$type_info->last_update = str_replace(" ", "T", $last_update);
					$type_info->save();
				} else {
					echo "END INFO";
				}
			}
		}

		exit();
	}

	public function actionRegister_produit()
	{

		$all_information = Information::find()->where(['type' => '30', 'extract' => '0'])->limit($this->nbrelimit)->all();

		//if(isset($_GET['token'])){

		if (sizeof($all_information) > 0) {
			foreach ($all_information as $info_recup) {
				$all_content = (array) json_decode($info_recup->content);
				$info_recup->extract = "2";
				$info_recup->save();


				$id_adjafi = $all_content['id'];
				$nom_produit = $all_content['name'];
				$prix_produit = round($all_content['regular_price'], 0, PHP_ROUND_HALF_UP);;
				$image_produit = "";
				if (isset($all_content['images']) && isset($all_content['images'][0])) {
					$image_produit = $all_content['images'][0]->src;
				}
				if (isset($all_content['meta_data'][0]->value)) {

					$id_commercant = $all_content['meta_data'][0]->value;

					$find_acteur = ActeurUser::findOne(['provenance_idacteur' => $id_commercant, 'etat' => [0, 1], 'type_acteur' => 1, 'provenance_acteur' => 100]);
					if ($find_acteur != null) {

						$id_acteur = $find_acteur->id_acteur_user;

						$photo_url = "produit_" . $id_acteur . "_" . time() . ".jpg";
						$url = Yii::$app->basePath;
						$recup = explode(DIRECTORY_SEPARATOR . "api", $url);
						$target_path = $recup[0] . DIRECTORY_SEPARATOR . "backend" . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "produit" . DIRECTORY_SEPARATOR;
						$source_url = $target_path . $photo_url;

						$continue = true;
						if ($image_produit != "") {
							$otherserver_url = file_get_contents($image_produit);
							if (file_put_contents($source_url, $otherserver_url)) {
								$continue = true;
							} else {
								$continue = false;
							}
						}

						if ($continue) {

							$test_produits = Mproduits::find()->where(['id_acteur' => $id_acteur, 'id_adjafi' => $id_adjafi, 'etat' => 1])->one();
							if ($test_produits != null) {

								$test_produits->denomination = $nom_produit;
								$test_produits->prix_vente = $prix_produit;
								$test_produits->photo_produit = $photo_url;
								$test_produits->save();
							} else {

								$test_position = Mproduits::find()->where(['id_acteur' => $id_acteur, 'etat' => [1, 2]])->orderBy(['position_menu_ussd' => SORT_DESC])->one();
								if ($test_position != null) {
									$position_menu_ussd = ((int)$test_position->position_menu_ussd) + 1;
								} else {
									$position_menu_ussd = 1;
								}

								$model = new Mproduits;
								$model->id_acteur = $id_acteur;
								$model->photo_produit = $photo_url;
								$model->denomination = $nom_produit;
								$model->unite = "Kg";
								$model->qte = "1";
								$model->prix_vente = $prix_produit;
								$model->id_adjafi = $id_adjafi;
								$model->position_menu_ussd = $position_menu_ussd;
								$model->poids_unitaire = "1";
								$model->etat = 1;
								$model->created_by = $find_acteur->idUser->id;
								$model->key_produit = Yii::$app->security->generateRandomString(32);
								$model->save();
							}

							$info_recup->extract = "1";
							$info_recup->save();
						} else {

							$info_recup->extract = "10";
							$info_recup->save();
						}
					} else {

						$info_recup->extract = "11";
						$info_recup->save();
					}
				} else {
					$info_recup->extract = "12";
					$info_recup->save();
				}
			}
		}
		//}
	}


	public function actionGet_commande()
	{

		set_time_limit(0);
		date_default_timezone_set('UTC');

		$type_info = TamponAdjafi::find()->where(['field_name' => 'commande_liste'])->one();
		if ($type_info != null) {

			$last_update = str_replace("T", " ", $type_info->last_update);
			$url_api = "http://adjafi-9.com/wp-json/wc/v3/orders?after=" . $type_info->last_update . "Z&order=asc&per_page=50";

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url_api,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_USERPWD => $this->username . ":" . $this->password,
				CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17',
				CURLOPT_AUTOREFERER => true,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_VERBOSE => 1,
				CURLOPT_CUSTOMREQUEST => "GET",
			));

			echo $response = curl_exec($curl);
			$err = curl_error($curl);



			curl_close($curl);



			if ($err) {
				echo "cURL Error #:" . $err;
			} else {

				$all_information = json_decode($response);
				if (sizeof($all_information) > 0) {
					$i = 0;
					foreach ($all_information as $information) {

						if (isset($information->id) && (string)$information->id != "" && (string)$information->id != null) {
							$model = new Information;
							$model->extract = 0;
							$model->type = 40;
							$model->content = json_encode($information);
							$model->dateRegistration = date("Y-m-d H:i:s");
							$model->save();

							$new_update = str_replace("T", " ", $information->date_created);
							if ($last_update < $new_update) {
								$last_update = $new_update;
							}
						}
					}
					$type_info->last_update = str_replace(" ", "T", $last_update);
					$type_info->save();
				} else {
					echo "END INFO";
				}
			}
		}

		exit();
	}


	public function actionRegister_commande()
	{

		$all_information = Information::find()->where(['type' => '40', 'extract' => '0'])->limit($this->nbrelimit)->all();

		//if(isset($_GET['token'])){

		if (sizeof($all_information) > 0) {
			foreach ($all_information as $info_recup) {
				$all_content = (array) json_decode($info_recup->content);


				$info_recup->extract = "2";
				$info_recup->save();


				$customer_nom = $all_content['billing']->first_name;
				$customer_prenom = $all_content['billing']->last_name;
				$customer_email = $all_content['billing']->email;
				$customer_phone = $all_content['billing']->phone;
				$id_trans = $all_content['id'];

				if (isset($all_content['line_items'][0]->meta_data[0]->value)) {

					$id_commercant = $all_content['line_items'][0]->meta_data[0]->value;
					//verifier si le marchand existe
					$find_acteur = ActeurUser::findOne(['provenance_idacteur' => $id_commercant, 'etat' => [0, 1], 'type_acteur' => 1, 'provenance_acteur' => 100]);
					if ($find_acteur != null) {

						$id_acteur = $find_acteur->id_acteur_user;


						$customer_phone = preg_replace('~\D~', '', $customer_phone);

						if (strlen($customer_phone) == 8) {
							$customer_phone = "228" . $customer_phone;
						} else {
							if (substr($customer_phone, 0, 2) == "00") {
								$customer_phone = substr($customer_phone, 2);
							}
						}

						//verifier si le client existe
						$info_user = User::find()->where(['username' => $customer_phone])->one();
						if ($info_user == null) {
							$password = "FA@" . rand(123456, 999999) . "!20";

							$info_user = new User();
							$info_user->username = $customer_phone;
							$info_user->nom = $customer_nom;
							$info_user->prenoms = $customer_prenom;
							$info_user->email = $customer_email;
							$info_user->creation = "WHATSAPP";
							$info_user->canal = "WHATSAPP";
							$info_user->id_collecteur = 1;
							$info_user->idP = 1;
							$info_user->idProvenance = 100;
							$info_user->id_user_profil = 3;
							$info_user->status = 40;
							$info_user->role = 10;
							$info_user->created_at = time();
							$info_user->updated_at = time();
							$info_user->auth_key = Yii::$app->security->generateRandomString(32);
							$info_user->password_hash = Yii::$app->security->generatePasswordHash($password);
							$info_user->save();
						}

						$liste_produits = $all_content['line_items'];
						if (sizeof($liste_produits) > 0) {

							$continue = true;
							$id_produits = "";
							$qte_livraison = "";
							$livraison_lat = "0.0";
							$livraison_long = "0.0";
							$prix_produits = 0;
							$prix_livraison = 0;
							$poids_livraison = 0;

							foreach ($liste_produits as $info_demande) {
								$product_id = $info_demande->product_id;
								$qte = $info_demande->quantity;
								$prix_vente = round($info_demande->price, 0, PHP_ROUND_HALF_UP);

								//verifier si ce produit existe
								$test_produits = Mproduits::find()->where(['id_acteur' => $id_acteur, 'id_adjafi' => $product_id, 'etat' => 1])->one();
								if ($test_produits != null) {
									if ($test_produits->prix_vente != $prix_vente) {
										$test_produits->prix_vente = $prix_vente;
										$test_produits->save();
									}
									if ($id_produits != "") $id_produits .= "-";
									if ($qte_livraison != "") $qte_livraison .= "-";

									$id_produits .= $test_produits->idProduit;
									$qte_livraison .= $qte;

									$prix_produits += $prix_vente * $qte;
									$poids_livraison += $test_produits->poids_unitaire * $qte;
								} else {
									$continue = false;
								}
							}

							if ($continue) {
								$commande_march = new CommandeMarchand();
								$commande_march->id_user = $info_user->id;
								$commande_march->id_marchand = $id_acteur;
								$commande_march->id_livreur = 0;
								$commande_march->idtransaction = $id_trans;
								$commande_march->produit_commande = (string)$id_produits;
								$commande_march->lieu_livraison = "--";
								$commande_march->info_supplementaire = "---";
								$commande_march->etat = 0;
								$commande_march->etat_confirmation = 0;
								$commande_march->prix_livraison = (int) $prix_livraison;
								$commande_march->prix_produits = (int)$prix_produits;
								$commande_march->prix_commande = (int)$prix_produits;
								$commande_march->produit_livraison = (string)$id_produits;
								$commande_march->poids_livraison = (string)$poids_livraison;
								$commande_march->distance_livraison = "";
								$commande_march->qte_livraison = (string)$qte_livraison;
								$commande_march->lat_livraison = (float)$livraison_lat;
								$commande_march->long_livraison = (float)$livraison_long;
								$commande_march->commande_key = Yii::$app->security->generateRandomString(32);
								if ($commande_march->save()) {

									$find_commande = CommandeMarchand::findOne(['id_commande_marchand' => $commande_march->id_commande_marchand]);

									//flash sms Amen pour signaler larriver dune commande provenant de adjafi

									$tab_numero = array("22891393958", "22891886249", "22893642204");
									$plus = $info_user->nom . " " . $info_user->prenoms . ". ";
									$plus .= "\nLien: https://wa.me/" . $info_user->username;
									$alert_sms = "Une commande provenant de ADJAFI vient d'être effectuée. Veuillez la prendre en charge dans les plus brefs délais.";
									$alert_sms .= "\nCommande num: " . $find_commande->num_commande;
									$alert_sms .= "\nTotal Commande: " . $find_commande->prix_commande . " FCFA";
									$alert_sms .= "\n\nCommande effectuée par:\n" . $plus;

									foreach ($tab_numero as $numero) {
										Utils::send_information($numero, $alert_sms, "WHATSAPP");
									}
									$info_recup->extract = "1";
									$info_recup->save();
								} else {
									$info_recup->extract = "12";
									$info_recup->save();
								}
							} else {
								$info_recup->extract = "0";
								$info_recup->save();
							}
						} else {
							$info_recup->extract = "0";
							$info_recup->save();
						}
					} else {

						$info_recup->extract = "11";
						$info_recup->save();
					}
				} else {

					$info_recup->extract = "12";
					$info_recup->save();
				}
			}
		}

		//}
	}

	public function actionMove_seller()
	{

		set_time_limit(0);
		date_default_timezone_set('UTC');

		$all_post = Yii::$app->request->get();
		if (isset($all_post["access_token"]) && Common::api_passe == $all_post["access_token"]) {

			if (isset($all_post["acteur_key"])) {

				if (trim($all_post["acteur_key"]) != "") {

					$acteur_key = trim($all_post["acteur_key"]);
					//recuperer les info de l'acteur


					$find_acteur = ActeurUser::findOne(['acteur_key' => $acteur_key, 'etat' => [0, 1], 'type_acteur' => 1]);
					if ($find_acteur != null) {

						$provenance_idacteur = (int)$find_acteur->provenance_idacteur;

						if ($provenance_idacteur == 0) {

							$info_user = $find_acteur->idUser;
							$id_acteur = $find_acteur->id_acteur_user;
							$reference = $find_acteur->code_reference;

							//ENREGISTREMENT VERS ADJAFI ET ENVOIE DE SMS

							$password = "FA@" . rand(123456, 999999) . "!20";
							$un_prenom = explode(' ', $info_user->prenoms)[0];

							$adj_username = $this->content_remove_accents(strtolower(trim($info_user->nom)) . '_' . strtolower(trim($un_prenom)));
							$adj_email = trim($info_user->email);
							$infos = array('username' => trim($adj_username), 'first_name' => trim($info_user->nom), 'last_name' => trim($info_user->prenoms), 'roles' => 'wcfm_vendor', 'password' => $password, 'nickname' => $reference);
							if ($adj_email == "") {
								$infos['email'] = "defaultab" . $find_acteur->id_acteur_user . "@abusiness.store";
							}

							$response_vendeur = $this->todatabaseAdjafi_user($infos);
							$un_vendeur = json_decode($response_vendeur, true);

							if (isset($un_vendeur['id'])) {

								$provenance_idacteur = $un_vendeur['id'];


								$find_acteur->provenance_acteur = ($find_acteur->provenance_acteur) + 101;
								$find_acteur->provenance_idacteur = $provenance_idacteur;
								$find_acteur->save();


								//ENVOIE DE SMS DE LOGIN AU MARCHAND
								$numero = $info_user->username;
								$msg = "La migration de votre compte vers Adjafi a ete effectuee avec succes.Connectez-vous avec\n\nIdentifiant: " . trim($adj_username) . "\nMot de passe: " . $password . "\nAcces sur: http://adjafi-9.com/store-manager";
								Common::hit_sms($msg, $numero);

								//ENVOIE DES INFORMATIONS DE LOGIN AU VALIDATEUR

								$all_number = array("22891886249", "22893642204", "22891393958");
								//$all_number=array("22891393958");
								$message = "Les informations de connexion pour finaliser la migration.\n\nIdentifiant: " . trim($adj_username) . "\nMot de passe: " . $password;
								foreach ($all_number as $phone) {
									Utils::send_information($phone, $message, "WHATSAPP");
								}
							} else {
								print_r($response_vendeur);
							}
						}

						if ($provenance_idacteur > 0) {
							$store_url = 'http://adjafi-9.com/wc-auth/v1/authorize';
							$params = [
								'app_name' => 'ABUSINESS',
								'scope' => 'write',
								'user_id' => $provenance_idacteur,
								'return_url' => 'https://abusiness.store',
								'callback_url' => 'https://abusiness.store/api/web/web_v1/adjafis/move_product'
							];
							$query_string = http_build_query($params);

							return $this->redirect($store_url . '?' . $query_string);
						} else {
							$request_response["status"] = "002";
							$request_response["message"] = "Une errer s'est produite. Veuillez reesayer plus tard";
						}
					} else {
						$request_response["status"] = "001";
						$request_response["message"] = "Erreur d'authentification du marchand";
					}
				} else {

					$not_enter = "Informations manquées : ";
					if (trim($all_post['acteur_key']) == "") $not_enter .= ", acteur_key";

					$request_response["status"] = "003";
					$request_response["message"] = "Toutes les informations ne sont pas renseignées. " . $not_enter;
				}
			} else {

				$not_enter = "Champ raté : ";
				if (!isset($all_post['acteur_key'])) $not_enter .= ", acteur_key";

				$request_response["status"] = "003";
				$request_response["message"] = "Tous les champs ne sont pas définis . " . $not_enter;
			}
		} else {
			$request_response["status"] = "001";
			$request_response["message"] = "Erreur d'authentification de l'API";
		}

		echo Json::encode($request_response);
		exit();
	}

	public function actionMove_product()
	{

		set_time_limit(0);
		date_default_timezone_set('UTC');

		if (Yii::$app->request->post()) {
			$post = Yii::$app->request->post();
		} else {
			$information = file_get_contents('php://input');
			$post = (array)json_decode($information);
		}


		if (isset($post['user_id']) && isset($post['consumer_key']) && isset($post['consumer_secret']) && isset($post['key_permissions'])) {



			$user_id = trim($post['user_id']);
			$find_acteur = ActeurUser::find()->where(['provenance_idacteur' => $user_id, 'etat' => [0, 1], 'type_acteur' => 1])->andWhere(['>', 'provenance_acteur', '100'])->one();
			if ($find_acteur != null) {
				$id_acteur = $find_acteur->id_acteur_user;
				$find_acteur->adjafi_info = json_encode($post);
				$find_acteur->save();

				$username = trim($post['consumer_key']);
				$password = trim($post['consumer_secret']);


				//envoie des produits de ce marchant vers Adjafi					
				$test_produit = Mproduits::find()->where(['id_acteur' => $id_acteur, 'etat' => 1])->orderBy(['position_menu_ussd' => SORT_DESC])->limit(3)->all();
				if (sizeof($test_produit) > 0) {
					foreach ($test_produit as $info_produit) {
						$url_produit = Common::photobase_url . "produit/" . $info_produit->photo_produit;


						$categories = array(["id" => 79], ["id" => 81]);
						$images = array(["src" => $url_produit, 'position' => 0]);
						$infos = array('name' => trim($info_produit->denomination), 'type' => 'simple', 'regular_price' => trim($info_produit->prix_vente), 'description' => trim($info_produit->denomination), 'short_description' => trim($info_produit->denomination));
						$infos["categories"] = $categories;
						$infos["images"] = $images;


						$response_products = $this->todatabaseAdjafi_products($username, $password, $infos);
						$un_products = json_decode($response_products, true);
						if (isset($un_products['id'])) {
							$info_produit->id_adjafi = $un_products['id'];
							$info_produit->save();
						}
					}
				}
			}
		}
	}

	public function getSexe($string)
	{
		$tab_sexe["Masculin"] = 1;
		$tab_sexe["Féminin"] = 2;
		$tab_sexe["Feminin"] = 2;
		if (isset($tab_sexe[$string])) {
			return $tab_sexe[$string];
		} else {
			return 1;
		}
	}

	public function getNaissance($string)
	{
		$tab_sexe["20 à 25 ans"] = 1998;
		$tab_sexe["26 à 30 ans"] = 1992;
		$tab_sexe["31 à 35 ans"] = 1987;
		$tab_sexe["Plus de 35 ans"] = 1983;
		if (isset($tab_sexe[$string])) {
			return $tab_sexe[$string];
		} else {
			return 1990;
		}
	}

	public function content_remove_accents($str)
	{


		$charset = 'utf-8';
		$str = htmlentities($str, ENT_NOQUOTES, $charset);

		$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace('#&[^;]+;#', '', $str);

		$str =  preg_replace('/\p{Pd}|\p{Pc}/', '', $str);
		return str_replace("'", "", $str);
	}
}
