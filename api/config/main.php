<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),    
    'bootstrap' => ['log'],
    'modules' => [
         'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],'web_v1' => [
            'basePath' => '@app/modules/website/v1',
            'class' => 'api\modules\website\v1\Module'
        ], 'school_v1' => [
            'basePath' => '@app/modules/geschool/v1',
            'class' => 'api\modules\geschool\v1\Module'
        ]
    ],
    'components' => [        
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/diplome',
                    'extraPatterns' => [
                        'POST diplomes' => 'diplomes',
                        'POST adddiplome' => 'adddiplome',
                        'POST diplome' => 'diplome',
                        'POST deletediplome' => 'deletediplome',
                    ],

                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/typeabsence',
                    'extraPatterns' => [
                        'POST typeabsences' => 'typeabsences',
                        'POST addtypeabsence' => 'addtypeabsence',
                        'POST typeabsence' => 'typeabsence',
                        'POST deletetypeabsence' => 'deletetypeabsence',
                    ],

                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/absence',
                    'extraPatterns' => [
                        'POST absences' => 'absences',
                        'POST addabsence' => 'addabsence',
                        'POST absence' => 'absence',
                        'POST deleteabsence' => 'deleteabsence',
                    ],

                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/mission',
                    'extraPatterns' => [
                        'POST missions' => 'missions',
                        'POST addmission' => 'addmission',
                        'POST mission' => 'mission',
                        'POST deletemission' => 'deletemission',

                        'POST detailmissions' => 'detailmissions',
                        'POST detailmission' => 'detailmission',
                        'POST adddetailmission' => 'adddetailmission',
                        'POST deletedetailmission' => 'deletedetailmission',

                    ],

                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/personnel',
                    'extraPatterns' => [
                        'POST personnels' => 'personnels',
                        'POST addpersonnel' => 'addpersonnel',
                        'POST personnel' => 'personnel',
                        'POST deletepersonnel' => 'deletepersonnel',
                    ],

                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/typepersonnel',
                    'extraPatterns' => [
                        'POST typepersonnels' => 'typepersonnels',
                        'POST addtypepersonnel' => 'addtypepersonnel',
                        'POST typepersonnel' => 'typepersonnel',
                        'POST deletetypepersonnel' => 'deletetypepersonnel',
                    ],
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/planaffectation',
                    'extraPatterns' => [
                        'POST planaffectations' => 'planaffectations',
                        'POST addplanaffectation' => 'addplanaffectation',
                        'POST planaffectation' => 'planaffectation',
                        'POST deleteplanaffectation' => 'deleteplanaffectation',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/typedecoupage',
                    'extraPatterns' => [
                        'POST typedecoupages' => 'typedecoupages',
                        'POST addtypedecoupage' => 'addtypedecoupage',
                        'POST typedecoupage' => 'typedecoupage',
                        'POST deletetypedecoupage' => 'deletetypedecoupage',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/anneescolaire',
                    'extraPatterns' => [
                        'POST anneescolaires' => 'anneescolaires',
                        'POST addanneescolaire' => 'addanneescolaire',
                        'POST anneescolaire' => 'anneescolaire',
                        'POST deleteanneescolaire' => 'deleteanneescolaire',
                        'POST clotureranneescolaire' => 'clotureranneescolaire',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/jourferie',
                    'extraPatterns' => [
                        'POST jourferies' => 'jourferies',
                        'POST addjourferie' => 'addjourferie',
                        'POST jourferie' => 'jourferie',
                        'POST deletejourferie' => 'deletejourferie',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/cours',
                    'extraPatterns' => [
                        'POST typecours' => 'typecours',
                        'POST cours' => 'cours',
                        'POST addcours' => 'addcours',
                        'POST cour' => 'cour',
                        'POST deletecours' => 'deletecours',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/activite',
                    'extraPatterns' => [
                        'POST categorieactivites' => 'categorieactivites',
                        'POST activites' => 'activites',
                        'POST addactivite' => 'addactivite',
                        'POST activite' => 'activite',
                        'POST deleteactivite' => 'deleteactivite',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/section',
                    'extraPatterns' => [
                        'POST sections' => 'sections',
                        'POST addsection' => 'addsection',
                        'POST section' => 'section',
                        'POST deletesection' => 'deletesection',
                    ],
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/specialite',
                    'extraPatterns' => [
                        'POST specialites' => 'specialites',
                        'POST addspecialite' => 'addspecialite',
                        'POST specialite' => 'specialite',
                        'POST deletespecialite' => 'deletespecialite',
                    ],
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/classe',
                    'extraPatterns' => [
                        'POST classes' => 'classes',
                        'POST addclasse' => 'addclasse',
                        'POST classe' => 'classe',
                        'POST deleteclasse' => 'deleteclasse',
                    ],
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/typeformation',
                    'extraPatterns' => [
                        'POST typeformations' => 'typeformations',
                        'POST addtypeformation' => 'addtypeformation',
                        'POST typeformation' => 'typeformation',
                        'POST deletetypeformation' => 'deletetypeformation',
                    ],
                ],
				
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/salle',
                    'extraPatterns' => [
                        'POST salles' => 'salles',
                        'POST addsalle' => 'addsalle',
                        'POST salle' => 'salle',
                        'POST deletesalle' => 'deletesalle',
                    ],
                ],

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/ressource',
                    'extraPatterns' => [
                        'POST etatusages' => 'etatusages',
                        'POST ressources' => 'ressources',
                        'POST addressource' => 'addressource',
                        'POST ressource' => 'ressource',
                        'POST deleteressource' => 'deleteressource',
                    ],
                ],
				
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'school_v1/decoupage',
                    'extraPatterns' => [
                        'POST decoupages' => 'decoupages',
                        'POST adddecoupage' => 'adddecoupage',
                        'POST decoupage' => 'decoupage',
                        'POST deletedecoupage' => 'deletedecoupage',
                    ],
                ],
            ],
        ]
    ],
    'params' => $params,
];