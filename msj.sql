/*17-05-2021*/
create table module(
  id int(11) NOT NULL AUTO_INCREMENT,
  libelle_module varchar(255) NOT NULL,
  etat int(1) NOT NULL DEFAULT 1,
  date_create timestamp NOT NULL DEFAULT current_timestamp(),
  created_by int(11) NOT NULL,
  date_update timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  updated_by int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MYISAM  DEFAULT CHARSET=utf8;


insert into module (libelle_module, etat, created_by)
	values( 'Paramétrage système', 1, 1),
		( 'Gestion des Ressources Humaines', 1, 1),
		( 'Gestion Pédagogique', 1, 1),
		( 'Gestion Comptable', 1, 1),
		( 'Gestion du Stock', 1, 1),
		( 'Gestion de Maintenance et Inventaire', 1, 1),
		( 'Gestion de Production', 1, 1);

alter table access_right add idmodule int(11);

alter table access_right add constraint fk_access_right_module foreign key (idmodule)
      references school_center (module) on delete restrict on update restrict;

update access_right set idmodule = 1 where id_right in(4,5);
update access_right set idmodule = 2 where id_right not in(4,5);

alter table access_right change idmodule idmodule int(11) not null;

/*18-05-2021*/
create table planaffectation(
  id int(11) NOT NULL AUTO_INCREMENT,
  keyplanaffectation char(32) not null,
   iduser bigint not null,
   idcenter int(11) not null,
   datedebutaffectation    date not null,
   datedebuteffective    date ,
   datefinaffectation       date not null,
   datedfineffective    date ,
   status int(6) NOT NULL DEFAULT 1,
  created_at timestamp NOT NULL DEFAULT current_timestamp(),
  created_by int(11) NOT NULL,
  updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  updated_by int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MYISAM  DEFAULT CHARSET=utf8;


alter table planaffectation add constraint fk_planaffectation_user foreign key (iduser)
      references personnel (iduser) on delete restrict on update restrict;

alter table planaffectation add constraint fk_planaffectation_centre foreign key (idcenter)
      references personnel (id_center) on delete restrict on update restrict;

/*19-05-2021*/
create table typeformation
(
   id                int not null auto_increment,
   libelletypeformation           varchar(254),
   keytypeformation               char(32),
   status int(6) NOT NULL DEFAULT 1,
	created_at int(11) NOT NULL,
	updated_at int(11) DEFAULT NULL,
	create_by int(11) NOT NULL,
	 updated_by int(11) DEFAULT NULL,
   primary key (id)
)ENGINE=MYISAM DEFAULT CHARSET=utf8;

/*20-05-2021*/
create table typecours
(
   id                int not null auto_increment,
   libelletypecours           varchar(254),
   keytypecours               char(32),
   status int(6) NOT NULL DEFAULT 1,
	created_at int(11) NOT NULL,
	updated_at int(11) DEFAULT NULL,
	create_by int(11) NOT NULL,
	 updated_by int(11) DEFAULT NULL,
   primary key (id)
)ENGINE=MYISAM DEFAULT CHARSET=utf8;

insert into typecours ( libelletypecours, keytypecours, status, created_at,  create_by)
	values( 'Matières de Spécialité', '0Goiz0ydot8zQoeOjXnpH4xEsLxlzRez', 1, 1621440689 , 0);

insert into typecours ( libelletypecours, keytypecours, status, created_at,  create_by)
	values( 'Matières générales', '1Roiz0ydot0gQoeOjXnpH4xEsLxlaGhte', 1, 1621440688 , 0);

alter table cours change typecours idtypecours int(11);

alter table cours add constraint fk_cours_type foreign key (idtypecours)
      references typecours (id) on delete restrict on update restrict;


alter table mission add idcentre int(11) not null;
alter table mission add constraint fk_mission_centre foreign key (idcentre)
      references school_center (id_center) on delete restrict on update restrict;
	  
	  
/*25-05-2021*/
alter table departement rename to specialite;

alter table specialite change  libelledepartement libellespecialite varchar(254) not null;
alter table specialite change  keydepartement  keyspecialite char(32) not null;

alter table specialite drop FOREIGN KEY fk_departement_section;
alter table specialite drop index fk_departement_section;

alter table specialite add constraint fk_specialite_section foreign key (idsection)
      references section (id) on delete restrict on update restrict;
	  
/*26-05-2021*/
create table sectionformation
(
   id                int not null auto_increment,
   keysectionformation               char(32),
   idsection int not null, 
   idtypeformation int not null, 
   status int(6) NOT NULL DEFAULT 1,
	created_at int(11) NOT NULL,
	updated_at int(11) DEFAULT NULL,
	create_by int(11) NOT NULL,
	 updated_by int(11) DEFAULT NULL,
   primary key (id)
)ENGINE=MYISAM DEFAULT CHARSET=utf8;


alter table sectionformation add constraint fk_sectionformation_s foreign key (idsection)
      references section (id) on delete restrict on update restrict;

alter table sectionformation add constraint fk_sectionformation_t foreign key (idtypeformation)
      references typeformation (id) on delete restrict on update restrict;
	  
/*27-05-2021*/
alter table personnel add iddiplome int not null;
alter table personnel add autrecompetence text;
alter table personnel add constraint fk_personnel_diplome foreign key (iddiplome)
      references diplome (id) on delete restrict on update restrict;
	  
alter table jourferie drop column centreconcerne;

/*31-05-2021*/
create table etatusage
(
   id                int not null auto_increment,
   libelle          varchar(254),
   keyetatusage               char(32),
   idcentre       int(11),
   status int(6) NOT NULL DEFAULT 1,
	created_at int(11) NOT NULL,
	updated_at int(11) DEFAULT NULL,
	create_by int(11) NOT NULL,
	 updated_by int(11) DEFAULT NULL,
   primary key (id)
)ENGINE=MYISAM DEFAULT CHARSET=utf8;

insert into etatusage ( libelle, keyetatusage,idcentre, status, created_at,  create_by)
	values( 'Bon', '0Goiz0ydot8zQoeOjXnpH4xEsLxlzRez',2, 1, 1621440689 , 0);

insert into etatusage ( libelle, keyetatusage,idcentre, status, created_at,  create_by)
	values( 'Mauvais', '1Roiz0ydot0gQoeOjXnpH4xEsLxlaGhte',2, 1, 1621440689 , 0);

insert into etatusage ( libelle, keyetatusage,idcentre, status, created_at,  create_by)
	-- values( 'Défectueux', '1Roiz0ydot0gR9yjXnpH4xEsLxlaGhte',2, 1, 1621440689 , 0);

alter table ressource change etatusage idetatusage int(11);

alter table ressource add constraint fk_ressource_etat foreign key (idetatusage)
      references etatusage (id) on delete restrict on update restrict;

alter table categorieactivite add idcentre int(11) not null;
alter table categorieactivite add constraint fk_categ_activite_centre foreign key (idcentre)
      references school_center (id_center) on delete restrict on update restrict;

update categorieactivite set idcentre = 2;

/*01-06-2021*/
alter table jourferie add idcentre int(11) not null;
alter table jourferie add constraint fk_jourferie_centre foreign key (idcentre)
      references school_center (id_center) on delete restrict on update restrict;
	  
alter table decoupage change idanneescolaire idannee int(11) not null;

        
alter table decoupage add idannee int(11) not null;
alter table typedecoupage add constraint fk_decoupage_annee foreign key (idannee)
      references anneescolaire (id) on delete restrict on update restrict;

alter table decoupage add idcentre int(11) not null;
alter table decoupage add constraint fk_decoupage_centre foreign key (idcentre)
      references school_center (id_center) on delete restrict on update restrict;
	  
alter table typedecoupage add idcentre int(11) not null;
alter table typedecoupage add constraint fk_typedecoupage_centre foreign key (idcentre)
      references school_center (id_center) on delete restrict on update restrict;

update typedecoupage set idcentre = 2;


/*02-06-2021*/
INSERT INTO `access_right` (`id_right`, `key_right`, `lib_right`, `etat_right`, `date_create`, `created_by`, `date_update`, `updated_by`, `idmodule`) VALUES (NULL, '', 'manage_decoupage', '1', '2021-05-26 09:02:43', '0', '2021-05-26 10:31:21', NULL, '2')

