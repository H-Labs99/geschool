<?php


return array(		
	'WELCOME_GOAGRI' => 'Bienvenue sur <b>ABusiness</b>, votre Assistant e-commerce des produits de 1ère nécessité',
	'SUGES_MESSAGE' => 'Faites passer votre Business en ligne avec ABusiness. Aucune  installation requise si vous avez déjà whatsApp ou Telegram. Ecrivez juste au +22891047373 sur whatsapp ou directement en cliquant sur le lien ci-dessous (Telegram) et en envoyant automatiquement le code contenu dans votre champ de saisie.',
	
	'NUMERO_FRIEND_SUGERE' => 'Suggerez <b>ABusiness</b> au numéro :',
	'share_resultat2' => 'Je viens de faire mon test d’auto diagnostic chez <b>AB sur whatsApp</b>.', 
	'cancel_operation' => 'Votre interaction avec le chatbot <b>AB</b> a été annulée avec succès. Merci',
	'name_souscription' => 'Souscription à <b>AB</b> sous le nom ',
	'share_resultat4' => 'https://api.whatsapp.com/send?phone=22891047373&text=Go+Agri',
	


	'WELCOME_GONSIA' => 'Bienvenue sur GO NSIA de NSIA ASSURANCE, le chatBot de sensibilisation en riposte au COVID-19',
	'SUGES_MESSAGENSIA' => 'vous invite à utiliser le service d\'information sur COVID-19 . Restons chez nous et sauvons nos vies. Envoyez Go NSIA au numéro 22870523434 sur whatsapp ou cliquez su le lien : https://wa.me/22870523434?text=GoNSIA',
	'NUMERO_FRIEND_SUGERE_NSIA' => 'Suggerez GO NSIA au numéro :',
	'cancel_operation_NSIA' => 'Votre interaction avec le chatbot GO NSIA a été annulée avec succès. Merci',
	'name_souscription_NSIA' => 'Souscription à GO NSIA sous le nom ',
	'share_resultat2_NSIA' => 'Je viens de faire mon test d’auto diagnostic chez <b>Go NSIA sur whatsApp</b>.', 
	'share_resultat4_NSIA' => 'https://api.whatsapp.com/send?phone=22870523434&text=Go+NSIA',
	
	
	'WELCOME_GOPSA' => 'Bienvenue sur GO PSA de LA PROTECTRICE SA, le chatBot de sensibilisation en riposte au COVID-19',
	'SUGES_MESSAGEPSA' => 'vous invite à utiliser le service d\'information sur COVID-19 . Restons chez nous et sauvons nos vies. Envoyez Go PSA au numéro 22870523434 sur whatsapp ou cliquez su le lien : https://wa.me/22870523434?text=GoPSA',
	'NUMERO_FRIEND_SUGERE_PSA' => 'Suggerez GO PSA au numéro :',
	'cancel_operation_PSA' => 'Votre interaction avec le chatbot GO PSA a été annulée avec succès. Merci',
	'name_souscription_PSA' => 'Souscription à GO PSA sous le nom ',
	'share_resultat2_PSA' => 'Je viens de faire mon test d’auto diagnostic chez <b>Go PSA sur whatsApp</b>.', 
	'share_resultat4_PSA' => 'https://api.whatsapp.com/send?phone=22870523434&text=Go+PSA',

	
	'WELCOME_GOORABANK' => 'Bienvenue sur GO ORABANK de ORABANK, le chatBot de sensibilisation en riposte au COVID-19',
	'SUGES_MESSAGEORABANK' => 'vous invite à utiliser le service d\'information sur COVID-19 . Restons chez nous et sauvons nos vies. Envoyez Go ORABANK au numéro 22870523434 sur whatsapp ou cliquez su le lien : https://wa.me/22870523434?text=GoORABANK',
	'NUMERO_FRIEND_SUGERE_ORABANK' => 'Suggerez GO ORABANK au numéro :',	
	'cancel_operation_ORABANK' => 'Votre interaction avec le chatbot GO ORABANK a été annulée avec succès. Merci',
	'name_souscription_ORABANK' => 'Souscription à GO ORABANK sous le nom ',
	'share_resultat4_ORABANK' => 'https://api.whatsapp.com/send?phone=22870523434&text=Go+ORABANK', 
	'share_resultat2_ORABANK' => 'Je viens de faire mon test d’auto diagnostic chez <b>Go ORABANK sur whatsApp</b>.', 
	
	
	'WELCOME_GOGTA' => 'Bienvenue sur GO GTA de GTA ASSURANCE, le chatBot de sensibilisation en riposte au COVID-19',
	'SUGES_MESSAGEGTA' => 'vous invite à utiliser le service d\'information sur COVID-19 . Restons chez nous et sauvons nos vies. Envoyez Go GTA au numéro 22870523434 sur whatsapp ou cliquez su le lien : https://wa.me/22870523434?text=GoGTA',
	'NUMERO_FRIEND_SUGERE_GTA' => 'Suggerez GO GTA au numéro :',
	'cancel_operation_GTA' => 'Votre interaction avec le chatbot GO GTA a été annulée avec succès. Merci',
	'name_souscription_GTA' => 'Souscription à GO GTA sous le nom ',
	'share_resultat2_GTA' => 'Je viens de faire mon test d’auto diagnostic chez <b>Go GTA sur whatsApp</b>.', 
	'share_resultat4_GTA' => 'https://api.whatsapp.com/send?phone=22870523434&text=Go+GTA',
	
	
	'WELCOME_GOSAHAM' => 'Bienvenue sur GO SAHAM de SAHAM ASSURANCE, le chatBot de sensibilisation en riposte au COVID-19',
	'NUMERO_FRIEND_SUGERE_SAHAM' => 'Suggerez GO SAHAM au numéro :',
	'SUGES_MESSAGESAHAM' => 'vous invite à utiliser le service d\'information sur COVID-19 . Restons chez nous et sauvons nos vies. Envoyez Go SAHAM au numéro 22870523434 sur whatsapp ou cliquez su le lien : https://wa.me/22870523434?text=GoSAHAM',
	'cancel_operation_SAHAM' => 'Votre interaction avec le chatbot GO SAHAM a été annulée avec succès. Merci',
	'name_souscription_SAHAM' => 'Souscription à GO SAHAM sous le nom ',
	'share_resultat2_SAHAM' => 'Je viens de faire mon test d’auto diagnostic chez <b>Go SAHAM sur whatsApp</b>.',
	'share_resultat4_SAHAM' => 'https://api.whatsapp.com/send?phone=22870523434&text=Go+SAHAM',
	
	'WELCOME_GOFIDELIA' => 'Bienvenue sur GO FIDELIA de FIDELIA ASSURANCE, le chatBot de sensibilisation en riposte au COVID-19',
	'NUMERO_FRIEND_SUGERE_FIDELIA' => 'Suggerez GO FIDELIA au numéro :',
	'SUGES_MESSAGEFIDELIA' => 'vous invite à utiliser le service d\'information sur COVID-19 . Restons chez nous et sauvons nos vies. Envoyez Go FIDELIA au numéro 22870523434 sur whatsapp ou cliquez su le lien : https://wa.me/22870523434?text=GoFIDELIA',
	'cancel_operation_FIDELIA' => 'Votre interaction avec le chatbot GO FIDELIA a été annulée avec succès. Merci',
	'name_souscription_FIDELIA' => 'Souscription à GO FIDELIA sous le nom ',
	'share_resultat2_FIDELIA' => 'Je viens de faire mon test d’auto diagnostic chez <b>Go FIDELIA sur whatsApp</b>.',
	'share_resultat4_FIDELIA' => 'https://api.whatsapp.com/send?phone=22870523434&text=Go+FIDELIA',
	
	
	
	'HELP' => 'Aide',
	'tape' => '<b>Tapez</b>',
	'HAVE_HELP' => 'Avoir de l\'aide sur',
	'MENU_PRINCIPAL' => 'Menu principal',
	'MENU_QUESTION' => 'Liste des questions',
	'MENU_INFO' => 'Liste des fausses informations',
	'NEXT' => 'Suivant',
	'PREVIOR' => 'Précédent',
	'ERROR' => 'Erreur',
	'CONFIRM' => 'Confirmer',
	'CONTINUER' => 'Valider la commande',
	'MODIFIER' => 'Modifier la commande',
	'CANCEL_CMD' => 'Annuler la commande',
	'CANCEL' => 'Annuler',
	'RETOUR' => 'Retour',
	'TRY_LATER' => 'Une erreur s\'est produite. Veuillez réesayer plus tard. Merci',
	'ACCOUNT_NOTVALID' => 'Désolé, vous n\'avez pas encore validé votre compte.',
	'FALSE_INFORMATION' => 'Les informations fournies sont incorrectes',
	'NUMERO_FRIEND' => 'Entrez le numéro de la personne au format international(ex: 22891393958)',
	'SUGGESTION_SUCCESS' => 'Message de suggestion envoyé avec succès',
	'resume_commande' => '<b>Résumé des produits commandés:</b>',
	'resumecommande' => '<b>Résumé de la commande</b>',
	'ERROR_NUMERO' => 'Désolé, vous ne pouvez pas faire une recommandation à vous même',
	
	
	
	
	'cancel_souscription' => 'Votre demande de souscription a été annulée avec succès. Merci',
	'succes_souscription' => 'Inscription effectuée avec succès',
	'enter_nom' => 'Entrez votre Nom de famille',
	'enter_prenom' => 'Entrez votre prénom(s)',
	
	'ask_soucription' => 'S\'inscrire',
	'cancel_process' => 'Annuler le processus',
	'place_pulverisation' => 'Précisez le lieu à pulvériser',
	'place_estimation' => 'Précisez l\'estimation de la surface (ex:5 hectares)',
	'place_contact' => 'Précisez un autre contact sur lequel vous êtes joignable.',
	'place_pulverisation_tense1' => 'Réservation de drone pour pulvériser une place à ',
	'place_pulverisation_tense2' => 'surface de pulvérisation estimée à',
	'place_pulverisation_tense3' => 'Autre contact :',
	
	'admin_pulverisation_tense1' => 'Votre demande de pulvérisation par drone pour désinfecter un lieu situé à  ',
	'admin_pulverisation_tense2' => ' pour une superficie totale estimée à ',
	'admin_pulverisation_tense3' => '  a été prise en compte. Un opérateur de E-AGRIBUSINESS vous contactera par téléphone pour organiser la séance de traitement.',
	'admin_pulverisation_tense4' => 'Réservation effectuée par :',
	
	'select_produit' => 'Sélectionnez le produit à commander',
	'select_qte' => 'Sélectionnez la quantité à commander',
	'select_question' => 'Sélectionnez la question',
	'select_info' => 'Sélectionnez l\'information à verifier',
	
	'produit_selected' => 'Commande du produit ',
	'qte_selected' => ', quantité commandée ',
	'success_command' => 'Votre commande a été prise en compte. Un opérateur de <b>ABUSINESS</b> vous contactera par téléphone pour organiser la livraison.',
	'success_demande' => 'Votre demande a été prise en compte. Un opérateur de <b>ABUSINESS</b> vous contactera par téléphone dans les plus brefs délais.',
	'commanded_by' => 'Commande effectuée par :',
	'demanded_by' => 'Demande effectuée par :',
	'asked_by' => 'Demande effectuée par :',
	'select_package' => 'Sélectionnez le package à commander',
	'deno_package' => ' Package ',
	'package_selected' => 'Commande du package ',
	'exist_acteur' => 'Désolé un acteur avec les mêmes informations est déjà lié à votre compte dans notre systeme',
	'parler_operateurWHATSAPP' => 'Veuillez cliquer sur le lien suivant pour discuter directement avec un opérateur https://wa.me/22892929285',
	'parler_operateurTELEGRAM' => 'Veuillez cliquer sur le lien suivant pour discuter directement avec un opérateur https://t.me/ABusiness_Togo',
	'parler_operateurWEBSITE' => 'Veuillez appeller le numéro suivant pour discuter directement avec un opérateur: (228) 92929285',
	'send_operateur1' => 'Un client souhaiterait discuter avec vous. Veuillez le contacter en cliquant sur le lien https://wa.me/',
	'send_operateur2' => 'Un client souhaiterait discuter avec vous. Veuillez le contacter en cliquant sur le lien https://t.me/',
	
	'oui' => 'Oui',	
	'non' => 'Non',
	'jesaispas' => 'Je ne sais pas',
	'no_applicable' => 'Non applicable',
	'age_diagnostique' => 'Prenez contact avec votre médecin généraliste au moindre doute. Cette application n’est pour l’instant pas adaptée aux personnes de moins de 15 ans. En cas d’urgence, <b>appelez le 111.</b>',
	
	'question_1' => '<b>1/20:</b> Pensez-vous avoir eu de la fièvre ces derniers jours (frissons, sueurs) ?',
	'question_100' => '<b>1bis/20:</b> Quelle est votre température corporelle la plus élevée ?(ex: 38)',
	'question_2' => '<b>2/20:</b> Avez-vous <b>une toux ou une augmentation de votre toux</b> habituelle ces derniers jours ?',
	'question_3' => '<b>3/20:</b> Avez-vous noté <b>une forte diminution de votre gout ou de l’odorat</b> ces derniers jours ?',
	'question_4' => '<b>4/20:</b> Avez-vous un <b>mal de gorge</b> ou des <b>douleurs musculaires</b> ou des <b>courbatures</b> inhabituelles ces derniers jours ?',	
	'question_5' => '<b>5/20:</b> Avez-vous de la <b>diarrhée</b> ces dernièrs 24 heures (au moins 3 selles molles) ?',
	'question_6' => '<b>6/20:</b> Avez-vous une <b>fatigue inhabituelle</b> ces derniers jours ?',
	'question_600' => '<b>6bis/20:</b> Cette fatigue vous oblige-t-elle à vous reposer plus de la moitié de la journée ?',
	'question_7' => '<b>7/20:</b> Êtes-vous dans l\'impossibilité de vous alimenter ou de boire <b>DEPUIS 24 HEURES OU PLUS</b> ?',
	'question_8' => '<b>8/20:</b> Dans les dernières 24 heures, avez-vous noté un <b>manque de souffle INHABITUEL</b> lorsque vous parlez ou faites un petit effort ?',
	'question_9' => '<b>9/20:</b> Quel est votre âge ? Ceci, afin de calculer un facteur de risque spécifique (ex: 28).',
	'question_10' => '<b>10/20:</b> Quel est votre <b>poids</b> en kilogrammes (ex: 84) ? Afin de calculer l’indice de masse corporelle qui est un facteur influençant le risque de complications de l’infection.',
	'question_11' => '<b>11/20:</b> Quel est votre <b>taille</b> en centimetres (ex: 175) ? Afin de calculer l’indice de masse corporelle qui est un facteur influençant le risque de complications de l’infection.',
	'question_12' => '<b>12/20:</b> Avez-vous de <b>l’hypertension artérielle</b> mal équilibrée ? Ou avez-vous une <b>maladie cardiaque ou vasculaire</b> ? Ou prenez-vous un <b>traitement à visée cardiologique</b> ?',
	'question_13' => '<b>13/20:</b> Êtes-vous diabétique ?',
	'question_14' => '<b>14/20:</b> Avez-vous ou avez-vous eu un <b>cancer</b> ?',
	'question_15' => '<b>15/20:</b> Avez-vous une <b>maladie respiratoire</b> ? Ou êtes-vous <b>suivi par un pneumologue</b> ?',
	'question_16' => '<b>16/20:</b> Avez-vous une <b>insuffisance rénale chronique dialysée</b> ?',
	'question_17' => '<b>17/20:</b> Avez-vous une <b>maladie chronique du foie</b> ?',
	'question_18' => '<b>18/20:</b> Êtes-vous <b>enceinte</b> ?',
	'question_19' => '<b>19/20:</b> Avez-vous une maladie connue pour diminuer vos défenses immunitaires ?',
	'question_20' => '<b>20/20:</b> Prenez-vous un <b>traitement immunosuppresseur</b> ? C’est un traitement qui diminue vos défenses contre les infections. Voici quelques exemples : corticoïdes, méthotrexate, ciclosporine, tacrolimus, azathioprine, cyclophosphamide (liste non exhaustive). ',
	
	'resulat_diagnostic00' => 'Malheureusement, <b>nous n\'avons pas assez de donnée pour vous réaliser un autodiagnostic</b>. Prenez contact avec votre médecin généraliste au moindre doute️. En cas d’urgence, <b>appelez le 111</b>',
	'resulat_diagnostic000' => 'Pour vous protéger et protéger les autres, dans la mesure du possible, <b>restez chez vous</b> et limitez les contacts avec d\'autres personnes.',
	'resulat_diagnostic1' => 'Nous vous conseillons de rester à votre domicile et de contacter votre médecin en cas d’apparition de nouveaux symptômes. Vous pourrez aussi utiliser à nouveau l’application pour réévaluer vos symptômes.',
	'resulat_diagnostic2' => 'Nous vous conseillons la téléconsultation ou médecin généraliste ou visite à domicile (SOS médecins…)',
	'resulat_diagnostic3' => 'Nous vous conseillons la téléconsultation ou médecin généraliste ou visite à domicile',	
	'resulat_diagnostic4' => 'Nous vous conseillons la consultation d\'un médecin généraliste ou la téléconsultation et si pas possible de le joindre ou téléconsultation , <b>appelez le
111</b> si une gêne respiratoire ou des difficultés importantes pour s’alimenter ou boire pendant plus de 24h apparaissent.',
	'resulat_diagnostic5' => '<b>Appelez le 111</b> si une gêne respiratoire ou des difficultés importantes pour s’alimenter ou boire pendant plus de 24h apparaissent.',
	'resulat_diagnostic6' => 'Votre situation ne relève probablement pas du Covid-19. Consultez votre médecin au moindre doute. Si de nouveaux symptomes apparaissent, refaites le test',	
	'resulat_diagnostic7' => 'Votre situation ne relève probablement pas du Covid-19. Un avis médical est recommandé. Au moindre doute, <b>appelez le 111</b>',	
	'resulat_diagnostic8' => 'Votre situation ne relève probablement pas du Covid-19. N’hésitez pas à contacter votre médecin en cas de doute. Vous pouvez refaire le test en cas de nouveau symptôme pour réévaluer la situation.',	
	'resulat_diagnostic0' => '<b>Rappel:</b> Restez chez vous au maximum en attendant que les symptômes disparaissent.Prenez votre température deux fois par jour. Rappel des mesures d’hygiène.',
	'share_resultat1' => 'Mon nom est ',
	'share_resultat3' => '<b>C’est super.</b>

Je vais <b>bien</b>,  je ne suis pas atteint et je ne souffre d’aucun symptôme du <b>Corona virus</b> à la  vue des informations fiables que j’ai renseignées.

Je vous  conseille vivement  de le faire  et de le partager autour de vous.

<b>Respectons les gestes barrières</b>

<b>Pour commencer votre auto diagnostic au Corona Virus sur whatsApp c’est ici :</b>',
	'share_resultat5' => '<b>Partagez la bonne nouvelle avec vos relations et proches.</b>',	
	'SELECT_ACTEUR' => 'Sélectionnez votre type d\'acteur',	
	'acteur1' => '<b>Marchand:</b> Vous disposez d\'un magasin,boutique superette ou super marché',
	'acteur2' => '<b>Livreur:</b> Vous disposez d\'un moyen de déplacement pouvant servir dans la livraison des produits',
	
	'nom_boutique' => 'Donnez  un nom à votre commerce.',
	'nbnom_boutique' => '( <b>NB:</b> Vous n\'êtes pas obligé d\'avoir une boutique / supermarché physique formel avant de vendre ici )',
	'adresse_boutiqueold' => 'Entrez l\'adresse de votre boutique/magasin/superette ou super marché',
	'adresse_boutique' => 'Entrez l\'adresse de: ',
	'description_activite' => 'Decrivez votre activité en <b>150 caractères</b> maximum',
	'localisation_boutiqueold' => 'Envoyez la position de géolocalisation de votre boutique/magasin/superette ou super marché <b>avec une précision de ( mètres maximum</b>',
	'localisation_boutique1' => 'Envoyez la position de géolocalisation de: ',
	'localisation_user' => 'Envoyez votre position de géolocalisation <b>Avec une précision de 25 mètres maximum</b>',
	'localisation_boutique2' => '<b>Avec une précision de 25 mètres maximum</b>',
	'autre_numero' => 'Précisez un autre numéro sur le quel on peut vous joindre',
	
	'type_moyendeplacement' => 'Précisez le ou les moyens de déplacements que vous allez utiliser pour les livraisons',
	'adresse_livreur' => 'Entrez  l\'adresse de votre lieu de résidence',
	'localisation_livreur' => 'Envoyez la position de géolocalisation de votre lieu de résidence <b>avec une précision de 25 mètres maximum</b> (ceci pour pouvoir vous envoyer les colis les plus proche de vous)',
	'localisation_livraison' => 'Envoyez la position de géolocalisation du lieu de livraison <b>avec une précision de 25 mètres maximum</b> (ceci pour pouvoir aider le livreur et à vous afficher les frais de livraison)',
	'autre_numero' => 'Précisez un autre numéro sur le quel on peut vous joindre',
	
	
	'demande_creation' => 'Demande de création d\'un compte ',
	'name_boutique' => '<b>Dénomination:</b> ',
	'moyendeplacement' => '<b>Moyen de transport:</b> ',
	'adresseboutique' => '<b>Adresse lieu:</b> ',
	'entrer_lieulivraison' => '<b>Precisez le lieu de livraison</b> ',
	'adresselivreur' => '<b>Lieu de résidence:</b> ',
	'autre_contact' => '<b>Autre contact:</b> ',
	'help_location0' => '<b>Comment envoyer votre position de géolocalisation ?</b>
	
- Appuyer sur l\'icône pièce jointe, comme pour envoyer un fichier.
- Dans « Localisation », une nouvelle option est apparue !
- Cliquer sur « Envoyer votre position actuelle » . La précision conseillée avant envoi est entre 5 et 25 mètres',

	'help_location1' => '<b>Comment envoyer votre position de géolocalisation ?</b>
	
- Dans un premier temps,rendez vous sur le lieu en question (votre boutique/magasin/superette ou super marché)
- Appuyer sur l\'icône pièce jointe, comme pour envoyer un fichier.
- Dans « Localisation », une nouvelle option est apparue !
- Cliquer sur « Envoyer votre position actuelle » . La précision conseillée avant envoi est entre 5 et 25 mètres',

	'help_location3' => '<b>Comment envoyer la position de géolocalisation:</b>
	
- Dans un premier temps,rendez vous sur le lieu de livraison
- Appuyer sur l\'icône pièce jointe, comme pour envoyer un fichier.
- Dans « Localisation », une nouvelle option est apparue !
- Cliquer sur « Envoyer votre position actuelle » . La précision conseillée avant envoi est entre 5 et 25 mètres',

	'help_location2' => 'Comment envoyer votre position de géolocalisation ?
- Dans un premier temps,rendez vous devant votre domicile ou votre lieu de stationnement
- Appuyer sur l\'icône pièce jointe, comme pour envoyer un fichier.
- Dans « Localisation », une nouvelle option est apparue !
- Cliquer sur « Envoyer votre position actuelle » . La précision conseillée avant envoi est entre 5 et 25 mètres',
	'entrer_codemarchand' => 'Je connais mon marchand ',
	'entrer_marchand1' => 'Je connais le code du marchand',
	'entrer_marchand2' => 'Les marchands de mon entourage',
	'entrer_marchand3' => 'Recherche de produit',
	'entrer_marchand4' => 'La foire <b>ADJAFI</b>',
	
	'entrer_codemarchand' => '<b>Veuillez taper le code du marchand</b> ',
	'error_reference' => 'Code référence marchand erroné ',
	'help_marchand' => 'Veuillez taper le numéro des produits à commander en les séparant par des <b>virgules</b> .Pour ajouter plusieurs quantités faire Numéro * quantité (Exemple : <b>1*2</b> , <b>2*3</b> )',
	'select_marchand' => 'Veuillez sélectionner le numéro d\'un marchand',
	'no_produits' => 'Désolé, la liste des produits de ce marchand est vide. Veuillez réesayer plus tard. Merci',
	'bno_produits' => 'Désolé, la liste des produits de votre boutique est vide. Veuillez ajouter vos produits',
	'entrer_infosupp' => 'Ajoutez des informations supplémentaires pour la commande',
	'no_marchand1' => 'Désolé, aucun marchand utilisant notre solution n\'est proche de votre position géographique dans les ',
	'no_marchand2' => 'Vous pourrez inviter des marchands à rejoindre notre plateforme en leur fournissant le lien d\'inscription suivant : https://wa.me/22891047373 ',
	'no_marchand3' => 'Si vous souhaitez discuter directement avec un opérateur du service clientèle c\'est ici : https://wa.me/22892929285 ',
	
	
	'SELECT_BOUTIQUE' => 'Sélectionnez votre type d\'operation',	
	'boutique1' => '<b>Voir mes produits:</b> Vous avez la possibilité de consulter, supprimer ou modifier les informations des produits de votre boutique',
	'boutique2' => '<b>Ajouter mes produits:</b> Vous avez la possibilité d\'ajouter vos produits et de definir le prix de vente',
	'boutique3' => '<b>Transactions en cours:</b> Vous avez la possibilité de voir la liste des 10  commandes en cours de vos clients',
	'boutique4' => '<b>Transactions finalisées:</b> Vous avez la possibilité de voir la liste des commandes finalisées de vos clients (10)',
	'bdenomination_produit' => 'Entrez le nom du produit',	
	'bunite_produit' => 'Entrez l\'unité de mesure du produit Ex: Kg,L ...',	
	'bprix_produit' => 'Entrez le prix de vente unitaire',	
	'bqte_produit' => 'Entrez la quantité minimale possible que vous vendez',	
	'bphoto_produit' => 'Envoyez une photo du produit',	
	'UPDATE' => 'Modifier',	
	'UPDATEINFO' => 'Modifier les info du produit',	
	'UPDATEPHOTO' => 'Modifier la photo du produit',
	'DELETE' => 'Supprimer',	
	'DELETEPRODUIT' => 'Supprimer le produit',	
	'MPRODUITS' => 'Liste de mes produits',	
	'success_publication' => 'Votre produit a été publié avec succès. Il est déjà disponible dans votre boutique sur notre plateforme et sur tous les reseaux sociaux actifs',
	'MABOUTIQUE' => 'Ma boutique',	
	'CONTINUE_PUBLICATION' => 'Publiez un autre produit',	
	'help_bmarchand' => 'Veuillez taper le numéro du produit à visiualiser,supprimer ou modifier',
	
	
	'ubdenomination_produit' => 'Entrez le nouveau nom du produit',	
	'ubunite_produit' => 'Entrez l\'unité de mesure du produit Ex: Kg,L ...',	
	'ubprix_produit' => 'Entrez le prix de vente unitaire',	
	'ubqte_produit' => 'Entrez la quantité minimum de vente',
	'ubphoto_produit' => 'Envoyez la nouvelle photo du produit',	
	'produit_max_atteint' => 'Le nombre maximum de produit autorisé est atteint',	
	
	'SELECT_TYPEACTEUR' => 'Sélectionnez le type de CGU',
	'cgu_acteur1' => 'CGU Client',
	'cgu_acteur2' => 'CGU Livreur',
	'cgu_acteur3' => 'CGU Marchand',
	
'empty_attentecommande' => 'Désolé, votre liste des commandes en cours est vide',
'empty_finishcommande' => 'Désolé, votre liste des commandes finalisées est vide',
'finishcommande' => 'Liste des commandes finalisées',
'attentecommande' => 'Liste des commandes en cours',
'poids_unitaire' => 'Poids de vente unitaire en Kg(utiliser le point pour les nombres décimaux)',
'dispose_location' => 'Êtes-vous sur le lieu de livraison ou pouvez-vous nous envoyer les coordonnées de geolocalisation du lieu de livraison ?',
'oui1_location' => 'Je suis sur le lieu',
'oui2_location' => 'Je peux envoyer la localisation',
'non_location' => 'je ne peux pas envoyer',
'nos_conditions' => 'Nos Conditions Générales (CGU)',
'nos_conditions_messenger' => 'Nos Conditions',
'enter_produit_name' => 'Entrez le nom du produit recherché',
'enter_produit_found1' => 'Désolé, aucun marchand utilisant notre solution dans les  ',
'enter_produit_found2' => ' ne dispose de ce produit',
'searchproduit_result1' => '<b>VOS RESULTATS SONT EN BAS  :</b> 👇🏽',
'searchproduit_result2' => 'Veuillez cliquer sur le lien puis envoyer le texte ID sans le  modifier pour accéder directement à la boutique d\'un marchand',
'photo_boutique' => 'Envoyez une photo de votre commerce ou votre boutique',
'photo_livreur' => 'Envoyez une photo de vous (un selfie de preference)',
'no_access' => 'Désolé, ce lien est associé à un compte différent du vôtre. Taper <b>AB</b> pour démarrer votre session.',
'select_sexe' => 'Selectionnez votre sexe',
'sexe1' => 'Masculin',
'sexe2' => 'Feminin',
'acteur_age' => 'Svp, vous avez quel age? (Exemple: 30)',
//'compte_existe' => 'Je dispose déjà d\'un compte à travers l\'un des <b>reseaux sociaux de AB</b>',
'compte_existe' => 'Je dispose déjà d\'un compte sur <b>AB</b>',
'compte_existe_messenger' => 'J\'ai un compte',
'compte_noexiste_messenger' => 'Je suis nouveau',
'compte_noexiste' => 'Je suis nouveau. <b>Pas de compte sur AB</b>',
'enter_phonenumber' => 'Entrez votre numéro de téléphone au format international (<b>22870523535</b>)',
'enter_phonenumberbtn' => 'Veuillez cliquer sur le button pour envoyer votre numéro de téléphone',
'ERROR_PHONE' => 'Désolé, ce numéro ne possede pas de compte sur <b>AB</b>',
'EXIST_PHONE' => 'Désolé, ce numéro existe déjà dans le systeme',
'ERROR_TPHONE' => 'Désolé, veuillez communiquer avec moi par le bouton',
'enter_pine_code1' => 'Veuillez entrer votre code pin',
'enter_pine_code2' => 'Veuillez entrer le code pin recu par sms',
'enter_verification_code' => 'Veuillez entrer le code de vérification recu par sms',












);