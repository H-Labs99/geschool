<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use console\controllers\Common;

use backend\models\ParametreSysteme;
use console\models\User;
use console\models\Ussdtransation;
use console\models\MenuUssd;
use console\models\Produits;
use console\models\ProduitsQte;
use console\models\Package;
use console\models\PackageInfo;
use console\models\FaqCovid;


class Console_gta_initController extends Controller
{
    public $telephone;	
    public $name;	
    public $firstname;	
    public $status;	
    public $idtransaction;	
    public $response;	
	
    
	const MENU_SYSTEME 	= '10';

	const ALL_SYSTEME 	= '90';
	const NEXT_SYSTEME 	= '99';
	const PREVIOR_SYSTEME 	= '98';
	
	
	
	const MENU_FAQCORONA 	= '1';
	const MENU_INFOCORONA 	= '2';
	const MENU_DIAGNOSTIQUE 	= '3';
	const MENU_RECOMMANDER 	= '4';
		
	
    public function options($id){	
		
		return ['telephone','name','firstname','status','idtransaction','response'];
	}
	
	public function optionAliases(){
			return ['t'=>'telephone','n'=>'name','f'=>'firstname','s'=>'status','i'=>'idtransaction','r'=>'response'];
	}
	
		
    public function get_first_menu($find_transaction){
		
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'gta','sub_menu'=>'0'])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;	
		$nest_show=Console_gta_initController::NEXT_SYSTEME;
		$previor_show=Console_gta_initController::PREVIOR_SYSTEME;
		
		if(sizeof($all_menu)>0){
		
			$message=\Yii::t('app', 'WELCOME_GOGTA')."\n\n".\Yii::t('app', 'tape');			
			
			$i=1;

			if(sizeof($all_menu)<= $nbre_show ){
				
				foreach ($all_menu as $menu){
					$message.="\n".$menu->position_menu_ussd.".*. ".$menu->denomination;
					$i++;
				}
				$message.="\n\n0.*. ".\Yii::t('app', 'HELP');
				$find_transaction->page_menu=0;
			}else{
				
				$count=0;
				for($i=0;$i<$nbre_show;$i++){											
					$message.="\n".$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					$count++;
				}
				
				$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');	
				$message.="\n0.*. ".\Yii::t('app', 'HELP');
				$find_transaction->page_menu=1;
				
			}
			
			$find_transaction->message_send=$message;
			$find_transaction->save();
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
		}
		
		$find_transaction->menu=(string)"-1";
		$find_transaction->sub_menu=0;
		$find_transaction->save();
		return $message ;
	}
    	
    public function get_sub_menu($response,$find_transaction){
		
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;	
		$nest_show=Console_gta_initController::NEXT_SYSTEME;
		$previor_show=Console_gta_initController::PREVIOR_SYSTEME;
		
		
		
		$message="1";
		if($response!=$nest_show and $response!=$previor_show){								
								
			//verifier si cest un menu avec des sous menu
			$test_bigmenu=MenuUssd::find()->where(['position_menu_ussd'=>$response,'status'=>'1','client'=>'gta','sub_menu'=>'0'])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->one();
			if($test_bigmenu!==null){
				//liste de ces sous menu 									
				$all_submenu=MenuUssd::find()->where(['status'=>'1','client'=>'gta','sub_menu'=>$response])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
				if(sizeof($all_submenu)>0){
					
					
					$message=\Yii::t('app', 'tape');										
					$i=1;
		
					if(sizeof($all_submenu)<= $nbre_show ){
						
						foreach ($all_submenu as $menu){
							if($message!="")$message.="\n";
							$message.=$menu->position_menu_ussd.".*. ".$menu->denomination;
							$i++;
						}
						$find_transaction->page_menu=0;
					}else{
						
						$count=0;
						for($i=0;$i<$nbre_show;$i++){	
							if($message!="")$message.="\n";
							$message.=$all_submenu[$i]->position_menu_ussd.".*. ".$all_submenu[$i]->denomination;
							$count++;
						}
						
						$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');	
						$find_transaction->page_menu=1;
						
					}
					
					$find_transaction->menu=(string)-11;
					$find_transaction->sub_menu=(int)$response;
					$find_transaction->message_send=$message;
					$find_transaction->save();
					
				}else{
					$find_transaction->menu=(string)$response;
					$find_transaction->sub_menu=1;
					$find_transaction->save();
				}
				
			}else{									
				$find_transaction->menu=(string)$response;
				$find_transaction->sub_menu=1;
				$find_transaction->save();
			}
		}
		return $message ;
	}
    	
	public function get_previor_menu($big_menu,$find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'gta','sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;	
		$nest_show=Console_gta_initController::NEXT_SYSTEME;
		$previor_show=Console_gta_initController::PREVIOR_SYSTEME;	

		if(sizeof($all_menu)>0){
											
											
			$page_actuelle=(int)$find_transaction->page_menu;
			$message=\Yii::t('app', 'tape');
			
			if($page_actuelle>1){
			
				$start=($page_actuelle-2)*$nbre_show;
				
				$count=0;
				for($i=$start;$i<($nbre_show+$start);$i++){	
					if($i<sizeof($all_menu)){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
						$count++;
					}
				}
				
				
				if($start>0){
					$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR')."\n";	
				}else{		
					if($big_menu=="0"){
						$message=\Yii::t('app', 'WELCOME_GOGTA')."\n\n".\Yii::t('app', 'tape').$message;
					}
					$message.="\n\n";
				}
				$message.=$nest_show.".*. ".\Yii::t('app', 'NEXT');	
				
				$find_transaction->page_menu=$page_actuelle-1;
				
			}else{
				if($big_menu=="0"){
					$message=\Yii::t('app', 'WELCOME_GOGTA')."\n\n".\Yii::t('app', 'tape');
				}
				for($i=0;$i<$nbre_show;$i++){	
					if($i<=sizeof($all_menu)){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					}
				}											
				
				$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
			}
			if($big_menu=="0"){
				$message.="\n0.*. ".\Yii::t('app', 'HELP');
			}
			
			$find_transaction->message_send=$message;
			
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}									
											
		$find_transaction->save();	
		return $message ;
	}
	
	public function get_next_menu($big_menu,$find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'gta','sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;	
		$nest_show=Console_gta_initController::NEXT_SYSTEME;
		$previor_show=Console_gta_initController::PREVIOR_SYSTEME;
		
		if(sizeof($all_menu)>0){
			$message=\Yii::t('app', 'tape');
			$i=1;
			
			$page_actuelle=(int)$find_transaction->page_menu;
			$start=$page_actuelle*$nbre_show;
			
			if(sizeof($all_menu)<($nbre_show+$start) ){
				
				$nbre_reste=sizeof($all_menu)-$start;											
				
				if($nbre_reste>=1){
					$count=0;
					for($i=$start;$i<($nbre_reste+$start);$i++){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
						$count++;
					}
					
					if($page_actuelle>0){
						$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
					}else{
						$message.="\n";
					}
					
					
					if($count==$nbre_show){
						if(sizeof($all_menu)>($nbre_show+$start)){
							$message.=$nest_show." ".\Yii::t('app', 'NEXT');
						}
					}
					
					$find_transaction->page_menu=$page_actuelle+1;
				}else{
				
					$start=($page_actuelle-1)*$nbre_show;
					for($i=$start;$i<sizeof($all_menu);$i++){	
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					}
					$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
					
				}
				
			}else{
				
				$count=0;
				for($i=$start;$i<($nbre_show+$start);$i++){	
					if($message!="")$message.="\n";
					$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					$count++;
				}
				
				$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
				if($count==$nbre_show){
					if(sizeof($all_menu)>($nbre_show+$start)){
						$message.="\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
					}
				}
				
				
				
				$find_transaction->page_menu=$page_actuelle+1;
			}
			if($big_menu=="0"){
				$message.="\n0.*. ".\Yii::t('app', 'HELP');
			}
			$find_transaction->message_send=$message;
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}		
			
			
											
		$find_transaction->save();
		return $message ;
	}
	
	
	
	public function get_help_menu($find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'gta','sub_menu'=>'0'])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;	
		$nest_show=Console_gta_initController::NEXT_SYSTEME;
		$previor_show=Console_gta_initController::PREVIOR_SYSTEME;
		
		if(sizeof($all_menu)>0){
											
											
			$message=\Yii::t('app', 'HAVE_HELP');
			$i=1;

			if(sizeof($all_menu)<= $nbre_show ){
				
				foreach ($all_menu as $menu){
					$message.="\n".$menu->position_menu_ussd.".*. ".$menu->denomination;
					$i++;
				}
				$message.="\n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
				$find_transaction->page_menu=0;
				$find_transaction->message_send=$message;
			}else{
				
				$count=0;
				for($i=0;$i<$nbre_show;$i++){											
					$message.="\n".$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					$count++;
				}
				
				$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');	
				$message.="\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
				$find_transaction->page_menu=1;
				
				$find_transaction->message_send=$message;
			}
				
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}
		$find_transaction->save();
		
		return $message ;
	}
		 	
	public function get_hprevior_menu($big_menu,$find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'gta','sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;	
		$nest_show=Console_gta_initController::NEXT_SYSTEME;
		$previor_show=Console_gta_initController::PREVIOR_SYSTEME;	

		if(sizeof($all_menu)>0){
											
											
			$page_actuelle=(int)$find_transaction->page_menu;
			$message=\Yii::t('app', 'tape');
			
			if($page_actuelle>1){
			
				$start=($page_actuelle-2)*$nbre_show;
				
				$count=0;
				for($i=$start;$i<($nbre_show+$start);$i++){	
					if($i<sizeof($all_menu)){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
						$count++;
					}
				}
				
				
				if($start>0){
					$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');	
				}else{
					$message=\Yii::t('app', 'HAVE_HELP')."\n".$message."\n";
				}
				$message.="\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');	
				
				$find_transaction->page_menu=$page_actuelle-1;
				
			}else{
			
				$message=\Yii::t('app', 'HAVE_HELP');
				for($i=0;$i<$nbre_show;$i++){	
					if($i<=sizeof($all_menu)){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					}
				}											
				
				$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
			}
			$message.="\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
			
			$find_transaction->message_send=$message;
			
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}									
												
		$find_transaction->save();	
		return $message ;
	}
	
	public function get_hnext_menu($big_menu,$find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'gta','sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;	
		$nest_show=Console_gta_initController::NEXT_SYSTEME;
		$previor_show=Console_gta_initController::PREVIOR_SYSTEME;
		if(sizeof($all_menu)>0){
			$message=\Yii::t('app', 'tape');
			$i=1;
			
			$page_actuelle=(int)$find_transaction->page_menu;
			$start=$page_actuelle*$nbre_show;
			
			if(sizeof($all_menu)<($nbre_show+$start) ){
				
				$nbre_reste=sizeof($all_menu)-$start;											
				
				if($nbre_reste>=1){
					$count=0;
					for($i=$start;$i<($nbre_reste+$start);$i++){	
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
						$count++;
					}
					
					if($page_actuelle>0){
						$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
					}else{
						$message.="\n";
					}
					
					
					if($count==$nbre_show){
						if(sizeof($all_menu)>($nbre_show+$start)){
							$message.="\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
						}
					}
					
					$find_transaction->page_menu=$page_actuelle+1;
				}else{
				
					$start=($page_actuelle-1)*$nbre_show;
					for($i=$start;$i<sizeof($all_menu);$i++){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					}
					$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
					
				}
				
			}else{
				
				$count=0;
				for($i=$start;$i<($nbre_show+$start);$i++){	
					if($message!="")$message.="\n";
					$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					$count++;
				}
				
				$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
				if($count==$nbre_show){
					if(sizeof($all_menu)>($nbre_show+$start)){
						$message.="\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
					}
				}
				
				
				
				$find_transaction->page_menu=$page_actuelle+1;
			}
			
			$message.="\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
			$find_transaction->message_send=$message;
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}		
											
		$find_transaction->save();
		return $message ;
	}
	
		
		
    public function actionProcess(){
	
	    $status=trim($this->status);
		$telephone=trim($this->telephone);
		$idtransaction=trim($this->idtransaction);
		$response=trim($this->response);
		
		
		
		
			$name="";
			$firstname="";
			
			$nbre_show=Console_gta_initController::MENU_SYSTEME;
			$nest_show=Console_gta_initController::NEXT_SYSTEME;
			$previor_show=Console_gta_initController::PREVIOR_SYSTEME;	
				
			$reference="ERREUR";
			if($status==0){
			
				$name=trim($this->name);
				$firstname=trim($this->firstname);
				$reference="GTA";
			}
		  
			$id_user=$this->actionCheck_user($telephone,$name,$firstname,$status);	
			
			$id_trans=0;
			if($id_user=="0"){
			
				$message=\Yii::t('app', 'TRY_LATER');
				 
			}else{
				
				
				$id_trans=$this->actionCheck_transaction($idtransaction,$telephone,$id_user,$status,$reference);
				if($id_trans==0){
			
					$message=\Yii::t('app', 'TRY_LATER');
				 
				}else{
					$info_user=User::findOne(['id'=>$id_user]);
					
					if($info_user!==null){
							
							if($info_user !==null and ($info_user->nom=="" || $info_user->prenoms=="") ){		
									$message=$this->ask_souscription($status,$id_trans,$response);
									
							}else if($status=="0"){							
								$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>'GTA']);
								$message=$this->get_first_menu($find_transaction);
								
							}else if($status=="1"){					
								
								$message=$this->consoleprocess($response,$id_trans);
								 
							}else{
								$message=\Yii::t('app', 'FALSE_INFORMATION');
							}
						
						
					}else{
						$message=\Yii::t('app', 'FALSE_INFORMATION');
					}
					
				}
				
			}
			
			echo $this->wd_remove_accents($message,$id_trans);       
		
		
		
    }
		
	public function consoleprocess($response,$id_trans){		
	     
	    
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction= Ussdtransation::findOne(['id'=>$id_trans,'reference'=>'GTA']);
		
		$menu=trim($find_transaction->menu);
		$sub_menu=trim($find_transaction->sub_menu);
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;
	    $nest_show=Console_gta_initController::NEXT_SYSTEME;
	    $previor_show=Console_gta_initController::PREVIOR_SYSTEME;
		
		$continue=true;
		$rec_menu=$menu;
		if($rec_menu=="-1"){
			$rec_menu=$response;
		}else if( $rec_menu=="-11"){
			if($response==0){
				$message=$this->get_first_menu($find_transaction);	
				$continue=false;				
			}else{
				$rec_menu=$response;
			}
		}
		$test_menu=MenuUssd::findone(['position_menu_ussd'=>$rec_menu,'status'=>'1']);
		if($continue==true){
			if($test_menu!==null or $rec_menu=="0" or $response==$nest_show or $response==$previor_show){		
			
					switch (trim($menu)) {	   
						
						
						case "-1":
						
								$message=$this->get_sub_menu($response,$find_transaction);							
								
								if($message=="1"){
										
									switch (trim($response)) {
										case "0":											
											$message=$this->get_help_menu($find_transaction);
										break;
										
										case $previor_show:											
											$message=$this->get_previor_menu("0",$find_transaction);										
										break;
										
										case $nest_show:											
											$message=$this->get_next_menu("0",$find_transaction);										
										break;
										
										default:									
											$message =$this->select_function($response,$response,$id_trans,0);
										break;
										
									}
									
								}
							
						break;
						
						case "-11":
								if($response!="0"){
									
									$continue=true;
									$search_sub_menu=$response;
									$big_menu=$find_transaction->sub_menu;
									if($response!=$nest_show and $response!=$previor_show){
										
										//verifier si le numero selectionner fait partie des sub
										$test_submenu=MenuUssd::find()->where(['status'=>'1','client'=>'gta','position_menu_ussd'=>$response,'sub_menu'=>$big_menu])->one();
										if($test_submenu==null){
											$continue=false;
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;											
										}else{
											$search_sub_menu=$big_menu."-".$response;
											$find_transaction->menu=(string)$search_sub_menu;
										}										
									}
									
									if($continue==true){
										$find_transaction->sub_menu=1;
										$find_transaction->save();
										
										switch (trim($search_sub_menu)) {								
										
											case $previor_show:										
												$message =$this->get_previor_menu($big_menu,$find_transaction);									
											break;
											
											case $nest_show:										
												$message =$this->get_next_menu($big_menu,$find_transaction);
											break;
											
											default:							
												
												$message =$this->select_function($search_sub_menu,$response,$id_trans,0);
											break;
										}
									
									}
									
								}else{
									$message=$this->get_first_menu($find_transaction);	
								}
						break;
						
						case "0":
								switch (trim($response)) {	
									case $previor_show:
											$message=$this->get_hprevior_menu(0,$find_transaction);
									break;
									
									case $nest_show:	
											$message=$this->get_hnext_menu(0,$find_transaction);
									break;
									
									case "0":
											$message=$this->get_first_menu($find_transaction);										
									break;
									
									default:
										$aide_menu=MenuUssd::findone(['position_menu_ussd'=>$response,'status'=>'1','client'=>'gta']);
								
										if($aide_menu!==null){
											$message =$aide_menu->description."\n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');;
										}else{
											$message =\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										}
									break;
									
								}
								
						break;
						
						default:									
							$message =$this->select_function($menu,$response,$id_trans,1);
						break;					
						
				 
					}
			
			}else{
					$find_transaction->menu=(string)-1;
					$find_transaction->sub_menu=0;
					$find_transaction->page_menu=1;
					$find_transaction->save();
					$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
			}
		}
		
		return $message;
	   
	}
	
	public function select_function($menu,$response,$id_trans,$type){
		
		switch (trim($menu)) {			
			
					
			case Console_gta_initController::MENU_FAQCORONA:                		
				$message =$this->question_corona($response,$id_trans,$type);					
			break;
			case Console_gta_initController::MENU_INFOCORONA:                		
				$message =$this->information_corona($response,$id_trans,$type);					
			break;
			case Console_gta_initController::MENU_DIAGNOSTIQUE:                		
				$message =$this->diagnostique_corona($response,$id_trans,$type);					
			break;
			case Console_gta_initController::MENU_RECOMMANDER :                		
				$message =$this->recommander_solution($response,$id_trans,$type);					
			break;
			
			default:
				$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>'GTA']);
				if($find_transaction!=null){
					$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
				}else{
					$message =\Yii::t('app', 'TRY_LATER');
				}
			break;
		}
		
		return $message;
	}
		
		
	public function recommander_solution($response,$id_trans,$status){	   
		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>'GTA']);
		
		$sub_menu=trim($find_transaction->sub_menu);
		
		$nbre_show=Console_gta_initController::MENU_SYSTEME;
	    $nest_show=Console_gta_initController::NEXT_SYSTEME;
	    $previor_show=Console_gta_initController::PREVIOR_SYSTEME;
		
		
		switch (trim($status)) {
			case "0":
				    $message=\Yii::t('app', 'NUMERO_FRIEND')." \n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
					$find_transaction->message_send=$message;
					$find_transaction->sub_menu=1;
					$find_transaction->save();
			break;
			
			case "1":
				   
			       if($response=="0"){
							$message=$this->get_first_menu($find_transaction);
				    }else{
						switch (trim($sub_menu)) {
							case "1":	
									
									
									if($find_transaction->username!=$response){
										$message=\Yii::t('app', 'NUMERO_FRIEND_SUGERE_GTA')." ".$response."\n\n1.*. ".\Yii::t('app', 'CONFIRM')."\n2.*. ".\Yii::t('app', 'CANCEL')."\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
										$find_transaction->others=$response;
										$find_transaction->message_send=$message;
										$find_transaction->sub_menu=2;
										$find_transaction->save();
									}else{
										$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
									}
							break;
							case "2":
							
									if($response=="1"){										
											
											$sms=$find_transaction->username." ".\Yii::t('app', 'SUGES_MESSAGEGTA');
											$receiver=$find_transaction->others;
											$this->send_sms($sms,$receiver);
											
											$find_transaction->message=$sms;
											$find_transaction->etat_transaction=1;
											$find_transaction->save();	
											
											
											$message=\Yii::t('app', 'SUGGESTION_SUCCESS');
										
									}else if($response=="2"){
											$message=$this->get_first_menu($find_transaction);
									}else {
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
									}
									
							break;
						}
				    }
			break;
		}			   
		
		return $message;
	   
	}
	
	
	public function ask_souscription($status,$id_trans,$response){		
	
	    $nbre_show=Console_gta_initController::MENU_SYSTEME;
	    $nest_show=Console_gta_initController::NEXT_SYSTEME;
	    $previor_show=Console_gta_initController::PREVIOR_SYSTEME;	
		
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>'GTA']);
		$sub_menu=$find_transaction->sub_menu;
		if($status=="0"){
			
				$message=\Yii::t('app', 'WELCOME_GOGTA')."\n\n".\Yii::t('app', 'tape');
				//$message.="W_IMAGEwelcome.jpegW_IMAGE";
				$message.="\n1.*. ".\Yii::t('app', 'ask_soucription')."\n0.*. ".\Yii::t('app', 'cancel_process');
				$find_transaction->message_send=$message;
				$find_transaction->save();
		}else{
			
			if($response=="0"){
					$message =\Yii::t('app', 'cancel_operation_GTA');
					$find_transaction->message_send=$message;
					$find_transaction->etat_transaction=2;
					$find_transaction->save();													
			}else{
					
				switch (trim($sub_menu)) {	
					case "0":
						if($response=="1"){
					
							$message =\Yii::t('app', 'enter_nom');
							$find_transaction->sub_menu=1;
							$find_transaction->message_send=$message;
							$find_transaction->save();
							
							
						}else if($response=="0"){
							$message =\Yii::t('app', 'cancel_operation_GTA');
							$find_transaction->etat_transaction=2;
							$find_transaction->save();
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
					break ;
					case "1":
					
							$orther='"nom":"'.$response.'"';
							$message =\Yii::t('app', 'enter_prenom');
							$info_message =\Yii::t('app', 'name_souscription_GTA').$response;
							$find_transaction->others=$orther;
							$find_transaction->message=$info_message;
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=2;
							$find_transaction->save();
							
							
					break;
					case "2":
					
							$orther=$find_transaction->others.',"prenom":"'.$response.'"';
							$info_message =$find_transaction->message." ".$response;
							$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'CANCEL')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
							$find_transaction->others=$orther;
							$find_transaction->message=$info_message;
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=4;
							$find_transaction->save();
							
							
					break;
					
					case "4":
					
							if($response=="1"){		
									
									$recup_information=Json::decode("[{".$find_transaction->others."}]");										
									$information=$recup_information[0];
									
									$message=\Yii::t('app', 'succes_souscription');	
									
									
									
									$info_user=User::findOne(['id'=>$find_transaction->iduser]);
									$info_user->nom=$information['nom'];
									$info_user->prenoms=$information['prenom'];
									$info_user->save();
									
									$message.="\n\n".$this->get_first_menu($find_transaction);	
																	
								
									
								
							}else if($response=="2"){
								$message =\Yii::t('app', 'cancel_souscription');
								$find_transaction->message_send=$message;
								$find_transaction->etat_transaction=2;
								$find_transaction->save();
							}else {
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}
							
					break;
					
				}
				
			}	
		}

		
	   return $message;
	}
	
	public function question_corona($response,$id_trans,$status){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>'GTA']);
		
		$sub_menu=trim($find_transaction->sub_menu);
				
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
					$message=\Yii::t('app', 'select_question')."\n\n".\Yii::t('app', 'tape') ;
					//recuperation de la liste des produits
					$all_questions=FaqCovid::find()->where(['etat'=>'1','type_faq'=>1])->orderBy('position_menu_ussd ASC')->all();
					if(sizeof($all_questions)>0){
						
						$nbre_show=sizeof($all_questions);
						for($i=0;$i<$nbre_show;$i++){
							if($message!="")$message.="\n\n";
							$message.=$all_questions[$i]->position_menu_ussd.".*. ".$all_questions[$i]->question;							
						}	
						
						$find_transaction->message_send=$message;
						$find_transaction->others='"all_idFaq":"--"';						
						$find_transaction->sub_menu=1;
						$find_transaction->save();
					}else{
						$message =\Yii::t('app', 'TRY_LATER');
						$find_transaction->message_send=$message;
						$find_transaction->etat_transaction=2;
						$find_transaction->save();	
					}
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":
						    $find_question=FaqCovid::find()->where(['etat'=>'1','type_faq'=>1,'position_menu_ussd'=>$response])->one();						
								
							if($find_question!==null){
								
								$info_message="*".$find_question->question."* \n\n".$find_question->reponse;	
								$idFaq=$find_question->idFaq;
								
								$recup_information=Json::decode("[{".$find_transaction->others."}]");										
								$information=$recup_information[0];	
								$all_idFaq= $information['all_idFaq'];
								if($all_idFaq=="--"){
									$all_idFaq=$idFaq;
								}else{
									$all_idFaq.=",".$idFaq;									
								}
								
								$orther='"all_idFaq":"'.$all_idFaq.'"';								
								$message=$info_message."\n\n".\Yii::t('app', 'tape')."\n1.*.".\Yii::t('app', 'MENU_QUESTION')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
								
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$info_message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=2;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "2":			
							if($response=="1"){
								
								$message=\Yii::t('app', 'select_question')."\n\n".\Yii::t('app', 'tape') ;
								//recuperation de la liste des produits
								$all_questions=FaqCovid::find()->where(['etat'=>'1','type_faq'=>1])->orderBy('position_menu_ussd ASC')->all();
								if(sizeof($all_questions)>0){
									
									$nbre_show=sizeof($all_questions);
									for($i=0;$i<$nbre_show;$i++){
										if($message!="")$message.="\n\n";
										$message.=$all_questions[$i]->position_menu_ussd.".*. ".$all_questions[$i]->question;							
									}	
									
									$find_transaction->message_send=$message;					
									$find_transaction->sub_menu=1;
									$find_transaction->save();
								}else{
									$message =\Yii::t('app', 'TRY_LATER');
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=2;
									$find_transaction->save();	
								}
								
							}else {
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}
							
						break;
						
					}				
				break;
				
			}			   
		
		}
		return $message;
	   
	}
	
	public function information_corona($response,$id_trans,$status){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>'GTA']);
		
		$sub_menu=trim($find_transaction->sub_menu);
				
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
					$message=\Yii::t('app', 'select_info')."\n\n".\Yii::t('app', 'tape') ;
					//recuperation de la liste des produits
					$all_questions=FaqCovid::find()->where(['etat'=>'1','type_faq'=>2])->orderBy('position_menu_ussd ASC')->all();
					if(sizeof($all_questions)>0){
						
						$nbre_show=sizeof($all_questions);
						for($i=0;$i<$nbre_show;$i++){
							if($message!="")$message.="\n\n";
							$message.=$all_questions[$i]->position_menu_ussd.".*. ".$all_questions[$i]->question;							
						}	
						
						$find_transaction->message_send=$message;
						$find_transaction->others='"all_idFaq":"--"';						
						$find_transaction->sub_menu=1;
						$find_transaction->save();
					}else{
						$message =\Yii::t('app', 'TRY_LATER');
						$find_transaction->message_send=$message;
						$find_transaction->etat_transaction=2;
						$find_transaction->save();	
					}
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":
						    $find_question=FaqCovid::find()->where(['etat'=>'1','type_faq'=>2,'position_menu_ussd'=>$response])->one();						
								
							if($find_question!==null){
								
								$info_message="*".$find_question->question."* \n\n".$find_question->reponse;	
								$idFaq=$find_question->idFaq;
								
								$recup_information=Json::decode("[{".$find_transaction->others."}]");										
								$information=$recup_information[0];	
								$all_idFaq= $information['all_idFaq'];
								if($all_idFaq=="--"){
									$all_idFaq=$idFaq;
								}else{
									$all_idFaq.=",".$idFaq;									
								}
								
								$orther='"all_idFaq":"'.$all_idFaq.'"';								
								$message=$info_message."\n\n".\Yii::t('app', 'tape')."\n1.*.".\Yii::t('app', 'MENU_INFO')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
								
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$info_message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=2;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "2":			
							if($response=="1"){
								
								$message=\Yii::t('app', 'select_info')."\n\n".\Yii::t('app', 'tape') ;
								//recuperation de la liste des produits
								$all_questions=FaqCovid::find()->where(['etat'=>'1','type_faq'=>2])->orderBy('position_menu_ussd ASC')->all();
								if(sizeof($all_questions)>0){
									
									$nbre_show=sizeof($all_questions);
									for($i=0;$i<$nbre_show;$i++){
										if($message!="")$message.="\n\n";
										$message.=$all_questions[$i]->position_menu_ussd.".*. ".$all_questions[$i]->question;							
									}	
									
									$find_transaction->message_send=$message;					
									$find_transaction->sub_menu=1;
									$find_transaction->save();
								}else{
									$message =\Yii::t('app', 'TRY_LATER');
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=2;
									$find_transaction->save();	
								}
								
							}else {
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}
							
						break;
						
					}				
				break;
				
			}			   
		
		}
		return $message;
	   
	}
		
	public function diagnostique_corona($response,$id_trans,$status){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>'GTA']);
		
		$sub_menu=trim($find_transaction->sub_menu);
				
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
					$message=\Yii::t('app', 'question_1')."\n\n".\Yii::t('app', 'tape');
					$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'jesaispas');
					$find_transaction->message_send=$message;					
					$find_transaction->sub_menu=1;
					$find_transaction->save();
					
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther='"question_1":"'.$response.'"';	
								$sub_menu=2;

								if($response=="1" || $response=="3"){
									$sub_menu=100;	
									$message=\Yii::t('app', 'question_100');
								}else{
									$orther.=',"question_100":"37.5"';
									$message=\Yii::t('app', 'question_2')."\n\n".\Yii::t('app', 'tape');
									$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									$sub_menu=2;
								}
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=$sub_menu;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "100":
							$format_response=str_replace(" ","",$response);
							$format_response=str_replace(",",".",$format_response);
						    								
							if(is_numeric($format_response)){								
								$orther=$find_transaction->others.',"question_100":"'.$format_response.'"';	
								
								$message=\Yii::t('app', 'question_2')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=2;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "2":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_2":"'.$response.'"';
								$message=\Yii::t('app', 'question_3')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=3;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "3":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_3":"'.$response.'"';
								$message=\Yii::t('app', 'question_4')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=4;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "4":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_4":"'.$response.'"';
								$message=\Yii::t('app', 'question_5')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=5;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "5":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_5":"'.$response.'"';
								$message=\Yii::t('app', 'question_6')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=6;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "6":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								
								
								$sub_menu=7;

								if($response=="1"){
									$orther=$find_transaction->others.',"question_6":"'.$response.'"';
									$sub_menu=600;	
									$message=\Yii::t('app', 'question_600')."\n\n".\Yii::t('app', 'tape');
									$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
								}else{
									$orther=$find_transaction->others.',"question_6":"'.$response.'","question_600":"2"';
									$sub_menu=7;
									$message=\Yii::t('app', 'question_7')."\n\n".\Yii::t('app', 'tape');
									$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
								}
								
								
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=$sub_menu;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "600":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_600":"'.$response.'"';
								$message=\Yii::t('app', 'question_7')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=7;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "7":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_7":"'.$response.'"';
								$message=\Yii::t('app', 'question_8')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=8;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "8":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_8":"'.$response.'"';
								
								$message=\Yii::t('app', 'question_9');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=9;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "9":
							$format_response=str_replace(" ","",$response);
							$format_response=str_replace(",","",$format_response);
							$format_response=str_replace(".","",$format_response);
							$format_response=str_replace("ans","",$format_response);
						    								
							if(is_numeric($format_response)){								
								$orther=$find_transaction->others.',"question_9":"'.$format_response.'"';

								if($format_response<15){
									
									$message=\Yii::t('app', 'age_diagnostique');
										
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=1;
									$find_transaction->save();	
									
								}else{
								
									$message=\Yii::t('app', 'question_10');
										
									$find_transaction->message_send=$message;
									$find_transaction->message=$message;
									$find_transaction->others=$orther;
									$find_transaction->sub_menu=10;
									$find_transaction->save();		
								}								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "10":
							$format_response=str_replace(" ","",$response);
							$format_response=str_replace(",",".",$format_response);
							$format_response=str_replace("kg","",$format_response);
						    								
							if(is_numeric($format_response)){								
								$orther=$find_transaction->others.',"question_10":"'.$format_response.'"';
								
								$message=\Yii::t('app', 'question_11');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=11;
								$find_transaction->save();		
																
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "11":
							$format_response=str_replace(" ","",$response);
							$format_response=str_replace(",",".",$format_response);
							$format_response=str_replace("cm","",$format_response);
						    								
							if(is_numeric($format_response)){								
								$orther=$find_transaction->others.',"question_11":"'.$format_response.'"';
								
								$message=\Yii::t('app', 'question_12')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'jesaispas');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=12;
								$find_transaction->save();		
																
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "12":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_12":"'.$response.'"';
								$message=\Yii::t('app', 'question_13')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=13;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "13":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_13":"'.$response.'"';
								$message=\Yii::t('app', 'question_14')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=14;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "14":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_14":"'.$response.'"';
								$message=\Yii::t('app', 'question_15')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=15;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "15":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_15":"'.$response.'"';
								$message=\Yii::t('app', 'question_16')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=16;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "16":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_16":"'.$response.'"';
								$message=\Yii::t('app', 'question_17')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=17;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "17":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_17":"'.$response.'"';
								$message=\Yii::t('app', 'question_18')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'no_applicable');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=18;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "18":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_18":"'.$response.'"';
								$message=\Yii::t('app', 'question_19')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'jesaispas');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=19;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "19":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_19":"'.$response.'"';
								$message=\Yii::t('app', 'question_20')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'jesaispas');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=20;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "20":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_20":"'.$response.'"';
								
								$resultat=$this->diagnostique_resultat($orther);
								$type_message=$resultat[0];
								$message=$resultat[1];
								
								
								if($type_message==6 || $type_message==7 || $type_message==8){
									$info_user=User::findOne(['id'=>$find_transaction->iduser]);
									$plus="*".$info_user->nom." ".$info_user->prenoms."*";
									$message.="GOOD_DIAGNOSTIC".\Yii::t('app', 'share_resultat1').$plus.".\n\n".\Yii::t('app', 'share_resultat2_GTA').".\n\n".\Yii::t('app', 'share_resultat3').".\n".\Yii::t('app', 'share_resultat4_GTA').".\n\n".\Yii::t('app', 'share_resultat5')."GOOD_DIAGNOSTIC";
								}
								
								$find_transaction->others=$orther;
								$find_transaction->message_send=$message;
								$find_transaction->message=json_encode($resultat);
								$find_transaction->etat_transaction=1;
								$find_transaction->save();
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
					}				
				break;
				
			}			   
		
		}
		return $message;
	   
	}
	
	
	
	
	public function diagnostique_resultat($orther){
		
		
		$recup_information=Json::decode("[{".$orther."}]");										
		$information=$recup_information[0];		

		$facteur_gravite1=0;
		$facteur_gravite2=0;
		$facteur_pronostic=0;
		
		$age=$information["question_9"];
		$toux=$information["question_2"];
		$odorat=$information["question_3"];
		$gorge=$information["question_4"];
		$diaree=$information["question_5"];
		
		$fievre=0;
		if($information["question_1"]==3 || $information["question_1"]==1){
			if($information["question_100"]>=39){
				$fievre=1;
				$facteur_gravite1++;
			}else{
				$fievre=0;				
			}
		}
		
		if($information["question_6"]==1){
			if($information["question_600"]==1){
				$facteur_gravite1++;
			}
		}
		
		if($information["question_7"]==1){
			$facteur_gravite2++;
		}
		
		if($information["question_8"]==1){
			$facteur_gravite2++;
		}
		
		
		
		if($information["question_9"]>=70) $facteur_pronostic++;
		
		$poids=$information["question_10"];
		$taille=$information["question_11"];
		$taille_carre=($taille/100)*($taille/100);
		$imc=$poids/$taille_carre;
		if($imc>=30){
			$facteur_pronostic++;
		}
		
		if($information["question_12"]==1)$facteur_pronostic++;
		if($information["question_13"]==1)$facteur_pronostic++;
		if($information["question_14"]==1)$facteur_pronostic++;
		if($information["question_15"]==1)$facteur_pronostic++;
		if($information["question_16"]==1)$facteur_pronostic++;
		if($information["question_17"]==1)$facteur_pronostic++;
		if($information["question_18"]==1)$facteur_pronostic++;
		if($information["question_19"]==1)$facteur_pronostic++;
		if($information["question_20"]==1)$facteur_pronostic++;
		
		
		$facteur_gravite=$facteur_gravite1+$facteur_gravite2;
		$type_message=0;
		$message=\Yii::t('app', 'resulat_diagnostic00')."\n\n".\Yii::t('app', 'resulat_diagnostic000');
		
		if( $fievre==1 && $toux==1 ){
			
			if($facteur_pronostic==0){
				if($facteur_gravite2>=1){
					$message=\Yii::t('app', 'resulat_diagnostic5');
					$type_message=5;
				}else if( $facteur_gravite==0 || ($facteur_gravite1>0 && $facteur_gravite2==0) ){
					$message=\Yii::t('app', 'resulat_diagnostic2');
					$type_message=2;
				}
			}else{
				if($facteur_gravite2>=1){
					$message=\Yii::t('app', 'resulat_diagnostic5');
					$type_message=5;
				}else if( $facteur_gravite==0 ){
					$message=\Yii::t('app', 'resulat_diagnostic3');
					$type_message=3;
				}else if ($facteur_gravite1==1 && $facteur_gravite2==0){
					$message=\Yii::t('app', 'resulat_diagnostic3');	
					$type_message=3;
				}else if ($facteur_gravite1==2){
					$message=\Yii::t('app', 'resulat_diagnostic4');	
					$type_message=4;
				}
			}
			
			
		}else if( ($fievre==1) || ($toux==1 && $gorge==1) || ($toux==1 && $odorat==1) || ($fievre==1 && $diaree==1) ){
		
			if($facteur_pronostic==0){
				if($facteur_gravite2>=1){
					$message=\Yii::t('app', 'resulat_diagnostic5');
					$type_message=5;
				}else if($facteur_gravite==0 && $age< 50){
					$message=\Yii::t('app', 'resulat_diagnostic1');
					$type_message=1;
				}else if($facteur_gravite==0 && $age>=50 && $age<=69){
					$message=\Yii::t('app', 'resulat_diagnostic2');
					$type_message=2;
				}else if($facteur_gravite1>=1 && $age>=50 && $age<=69){
					$message=\Yii::t('app', 'resulat_diagnostic2');
					$type_message=2;
				}
			}else{
				if($facteur_gravite2>=1){
					$message=\Yii::t('app', 'resulat_diagnostic5');
					$type_message=5;
				}else if($facteur_gravite==0){
					$message=\Yii::t('app', 'resulat_diagnostic3');
					$type_message=3;
				}else if($facteur_gravite1==1){
					$message=\Yii::t('app', 'resulat_diagnostic3');
					$type_message=3;
				}else if($facteur_gravite1==2){
					$message=\Yii::t('app', 'resulat_diagnostic4');
					$type_message=4;
				}
			}
		
		}else if( $gorge+$toux+$odorat==5  ){
			if( $facteur_gravite==0 ){
				$message=\Yii::t('app', 'resulat_diagnostic6');
				$type_message=6;
			}else if ($facteur_gravite>0 ||  $facteur_pronostic>0){
				$message=\Yii::t('app', 'resulat_diagnostic7');	
				$type_message=7;
			}			
		}else if($gorge+$toux+$odorat==6  ){
			$message=\Yii::t('app', 'resulat_diagnostic8');	
			$type_message=8;
					
		}
		
		if($type_message!=8 && $type_message!=0){
			$message.="\n\n".\Yii::t('app', 'resulat_diagnostic0');	
		}
		if($type_message==6 || $type_message==7 || $type_message==8){
			$message.="\n\n".\Yii::t('app', 'resulat_diagnostic000');	
		}
		
		$tab[0]=$type_message;
		$tab[1]=$message;
		return $tab;
	}
	
	
	public function show_nbre($nbre){
		if($nbre>0){
			$recup=explode(".",$nbre);
			if(sizeof($recup)>1 && $recup[1]!="00"){
				return number_format($nbre, 2, '.', ' ');
			}else{
				return number_format($nbre, 0, '.', ' ');				
			}
		}else if($nbre<0){
			$convert=(-1)*$nbre;
			$recup=explode(".",$convert);
			if(sizeof($recup)>1 && $recup[1]!="00"){
				return "-".number_format($convert, 2, '.', ' ');
			}else{
				return "-".number_format($convert, 0, '.', ' ');				
			}
		}else return 0 ;
	}
	
	public function actionCheck_user($numero,$name,$firstname,$status){
		
		
									
	    $find_user = User::find()->where(['username'=>$numero])->all();
	    $nbre_user = sizeof($find_user);
	        
		 
		if($nbre_user==0){
		
		    $password=rand(123456 , 999999);
			$user = new User();
			$user->username = $numero;
			$user->nom = $name;
			$user->prenoms = $firstname;
			$user->idP = 1;
			$user->idProvenance = 0;
			$user->id_user_profil = 3;
			$user->status = 40;
			$user->role = 10;
			$user->created_at = time();
			$user->updated_at = time();		
			$user->auth_key =Yii::$app->security->generateRandomString(32);
			$user->password_hash =Yii::$app->security->generatePasswordHash($password);
			if($user->save()){
				$id=$user->id;						
			}else{					
				$id=0;
				
			}		
       
        }else{
		    $id=$find_user[0]->id;
			
		}
		return $id ;
	}

	public function actionCheck_transaction($idtransaction,$telephone,$iduser,$status,$reference){
		
	    $find_transaction = Ussdtransation::findOne(['idtransaction'=>$idtransaction,'username'=>$telephone,'iduser'=>$iduser,'etat_transaction'=>0,'reference'=>'GTA']);
	    
		
		$id_transaction=0;
		if($find_transaction==null){
		
		    if($status=="0"){
				$transaction = new Ussdtransation();
				$transaction->idtransaction = $idtransaction;
				$transaction->username = $telephone;
				$transaction->iduser = $iduser;
				$transaction->datecreation = date("Y-m-d H:i:s");
				$transaction->menu = "-1";
				$transaction->sub_menu = 0;
				$transaction->page_menu = 0;
				$transaction->etat_transaction = 0;			
				$transaction->reference = $reference;			
				$transaction->others= "";
				$transaction->last_update=(int)explode("_",$idtransaction)[0];	
				
				if($transaction->save()){
					$id_transaction=$transaction->id;
				}					
			}
       
        }else{		    
			$id_transaction=$find_transaction->id;			
		}
		
		return $id_transaction ;
	}
	
	public function send_sms($message,$to) { 
		$send_message=$this->content_remove_accents($message);
		Common::hit_sms($send_message,$to,"GoGTA");
	}

	
	function content_remove_accents($str){
		$charset='utf-8';
		$str = htmlentities($str, ENT_NOQUOTES, $charset);
		
		$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace('#&[^;]+;#', '', $str);
		
		return $str;
	}
	
	function wd_remove_accents($str,$idtransaction)
	{
		$charset='utf-8';
		$test_info=explode(".*.",$str);
		if(sizeof($test_info)==1){
			
			$find_transaction = Ussdtransation::findOne(['id'=>$idtransaction,'reference'=>'GTA']);
			if($find_transaction!==null && $find_transaction->etat_transaction==0){
				
				$content=$str;
			}else{
				$content=$str;
				$content.="\n\nPour recommencer envoyez *GO GTA* \n*Goodbye !* \n";				
			}
		}else{
		
			//$all_ligne=explode("\n",$str);
			//$content=str_replace(".*.",": \n",$str);
			
			$all_ligne=explode("\n",$str);
			$content="";
			
				$nbre_ligne=sizeof($all_ligne);
				if($nbre_ligne>0){
					for($uv=0;$uv<$nbre_ligne;$uv++){
						$ligne=$all_ligne[$uv];
						$test_selection=explode(".*.",$ligne);
						if(sizeof($test_selection)==2){
							$key=$test_selection[0];
							$tense=$test_selection[1];
							$content.="*".$key." :* ".$tense."\n";
						}else{
							$content.=$ligne."\n";
						}
					}
				}						
			
		}
		return $content;	
		
	}
}

