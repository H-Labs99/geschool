<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use console\controllers\Common;

use backend\models\ParametreSysteme;
use console\models\User;
use console\models\Ussdtransation;
use console\models\MenuUssd;
use console\models\Produits;
use console\models\ProduitsQte;
use console\models\Package;
use console\models\PackageInfo;
use console\models\FaqCovid;
use console\models\ActeurUser;
use console\models\Mproduits;
use console\models\SecteurActivite;
use console\models\BoutiqueVisite;
use console\models\CommandeMarchand;
use console\models\PublicitePartenaire;
use console\models\HistoriquePubMessage;
use console\models\AbcVente;
use console\models\AbcHistorique;
use console\models\AbcSouscription;
use console\models\AbcRetrait;
use console\models\AbcCashout;
use console\models\AbcRetraitmobile;
use backend\controllers\Utils;
use console\controllers\Cgu_content_fr;
use backend\models\Collecteur;


class ConsoleappinitController extends Controller
{
    public $provenance;	
    public $telephone;	
    public $name;	
    public $firstname;	
    public $status;	
    public $idtransaction;	
    public $response;	
    public $distance=10;	
    public $nbre_limit=50;	
    public $tab_prov=array("WEBSITE","MESSENGER","VIBER");	
	
    public $adjafi=1;	
	
    public $lien_whatsapp="https://wa.me/22891047373?text=";	
    public $lien_telegram="https://t.me/abtogo_bot?start=";
	
    public $pourcentage_marchand=0;	
    public $pourcentage_livreur=85;	
    public $photo_info="\n\n\nTapez: <b>PHOTO Numéro</b> pour voir la photo des produits. Ex: PHOTO 1";	
    public $panier_info="\n\nTapez: <b>PANIER</b> pour revenir a votre menu commande";	
    //public $photo_info="";	
    //public $panier_info="";	
	
    
	const MENU_SYSTEME 	= '10';

	const ALL_SYSTEME 	= '90';
	const NEXT_SYSTEME 	= '99';
	const PREVIOR_SYSTEME 	= '98';
	
	
	
	const MENU_MARCHAND 	= '1';
	const MENU_ACTEUR 	= '2';
	const MENU_PARLER 	= '3';
	const MENU_RECOMMANDER 	= '4';
	const MENU_BOUTIQUEMARCHAND 	= '5';
	const MENU_CREDIT 	= '6';
	const MENU_CGU 	= '10';
	
	const MENU_COMMANDE_PACKAGE 	= '10-1';
	const MENU_COMMANDE_PRODUIT 	= '10-2';
	const MENU_DRONE 	= '20';
	const MENU_FAQCORONA 	= '40';
	const MENU_INFOCORONA 	= '50';
	const MENU_DIAGNOSTIQUE 	= '60';
		
	
    public function options($id){	
		
		return ['telephone','name','firstname','status','idtransaction','response','provenance'];
	}
	
	public function optionAliases(){
			return ['t'=>'telephone','n'=>'name','f'=>'firstname','s'=>'status','i'=>'idtransaction','r'=>'response','p'=>'provenance'];
	}
	
		
    public function get_first_menu($find_transaction){
		
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'go_agri','sub_menu'=>'0'])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;	
		$nest_show=ConsoleappinitController::NEXT_SYSTEME;
		$previor_show=ConsoleappinitController::PREVIOR_SYSTEME;
		
		if(sizeof($all_menu)>0){
			$message="";
			
			$content=$this->get_ppartenaire(1,$find_transaction->iduser,$find_transaction->username);
			if(is_array($content)){
				if($content[0]=="photo"){
					$message.="WA_IMAGEpartenaire/".$content[1]."WA_IMAGE";
				}else if($content[0]=="video"){
					$message.="WA_VIDEOpartenaire/".$content[1]."WA_VIDEO";
				}
			}
		
			$message.=\Yii::t('app', 'WELCOME_GOAGRI')."\n\n".\Yii::t('app', 'tape');			
			
			$i=1;

			if(sizeof($all_menu)<= $nbre_show ){
				
				foreach ($all_menu as $menu){
					$message.="\n".$menu->position_menu_ussd.".*. ".$menu->denomination;
					$i++;
				}
				$message.="\n\n0.*. ".\Yii::t('app', 'HELP');
				$find_transaction->page_menu=0;
			}else{
				
				$count=0;
				for($i=0;$i<$nbre_show;$i++){											
					$message.="\n".$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					$count++;
				}
				
				$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');	
				$message.="\n0.*. ".\Yii::t('app', 'HELP');
				$find_transaction->page_menu=1;
				
			}
			
			$find_transaction->message_send=$message;
			$find_transaction->save();
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
		}
		
		$find_transaction->menu=(string)"-1";
		$find_transaction->sub_menu=0;
		$find_transaction->save();
		
		
		return $message ;
	}
    	
    public function get_sub_menu($response,$find_transaction){
		
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;	
		$nest_show=ConsoleappinitController::NEXT_SYSTEME;
		$previor_show=ConsoleappinitController::PREVIOR_SYSTEME;
		
		
		
		$message="1";
		if($response!=$nest_show and $response!=$previor_show){								
								
			//verifier si cest un menu avec des sous menu
			$test_bigmenu=MenuUssd::find()->where(['position_menu_ussd'=>$response,'status'=>'1','client'=>'orabank','sub_menu'=>'0'])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->one();
			if($test_bigmenu!==null){
				//liste de ces sous menu 									
				$all_submenu=MenuUssd::find()->where(['status'=>'1','client'=>'go_agri','sub_menu'=>$response])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
				if(sizeof($all_submenu)>0){
					
					
					$message=\Yii::t('app', 'tape');										
					$i=1;
		
					if(sizeof($all_submenu)<= $nbre_show ){
						
						foreach ($all_submenu as $menu){
							if($message!="")$message.="\n";
							$message.=$menu->position_menu_ussd.".*. ".$menu->denomination;
							$i++;
						}
						$find_transaction->page_menu=0;
					}else{
						
						$count=0;
						for($i=0;$i<$nbre_show;$i++){	
							if($message!="")$message.="\n";
							$message.=$all_submenu[$i]->position_menu_ussd.".*. ".$all_submenu[$i]->denomination;
							$count++;
						}
						
						$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');	
						$find_transaction->page_menu=1;
						
					}
					
					$find_transaction->menu=(string)-11;
					$find_transaction->sub_menu=(int)$response;
					$find_transaction->message_send=$message;
					$find_transaction->save();
					
				}else{
					$find_transaction->menu=(string)$response;
					$find_transaction->sub_menu=1;
					$find_transaction->save();
				}
				
			}else{									
				$find_transaction->menu=(string)$response;
				$find_transaction->sub_menu=1;
				$find_transaction->save();
			}
		}
		return $message ;
	}
    	
	public function get_previor_menu($big_menu,$find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'go_agri','sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;	
		$nest_show=ConsoleappinitController::NEXT_SYSTEME;
		$previor_show=ConsoleappinitController::PREVIOR_SYSTEME;	

		if(sizeof($all_menu)>0){
											
											
			$page_actuelle=(int)$find_transaction->page_menu;
			$message=\Yii::t('app', 'tape');
			
			if($page_actuelle>1){
			
				$start=($page_actuelle-2)*$nbre_show;
				
				$count=0;
				for($i=$start;$i<($nbre_show+$start);$i++){	
					if($i<sizeof($all_menu)){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
						$count++;
					}
				}
				
				
				if($start>0){
					$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR')."\n";	
				}else{		
					if($big_menu=="0"){
						$message=\Yii::t('app', 'WELCOME_GOAGRI')."\n\n".\Yii::t('app', 'tape').$message;
					}
					$message.="\n\n";
				}
				$message.=$nest_show.".*. ".\Yii::t('app', 'NEXT');	
				
				$find_transaction->page_menu=$page_actuelle-1;
				
			}else{
				if($big_menu=="0"){
					$message=\Yii::t('app', 'WELCOME_GOAGRI')."\n\n".\Yii::t('app', 'tape');
				}
				for($i=0;$i<$nbre_show;$i++){	
					if($i<=sizeof($all_menu)){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					}
				}											
				
				$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
			}
			if($big_menu=="0"){
				$message.="\n0.*. ".\Yii::t('app', 'HELP');
			}
			
			$find_transaction->message_send=$message;
			
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}									
											
		$find_transaction->save();	
		return $message ;
	}
	
	public function get_next_menu($big_menu,$find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'go_agri','sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;	
		$nest_show=ConsoleappinitController::NEXT_SYSTEME;
		$previor_show=ConsoleappinitController::PREVIOR_SYSTEME;
		
		if(sizeof($all_menu)>0){
			$message=\Yii::t('app', 'tape');
			$i=1;
			
			$page_actuelle=(int)$find_transaction->page_menu;
			$start=$page_actuelle*$nbre_show;
			
			if(sizeof($all_menu)<($nbre_show+$start) ){
				
				$nbre_reste=sizeof($all_menu)-$start;											
				
				if($nbre_reste>=1){
					$count=0;
					for($i=$start;$i<($nbre_reste+$start);$i++){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
						$count++;
					}
					
					if($page_actuelle>0){
						$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
					}else{
						$message.="\n";
					}
					
					
					if($count==$nbre_show){
						if(sizeof($all_menu)>($nbre_show+$start)){
							$message.=$nest_show." ".\Yii::t('app', 'NEXT');
						}
					}
					
					$find_transaction->page_menu=$page_actuelle+1;
				}else{
				
					$start=($page_actuelle-1)*$nbre_show;
					for($i=$start;$i<sizeof($all_menu);$i++){	
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					}
					$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
					
				}
				
			}else{
				
				$count=0;
				for($i=$start;$i<($nbre_show+$start);$i++){	
					if($message!="")$message.="\n";
					$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					$count++;
				}
				
				$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
				if($count==$nbre_show){
					if(sizeof($all_menu)>($nbre_show+$start)){
						$message.="\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
					}
				}
				
				
				
				$find_transaction->page_menu=$page_actuelle+1;
			}
			if($big_menu=="0"){
				$message.="\n0.*. ".\Yii::t('app', 'HELP');
			}
			$find_transaction->message_send=$message;
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}		
			
			
											
		$find_transaction->save();
		return $message ;
	}
			
	public function get_help_menu($find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'go_agri','sub_menu'=>'0'])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;	
		$nest_show=ConsoleappinitController::NEXT_SYSTEME;
		$previor_show=ConsoleappinitController::PREVIOR_SYSTEME;
		
		if(sizeof($all_menu)>0){
											
											
			$message=\Yii::t('app', 'HAVE_HELP');
			$i=1;

			if(sizeof($all_menu)<= $nbre_show ){
				
				foreach ($all_menu as $menu){
					$message.="\n".$menu->position_menu_ussd.".*. ".$menu->denomination;
					$i++;
				}
				$message.="\n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
				$find_transaction->page_menu=0;
				$find_transaction->message_send=$message;
			}else{
				
				$count=0;
				for($i=0;$i<$nbre_show;$i++){											
					$message.="\n".$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					$count++;
				}
				
				$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');	
				$message.="\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
				$find_transaction->page_menu=1;
				
				$find_transaction->message_send=$message;
			}
				
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}
		$find_transaction->save();
		
		return $message ;
	}
		 	
	public function get_hprevior_menu($big_menu,$find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'go_agri','sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;	
		$nest_show=ConsoleappinitController::NEXT_SYSTEME;
		$previor_show=ConsoleappinitController::PREVIOR_SYSTEME;	

		if(sizeof($all_menu)>0){
											
											
			$page_actuelle=(int)$find_transaction->page_menu;
			$message=\Yii::t('app', 'tape');
			
			if($page_actuelle>1){
			
				$start=($page_actuelle-2)*$nbre_show;
				
				$count=0;
				for($i=$start;$i<($nbre_show+$start);$i++){	
					if($i<sizeof($all_menu)){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
						$count++;
					}
				}
				
				
				if($start>0){
					$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');	
				}else{
					$message=\Yii::t('app', 'HAVE_HELP')."\n".$message."\n";
				}
				$message.="\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');	
				
				$find_transaction->page_menu=$page_actuelle-1;
				
			}else{
			
				$message=\Yii::t('app', 'HAVE_HELP');
				for($i=0;$i<$nbre_show;$i++){	
					if($i<=sizeof($all_menu)){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					}
				}											
				
				$message.="\n\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
			}
			$message.="\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
			
			$find_transaction->message_send=$message;
			
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}									
												
		$find_transaction->save();	
		return $message ;
	}
	
	public function get_hnext_menu($big_menu,$find_transaction){
		$message="";
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$all_menu=MenuUssd::find()->where(['status'=>'1','client'=>'go_agri','sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->orderBy('position_menu_ussd ASC')->all();
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;	
		$nest_show=ConsoleappinitController::NEXT_SYSTEME;
		$previor_show=ConsoleappinitController::PREVIOR_SYSTEME;
		if(sizeof($all_menu)>0){
			$message=\Yii::t('app', 'tape');
			$i=1;
			
			$page_actuelle=(int)$find_transaction->page_menu;
			$start=$page_actuelle*$nbre_show;
			
			if(sizeof($all_menu)<($nbre_show+$start) ){
				
				$nbre_reste=sizeof($all_menu)-$start;											
				
				if($nbre_reste>=1){
					$count=0;
					for($i=$start;$i<($nbre_reste+$start);$i++){	
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
						$count++;
					}
					
					if($page_actuelle>0){
						$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
					}else{
						$message.="\n";
					}
					
					
					if($count==$nbre_show){
						if(sizeof($all_menu)>($nbre_show+$start)){
							$message.="\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
						}
					}
					
					$find_transaction->page_menu=$page_actuelle+1;
				}else{
				
					$start=($page_actuelle-1)*$nbre_show;
					for($i=$start;$i<sizeof($all_menu);$i++){
						if($message!="")$message.="\n";
						$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					}
					$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
					
				}
				
			}else{
				
				$count=0;
				for($i=$start;$i<($nbre_show+$start);$i++){	
					if($message!="")$message.="\n";
					$message.=$all_menu[$i]->position_menu_ussd.".*. ".$all_menu[$i]->denomination;
					$count++;
				}
				
				$message.="\n\n".$previor_show.".*. ".\Yii::t('app', 'PREVIOR');
				if($count==$nbre_show){
					if(sizeof($all_menu)>($nbre_show+$start)){
						$message.="\n".$nest_show.".*. ".\Yii::t('app', 'NEXT');
					}
				}
				
				
				
				$find_transaction->page_menu=$page_actuelle+1;
			}
			
			$message.="\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
			$find_transaction->message_send=$message;
			
		}else{
			$message=\Yii::t('app', 'TRY_LATER');
			$find_transaction->etat_transaction=2;
		}		
											
		$find_transaction->save();
		return $message ;
	}	
		
    public function actionProcess(){
	
		
	    $status=trim($this->status);
		$telephone=trim($this->telephone);
		$idtransaction=trim($this->idtransaction);
		$response=trim($this->response);
		$provenance=trim($this->provenance);
		
		
			$name="";
			$firstname="";
			
			$nbre_show=ConsoleappinitController::MENU_SYSTEME;
			$nest_show=ConsoleappinitController::NEXT_SYSTEME;
			$previor_show=ConsoleappinitController::PREVIOR_SYSTEME;	
				
			if($status==0){			
				$name=trim($this->name);
				$firstname=trim($this->firstname);
			}
		  
			$info_user=$this->actionCheck_user($telephone,$name,$firstname,$status,$response,$provenance);	
			
			$id_trans=0;
			if($info_user==null){
			
				$message=\Yii::t('app', 'TRY_LATER');
				 
			}else{
				
				$id_user=$info_user->id;
				$find_transaction=$this->actionCheck_transaction($idtransaction,$telephone,$id_user,$status,$provenance);
				
				if($find_transaction==null){
			
					$message=\Yii::t('app', 'TRY_LATER');
				}else{
					
					
						$id_trans=$find_transaction->id;
				
						//verifier si cest un lien direct
						$test_direct=explode("ABSELLER",$response);
						if(sizeof($test_direct)==3){
							
							$test_credit=explode("CRD",$test_direct[1]);
							if(sizeof($test_credit)==4){
								
								
								$idref_marchand=explode("ETC",$test_credit[2]);
								if(sizeof($idref_marchand)==3){
									
									/*
									ABSELLER
									CRD
									REFERENCE DE LA VENTE
									CRD
									REFERENCE DU MARCHAND
									ETC
									ID DU MARCHAND
									ETC
									REFERENCE DE LA SOUSCRIPTION
									CRD (1 ou 2) => 1 pour la vente, le 1 pour le retrait
									ABSELLER
																	
									ABSELLER CRD ABC4510 CRD CF63391 ETC 1 ETC SPT1021  CRD1  ABSELLER
									
									ABSELLERCRDABC6021CRDCF63391ETC1ETCSPT1314CRDABSELLER
									*/
																		
									$refcredit=$test_credit[1];
									$typeoperation=$test_credit[3];
									$reference_marchand=$idref_marchand[0];
									$id_acteur=$idref_marchand[1];
									$ref_souscription=$idref_marchand[2];
									
									
									//verification des informations du souscripteurs
									$find_souscripteur = AbcSouscription::findOne(['ref_souscription'=>$ref_souscription,'id_user'=>$id_user,'status_souscription'=>1]);
									
									if($find_souscripteur!=null){
											
											//verification des informations du marchand 
											$find_acteur = ActeurUser::findOne(['id_acteur_user'=>$id_acteur,'code_reference'=>$reference_marchand,'etat'=>1,'type_acteur'=>1]);
									
											if($find_acteur!=null){
												
												if($typeoperation=="2"){
													
													
													//statut retrait 0=> en cours, 1 confirme, 2 rejecte
													
													//verification du retrait
													$find_retrait = AbcRetrait::findOne(['ref_retrait'=>$refcredit,'id_acteur_user'=>$find_acteur->id_acteur_user,'idclient'=>$find_souscripteur->id_souscription,'status_retrait'=>[0,1,2]]);
													if($find_retrait!=null){
														
														if($find_retrait->status_retrait==0){
																														
															$infoFrais=AbcCashout::find()->One();
															$amount=0;
															if($infoFrais!=null){
																
																if($infoFrais->cout>0){
																	$amount=(int)$find_retrait->montant_retrait*$infoFrais->cout/100;
																}
															}
																														
															//verification du solde du client
															if(((int)$find_retrait->montant_retrait+$amount) <= (int)$find_souscripteur->solde_disponible){
																
																//Demarer la session de retrait a credit
																$orther='"type_operation":"2","ref_marchand":"'.$reference_marchand.'","ref_vente":"'.$refcredit.'","idacteur":"'.$id_acteur.'","ref_souscription":"'.$ref_souscription.'"';
																
																
																$reste=$find_souscripteur->solde_disponible-($find_retrait->montant_retrait+$amount);
																
																$message="Validation de votre demande de retrait ".$refcredit." sur votre carte ABCrédit";
																$message.="\n<b>Marchand: ".$find_acteur->denomination."</b>";
																$message.="\nMontant: <b>".Utils::show_nbre($find_retrait->montant_retrait)." FCFA</b>";
																if($amount>0){
																	$message.="\nFrais: <b>".Utils::show_nbre($amount)." FCFA</b>";																	
																}
																$message.="\nAprès cette opération il vous restera ".Utils::show_nbre($reste)." FCFA valable pour le mois en cours.";
																$message.="\n\n";
																$message.="1.*. Valider\n2.*. Rejeter";	
																
																$find_transaction->message_send=$message;
																$find_transaction->message=$message;
																$find_transaction->last_update=time();
																$find_transaction->others=$orther;
																$find_transaction->menu=ConsoleappinitController::MENU_CREDIT;
																$find_transaction->sub_menu=2;
																$find_transaction->save();
														
																
															}else{
																$message="Votre solde est insuffisant pour effectuer cette opération.";
																$message.="\nSolde actuel: ".Utils::show_nbre($find_souscripteur->solde_disponible);
																$message.="\nMontant du retrait: ".Utils::show_nbre($find_retrait->montant_retrait);
																if($amount>0){
																	$message.="\nFrait du retrait: ".Utils::show_nbre($amount);
																}
																$message.="\n\nMerci";
															}
														
														}else{
															
															$message="Cette confirmation a déjà été effectuée. Veuillez demander au marchand de vous regénérer une nouvelle transaction.\nMerci";
														}
														
													}else{
														$message=\Yii::t('app', 'TRY_LATER');
													}
													
												
												}else if($typeoperation=="1"){
													
													//verification de la commande
													$find_credit = AbcVente::findOne(['ref_vente'=>$refcredit,'id_acteur_user'=>$find_acteur->id_acteur_user,'idclient'=>$find_souscripteur->id_souscription,'status_vente_abc'=>[0,1,2]]);										
													if($find_credit!=null){
														
														if($find_credit->status_vente_abc==0){
															
															
															
															//verification du solde du client
															if((int)$find_credit->montant_vente_abc <= (int)$find_souscripteur->solde_disponible){
																
																//Demarer la session de paiement a credit
																$orther='"type_operation":"1","ref_marchand":"'.$reference_marchand.'","ref_vente":"'.$refcredit.'","idacteur":"'.$id_acteur.'","ref_souscription":"'.$ref_souscription.'"';
																
																
																$reste=$find_souscripteur->solde_disponible-$find_credit->montant_vente_abc;
																
																$message="Validation de votre commande ".$refcredit." sur votre carte ABCrédit";
																$message.="\n<b>Marchand: ".$find_acteur->denomination."</b>";
																$message.="\nDescription: ".$find_credit->libelle_vente_abc;
																$message.="\nMontant: <b>".Utils::show_nbre($find_credit->montant_vente_abc)." FCFA</b>";
																$message.="\nAprès cette opération il vous restera ".Utils::show_nbre($reste)." FCFA valable pour le mois en cours.";
																$message.="\n\n";
																$message.="1.*. Valider\n2.*. Rejeter";	
																
																$find_transaction->message_send=$message;
																$find_transaction->message=$message;
																$find_transaction->last_update=time();
																$find_transaction->others=$orther;
																$find_transaction->menu=ConsoleappinitController::MENU_CREDIT;
																$find_transaction->sub_menu=2;
																$find_transaction->save();
														
																
															}else{
																$message="Votre solde est insuffisant pour effectuer cette opération.";
																$message.="\nSolde actuel: ".Utils::show_nbre($find_souscripteur->solde_disponible);
																$message.="\nMontant de l'achat: ".Utils::show_nbre($find_credit->montant_vente_abc)."\nMerci";
															}
														
														}else{
															
															$message="Cette confirmation a déjà été effectuée. Veuillez demander au marchand de vous regénérer une nouvelle transaction.\nMerci";
														}
														
													}else{
														$message=\Yii::t('app', 'TRY_LATER');
													}
													
												}else{
													$message=\Yii::t('app', 'TRY_LATER');
												}
											
											}else{
												$message=\Yii::t('app', 'TRY_LATER');
											}	
									
									}else{
										$message=\Yii::t('app', 'TRY_LATER');
									}
									
								}else{
									$message=$this->get_first_menu($find_transaction);
								}
								
							}else{							
						
								$rtest_direct=explode("TTR",$test_direct[1]);
								if(sizeof($rtest_direct)==4){
									
									$id_acteur_user=$rtest_direct[0];
									$idmarchand=$rtest_direct[1];
									$type_acteur=$rtest_direct[2];
									
									if($idmarchand==$id_user){
									
									
										if($type_acteur==4)$type_acteur=1;
										$nullrequest=new \yii\db\Expression('null');
			
										$query= ActeurUser::find()->where(['etat'=>0,'type_acteur'=>$type_acteur,'id_user'=>$idmarchand,'id_acteur_user'=>$id_acteur_user]);
										$query->andFilterWhere(['or',['is','photo_acteur',$nullrequest],['=','LENGTH(photo_acteur)',0],['=','lat_acteur',0],['=','long_acteur',0]]);
										
										$find_acteur=$query->one();
										if($find_acteur!=null){
											
											$message="";
											if($type_acteur==1){
												$message =\Yii::t('app', 'localisation_boutique1')." <b>".trim($find_acteur->denomination)."</b>\n\n".\Yii::t('app', 'localisation_boutique2');
												$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location1');
											}else if($type_acteur==2){											
												$message =\Yii::t('app', 'localisation_livreur');											
												$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location2');
											}
											
											//visite d'une boutique d'un marchand
											
											//$find_transaction= Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
											
											$find_transaction->others='"id_acteur":"'.$id_acteur_user.'","type_acteur":"'.$type_acteur.'"';
											$find_transaction->menu="5";
											$find_transaction->sub_menu=20;
											$find_transaction->message_send=$message;
											$find_transaction->save();
											
														
										}else{
											$message=\Yii::t('app', 'TRY_LATER');
										}
										
									}else{
										$message=\Yii::t('app', 'no_access');										
									}
									
								}else{									
									$stest_direct=explode("TTB",$test_direct[1]);
									if(sizeof($stest_direct)==3){
										// visite d'un produit dans la boutique du marchand 
										
										$reference_marchand=$stest_direct[0];
										$ltest_direct=explode("U",$stest_direct[1]);
										if(sizeof($ltest_direct)==2){
											
											$id_acteur=$ltest_direct[0];
											$position_produit=$ltest_direct[1];
											
											$find_acteur = ActeurUser::findOne(['id_acteur_user'=>$id_acteur,'code_reference'=>$reference_marchand,'etat'=>1,'type_acteur'=>1]);
											if($find_acteur!=null){
												
												$name_marchand=$find_acteur->denomination;
												
												$orther='"type_search":"1","id_acteur":"'.$id_acteur.'"';
												
												//recuperer la liste des produits de ce marchand
									
												$info_message=\Yii::t('app', 'resumecommande');
												$info_message.="\n<b>Marchand: ".$find_acteur->denomination."</b>";
													
												$message="<b>Bienvenue chez ".$find_acteur->denomination."</b>";
												$message.="\n\n".\Yii::t('app', 'help_marchand')."\n";
												
												$all_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1])->orderBy('position_menu_ussd ASC')->all();
												if(sizeof($all_produits)>0){
													$tab_response=array();
													$nbre_show=sizeof($all_produits);
													$new_message="";
													for($i=0;$i<$nbre_show;$i++){

														
														if(in_array($provenance,$this->tab_prov)){	
															$tab_response[]=["marchand"=>$find_acteur->denomination,"photo"=>"produit/".$all_produits[$i]->photo_produit,"code"=>$all_produits[$i]->position_menu_ussd,"denomination"=>$all_produits[$i]->denomination,"qte_vente"=>Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite,"prix_vente"=>Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa"];
														}else{
															
															if($message!="")$message.="\n";
															$message.=$all_produits[$i]->position_menu_ussd.".*. ".$all_produits[$i]->denomination." - ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." à ".Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa";	

														}


														if($all_produits[$i]->position_menu_ussd==$position_produit){
															
															$new_message=$all_produits[$i]->denomination." - ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." à ".Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa";
															$new_message.=$this->panier_info;
															$photo_produit=(string)$all_produits[$i]->photo_produit;
															if($photo_produit!=""){
																$new_message.="WA_IMAGEproduit/".$photo_produit."WA_IMAGE";
															}
															
														}
													}	
													
													$message.=$this->photo_info;
													$photo_marchand=(string)$find_acteur->photo_acteur;
													if($photo_marchand!=""){
														$message.="WA_IMAGEacteurs/".$photo_marchand."WA_IMAGE";
													}
													
													if(in_array($provenance,$this->tab_prov)){	
														$message=json_encode(array("type"=>"produit","information"=>$tab_response));
													}
													
													$find_transaction->message_send=$message;
													$find_transaction->message=$info_message;
													$find_transaction->others=$orther;
													$find_transaction->menu="1";
													$find_transaction->sub_menu=5;
													$find_transaction->save();
													
													if($new_message==""){	
														if(in_array($provenance,$this->tab_prov)){	
															$message=$find_transaction->message_send;
														}else{
															$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
														}
													}else{
														$message=$new_message;
													}												
										
													
												}else{
													$message=\Yii::t('app', 'TRY_LATER');
												}
										
									
											}else{
												$message=\Yii::t('app', 'TRY_LATER');
											}
											
										}else{
											$message=\Yii::t('app', 'TRY_LATER');
										}
									}else{
										$stest_direct=explode("TTK",$test_direct[1]);
										if(sizeof($stest_direct)==3){
											
											//confirmation par le marchand davoir remis le colis au livreur
											$code_reference=$stest_direct[0];
											$ltest_direct=explode("U",$stest_direct[1]);
											if(sizeof($ltest_direct)==3){
												$idclient=$ltest_direct[0];
												$iduser_acteur=$ltest_direct[1];
												$idCommande=$ltest_direct[2];
												if($id_user==$iduser_acteur){
													
													$find_acteur = ActeurUser::findOne(['id_user'=>$iduser_acteur,'code_reference'=>$code_reference,'etat'=>1,'type_acteur'=>[1,2]]);
													if($find_acteur!=null){
														if($find_acteur->type_acteur==1){	
																//verifier si cette commande existe
																$test_commande=CommandeMarchand::find()->where(['id_user'=>$idclient,'id_marchand'=>$find_acteur->id_acteur_user,'id_commande_marchand'=>$idCommande])->one();
																if($test_commande!=null){
																	
																	if($test_commande->etat_confirmation==0){		
																		if($test_commande->etat_conformite==1){																		
																		
																			$this->send_information($test_commande,"1");
																			$message="Confirmation de la récupération de la commande effectueé avec succès.\nMerci";
																			$test_commande->etat_confirmation=1;
																			$test_commande->save();
																		}else{
																			$message="Veuillez demander au livreur de vérifier et valider la conformité du/des produit(s) commandé(s) avant d'effectuer cette opération.\nMerci";
																		}
																		
																	}else{
																		$message="Cette confirmation a été déjà effectueé.\nMerci";	
																	}
																	
																}else{
																	$message=\Yii::t('app', 'TRY_LATER');
																}
														}else if($find_acteur->type_acteur==2){	
														
																//verifier si cette commande existe
																$test_commande=CommandeMarchand::find()->where(['id_user'=>$idclient,'id_livreur'=>$find_acteur->id_acteur_user,'id_commande_marchand'=>$idCommande])->one();
																if($test_commande!=null){
																	if($test_commande->etat_confirmation==0 && $test_commande->etat_conformite==0){		
																		
																		$message="Conformité de la commande confirmeé avec succès.\nMerci";
																		$test_commande->etat_conformite=1;
																		$test_commande->save();
																		
																	}else{
																		$message="Cette confirmation a été déjà effectueé.\nMerci";	
																	}
																	
																}else{
																	$message=\Yii::t('app', 'TRY_LATER');
																}
														}		
																
														
													}else{
														$message=\Yii::t('app', 'TRY_LATER');
													}								
																							
												}else{
													$message=\Yii::t('app', 'TRY_LATER');												
												}
												
												
											}else{
												$message=\Yii::t('app', 'TRY_LATER');
											}
									
										}else{									
											
											$stest_direct=explode("TTF",$test_direct[1]);
											if(sizeof($stest_direct)==3){
												
												//confirmation par le client davoir recu le colis du livreur
												$message=$stest_direct[1];
												
												$reference_marchand=$stest_direct[0];
												$ltest_direct=explode("U",$stest_direct[1]);
												if(sizeof($ltest_direct)==3){
													
													$message=$stest_direct[1];
													
													$idclient=$ltest_direct[0];
													$idmarchand=$ltest_direct[1];
													$idCommande=$ltest_direct[2];
													
													if($id_user==$idclient){
														
														$find_acteur = ActeurUser::findOne(['id_user'=>$idmarchand,'code_reference'=>$reference_marchand,'etat'=>1,'type_acteur'=>1]);
														if($find_acteur!=null){
																
															//verifier si cette commande existe
															$test_commande=CommandeMarchand::find()->where(['id_user'=>$idclient,'id_marchand'=>$find_acteur->id_acteur_user,'id_commande_marchand'=>$idCommande,'etat_confirmation'=>[1,2]])->one();
															
															if($test_commande!=null){
																if($test_commande->etat_confirmation==1){																	
																	$this->send_information($test_commande,"2");
																	
																	$message="Confirmation de la récupération de la commande effectueé avec succès.\nMerci";
																	
																	$content=$this->get_ppartenaire(3,$find_transaction->iduser,$find_transaction->username);
																	if(is_array($content)){
																		if($content[0]=="photo"){
																			$message.="WA_IMAGEpartenaire/".$content[1]."WA_IMAGE";
																		}else if($content[0]=="video"){
																			$message.="WA_VIDEOpartenaire/".$content[1]."WA_VIDEO";
																		}
																	}
																	$test_commande->etat_confirmation=2;
																	$test_commande->save();
																}else{
																	$message="Cette confirmation a été déjà effectueé.\nMerci";	
																}
																
															}else{
																$message=\Yii::t('app', 'TRY_LATER');
															}
														}else{
															$message=\Yii::t('app', 'TRY_LATER');
														}								
																								
													}else{
														$message=\Yii::t('app', 'TRY_LATER');												
													}
													
												}else{
													$message=\Yii::t('app', 'TRY_LATER');
												}
												
											}else{
											
												$stest_direct=explode("TTL",$test_direct[1]);
												if(sizeof($stest_direct)==3){
													
													//confirmation par le livreur  de prendre encharge cette livraison
													
													$reference_marchand=$stest_direct[0];
													$ltest_direct=explode("U",$stest_direct[1]);
													if(sizeof($ltest_direct)==4){
														
														
														$idlivreur_acteur=$ltest_direct[0];
														$idlivreur=$ltest_direct[1];
														$idmarchand=$ltest_direct[2];
														$idCommande=$ltest_direct[3];
														
														if($id_user==$idlivreur){
															
															$find_acteur = ActeurUser::findOne(['id_user'=>$idmarchand,'code_reference'=>$reference_marchand,'etat'=>1,'type_acteur'=>1]);
															if($find_acteur!=null){
																	
																//verifier si cette commande existe
																$test_commande=CommandeMarchand::find()->where(['id_marchand'=>$find_acteur->id_acteur_user,'id_commande_marchand'=>$idCommande,'etat'=>1])->one();
																
																if($test_commande!=null){
																	
																	
																	$find_livreur = ActeurUser::findOne(['id_user'=>$idlivreur,'id_acteur_user'=>$idlivreur_acteur,'etat'=>1,'type_acteur'=>2]);
																	if($find_livreur!=null){
																
																	
																		 //verifier si cet livreur fait partie de la liste
																		$text_livreur=explode(",",$test_commande->all_livreurId);
																		if(in_array($idlivreur_acteur,$text_livreur)){
																			
																			$id_livreur=(string)$test_commande->id_livreur;
																			if($id_livreur=="0" || $id_livreur==""){
																				
																				$prix_livreur=($find_livreur->pourcentage)*$test_commande->prix_livraison/100;
																				$test_commande->id_livreur=$idlivreur_acteur;
																				$test_commande->prix_livreur=$prix_livreur;
																				$test_commande->save();	

																				$code_commande="TTK".$test_commande->id_user."U".$find_livreur->idUser->id."U".$test_commande->id_commande_marchand."TTKCLIN";
																				$lien_confirmation="https://wa.me/".Utils::real_phone."?text=ABSELLER".$find_livreur->code_reference.$code_commande."ABSELLER";
				
																				$message="Confirmation effectuée avec succès. Voici ci joint la localisation du marchand.";
																				$message.="\n\nVeuillez confirmer la conformité de la commande apres verification en cliquant sur le lien suivant.\n".$lien_confirmation;
																				$message.="\n\n\nPour recevoir les informations du client, veuillez demander au marchand de confirmer la récupération de la commande";	
																				$this->active_commande($test_commande);
																				
																			}else{
																				
																				
																				if($id_livreur==$idlivreur_acteur){
																					$message="Cette confirmation a été déjà effectueé.\nMerci";	
																				}else{
																					$message="Désolé, un autre livreur a déjà pris en charge cette commande.\nEn cas de retard de livraison sur la commande par le livreur, nous vous ferons appel de nouveau pour la prendre en charge.\nMerci";
																				}
																			}
																			 
																		}else{																	
																			$message="Désolé vous n'êtes pas autorisé à prendre en charge cette livraison.\nMerci";	
																		}																
																	
																	}else{
																		$message=\Yii::t('app', 'TRY_LATER');
																	}
																}else{
																	$message=\Yii::t('app', 'TRY_LATER');
																}
															}else{
																$message=\Yii::t('app', 'TRY_LATER');
															}								
																									
														}else{
															$message=\Yii::t('app', 'TRY_LATER');												
														}
														
													}else{
														$message=\Yii::t('app', 'TRY_LATER');
													}
													
												}else{
													
													$other='"type_search":"1"';
													$exist_transaction = Ussdtransation::find()->where(['idtransaction'=>$find_transaction->idtransaction,'username'=>$find_transaction->username,'iduser'=>$find_transaction->iduser,'etat_transaction'=>3,'reference'=>$provenance])->orderBy(['id'=>SORT_DESC])->one();
													if($exist_transaction!=null){
															
															$recup_information=Json::decode("[{".$exist_transaction->others."}]");										
															$information=$recup_information[0];
															if(isset($information['userlat']) && isset($information['userlong'])){
																$lat_end=$information['userlat'];
																$long_end=$information['userlong'];
																$full_coordonnee=$lat_end.";".$long_end;												
																$other='"type_search":"1","localisation_user":"'.$full_coordonnee.'","userlat":"'.$lat_end.'","userlong":"'.$long_end.'"';
															}													
															
													}												
													
											
													
													//visite d'une boutique d'un marchand
													$codeMarchand=$test_direct[1];
													//$find_transaction= Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
													
													$find_transaction->others=$other;
													$find_transaction->menu="1";
													$find_transaction->sub_menu=4;
													$find_transaction->message_send="<b>Veuillez taper le numéro d'un marchand</b>\n\n0.*. Annuler le processus";
													$find_transaction->save();
													
													$message=$this->consoleprocess($codeMarchand,$id_trans,$provenance);
												}
											}
											
										}
									}
									
								}	
							}	
						
						}else{		
									
							if( (string)$info_user->nom=="" || (string)$info_user->prenoms=="" ){	
								
									if($info_user->username=="00000000" && $find_transaction->page_menu!="2"){
										$message=$this->ask_provenance($response,$id_trans,$status,$provenance,$info_user);
									}else{
										$message=$this->ask_souscription($response,$id_trans,$status,$provenance,$info_user->username);
									}									
									
							}else if($info_user->status=="30"){		
									$message="Cher(e) ".$info_user->nom." ".$info_user->prenoms."\n";
									$message.="Votre <b>compte AB</b> sur <b>ABusiness</b> a été <b>désactivé</b>";									
									$message.="\n\n<b>Raison de la désactivation:</b> ".$info_user->raison_desactivation;
							}else if($status=="0"){							
								$message=$this->get_first_menu($find_transaction);
								
							}else if($status=="1"){		
								$message=$this->consoleprocess($response,$id_trans,$provenance);
								 
							}else{
								$message=\Yii::t('app', 'FALSE_INFORMATION');
							}
							
							
						}
					
					
				}
			}
				
					
		echo $this->wd_remove_accents($message,$id_trans,$provenance);    
						
		
		
		
    }
		
		
		
	public function active_commande($model){
		
		
		//recuperer les info sur le livreur
		//$find_marchand = ActeurUser::find()->where(['id_acteur_user'=>$model->id_marchand,'type_acteur'=>1])->one();	
		$find_marchand = $model->idMarchand;
		$find_livreur = ActeurUser::find()->where(['id_acteur_user'=>$model->id_livreur,'type_acteur'=>2])->one();	

		$info_commande="\n---------------------\n\n";
		$info_commande.="<b>Détail de la commande</b> \n";
		$tab_produits=explode("-",$model->produit_livraison);														
		$qte_livraison=explode("-",$model->qte_livraison);														
		$all_produits=Mproduits::find()->where(['idProduit'=>$tab_produits])->orderBy('position_menu_ussd ASC')->all();
		
		
		if(sizeof($all_produits)>0){
			$index=0;
			$total_commande=0;
			foreach($all_produits as $info_produits){
				
				$prix=$qte_livraison[$index]*$info_produits->prix_vente;													
				
				$info_commande.="\n<b>".$qte_livraison[$index]." X ".$info_produits->denomination."</b> - ".Utils::show_nbre($info_produits->qte)." ".$info_produits->unite." =".Utils::show_nbre($prix)." FCFA";
				$total_commande+=$prix;
				$index++;
			}
		}
		$info_commande.="\n\n<b>Total:</b> ".Utils::show_nbre($total_commande)." FCFA";
		
		
		//envoie message vers le livreur
		
		//envoie de la notification aux marchand 
		$photo_livreur=(string)$find_livreur->photo_acteur;
		$info_user=$find_livreur->idUser;
		
		
		$info_livreur="<b>Nouvelle commande sur votre boutique</b>\n\n";		
		$info_livreur.="Commande N°: <b>".$model->num_commande."</b>\n\n";
		$info_livreur.="<b>Détail du livreur</b> \n";
		$info_livreur.="Nom: ".$info_user->nom." ".$info_user->prenoms."\n";
		$info_livreur.="Contact: ".$info_user->username."\n";
		$info_livreur.=$info_commande;
		
		$code_commande="TTK".$model->id_user."U".$find_marchand->idUser->id."U".$model->id_commande_marchand."TTKCLIN";
		
		
		
		$info_muser=$find_marchand->idUser;			
		$provenance=$info_muser->canal;
		if($provenance=="TELEGRAM"){			
			$phone_marchand=$info_muser->canal_key;
			$lien_client=$this->lien_telegram."ABSELLER".$find_marchand->code_reference.$code_commande."ABSELLER";	
		}else{
			$phone_marchand=$info_muser->username;
			$lien_client=$this->lien_whatsapp."ABSELLER".$find_marchand->code_reference.$code_commande."ABSELLER";	
		}
		
		$info_livreur.="\n\nVeuillez confirmer la récupération de la commande par le livreur en cliquant sur le lien suivant\n".$lien_client;	
		
		if($photo_livreur==""){
			Utils::send_information($phone_marchand,$info_livreur,$provenance);	
		}else{
			$url_photo=Utils::photobase_cateurs.$photo_livreur;
			Utils::send_pinformation($phone_marchand,$url_photo,$photo_livreur,$info_livreur,$provenance);
		}
		
		
		//envoie message vers le marchand
				
		
		
		$content=$find_marchand->denomination;
		$content.="\nContact: ".$info_muser->username." - ".$find_marchand->address_acteur;
		
		
		$lat=$find_marchand->lat_acteur;
		$long=$find_marchand->long_acteur;
				
		$provenance2=$info_user->canal;
		if($provenance2=="TELEGRAM"){			
			$phone_livreur=$info_user->canal_key;
		}else{
			$phone_livreur=$info_user->username;
		}
		
		// envoie par geolocalisation la position du marchand
		Utils::send_linformation($phone_livreur,$lat,$long,$content,$provenance2);
		
		
	}
	
	public function send_information($model,$type){
		
		
		//recuperer les info sur le livreur
		$find_client = $model->idUser;											
		$find_marchand = $model->idMarchand;											
		$find_livreur = ActeurUser::find()->where(['id_acteur_user'=>$model->id_livreur,'type_acteur'=>2])->one();	
		$info_user=$find_livreur->idUser;

			
		if($type=="1"){	
		
			$info_commande="\n---------------------\n\n";
			$info_commande.="<b>Détail de la commande</b> \n";
			$info_commande.="Commande N°: <b>".$model->num_commande."</b>\n";
			$tab_produits=explode("-",$model->produit_livraison);														
			$qte_livraison=explode("-",$model->qte_livraison);														
			$all_produits=Mproduits::find()->where(['idProduit'=>$tab_produits])->orderBy('position_menu_ussd ASC')->all();
			
			if(sizeof($all_produits)>0){
				$index=0;
				$total_commande=0;
				foreach($all_produits as $info_produits){
					
					$prix=$qte_livraison[$index]*$info_produits->prix_vente;													
					
					$info_commande.="\n<b>".$qte_livraison[$index]." X ".$info_produits->denomination."</b> - ".Utils::show_nbre($info_produits->qte)." ".$info_produits->unite." =".Utils::show_nbre($prix)." FCFA";
					$total_commande+=$prix;
					$index++;
				}
			}
			$info_commande.="\n\n<b>Frais de livraison:</b> ".Utils::show_nbre($model->prix_livraison)." FCFA";
			$info_commande.="\n\n<b>Total:</b> ".Utils::show_nbre($total_commande+$model->prix_livraison)." FCFA";
			
			
			//envoie message vers le livreur
			
			//envoie de la notification aux marchand 
			$photo_livreur=(string)$find_livreur->photo_acteur;

			
			$code_commande="TTF".$model->id_user."U".$find_marchand->idUser->id."U".$model->id_commande_marchand."TTFCLIN";
		
		
			$info_livreur="<b>Commande en cours de livraison</b> \n\n";
			$info_livreur.="<b>Détail du livreur</b> \n";
			$info_livreur.="Nom: ".$info_user->nom." ".$info_user->prenoms."\n";
			$info_livreur.="Contact: ".$info_user->username."\n";
			$info_livreur.=$info_commande;		
			
				
			$provenance=$find_client->canal;
			if($provenance=="TELEGRAM"){			
				$phone_client=$find_client->canal_key;
				$lien_client=$this->lien_telegram."ABSELLER".$find_marchand->code_reference.$code_commande."ABSELLER";
			}else{
				$phone_client=$find_client->username;
				$lien_client=$this->lien_whatsapp."ABSELLER".$find_marchand->code_reference.$code_commande."ABSELLER";
			}
		
			
			$info_livreur.="\n\nVeuillez confirmer la récupération de votre commande  en cliquant sur le lien suivant une fois que le livreur vous aura remis les produits\n".$lien_client;	
						
			if($photo_livreur==""){
				Utils::send_information($phone_client,$info_livreur,$provenance);	
			}else{
				$url_photo=Utils::photobase_cateurs.$photo_livreur;
				Utils::send_pinformation($phone_client,$url_photo,$photo_livreur,$info_livreur,$provenance);
			}
		
		
			//envoie emplacement vers le livreur

			$content=$find_client->nom." ".$find_client->prenoms." - C N°".$model->num_commande;
			$content.="\nContact: ".$find_client->username." - ".$model->lieu_livraison;
			
			$lat=$model->lat_livraison;
			$long=$model->long_livraison;
			
			$provenance=$info_user->canal;
			if($provenance=="TELEGRAM"){			
				$phone=$info_user->canal_key;
			}else{
				$phone=$info_user->username;
			}
			// envoie par geolocalisation la position du client au livreur
			Utils::send_linformation($phone,$lat,$long,$content,$provenance);			
		}else{
			
			
			
			$content="Le client <b>".$find_client->nom." ".$find_client->prenoms."</b> vient de confirmer la réception de sa commande ( <b>".$model->num_commande."</b> ).\n\nVotre paiement vous sera envoyé sur votre compte mobile money dans les plus brefs délais.\nMerci";
			
			$provenance=$info_user->canal;
			if($provenance=="TELEGRAM"){			
				$phone1=$info_user->canal_key;
			}else{
				$phone1=$info_user->username;
			}
			$rec_content=$this->get_ppartenaire(3,$info_user->id,$info_user->username);
			
			if(is_array($rec_content)){
				if($rec_content[0]=="photo"){
					$message.="WA_IMAGEpartenaire/".$rec_content[1]."WA_IMAGE";
				}else if($rec_content[0]=="video"){
					$message.="WA_VIDEOpartenaire/".$rec_content[1]."WA_VIDEO";
				}
			}else{
				Utils::send_information($phone1,$content,$provenance);
			}


            $info_marchand=$find_marchand->idUser;
			$provenance2=$info_marchand->canal;
			if($provenance2=="TELEGRAM"){			
				$phone2=$info_marchand->canal_key;
			}else{
				$phone2=$info_marchand->username;
			}
			$rec_content=$this->get_ppartenaire(3,$info_marchand->id,$info_marchand->username);
			
			if(is_array($rec_content)){
				if($rec_content[0]=="photo"){
					
					$photo_pub=$rec_content[1];
					$url_photo=Utils::photobase_url."partenaire/".$photo_pub;
					Utils::send_pinformation($phone2,$url_photo,$photo_pub,$content,$provenance2);
				
				}else if($rec_content[0]=="video"){
					$video_pub=$rec_content[1];
					$url_video=Utils::photobase_url."partenaire/".$video_pub;
					Utils::send_pinformation($phone2,$url_video,$video_pub,$content,$provenance2);
				}
				
			}else{
				Utils::send_information($phone2,$content,$provenance2);
			}
			
		}
										
		
	}
			
	public function consoleprocess($response,$id_trans,$provenance){		
	     
	    
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction= Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$menu=trim($find_transaction->menu);
		$sub_menu=trim($find_transaction->sub_menu);
		
		$menu_invisible=ConsoleappinitController::MENU_CREDIT;
		
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;
	    $nest_show=ConsoleappinitController::NEXT_SYSTEME;
	    $previor_show=ConsoleappinitController::PREVIOR_SYSTEME;
		
		$message="Empty request";
		$continue=true;
		$rec_menu=$menu;
		if($rec_menu=="-1"){
			$rec_menu=$response;
		}else if( $rec_menu=="-11"){
			if($response==0){
				$message=$this->get_first_menu($find_transaction);	
				$continue=false;				
			}else{
				$rec_menu=$response;
			}
		}
		$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
		$test_menu=MenuUssd::find()->where(['position_menu_ussd'=>$rec_menu,'status'=>'1'])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->one();
		if($continue==true){
			if(  ($rec_menu==$menu_invisible) || ( $test_menu!==null or $rec_menu=="0" or $response==$nest_show or $response==$previor_show or ($rec_menu=="5" && in_array($sub_menu,array(20,21,22,23)))) ){		
			
					switch (trim($menu)) {	   
						
						
						case "-1":
						
								$message=$this->get_sub_menu($response,$find_transaction);							
								
								if($message=="1"){
										
									switch (trim($response)) {
										case "0":											
											$message=$this->get_help_menu($find_transaction);
										break;
										
										case $previor_show:											
											$message=$this->get_previor_menu("0",$find_transaction);										
										break;
										
										case $nest_show:											
											$message=$this->get_next_menu("0",$find_transaction);										
										break;
										
										default:									
											$message =$this->select_function($response,$response,$id_trans,0,$provenance);
										break;
										
									}
									
								}
							
						break;
						
						case "-11":
								if($response!="0"){
									
									$continue=true;
									$search_sub_menu=$response;
									$big_menu=$find_transaction->sub_menu;
									if($response!=$nest_show and $response!=$previor_show){
										
										//verifier si le numero sélectionner fait partie des sub
										$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();		
										$test_submenu=MenuUssd::find()->where(['status'=>'1','client'=>'go_agri','position_menu_ussd'=>$response,'sub_menu'=>$big_menu])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->one();
										if($test_submenu==null || (int)$response==0){
											$continue=false;
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;											
										}else{
											$search_sub_menu=$big_menu."-".$response;
											$find_transaction->menu=(string)$search_sub_menu;
										}										
									}
									
									if($continue==true){
										$find_transaction->sub_menu=1;
										$find_transaction->save();
										
										switch (trim($search_sub_menu)) {								
										
											case $previor_show:										
												$message =$this->get_previor_menu($big_menu,$find_transaction);									
											break;
											
											case $nest_show:										
												$message =$this->get_next_menu($big_menu,$find_transaction);
											break;
											
											default:							
												
												$message =$this->select_function($search_sub_menu,$response,$id_trans,0,$provenance);
											break;
										}
									
									}
									
								}else{
									$message=$this->get_first_menu($find_transaction);	
								}
						break;
						
						case "0":
								switch (trim($response)) {	
									case $previor_show:
											$message=$this->get_hprevior_menu(0,$find_transaction);
									break;
									
									case $nest_show:	
											$message=$this->get_hnext_menu(0,$find_transaction);
									break;
									
									case "0":
											$message=$this->get_first_menu($find_transaction);										
									break;
									
									default:
									
										$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
										$aide_menu=MenuUssd::find()->where(['position_menu_ussd'=>$response,'status'=>'1','client'=>'go_agri'])->andWhere(['like','user_access','"'.$find_user->id_user_profil.'"'])->one();
								
										if($aide_menu!==null){
											$message =$aide_menu->description."\n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');;
										}else{
											$message =\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										}
									break;
									
								}
								
						break;
						
						default:									
							$message =$this->select_function($menu,$response,$id_trans,1,$provenance);
						break;					
						
				 
					}
			
			}else{
					$find_transaction->menu=(string)-1;
					$find_transaction->sub_menu=0;
					$find_transaction->page_menu=1;
					$find_transaction->save();
					$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
			}
		}
		
		return $message;
	   
	}
	
	public function select_function($menu,$response,$id_trans,$type,$provenance){
		
		switch (trim($menu)) {			
			
			case ConsoleappinitController::MENU_CREDIT :                		
				$message =$this->credit_marchand($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_COMMANDE_PACKAGE :                		
				$message =$this->commande_paquet($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_COMMANDE_PRODUIT :                		
				$message =$this->commande_produit($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_DRONE :                		
				$message =$this->reservation_drone($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_PARLER :                		
				$message =$this->parler_operateur($response,$id_trans,$type,$provenance);					
			break;			
			case ConsoleappinitController::MENU_FAQCORONA:                		
				$message =$this->question_corona($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_INFOCORONA:                		
				$message =$this->information_corona($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_DIAGNOSTIQUE:                		
				$message =$this->diagnostique_corona($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_ACTEUR :                		
				$message =$this->demande_acteur($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_MARCHAND :                		
				$message =$this->achat_marchand($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_BOUTIQUEMARCHAND :                		
				$message =$this->boutique_marchand($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_CGU :                		
				$message =$this->condition_generale($response,$id_trans,$type,$provenance);					
			break;
			case ConsoleappinitController::MENU_RECOMMANDER :                		
				$message =$this->recommander_solution($response,$id_trans,$type,$provenance);					
			break;
			
			default:
				$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
				if($find_transaction!=null){
					$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
				}else{
					$message =\Yii::t('app', 'TRY_LATER');
				}
			break;
		}
		
		return $message;
	}
				
	public function recommander_solution($response,$id_trans,$status,$provenance){	   
		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$sub_menu=trim($find_transaction->sub_menu);
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;
	    $nest_show=ConsoleappinitController::NEXT_SYSTEME;
	    $previor_show=ConsoleappinitController::PREVIOR_SYSTEME;
		
		
		switch (trim($status)) {
			case "0":
				    $message=\Yii::t('app', 'NUMERO_FRIEND')." \n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
					$find_transaction->message_send=$message;
					$find_transaction->sub_menu=1;
					$find_transaction->save();
			break;
			
			case "1":
				   
			       if($response=="0"){
							$message=$this->get_first_menu($find_transaction);
				    }else{
						switch (trim($sub_menu)) {
							case "1":	
									
									
									if($find_transaction->username!=$response && $find_transaction->username!="00".$response){
										$message=\Yii::t('app', 'NUMERO_FRIEND_SUGERE')." ".$response."\n\n1.*. ".\Yii::t('app', 'CONFIRM')."\n2.*. ".\Yii::t('app', 'CANCEL')."\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
										$find_transaction->others=$response;
										$find_transaction->message_send=$message;
										$find_transaction->sub_menu=2;
										$find_transaction->save();
									}else{
										$message=\Yii::t('app', 'ERROR_NUMERO')."\n\n".$find_transaction->message_send;
									}
							break;
							case "2":
							
									if($response=="1"){										
											$find_user = User::find()->where(['id'=>$find_transaction->iduser])->one();
											$lien_user="https://t.me/abtogo_bot?start=AB";
											$sms=\Yii::t('app', 'SUGES_MESSAGE')."\n\n".$lien_user."\n\nSuggeré par ".$find_user->username;
											$receiver=$find_transaction->others;
											$this->send_sms($sms,$receiver);
											
											
											$find_transaction->message=$sms;
											$find_transaction->etat_transaction=1;
											$find_transaction->save();	
											
											
											$message=\Yii::t('app', 'SUGGESTION_SUCCESS');
										
									}else if($response=="2"){
											$message=$this->get_first_menu($find_transaction);
									}else {
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
									}
									
							break;
						}
				    }
			break;
		}			   
		
		return $message;
	   
	}
		
	public function parler_operateur($response,$id_trans,$status,$provenance){	   
		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$message=\Yii::t('app', 'parler_operateur'.$provenance);
		
		$find_transaction->message=$message;
		$find_transaction->etat_transaction=1;
		$find_transaction->save();	
		
		$info_user=User::findOne(['id'=>$find_transaction->iduser]);
		$plus=$info_user->nom." ".$info_user->prenoms;
		
		
		$content=$this->get_ppartenaire(3,$find_transaction->iduser,$find_transaction->username);
		if(is_array($content)){
			if($content[0]=="photo"){
				$message.="WA_IMAGEpartenaire/".$content[1]."WA_IMAGE";
			}else if($content[0]=="video"){
				$message.="WA_VIDEOpartenaire/".$content[1]."WA_VIDEO";
			}
		}
		
		
		
		if($provenance=="TELEGRAM"){
			$recup=explode("___",$find_transaction->idtransaction);
			if(sizeof($recup)>1){				
				$message.="CONT_ACT".\Yii::t('app', 'send_operateur2').$recup[1]."\n\n".\Yii::t('app', 'asked_by')."\n".$plus."CONT_ACT";			
			}
		}else{
			
			$message.="CONT_ACT".\Yii::t('app', 'send_operateur1').$find_transaction->username."\n\n".\Yii::t('app', 'asked_by')."\n".$plus."CONT_ACT";			
		}
		
		return $message;
	   
	}
	
	public function ask_provenance($response,$id_trans,$status,$provenance,$info_user){		
	
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		$sub_menu=$find_transaction->sub_menu;
		$message="?";
		
		if($status=="0" || $response=="0"){
			
			    
				$message=\Yii::t('app', 'WELCOME_GOAGRI')."\n\n".\Yii::t('app', 'tape');
				
				if($provenance=="MESSENGER"){
					$message.="\n1.*. ".\Yii::t('app', 'compte_existe_messenger')."\n2.*. ".\Yii::t('app', 'compte_noexiste_messenger')."\n\n10.*. ".\Yii::t('app', 'nos_conditions_messenger');
				}else{
					$message.="\n1.*. ".\Yii::t('app', 'compte_existe')."\n2.*. ".\Yii::t('app', 'compte_noexiste')."\n\n10.*. ".\Yii::t('app', 'nos_conditions');					
				}
				
				$find_transaction->message_send=$message;
				$find_transaction->sub_menu=0;
				$find_transaction->save();
				
		}else{			
			  
					
				switch (trim($sub_menu)) {	
					case "0":
						if($response=="1"){
					
							$message =\Yii::t('app', 'enter_phonenumber');
							$find_transaction->sub_menu=1;
							$find_transaction->message_send=$message;
							$find_transaction->save();
							
							
						}else if($response=="2"){
							$find_transaction->page_menu=2; 
							$find_transaction->save();							
							$message=$this->ask_souscription("0",$id_trans,"0",$provenance,$info_user->username);						
							
						}else if($response=="10"){
							$message=\Yii::t('app', 'SELECT_TYPEACTEUR')."\n\n".\Yii::t('app', 'tape');
							$message.="\n1.*. ".\Yii::t('app', 'cgu_acteur1')."\n2.*. ".\Yii::t('app', 'cgu_acteur2')."\n3.*. ".\Yii::t('app', 'cgu_acteur3')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=10;
							$find_transaction->save();
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
					break ;
					case "1":
							
							//verifier si un compte existe deja avec ce numero
							$find_user = User::find()->where(['username'=>$response])->all();
							if(sizeof($find_user)==0){
								$message=\Yii::t('app', 'ERROR_PHONE').": \n".$find_transaction->message_send;
							}else{
								
									$code=rand(10000,99999);
									$username=$response;
									//if($provenance=="WEBSITE"){
									if((string)$find_user[0]->validation_code==""){
										$sms="Votre code pin est ".$code.". Ce code pin sera utilisé lors de votre prochaine connexion sur le web";									
										$this->send_sms($sms,$username);
										$message =\Yii::t('app', 'enter_pine_code2');
									
									}else{
										$message =\Yii::t('app', 'enter_pine_code1');
										$code=$find_user[0]->validation_code;
									}	
									/*
									}else{
										//passez a lidentifiation
										$sms="Votre code de verification est ".$code;									
										$this->send_sms($sms,$username);
										$message =\Yii::t('app', 'enter_verification_code');
									}
									*/
									
									$orther='"numero":"'.$username.'","code":"'.$code.'"';
									
									$find_transaction->sub_menu=2;
									$find_transaction->others=$orther;
									$find_transaction->message_send=$message;
									$find_transaction->save();														
							} 
							
							
					break;
					case "2":
							
							//verifier si un compte existe deja avec ce numero
							$recup_information=Json::decode("[{".$find_transaction->others."}]");										
							$information=$recup_information[0];
							
							$response=preg_replace('~\D~', '', $response);
							if($information['code']!=trim($response)){
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}else{
								//passez a lidentifiation
								
								$find_user = User::find()->where(['username'=>$information['numero']])->all();
								$find_user[0]->updated_at = time();	
								if($provenance=="WEBSITE"){
									$find_user[0]->validation_code = $information['code'];	
									$find_user[0]->web_key=$info_user->web_key;	
								}else if($provenance=="MESSENGER"){
									$find_user[0]->validation_code = $information['code'];	
									$find_user[0]->messenger_key=$info_user->messenger_key;	
								}else if($provenance=="VIBER"){
									$find_user[0]->validation_code = $information['code'];	
									$find_user[0]->viber_key=$info_user->viber_key;	
								}else{
									$find_user[0]->canal_key=$info_user->canal_key;									
								}
								
								$find_user[0]->updated_at = time();	
								$find_user[0]->save();
								
								//modification de lid du user dans la transaction
								$find_transaction->iduser=$find_user[0]->id;
								$find_transaction->save();
								
								//suppression du compte tampon
								$info_user->updated_at = time();	
								$info_user->status = 30;
								$info_user->canal_key="TAMPON_".$info_user->canal_key;
								$info_user->save();								
								
								$message=$this->get_first_menu($find_transaction);								
							} 
							
							
					break;
					
					case "10":
						$tab_response=array("1","2","3");								
						if(in_array($response,$tab_response)){
							
							$message=Cgu_content_fr::cgu_entete0."\n\n";
							$message.=Cgu_content_fr::cgu_entete."\n\n";							
							if($response==1)$message.=Cgu_content_fr::cgu_content1."\n\n";	
							if($response==2)$message.=Cgu_content_fr::cgu_content2."\n\n";	
							if($response==3)$message.=Cgu_content_fr::cgu_content3."\n\n";	
							
							$message.=Cgu_content_fr::cgu_bas;
							$message.="\n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
							
							
							
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
					break;					
					
				}
				
				
				
		}
		
		
	   return $message;
	}
	
	public function ask_souscription($response,$id_trans,$status,$provenance,$username){		
	
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		$sub_menu=$find_transaction->sub_menu;
		$message="";
		
		if($status=="0"){
			
			    
				$message=\Yii::t('app', 'WELCOME_GOAGRI')."\n\n".\Yii::t('app', 'tape');
				$message.="WA_IMAGEpackages/welcome.jpegWA_IMAGE";
				$message.="\n1.*. ".\Yii::t('app', 'ask_soucription')."\n0.*. ".\Yii::t('app', 'cancel_process')."\n10.*. ".\Yii::t('app', 'nos_conditions');
				
				$find_transaction->message_send=$message;
				$find_transaction->save();
				
		}else{
			
			if($response=="0"){
					$message =\Yii::t('app', 'cancel_operation');
					$find_transaction->message_send=$message;
					$find_transaction->etat_transaction=2;
					$find_transaction->save();													
			}else{
					
				switch (trim($sub_menu)) {	
					case "0":
						if($response=="1"){
							$orther="";
							if($username=="00000000"){
								
								if($provenance=="WEBSITE" || $provenance=="VIBER"){
									$message =\Yii::t('app', 'enter_phonenumber');
									$find_transaction->sub_menu=100;
									$find_transaction->message_send=$message;

								}else{
									$message ="TTB";
									$sms =\Yii::t('app', 'enter_phonenumberbtn');
									$find_transaction->sub_menu=1;
									
									Utils::send_information($find_transaction->username,$sms,$provenance,"1");	
									$find_transaction->message_send=$sms;
								}
								
							}else{
								$orther='"numero":"'.$username.'"';
								$message =\Yii::t('app', 'enter_nom');
								$find_transaction->sub_menu=2;
								$find_transaction->message_send=$message;
							}			
							$find_transaction->others=$orther;
							$find_transaction->save();
							
						}else if($response=="10"){
							$message=\Yii::t('app', 'SELECT_TYPEACTEUR')."\n\n".\Yii::t('app', 'tape');
							$message.="\n1.*. ".\Yii::t('app', 'cgu_acteur1')."\n2.*. ".\Yii::t('app', 'cgu_acteur2')."\n3.*. ".\Yii::t('app', 'cgu_acteur3')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=10;
							$find_transaction->save();
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
					break ;
					case "100":
						
						$numero=preg_replace('~\D~', '', $response);
						
						if(strlen($numero)>=10){
							
								//verifier si le numero existe deja dans le systeme
								$find_user = User::find()->where(['username'=>$numero])->all();
								if(sizeof($find_user)==0){
									
									$message =\Yii::t('app', 'enter_nom');
									
									//verifier si cest un numero en supprim
									$orther='"numero":"'.$numero.'"';
									$find_transaction->others=$orther;
									$find_transaction->message_send=$message;
									$find_transaction->sub_menu=2;
									$find_transaction->save();
									
								}else{		
								
									$message =\Yii::t('app', 'EXIST_PHONE');
									
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=2;
									$find_transaction->save();
									
								}
						}else{
							
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
						
							
					break ;
					case "1":
					
							$test_btnsend=explode("bot_phone/",$response);
							if(sizeof($test_btnsend)==2){
								$numero=preg_replace('~\D~', '', $test_btnsend[1]);
								
								if(strlen($numero)>=10){
									
									    //verifier si le numero existe deja dans le systeme
										$find_user = User::find()->where(['username'=>$numero])->all();
										if(sizeof($find_user)==0){
											$message="";
											$sms =\Yii::t('app', 'enter_nom');
											
											//verifier si cest un numero en supprim
											$orther='"numero":"'.$numero.'"';
											$find_transaction->others=$orther;
											$find_transaction->message_send=$sms;
											$find_transaction->sub_menu=2;
											$find_transaction->save();
											
											Utils::send_information($find_transaction->username,$sms,$provenance,"2");
										}else{		
										
											$sms =\Yii::t('app', 'EXIST_PHONE');
											Utils::send_information($find_transaction->username,$sms,$provenance,"2");
											
											$message="";
											$find_transaction->message_send=$message;
											$find_transaction->etat_transaction=2;
											$find_transaction->save();
											
										}
								}else{
									
									if($provenance=="MESSENGER"){
										$sms =\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										Utils::send_information($find_transaction->username,$sms,$provenance,"1");	
										$message ="TTB";
									}else{
										$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										
									}
								}
							}else{
								
								if($provenance=="MESSENGER"){
									$sms =\Yii::t('app', 'ERROR_TPHONE').": \n".$find_transaction->message_send;
									Utils::send_information($find_transaction->username,$sms,$provenance,"1");	
									$message ="TTB";
								}else{
									$message=\Yii::t('app', 'ERROR_TPHONE').": \n".$find_transaction->message_send;
									
								}
							
							}
							
							
					break;
					case "2":
					
							$orther=$find_transaction->others.',"nom":"'.$response.'"';
							$message =\Yii::t('app', 'enter_prenom');
							$info_message =\Yii::t('app', 'name_souscription').$response;
							$find_transaction->others=$orther;
							$find_transaction->message=$info_message;
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=3;
							$find_transaction->save();
							
							
					break;
					case "3":
					
							$orther=$find_transaction->others.',"prenom":"'.$response.'"';
							$info_message =$find_transaction->message." ".$response;
							$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'CANCEL')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
							$find_transaction->others=$orther;
							$find_transaction->message=$info_message;
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=4;
							$find_transaction->save();
							
							
					break;
					
					case "4":
					
							if($response=="1"){		
									
									$recup_information=Json::decode("[{".$find_transaction->others."}]");										
									$information=$recup_information[0];
									
									$content1=\Yii::t('app', 'succes_souscription');	
									
									
									
									$info_user=User::findOne(['id'=>$find_transaction->iduser]);
									$info_user->nom=$information['nom'];
									$info_user->prenoms=$information['prenom'];
									$info_user->username=$information['numero'];
									$info_user->save();
									
									$content1.="\n\nUtiliser nos services revient à accepter nos Conditions Générales d'Utilisation (CGU) consultable dans le menu <b>10</b>";
									
									//$provenance=$info_user->canal;
									if($provenance=="TELEGRAM"){			
										$phone=$info_user->canal_key;
									}else if($provenance=="MESSENGER"){			
										$phone=$info_user->messenger_key;
									}else{
										$phone=$info_user->username;
									}
			
									$content2=$this->get_first_menu($find_transaction);	
									
									if($provenance=="MESSENGER"){
										$message=$content1."\n".$content2;
									}else{
										Utils::send_information($phone,$content1,$provenance);	
										$message=$content2;
									}
									
								
							}else if($response=="2"){
								$message =\Yii::t('app', 'cancel_souscription');
								$find_transaction->message_send=$message;
								$find_transaction->etat_transaction=2;
								$find_transaction->save();
							}else {
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}
							
					break;
					case "10":
						$tab_response=array("1","2","3");								
						if(in_array($response,$tab_response)){
							
							$message=Cgu_content_fr::cgu_entete0."\n\n";
							$message.=Cgu_content_fr::cgu_entete."\n\n";							
							if($response==1)$message.=Cgu_content_fr::cgu_content1."\n\n";	
							if($response==2)$message.=Cgu_content_fr::cgu_content2."\n\n";	
							if($response==3)$message.=Cgu_content_fr::cgu_content3."\n\n";	
							
							$message.=Cgu_content_fr::cgu_bas;
							$message.="\n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
							
							
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
					break;
					
				}
				
			}	
		}

		if($message==""){
			$message=$find_transaction->message_send;
		}else if($message=="TTB"){
			$message="";
		}
	   return $message;
	}
	
	public function reservation_drone($response,$id_trans,$status,$provenance){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$sub_menu=trim($find_transaction->sub_menu);
		
		$nbre_show=ConsoleappinitController::MENU_SYSTEME;
	    $nest_show=ConsoleappinitController::NEXT_SYSTEME;
	    $previor_show=ConsoleappinitController::PREVIOR_SYSTEME;
		
		
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
						$message=\Yii::t('app', 'place_pulverisation');
						$find_transaction->message_send=$message;
						$find_transaction->sub_menu=1;
						$find_transaction->save();
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":
						
								$orther='"lieu":"'.$response.'"';
								$info_message=\Yii::t('app', 'place_pulverisation_tense1').$response;							
														
								$message=\Yii::t('app', 'place_estimation');
								$find_transaction->message_send=$message;
								$find_transaction->message=$info_message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=2;
								$find_transaction->save();
						break;
						case "2":
								
									$orther=$find_transaction->others.',"surface":"'.$response.'"';
									$info_message=$find_transaction->message.", ".\Yii::t('app', 'place_pulverisation_tense2')." ".$response;
									
									$message=\Yii::t('app', 'place_contact');
									$find_transaction->message_send=$message;
									$find_transaction->message=$info_message;
									$find_transaction->others=$orther;
									$find_transaction->sub_menu=3;
									$find_transaction->save();
								
						break;
						case "3":								
								$orther=$find_transaction->others.',"contact":"'.$response.'"';
								$info_message=$find_transaction->message.".  ".\Yii::t('app', 'place_pulverisation_tense3')." ".$response;
								
								$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'CANCEL')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
								$find_transaction->others=$response;
								$find_transaction->message_send=$message;
								$find_transaction->message=$info_message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=4;
								$find_transaction->save();
						break;
						case "4":
						
								if($response=="1"){		
										
										$recup_information=Json::decode("[{".$find_transaction->others."}]");										
										$information=$recup_information[0];											
										
										$lieu= $information['lieu'];
										$surface= $information['surface'];
										$contact= $information['contact'];
										
										$message=\Yii::t('app', 'admin_pulverisation_tense1').$lieu.\Yii::t('app', 'admin_pulverisation_tense2').$surface.\Yii::t('app', 'admin_pulverisation_tense3');
										
										
										$find_transaction->message_send=$message;
										$find_transaction->etat_transaction=1;
										$find_transaction->save();	
										
										
										$info_user=User::findOne(['id'=>$find_transaction->iduser]);
										$plus=$info_user->nom." ".$info_user->prenoms .". ";
										if($provenance=="TELEGRAM"){
											$recup=explode("___",$find_transaction->idtransaction);
											if(sizeof($recup)>1){				
												$plus.="Lien: https://t.me/".$recup[1]." (".$find_transaction->username.")";
											}
										}else{											
											$plus.="Lien: https://wa.me/".$find_transaction->username;			
										}
										$message.="#####".$find_transaction->message."\n\n".\Yii::t('app', 'admin_pulverisation_tense4')."\n".$plus."#####";
										
										
									
								}else if($response=="2"){
									$message=$this->get_first_menu($find_transaction);	
								}else {
									$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
								}
								
						break;
					}
				
				break;
			}			   
		
		}
		return $message;
	   
	}
		
	public function commande_produit($response,$id_trans,$status,$provenance){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$sub_menu=trim($find_transaction->sub_menu);
				
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
					$message=\Yii::t('app', 'select_produit') ;
					//recuperation de la liste des produits
					$all_produits=Produits::find()->where(['etat'=>'1','type_produit'=>1])->orderBy('position_menu_ussd ASC')->all();
					if(sizeof($all_produits)>0){
						
						$nbre_show=sizeof($all_produits);
						for($i=0;$i<$nbre_show;$i++){
							if($message!="")$message.="\n";
							$message.=$all_produits[$i]->position_menu_ussd.".*. ".$all_produits[$i]->denomination;							
						}	
						
						$find_transaction->message_send=$message;
						$find_transaction->sub_menu=1;
						$find_transaction->save();
					}else{
						$message =\Yii::t('app', 'TRY_LATER');
						$find_transaction->message_send=$message;
						$find_transaction->etat_transaction=2;
						$find_transaction->save();	
					}
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":
						    $find_produits=Produits::find()->where(['etat'=>'1','type_produit'=>1,'position_menu_ussd'=>$response])->one();						
								
							if($find_produits!==null){
								
								$info_message=\Yii::t('app', 'produit_selected').$find_produits->denomination;	
								$idProduit=$find_produits->idProduit;
								$orther='"idProduit":"'.$idProduit.'"';
								
								$message=\Yii::t('app', 'select_qte');
								//recuperation des quantites disponible
								$all_prices=ProduitsQte::find()->where(['etat'=>'1','idProduit'=>$idProduit])->orderBy('prixLivraison ASC')->all();
								if(sizeof($all_prices)>0){
									
									$nbre_show=sizeof($all_prices);
									for($i=0;$i<$nbre_show;$i++){
										if($message!="")$message.="\n";
										$message.=($i+1).".*. ".$all_prices[$i]->qte." ".$find_produits->unite." - ".$this->show_nbre($all_prices[$i]->prixLivraison)." F CFA";							
									}	
									
									$find_transaction->message_send=$message;
									$find_transaction->message=$info_message;
									$find_transaction->others=$orther;
									$find_transaction->sub_menu=2;
									$find_transaction->save();
								
								}else{
									$message =\Yii::t('app', 'TRY_LATER');
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=2;
									$find_transaction->save();	
								}
								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "2":
						    $recup_information=Json::decode("[{".$find_transaction->others."}]");										
							$information=$recup_information[0];											
							
							$idProduit= $information['idProduit'];
							
							
							$all_prices=ProduitsQte::find()->where(['etat'=>'1','idProduit'=>$idProduit])->orderBy('prixLivraison ASC')->all();
							$nbre_price=sizeof($all_prices);
							if($nbre_price>0){
								
								$qte_selected=(int)$response;
								if($qte_selected<=$nbre_price){
									
									$info_price=$all_prices[($qte_selected-1)];
								
									$orther=$find_transaction->others.',"idQte":"'.$info_price->idproduits_qte.'"';	
									
									$info_message=$find_transaction->message.\Yii::t('app', 'qte_selected').$info_price->qte." ".$info_price->idProduit0->unite." pour ".$this->show_nbre($info_price->prixLivraison)." FCFA.";
									
									$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'CANCEL')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
									
									$find_transaction->message_send=$message;
									$find_transaction->message=$info_message;
									$find_transaction->others=$orther;
									$find_transaction->sub_menu=3;
									$find_transaction->save();
								
								}else{
									$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
								}
							
							}else{
								$message =\Yii::t('app', 'TRY_LATER');
								$find_transaction->message_send=$message;
								$find_transaction->etat_transaction=2;
								$find_transaction->save();	
							}							
															
						break;						
						case "3":
						
								if($response=="1"){		
										
										$message=\Yii::t('app', 'success_command');
										
										$find_transaction->message_send=$message;
										$find_transaction->etat_transaction=1;
										$find_transaction->save();	
																				
										$info_user=User::findOne(['id'=>$find_transaction->iduser]);
										$plus=$info_user->nom." ".$info_user->prenoms.". ";
										
										if($provenance=="TELEGRAM"){
											$recup=explode("___",$find_transaction->idtransaction);
											if(sizeof($recup)>1){				
												$plus.="Lien: https://t.me/".$recup[1]." (".$find_transaction->username.")";
											}
										}else{											
											$plus.="Lien: https://wa.me/".$find_transaction->username;			
										}
										
										$message.="LIVRA_ISON".$find_transaction->message."\n\n".\Yii::t('app', 'commanded_by')."\n".$plus."LIVRA_ISON";
										
									
								}else if($response=="2"){
									$message=$this->get_first_menu($find_transaction);	
								}else {
									$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
								}
								
						break;
					}
				
				break;
			}			   
		
		}
		return $message;
	   
	}
		
	public function commande_paquet($response,$id_trans,$status,$provenance){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$sub_menu=trim($find_transaction->sub_menu);
				
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
					$message=\Yii::t('app', 'select_package');
					//recuperation de la liste des produits
					$all_produits=Package::find()->where(['etat'=>'1'])->orderBy('position_menu_ussd ASC')->all();
					if(sizeof($all_produits)>0){
						
						$nbre_show=sizeof($all_produits);
						for($i=0;$i<$nbre_show;$i++){
							if($message!="")$message.="\n";
							$message.=$all_produits[$i]->position_menu_ussd.".*.".\Yii::t('app', 'deno_package').$all_produits[$i]->denomination." (".$this->show_nbre($all_produits[$i]->prix_package)." FCFA)";							
						}	
						
						$find_transaction->message_send=$message;
						$find_transaction->sub_menu=1;
						$find_transaction->save();
					}else{
						$message =\Yii::t('app', 'TRY_LATER');
						$find_transaction->message_send=$message;
						$find_transaction->etat_transaction=2;
						$find_transaction->save();	
					}
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":
						    $find_packages=Package::find()->where(['etat'=>'1','position_menu_ussd'=>$response])->one();						
								
							if($find_packages!==null){
								
								$photo_package=$find_packages->photo_package;
								
								//recuperer les details du package
								$all_details=PackageInfo::find()->where(['etat'=>'1','idPackage'=>$find_packages->idPackage])->all();
								if(sizeof($all_details)>0){
									$info_message=\Yii::t('app', 'package_selected').$find_packages->denomination."\nPrix total: ".$this->show_nbre($find_packages->prix_package)." FCFA\n";
									
									foreach($all_details as $info_details){										
										$info_message.="\n- ".$info_details->idProduit0->denomination.": ".$info_details->qte." ".$info_details->idProduit0->unite;
										
										$data=$info_details->idProduit0;
										if($data->qte_unitaire!==null && $data->qte_unitaire!==""){
											$total_poids=$this->show_nbre($info_details->qte*$data->qte_unitaire/1000);
											$info_message.=" / ".$total_poids."Kg";
										}									
									}
									$info_message.="WA_IMAGEpackages/".$photo_package."WA_IMAGE";
									
									$idPackage=$find_packages->idPackage;
									$orther='"idPackage":"'.$idPackage.'"';
									
									$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'RETOUR')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
									
									$find_transaction->message_send=$message;
									$find_transaction->message=$info_message;
									$find_transaction->others=$orther;
									$find_transaction->sub_menu=2;
									$find_transaction->save();
									
								}else{
									
									$message =\Yii::t('app', 'TRY_LATER');
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=2;
									$find_transaction->save();	
									
								}															
								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;						
						case "2":
						
								if($response=="1"){	

									$recup_information=Json::decode("[{".$find_transaction->others."}]");										
									$information=$recup_information[0];	
									$idPackage= $information['idPackage'];
									$find_packages=Package::find()->where(['etat'=>'1','idPackage'=>$idPackage])->one();
									if($find_packages!=null){
									
										$other_message=\Yii::t('app', 'package_selected').$find_packages->denomination."\nPrix total: ".$this->show_nbre($find_packages->prix_package)." FCFA";
										
										$message=\Yii::t('app', 'success_command');
										
										$find_transaction->message_send=$message;
										$find_transaction->etat_transaction=1;
										$find_transaction->save();	
										
										
										$info_user=User::findOne(['id'=>$find_transaction->iduser]);
										$plus=$info_user->nom." ".$info_user->prenoms .". ";
										
										if($provenance=="TELEGRAM"){
											$recup=explode("___",$find_transaction->idtransaction);
											if(sizeof($recup)>1){				
												$plus.="Lien: https://t.me/".$recup[1]." (".$find_transaction->username.")";
											}
										}else{											
											$plus.="Lien: https://wa.me/".$find_transaction->username;			
										}
										
										$message.="LIVRA_ISON".$other_message."\n\n".\Yii::t('app', 'commanded_by')."\n".$plus."LIVRA_ISON";
										
									}else{
										$message =\Yii::t('app', 'TRY_LATER');
										$find_transaction->message_send=$message;
										$find_transaction->etat_transaction=2;
										$find_transaction->save();										
									}										
									
								}else if($response=="2"){
									
									$message=\Yii::t('app', 'select_package');
									//recuperation de la liste des produits
									$all_produits=Package::find()->where(['etat'=>'1'])->orderBy('position_menu_ussd ASC')->all();
									if(sizeof($all_produits)>0){
										
										$nbre_show=sizeof($all_produits);
										for($i=0;$i<$nbre_show;$i++){
											if($message!="")$message.="\n";
											$message.=$all_produits[$i]->position_menu_ussd.".*.".\Yii::t('app', 'deno_package').$all_produits[$i]->denomination." (".$this->show_nbre($all_produits[$i]->prix_package)." FCFA)";							
										}	
										
										$find_transaction->message_send=$message;
										$find_transaction->sub_menu=1;
										$find_transaction->save();
									}else{
										$message =\Yii::t('app', 'TRY_LATER');
										$find_transaction->message_send=$message;
										$find_transaction->etat_transaction=2;
										$find_transaction->save();	
									}
									
								}else {
									$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
								}
								
						break;
					}
				
				break;
			}			   
		
		}
		return $message;
	   
	}
	
	public function question_corona($response,$id_trans,$status,$provenance){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$sub_menu=trim($find_transaction->sub_menu);
				
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
					$message=\Yii::t('app', 'select_question')."\n\n".\Yii::t('app', 'tape') ;
					//recuperation de la liste des produits
					$all_questions=FaqCovid::find()->where(['etat'=>'1','type_faq'=>1])->orderBy('position_menu_ussd ASC')->all();
					if(sizeof($all_questions)>0){
						
						$nbre_show=sizeof($all_questions);
						for($i=0;$i<$nbre_show;$i++){
							if($message!="")$message.="\n\n";
							$message.=$all_questions[$i]->position_menu_ussd.".*. ".$all_questions[$i]->question;							
						}	
						
						$find_transaction->message_send=$message;
						$find_transaction->others='"all_idFaq":"--"';						
						$find_transaction->sub_menu=1;
						$find_transaction->save();
					}else{
						$message =\Yii::t('app', 'TRY_LATER');
						$find_transaction->message_send=$message;
						$find_transaction->etat_transaction=2;
						$find_transaction->save();	
					}
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":
						    $find_question=FaqCovid::find()->where(['etat'=>'1','type_faq'=>1,'position_menu_ussd'=>$response])->one();						
								
							if($find_question!==null){
								
								$info_message="<b>".$find_question->question."</b> \n\n".$find_question->reponse;	
								$idFaq=$find_question->idFaq;
								
								$recup_information=Json::decode("[{".$find_transaction->others."}]");										
								$information=$recup_information[0];	
								$all_idFaq= $information['all_idFaq'];
								if($all_idFaq=="--"){
									$all_idFaq=$idFaq;
								}else{
									$all_idFaq.=",".$idFaq;									
								}
								
								$orther='"all_idFaq":"'.$all_idFaq.'"';								
								$message=$info_message."\n\n".\Yii::t('app', 'tape')."\n1.*.".\Yii::t('app', 'MENU_QUESTION')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
								
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$info_message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=2;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "2":			
							if($response=="1"){
								
								$message=\Yii::t('app', 'select_question')."\n\n".\Yii::t('app', 'tape') ;
								//recuperation de la liste des produits
								$all_questions=FaqCovid::find()->where(['etat'=>'1','type_faq'=>1])->orderBy('position_menu_ussd ASC')->all();
								if(sizeof($all_questions)>0){
									
									$nbre_show=sizeof($all_questions);
									for($i=0;$i<$nbre_show;$i++){
										if($message!="")$message.="\n\n";
										$message.=$all_questions[$i]->position_menu_ussd.".*. ".$all_questions[$i]->question;							
									}	
									
									$find_transaction->message_send=$message;					
									$find_transaction->sub_menu=1;
									$find_transaction->save();
								}else{
									$message =\Yii::t('app', 'TRY_LATER');
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=2;
									$find_transaction->save();	
								}
								
							}else {
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}
							
						break;
						
					}				
				break;
				
			}			   
		
		}
		return $message;
	   
	}
	
	public function information_corona($response,$id_trans,$status,$provenance){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$sub_menu=trim($find_transaction->sub_menu);
				
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
					$message=\Yii::t('app', 'select_info')."\n\n".\Yii::t('app', 'tape') ;
					//recuperation de la liste des produits
					$all_questions=FaqCovid::find()->where(['etat'=>'1','type_faq'=>2])->orderBy('position_menu_ussd ASC')->all();
					if(sizeof($all_questions)>0){
						
						$nbre_show=sizeof($all_questions);
						for($i=0;$i<$nbre_show;$i++){
							if($message!="")$message.="\n\n";
							$message.=$all_questions[$i]->position_menu_ussd.".*. ".$all_questions[$i]->question;							
						}	
						
						$find_transaction->message_send=$message;
						$find_transaction->others='"all_idFaq":"--"';						
						$find_transaction->sub_menu=1;
						$find_transaction->save();
					}else{
						$message =\Yii::t('app', 'TRY_LATER');
						$find_transaction->message_send=$message;
						$find_transaction->etat_transaction=2;
						$find_transaction->save();	
					}
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":
						    $find_question=FaqCovid::find()->where(['etat'=>'1','type_faq'=>2,'position_menu_ussd'=>$response])->one();						
								
							if($find_question!==null && (int)$response>0){ 
								
								$info_message="<b>".$find_question->question."</b> \n\n".$find_question->reponse;	
								$idFaq=$find_question->idFaq;
								
								$recup_information=Json::decode("[{".$find_transaction->others."}]");										
								$information=$recup_information[0];	
								$all_idFaq= $information['all_idFaq'];
								if($all_idFaq=="--"){
									$all_idFaq=$idFaq;
								}else{
									$all_idFaq.=",".$idFaq;									
								}
								
								$orther='"all_idFaq":"'.$all_idFaq.'"';								
								$message=$info_message."\n\n".\Yii::t('app', 'tape')."\n1.*.".\Yii::t('app', 'MENU_INFO')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
								
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$info_message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=2;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "2":			
							if($response=="1"){
								
								$message=\Yii::t('app', 'select_info')."\n\n".\Yii::t('app', 'tape') ;
								//recuperation de la liste des produits
								$all_questions=FaqCovid::find()->where(['etat'=>'1','type_faq'=>2])->orderBy('position_menu_ussd ASC')->all();
								if(sizeof($all_questions)>0){
									
									$nbre_show=sizeof($all_questions);
									for($i=0;$i<$nbre_show;$i++){
										if($message!="")$message.="\n\n";
										$message.=$all_questions[$i]->position_menu_ussd.".*. ".$all_questions[$i]->question;							
									}	
									
									$find_transaction->message_send=$message;					
									$find_transaction->sub_menu=1;
									$find_transaction->save();
								}else{
									$message =\Yii::t('app', 'TRY_LATER');
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=2;
									$find_transaction->save();	
								}
								
							}else {
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}
							
						break;
						
					}				
				break;
				
			}			   
		
		}
		return $message;
	   
	}
		
	public function diagnostique_corona($response,$id_trans,$status,$provenance){	  

		
		$id_trans=trim($id_trans);
		$response=trim($response);
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		
		$sub_menu=trim($find_transaction->sub_menu);
				
		if($response=="0"){
			$message=$this->get_first_menu($find_transaction);	
		}else{
			
			switch (trim($status)) {
				case "0":
					$message=\Yii::t('app', 'question_1')."\n\n".\Yii::t('app', 'tape');
					$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'jesaispas');
					$find_transaction->message_send=$message;					
					$find_transaction->sub_menu=1;
					$find_transaction->save();
					
				break;
				case "1":
					switch (trim($sub_menu)) {
						case "1":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther='"question_1":"'.$response.'"';	
								$sub_menu=2;

								if($response=="1" || $response=="3"){
									$sub_menu=100;	
									$message=\Yii::t('app', 'question_100');
								}else{
									$orther.=',"question_100":"37.5"';
									$message=\Yii::t('app', 'question_2')."\n\n".\Yii::t('app', 'tape');
									$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									$sub_menu=2;
								}
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=$sub_menu;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						
						break;
						case "100":
							$format_response=str_replace(" ","",$response);
							$format_response=str_replace(",",".",$format_response);
						    								
							if(is_numeric($format_response)){								
								$orther=$find_transaction->others.',"question_100":"'.$format_response.'"';	
								
								$message=\Yii::t('app', 'question_2')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=2;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "2":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_2":"'.$response.'"';
								$message=\Yii::t('app', 'question_3')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=3;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "3":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_3":"'.$response.'"';
								$message=\Yii::t('app', 'question_4')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=4;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "4":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_4":"'.$response.'"';
								$message=\Yii::t('app', 'question_5')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=5;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "5":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_5":"'.$response.'"';
								$message=\Yii::t('app', 'question_6')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=6;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "6":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								
								
								$sub_menu=7;

								if($response=="1"){
									$orther=$find_transaction->others.',"question_6":"'.$response.'"';
									$sub_menu=600;	
									$message=\Yii::t('app', 'question_600')."\n\n".\Yii::t('app', 'tape');
									$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
								}else{
									$orther=$find_transaction->others.',"question_6":"'.$response.'","question_600":"2"';
									$sub_menu=7;
									$message=\Yii::t('app', 'question_7')."\n\n".\Yii::t('app', 'tape');
									$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
								}
								
								
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=$sub_menu;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "600":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_600":"'.$response.'"';
								$message=\Yii::t('app', 'question_7')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=7;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "7":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_7":"'.$response.'"';
								$message=\Yii::t('app', 'question_8')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=8;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "8":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_8":"'.$response.'"';
								
								$message=\Yii::t('app', 'question_9');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=9;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "9":
							$format_response=str_replace(" ","",$response);
							$format_response=str_replace(",","",$format_response);
							$format_response=str_replace(".","",$format_response);
							$format_response=str_replace("ans","",$format_response);
						    								
							if(is_numeric($format_response)){								
								$orther=$find_transaction->others.',"question_9":"'.$format_response.'"';

								if($format_response<15){
									
									$message=\Yii::t('app', 'age_diagnostique');
										
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=1;
									$find_transaction->save();	
									
								}else{
								
									$message=\Yii::t('app', 'question_10');
										
									$find_transaction->message_send=$message;
									$find_transaction->message=$message;
									$find_transaction->others=$orther;
									$find_transaction->sub_menu=10;
									$find_transaction->save();		
								}								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "10":
							$format_response=str_replace(" ","",$response);
							$format_response=str_replace(",",".",$format_response);
							$format_response=str_replace("kg","",$format_response);
						    								
							if(is_numeric($format_response)){								
								$orther=$find_transaction->others.',"question_10":"'.$format_response.'"';
								
								$message=\Yii::t('app', 'question_11');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=11;
								$find_transaction->save();		
																
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "11":
							$format_response=str_replace(" ","",$response);
							$format_response=str_replace(",",".",$format_response);
							$format_response=str_replace("cm","",$format_response);
						    								
							if(is_numeric($format_response)){								
								$orther=$find_transaction->others.',"question_11":"'.$format_response.'"';
								
								$message=\Yii::t('app', 'question_12')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'jesaispas');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=12;
								$find_transaction->save();		
																
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "12":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_12":"'.$response.'"';
								$message=\Yii::t('app', 'question_13')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=13;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "13":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_13":"'.$response.'"';
								$message=\Yii::t('app', 'question_14')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=14;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "14":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_14":"'.$response.'"';
								$message=\Yii::t('app', 'question_15')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=15;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "15":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_15":"'.$response.'"';
								$message=\Yii::t('app', 'question_16')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=16;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "16":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_16":"'.$response.'"';
								$message=\Yii::t('app', 'question_17')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=17;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "17":						
						    $tab_response=array("1","2");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_17":"'.$response.'"';
								$message=\Yii::t('app', 'question_18')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'no_applicable');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=18;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "18":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_18":"'.$response.'"';
								$message=\Yii::t('app', 'question_19')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'jesaispas');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=19;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "19":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_19":"'.$response.'"';
								$message=\Yii::t('app', 'question_20')."\n\n".\Yii::t('app', 'tape');
								$message.="\n1.*. ".\Yii::t('app', 'oui')."\n2.*. ".\Yii::t('app', 'non')."\n3.*. ".\Yii::t('app', 'jesaispas');
									
								$find_transaction->message_send=$message;
								$find_transaction->message=$message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=20;
								$find_transaction->save();								
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
						case "20":						
						    $tab_response=array("1","2","3");								
							if(in_array($response,$tab_response)){								
								$orther=$find_transaction->others.',"question_20":"'.$response.'"';
								
								$resultat=$this->diagnostique_resultat($orther);
								$type_message=$resultat[0];
								$message=$resultat[1];
								
								
								if($type_message==6 || $type_message==7 || $type_message==8){
									$info_user=User::findOne(['id'=>$find_transaction->iduser]);
									$plus="<b>".$info_user->nom." ".$info_user->prenoms."</b>";
									$message.="GOOD_DIAGNOSTIC".\Yii::t('app', 'share_resultat1').$plus.".\n\n".\Yii::t('app', 'share_resultat2').".\n\n".\Yii::t('app', 'share_resultat3').".\n".\Yii::t('app', 'share_resultat4').".\n\n".\Yii::t('app', 'share_resultat5')."GOOD_DIAGNOSTIC";
								}
								
								$find_transaction->others=$orther;
								$find_transaction->message_send=$message;
								$find_transaction->message=json_encode($resultat);
								$find_transaction->etat_transaction=1;
								$find_transaction->save();
								
							}else{
								$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
							}								
						break;
					}				
				break;
				
			}			   
		
		}
		return $message;
	   
	}
	
	public function demande_acteur($response,$id_trans,$status,$provenance){				
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		$sub_menu=$find_transaction->sub_menu;
		if($status=="0"){
			
				$message=\Yii::t('app', 'SELECT_ACTEUR')."\n\n".\Yii::t('app', 'tape');
				$message.="\n1.*. ".\Yii::t('app', 'acteur1')."\n2.*. ".\Yii::t('app', 'acteur2')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
				$find_transaction->message_send=$message;
				$find_transaction->sub_menu=1;
				$find_transaction->save();
		}else{
			
			if($response=="0"){
					$message=$this->get_first_menu($find_transaction);													
			}else{
					
				switch (trim($sub_menu)) {	
					case "1":
						 $tab_response=array("1","2");								
						if(in_array($response,$tab_response)){

							
							$orther='"type_acteur":"'.$response.'"';
							
							$iduser= $find_transaction->iduser;
							$find_acteur = ActeurUser::findOne(['id_user'=>$iduser,'etat'=>[0,1],'type_acteur'=>$response]);
								 
							if($find_acteur==null){
								
								$type_compte=" livreur";
								$message=\Yii::t('app', 'type_moyendeplacement');
								if($response==1){
									$message=\Yii::t('app', 'nom_boutique');
									$message.="\n".\Yii::t('app', 'nbnom_boutique');
									$type_compte=" marchand";
								}
								$info_message=\Yii::t('app', 'demande_creation').$type_compte;
								$find_transaction->message_send=$message;
								$find_transaction->message=$info_message;
								$find_transaction->others=$orther;
								$find_transaction->sub_menu=2;
								$find_transaction->save();	
								
							}else{		    
								$message=\Yii::t('app', 'exist_acteur')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
								$find_transaction->message_send=$message;
								$find_transaction->others=$orther;
								$find_transaction->etat_transaction=2;
								$find_transaction->save();				
							}
							
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}								
						
					break ;
					case "7":
									
						if($response=="1"){	
								$recup_information=Json::decode("[{".$find_transaction->others."}]");										
								$information=$recup_information[0];							
								$type_acteur= $information['type_acteur'];
								$lat= $information['lat'];
								$long= $information['long'];
								$number= $information['autre_numero'];
								
								$iduser= $find_transaction->iduser;
								$id_trans= $find_transaction->id;
								
								$deno= "";
								$address= "";
								$activite= "";
								$pourcentage= 0;
								$etat= 0;
								
								if($type_acteur=="1"){
									$deno= $information['nom_boutique'];
									$address= $information['adresse_boutique'];
									$activite= $information['description_activite'];
									$pourcentage= $this->pourcentage_marchand;
									$etat= 1;
								}else if($type_acteur=="2"){
									$deno= $information['type_moyendeplacement'];
									$address= $information['adresse_livreur'];
									$pourcentage= $this->pourcentage_livreur;
								}
								
								
								$info_user=User::findOne(['id'=>$iduser]);
								
								
								$continue=true;
								$reference="CF".rand(10000,99999);
								while($continue){
									$test_marchand = ActeurUser::find()->where(['code_reference'=>$reference])->one();
									if($test_marchand!=null){
										$reference="CF".rand(10000,99999);
									}else{
										$continue=false;
									}
								}
								
								
								$acteur_chaine = new ActeurUser();
								$acteur_chaine->id_user = $iduser;
								$acteur_chaine->idtransaction = $id_trans;
								$acteur_chaine->code_reference = $reference;
								$acteur_chaine->type_acteur = (int) $type_acteur;
								$acteur_chaine->pourcentage = (double) $pourcentage;
								$acteur_chaine->denomination = $deno;
								$acteur_chaine->address_acteur = $address;
								$acteur_chaine->description_activite = $activite;
								$acteur_chaine->lat_acteur = $lat;
								$acteur_chaine->long_acteur = $long;
								$acteur_chaine->other_number = $number;	
								$acteur_chaine->id_collecteur = $info_user->id_collecteur;	
								$acteur_chaine->etat = 0;	
								$acteur_chaine->acteur_key = Yii::$app->security->generateRandomString(32);	
								if($acteur_chaine->save()){
									
									
									$message=\Yii::t('app', 'success_demande');	
									if($type_acteur=="1"){
										
										
										$code_user="TTR".$info_user->id."TTR".$acteur_chaine->type_acteur."TTR".rand(100,999)."CLIN";
										
										if($provenance=="TELEGRAM"){
											$lien_user=$this->lien_telegram."ABSELLER".$acteur_chaine->id_acteur_user.$code_user."ABSELLER";
										}else {
											$lien_user=$this->lien_whatsapp."ABSELLER".$acteur_chaine->id_acteur_user.$code_user."ABSELLER";											
										}
				
				
										$sms="Cher(e) Client ".$info_user->nom." ".$info_user->prenoms."\n";
										$sms.="Votre demande de <b>compte marchand</b> sur <b>ABusiness</b> a été acceptée.\n\n";
										$sms.="<b>Attention ce n'est pas fini!!!</b>\n\n";
										$sms.="Pour obtenir le statut validé et être visible à des clients qui recherchent des produits dans votre entourage,vous devez ajouter la position de géolocalisation, une photo de votre commerce et publier quelques produits disponibles en accédant au <b>menu 5</b> ou directement en cliquant sur le lien ci-dessous et en envoyant automatiquement le code contenu dans votre champ de saisie\n".$lien_user;
										$sms.="\n\n<b>Denomination:</b> ".$acteur_chaine->denomination;	
										$sms.="\n".$acteur_chaine->description_activite;	
										$sms.="\n<b>Adresse lieu:</b> ".$acteur_chaine->address_acteur;	
										$sms.="\n<b>Autre contact:</b> ".$acteur_chaine->other_number;
										
										$info_user->id_user_profil=4;
										if($info_user->idProvenance==0){
											
											
											$password=rand(100000,999999);
											$info_user->idProvenance=1;
											$info_user->password_hash=Yii::$app->security->generatePasswordHash($password);
											$info_user->status=10;
											
											$sms.="\n\n<b>Information de connexion:</b>";		
											$sms.="\n<b>Host:</b> ".Utils::host;	
											$sms.="\n<b>Login:</b> ".$info_user->username;	
											$sms.="\n<b>Mot de passe:</b> ".$password;	
											$sms.="\n\n<b>Code d'identification marchand:</b> ".$reference;	
										}
										$info_user->save();
										
										$message=$sms;
									}
									
									$find_transaction->message_send=$message;
									$find_transaction->etat_transaction=1;
									$find_transaction->save();										
									
									
									
									if($type_acteur=="1"){
										
										$test_collecteur=Collecteur::find()->where(['idCollecteur'=>$info_user->id_collecteur])->one();
										if($test_collecteur !== null && $info_user->id_collecteur!=1 && trim($test_collecteur->phone_collecteur)!=""){
											
											$telegram_collecteur=trim($test_collecteur->info_supplementaire);
											
											
											//envoyer le code de reference au collecteur
											$content ="La demande de <b>compte marchand</b> sur <b>ABusiness</b> a été acceptée.";	
											$content.="\n\n<b>Denomination:</b> ".$acteur_chaine->denomination;	
											$content.="\n".$acteur_chaine->description_activite;	
											$content.="\n<b>Adresse lieu:</b> ".$acteur_chaine->address_acteur;	
											$content.="\n<b>Autre contact:</b> ".$acteur_chaine->other_number;
											$content.="\n\n<b>Code d'identification marchand:</b> ".$reference;
											$content.="\n\n".$info_user->nom." ".$info_user->prenoms ." - Contact: ".$info_user->username;
											Utils::send_information($telegram_collecteur,$content,"TELEGRAM");	
											
											
											$phone_collecteur=trim($test_collecteur->phone_collecteur);
											Utils::send_information($phone_collecteur,$content,"WHATSAPP");	
												
											
										}										
									}
								
								}else{
									$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
								}
								
								
															
							
						}else if($response=="2"){
							
							$message=$this->get_first_menu($find_transaction);
							
						}else {
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
							
					break;
					
					default:
						$message=$find_transaction->others;
						
						$recup_information=Json::decode("[{".$find_transaction->others."}]");										
						$information=$recup_information[0];							
						$type_acteur= $information['type_acteur'];
						if($type_acteur=="1"){	
						
							switch (trim($sub_menu)) {	
									case "2":
					
											$orther=$find_transaction->others.',"nom_boutique":"'.$response.'"';
											$info_message =$find_transaction->message."\n".\Yii::t('app', 'name_boutique').$response;
											$message =\Yii::t('app', 'description_activite')." <b>(".trim($response).")</b>";
											
											
											$find_transaction->others=$orther;
											$find_transaction->message=$info_message;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=3;
											$find_transaction->save();
											
											
									break;
									case "3":
					
											if(strlen($response)<=150){
												$recup_information=Json::decode("[{".$find_transaction->others."}]");										
												$information=$recup_information[0];							
												$nom_boutique= $information['nom_boutique'];
												
												$orther=$find_transaction->others.',"description_activite":"'.$response.'"';
												$info_message =$find_transaction->message."\n".$response;
												$message =\Yii::t('app', 'adresse_boutique')." <b>".trim($nom_boutique)."</b>";
												
												
												$find_transaction->others=$orther;
												$find_transaction->message=$info_message;
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=4;
												$find_transaction->save();
												
											}else{
												$message="Vous avez depassé les <b>150 caractères</b>\n\n".$find_transaction->message_send;
											}	
									break;
									case "4":
									
											$recup_information=Json::decode("[{".$find_transaction->others."}]");										
											$information=$recup_information[0];							
											$nom_boutique= $information['nom_boutique'];
								
											$orther=$find_transaction->others.',"adresse_boutique":"'.$response.'"';
											$info_message =$find_transaction->message."\n".\Yii::t('app', 'adresseboutique').$response;
											$message =\Yii::t('app', 'localisation_boutique1')." <b>".trim($nom_boutique)."</b>\n\n".\Yii::t('app', 'localisation_boutique2');
											$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location1');
											
											
											$find_transaction->others=$orther;
											$find_transaction->message=$info_message;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=5;
											$find_transaction->save();
											
											
											$message=$this->demande_acteur("0;0",$id_trans,$status,$provenance);
											
											
									break;
									case "5":
											//verification si la position a ete envoyé
											$test_geo=explode(';',$response);
											if(sizeof($test_geo)==2 && is_numeric(trim($test_geo[0])) && is_numeric(trim($test_geo[1])) ){
												$orther=$find_transaction->others.',"localisation_boutique":"'.$response.'","lat":"'.$test_geo[0].'","long":"'.$test_geo[1].'"';
												//$info_message =$find_transaction->message." (".$response.")";
												$message =\Yii::t('app', 'autre_numero');
												
												
												$find_transaction->others=$orther;
												//$find_transaction->message=$info_message;
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=6;
												$find_transaction->save();
											
											}else{
												$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
											}
											
									break;
									case "6":
									
											$orther=$find_transaction->others.',"autre_numero":"'.$response.'"';
											$info_message =$find_transaction->message."\n".\Yii::t('app', 'autre_contact').$response;
											
											$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'CANCEL')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
											
											$find_transaction->others=$orther;
											$find_transaction->message=$info_message;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=7;
											$find_transaction->save();
											
											
									break;
									
							}
						
						}else if($type_acteur=="2"){
							
							
							switch (trim($sub_menu)) {	
									case "2":
					
											$orther=$find_transaction->others.',"type_moyendeplacement":"'.$response.'"';
											$info_message =$find_transaction->message."\n".\Yii::t('app', 'moyendeplacement').$response;
											$message =\Yii::t('app', 'adresse_livreur');
											
											
											$find_transaction->others=$orther;
											$find_transaction->message=$info_message;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=3;
											$find_transaction->save();
											
											
									break;
									case "3":
									
											$orther=$find_transaction->others.',"adresse_livreur":"'.$response.'"';
											$info_message =$find_transaction->message."\n".\Yii::t('app', 'adresselivreur').$response;
											
											$message =\Yii::t('app', 'localisation_livreur');											
											$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location2');
											
											
											$find_transaction->others=$orther;
											$find_transaction->message=$info_message;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=4;
											$find_transaction->save();
											
											$message=$this->demande_acteur("0;0",$id_trans,$status,$provenance);
											
											
									break;
									case "4":
											//verification si la position a ete envoyé
											$test_geo=explode(';',$response);
											if(sizeof($test_geo)==2 && is_numeric(trim($test_geo[0])) && is_numeric(trim($test_geo[1])) ){
												$orther=$find_transaction->others.',"localisation_livreur":"'.$response.'","lat":"'.$test_geo[0].'","long":"'.$test_geo[1].'"';
												//$info_message =$find_transaction->message." (".$response.")";
												$message =\Yii::t('app', 'autre_numero');
												
												
												$find_transaction->others=$orther;
												//$find_transaction->message=$info_message;
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=5;
												$find_transaction->save();
											
											}else{
												$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
											}
											
									break;
									case "5":
									
											$orther=$find_transaction->others.',"autre_numero":"'.$response.'"';
											$info_message =$find_transaction->message."\n".\Yii::t('app', 'autre_contact').$response;
											
											$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'CANCEL')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
											
											$find_transaction->others=$orther;
											$find_transaction->message=$info_message;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=7;
											$find_transaction->save();
											
											
									break;
									
							}
						
						}else{
							$message=$this->get_first_menu($find_transaction);
						}
						
						
					break ;
							
					
					
					
				}
				
			}	
		}

		
	   return $message;
	}
		
	public function achat_marchand($response,$id_trans,$status,$provenance){				
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		$sub_menu=$find_transaction->sub_menu;
		if($status=="0"){
			
				
				if($this->adjafi=="1"){
					$message="1.*. ".\Yii::t('app', 'entrer_marchand1')."\n2.*. ".\Yii::t('app', 'entrer_marchand2')."\n3.*. ".\Yii::t('app', 'entrer_marchand3')."\n4.*. ".\Yii::t('app', 'entrer_marchand4')."\n\n0.*. ".\Yii::t('app', 'cancel_process');					
				}else{
					$message="1.*. ".\Yii::t('app', 'entrer_marchand1')."\n2.*. ".\Yii::t('app', 'entrer_marchand2')."\n3.*. ".\Yii::t('app', 'entrer_marchand3')."\n\n0.*. ".\Yii::t('app', 'cancel_process');					
				}
				$find_transaction->message_send=$message;
				$find_transaction->sub_menu=1;
				$find_transaction->save();
		}else{
			
			if($response=="0"){
					$message=$this->get_first_menu($find_transaction);													
			}else{
					
				switch (trim($sub_menu)) {	
				
				
					
					case "1":
									
						if($response=="1"){	
								
								$message=\Yii::t('app', 'entrer_codemarchand')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
								$orther='"type_search":"1"';
								$find_transaction->others=$orther;
								$find_transaction->message_send=$message;
								$find_transaction->sub_menu=4;
								$find_transaction->save();	
															
						}else if($response=="2" || ($response=="4" && $this->adjafi=="1")){
							
							//selectionnez le type de boutique que la personne desire
							
							$all_secteur=SecteurActivite::find()->where(['etat'=>1])->orderBy('position_menu_ussd ASC')->all();
							$nbre_show=sizeof($all_secteur);
							if($nbre_show>0){
								
								//$message="Veuillez sélectionner le type de boutique que vous désirez";
								$message="Veuillez sélectionner la categeorie";
								foreach($all_secteur as $info_secteur){
									$message.="\n".$info_secteur->position_menu_ussd.".*. ".$info_secteur->denomination_secteur;							
								}									
								
								$orther='"type_search":"'.$response.'"';
								$find_transaction->others=$orther;
								$find_transaction->message_send=$message;
								$find_transaction->sub_menu=20;
								$find_transaction->save();
							}else{
								$message=\Yii::t('app', 'TRY_LATER')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
								$find_transaction->message_send=$message;
								$find_transaction->etat_transaction=2;
								$find_transaction->save();
							}
								
						}else if($response=="3"){
							
							//selectionnez le type de boutique que la personne desire
							$message=\Yii::t('app', 'localisation_user');
							$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location0')."\n\n\n0.*. ".\Yii::t('app', 'cancel_process');
							
							$orther='"type_search":"3"';
							$find_transaction->others=$orther;
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=2;
							$find_transaction->save();
							
								
						}else {
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
							
					break;
					default:
						$message="Erreur";
						
						$recup_information=Json::decode("[{".$find_transaction->others."}]");										
						$information=$recup_information[0];							
						$type_search= $information['type_search'];
						
						if($type_search=="1" || $type_search=="2" || ($type_search=="4" && $this->adjafi=="1")){
					
							switch (trim($sub_menu)) {	
								
								case "20":
									
										$test_secteur=SecteurActivite::find()->where(['etat'=>1,'position_menu_ussd'=>$response])->one();
										if($test_secteur!=null && (int)$response>0){
								
											$orther=$find_transaction->others.',"secteur_user":"'.$test_secteur->idsecteur_activite.'"';
											$message=\Yii::t('app', 'localisation_user');
											$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location0')."\n\n\n0.*. ".\Yii::t('app', 'cancel_process');
											
											$find_transaction->message_send=$message;
											$find_transaction->others=$orther;
											$find_transaction->sub_menu=2;
											$find_transaction->save();
										
										}else {
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										}
								break;
								case "2":
								
										//verification si la position a ete envoyé
										$test_geo=explode(';',$response);
										if(sizeof($test_geo)==2 && is_numeric(trim($test_geo[0])) && is_numeric(trim($test_geo[1])) ){
											$orther=$find_transaction->others.',"localisation_user":"'.$response.'","userlat":"'.$test_geo[0].'","userlong":"'.$test_geo[1].'"';
											
											$recup_information=Json::decode("[{".$find_transaction->others."}]");	
											$information=$recup_information[0];							
											$secteur_user= $information['secteur_user'];
											
											//recuperer la liste des 10 marchands les plus proches de lui 50 km maxi
											$lat_acteur=$test_geo[0];
											$long_acteur=$test_geo[1];
											
											$distance=$this->distance;
											$querymarchand = ActeurUser::find()->select(['*','(
												  6371 * acos (
												  cos ( radians('.$lat_acteur.') )
												  * cos( radians( lat_acteur ) )
												  * cos( radians( long_acteur ) - radians('.$long_acteur.') )
												  + sin ( radians('.$lat_acteur.') )
												  * sin( radians( lat_acteur ) )
												)
											) AS distance'])
											->where(['etat'=>1,'type_acteur'=>[1]])	
											->andWhere(['or like', 'secteur_activite', '"'.$secteur_user.'"']);
											if($type_search=="4"){
												$querymarchand=$querymarchand->andWhere(['>=', 'provenance_acteur', '100']);
											}											
											$all_marchand=$querymarchand->having(['<=','distance',$distance])->orderby(['distance'=>SORT_ASC])->limit($this->nbre_limit)->all();
											
											
											if(sizeof($all_marchand)>0){
												
												$message=\Yii::t('app', 'select_marchand');
												$i=1;
												$nbre_show=sizeof($all_marchand);
												
												$tab_response=array();
												foreach($all_marchand as $infomarchand){													
													
													if($infomarchand->distance>=1){
														$calcul_distance=" (".$this->show_nbre($infomarchand->distance)." Km)";										
													}else{
														$calcul_distance=" (".$this->show_nbre($infomarchand->distance*1000)." m)";
													}
													
													if(in_array($provenance,$this->tab_prov)){	
														$tab_response[]=["photo"=>"acteurs/".$infomarchand->photo_acteur,"code"=>"ABSELLER".$infomarchand->code_reference."ABSELLER","denomination"=>$infomarchand->denomination,"description"=>$infomarchand->description_activite,"adresse"=>$infomarchand->address_acteur.$calcul_distance];
													}else{													
														if($message!="")$message.="\n\n";
														$message.=$i.".*. ".$infomarchand->denomination." (".$infomarchand->code_reference.")\n".$infomarchand->address_acteur.$calcul_distance;	
													}	
													$i++;
												}
												if(in_array($provenance,$this->tab_prov)){	
													$message=json_encode(array("type"=>"boutique","information"=>$tab_response));
												}
												
												$find_transaction->others=$orther;
												//$find_transaction->message=$info_message;
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=3;
												$find_transaction->save();
												
											}else{
												
												$message =\Yii::t('app', 'no_marchand1')." ".$this->distance."Km.";
												$message.="\n\n".\Yii::t('app', 'no_marchand2');
												$message.="\n\n".\Yii::t('app', 'no_marchand3');
											
												$find_transaction->message_send=$message;
												$find_transaction->etat_transaction=1;
												$find_transaction->save();		
											}							
										}else{
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										}
										
								break;
								
								case "3":
							
							
										$position_marchand=(int)$response - 1 ;
										if($position_marchand>=0){
											
											$recup_information=Json::decode("[{".$find_transaction->others."}]");										
											$information=$recup_information[0];							
											$lat_acteur= $information['userlat'];
											$long_acteur= $information['userlong'];
											$secteur_user= $information['secteur_user'];
											
											$distance=$this->distance;
											$querymarchand = ActeurUser::find()->select(['*','(
												  6371 * acos (
												  cos ( radians('.$lat_acteur.') )
												  * cos( radians( lat_acteur ) )
												  * cos( radians( long_acteur ) - radians('.$long_acteur.') )
												  + sin ( radians('.$lat_acteur.') )
												  * sin( radians( lat_acteur ) )
												)
											) AS distance'])
											->where(['etat'=>1,'type_acteur'=>[1]])		
											->andWhere(['or like', 'secteur_activite', '"'.$secteur_user.'"']);
											if($type_search=="4"){
												$querymarchand=$querymarchand->andWhere(['>=', 'provenance_acteur', '100']);
											}
											$all_marchand=$querymarchand->having(['<=','distance',$distance])->orderby(['distance'=>SORT_ASC])->limit($this->nbre_limit)->all();
											
											
											if(sizeof($all_marchand)>$position_marchand){
												
												$find_acteur=$all_marchand[$position_marchand];
												
												$this->record_visitemarchand($find_acteur,$find_transaction);
												
												$id_acteur=$find_acteur->id_acteur_user;
												$orther=$find_transaction->others.',"id_acteur":"'.$id_acteur.'","lat_acteur":"'.$find_acteur->lat_acteur.'","long_acteur":"'.$find_acteur->long_acteur.'"';
												
												
												//recuperer la liste des produits de ce marchand
												
												$info_message="<b>Résume de la commande</b>";
												$info_message.="\n<b>Marchand: ".$find_acteur->denomination."</b>";
													
												$message="<b>Bienvenue chez ".$find_acteur->denomination."</b>";
												$message.="\n\n".\Yii::t('app', 'help_marchand')."\n";

												$tab_response=array();
												$all_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1])->orderBy('position_menu_ussd ASC')->all();
												if(sizeof($all_produits)>0){
													
													$nbre_show=sizeof($all_produits);
													for($i=0;$i<$nbre_show;$i++){
														
														if(in_array($provenance,$this->tab_prov)){	
															$tab_response[]=["marchand"=>$find_acteur->denomination,"photo"=>"produit/".$all_produits[$i]->photo_produit,"code"=>$all_produits[$i]->position_menu_ussd,"denomination"=>$all_produits[$i]->denomination,"qte_vente"=>Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite,"prix_vente"=>Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa"];
														}else{
															if($message!="")$message.="\n";
															$message.=$all_produits[$i]->position_menu_ussd.".*. ".$all_produits[$i]->denomination." - ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." à ".Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa";							
														}	
													}	
													
													
													$message.=$this->photo_info;
													$photo_marchand=(string)$find_acteur->photo_acteur;
													if($photo_marchand!=""){
														$message.="WA_IMAGEacteurs/".$photo_marchand."WA_IMAGE";
													}
													
													if(in_array($provenance,$this->tab_prov)){	
														$message=json_encode(array("type"=>"produit","information"=>$tab_response));
													}
													
													
													$find_transaction->message_send=$message;
													$find_transaction->message=$info_message;
													$find_transaction->others=$orther;
													$find_transaction->sub_menu=5;
													$find_transaction->save();
												}else{
													$info_message="<b>Marchand: ".$find_acteur->denomination."</b>";
													$message =$info_message."\n\n".\Yii::t('app', 'no_produits');
													$find_transaction->others=$orther;
													$find_transaction->message_send=$message;
													$find_transaction->etat_transaction=2;
													$find_transaction->save();	
													
													$this->flah_marchand($find_acteur);
												}
										
										
												
											}else{
												if(in_array($provenance,$this->tab_prov)){	
													$message=$find_transaction->message_send;
												}else{
													$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
												}
											}
																	
											
										}else{
											$find_acteur = ActeurUser::findOne(['code_reference'=>$response,'etat'=>1,'type_acteur'=>1]);	
											if($find_acteur==null){
												if(in_array($provenance,$this->tab_prov)){	
													$message=$find_transaction->message_send;
												}else{
													$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
												}
											}else{
												$this->record_visitemarchand($find_acteur,$find_transaction);
												$id_acteur=$find_acteur->id_acteur_user;
												$orther=$find_transaction->others.',"id_acteur":"'.$id_acteur.'","lat_acteur":"'.$find_acteur->lat_acteur.'","long_acteur":"'.$find_acteur->long_acteur.'"';
												
												//recuperer la liste des produits de ce marchand
												
												$info_message="<b>Résume de la commande</b>";
												$info_message.="\n<b>Marchand: ".$find_acteur->denomination."</b>";
													
												$message="<b>Bienvenue chez ".$find_acteur->denomination."</b>";
												$message.="\n\n".\Yii::t('app', 'help_marchand')."\n";
												
												$tab_response=array();
												$all_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1])->orderBy('position_menu_ussd ASC')->all();
												if(sizeof($all_produits)>0){
													
													$nbre_show=sizeof($all_produits);
													for($i=0;$i<$nbre_show;$i++){
														
														if(in_array($provenance,$this->tab_prov)){	
															$tab_response[]=["marchand"=>$find_acteur->denomination,"photo"=>"produit/".$all_produits[$i]->photo_produit,"code"=>$all_produits[$i]->position_menu_ussd,"denomination"=>$all_produits[$i]->denomination,"qte_vente"=>Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite,"prix_vente"=>Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa"];
														}else{
															if($message!="")$message.="\n";
															$message.=$all_produits[$i]->position_menu_ussd.".*. ".$all_produits[$i]->denomination." - ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." à ".Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa";							
														}	
													}	
													$message.=$this->photo_info;
													$photo_marchand=(string)$find_acteur->photo_acteur;
													if($photo_marchand!=""){
														$message.="WA_IMAGEacteurs/".$photo_marchand."WA_IMAGE";
													}
													
													if(in_array($provenance,$this->tab_prov)){	
														$message=json_encode(array("type"=>"produit","information"=>$tab_response));
													}
													
													$find_transaction->message_send=$message;
													$find_transaction->message=$info_message;
													$find_transaction->others=$orther;
													$find_transaction->sub_menu=5;
													$find_transaction->save();
												}else{
													$info_message.="<b>Marchand: ".$find_acteur->denomination."</b>";
													$message =$info_message."\n\n".\Yii::t('app', 'no_produits');
													$find_transaction->others=$orther;
													$find_transaction->message_send=$message;
													$find_transaction->etat_transaction=2;
													$find_transaction->save();	
													
													$this->flah_marchand($find_acteur);
												}
											}
										}
							
							
								break;
								
								case "4":
										
										$find_acteur = ActeurUser::findOne(['code_reference'=>$response,'etat'=>1,'type_acteur'=>1]);								 
										if($find_acteur!=null){
											
											$this->record_visitemarchand($find_acteur,$find_transaction);
											$id_acteur=$find_acteur->id_acteur_user;
											$orther=$find_transaction->others.',"id_acteur":"'.$id_acteur.'","lat_acteur":"'.$find_acteur->lat_acteur.'","long_acteur":"'.$find_acteur->long_acteur.'"';
											
											//recuperer la liste des produits de ce marchand
											
											$info_message=\Yii::t('app', 'resumecommande');
											$info_message.="\n<b>Marchand: ".$find_acteur->denomination."</b>";
												
											$message="<b>Bienvenue chez ".$find_acteur->denomination."</b>";
											$message.="\n\n".\Yii::t('app', 'help_marchand')."\n";
											
											$tab_response=array();
											$all_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1])->orderBy('position_menu_ussd ASC')->all();
											if(sizeof($all_produits)>0){
												
												$nbre_show=sizeof($all_produits);
												for($i=0;$i<$nbre_show;$i++){
													
													if(in_array($provenance,$this->tab_prov)){	
															$tab_response[]=["marchand"=>$find_acteur->denomination,"photo"=>"produit/".$all_produits[$i]->photo_produit,"code"=>$all_produits[$i]->position_menu_ussd,"denomination"=>$all_produits[$i]->denomination,"qte_vente"=>Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite,"prix_vente"=>Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa"];
													}else{
														if($message!="")$message.="\n";
														
														if($all_produits[$i]->unite=="Kg"){
															$message.=$all_produits[$i]->position_menu_ussd.".*. ".$all_produits[$i]->denomination." - ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." à ".Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa";
														}else{
															$message.=$all_produits[$i]->position_menu_ussd.".*. ".$all_produits[$i]->denomination." - ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." (".$all_produits[$i]->poids_unitaire."Kg) à ".Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa";							
														}
													}
												}	
												
												$message.=$this->photo_info;
												$photo_marchand=(string)$find_acteur->photo_acteur;
												if($photo_marchand!=""){
													$message.="WA_IMAGEacteurs/".$photo_marchand."WA_IMAGE";
												}
												
												if(in_array($provenance,$this->tab_prov)){	
													$message=json_encode(array("type"=>"produit","information"=>$tab_response));
												}
												
												$find_transaction->message_send=$message;
												$find_transaction->message=$info_message;
												$find_transaction->others=$orther;
												$find_transaction->sub_menu=5;
												$find_transaction->save();
											}else{
												$info_message="<b>Marchand: ".$find_acteur->denomination."</b>";
												$message =$info_message."\n\n".\Yii::t('app', 'no_produits');
												$find_transaction->others=$orther;
												$find_transaction->message_send=$message;
												$find_transaction->etat_transaction=2;
												$find_transaction->save();	
												
												$this->flah_marchand($find_acteur);
											}
											
										}else{		    
											if(in_array($provenance,$this->tab_prov)){	
												$message=$find_transaction->message_send;
											}else{
												$message=\Yii::t('app', 'error_reference')."\n".$find_transaction->message_send;
											}	
										}						
									
								break ;
								
								case "5":
										if(strtoupper($response)=="PANIER"){
											$message=$find_transaction->message_send;
										}else{
												
											$test_photo=explode("PHOTO",strtoupper($response));
											if(sizeof($test_photo)==1){	
													
												$recup_information=Json::decode("[{".$find_transaction->others."}]");										
												$information=$recup_information[0];							
												$id_acteur= $information['id_acteur'];
												$response=str_replace(" ","",$response);
												$response=str_replace(";",",",$response);
												$response=str_replace("-",",",$response);
												$response=str_replace("x","*",$response);
												$response=str_replace("X","*",$response);
												
												$all_selected=explode(",",$response);
												
												$tab_selected=array();
												$tab_selected_indice=array();
												if(sizeof($all_selected)>0){
													
													foreach($all_selected as $info_sel){
														$get_split=explode("*",$info_sel);
														$tab_selected[]=(int)$get_split[0];
														if(sizeof($get_split)>1){
															$tab_selected_indice[(int)$get_split[0]]=$get_split[1];
														}else{
															$tab_selected_indice[(int)$get_split[0]]=1;
														}
													}
													
													$info_message=\Yii::t('app', 'resume_commande')."\n";
													$all_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1,'position_menu_ussd'=>$tab_selected])->orderBy('position_menu_ussd ASC')->all();
													$selected="";
													$qte_livraison="";
													if(sizeof($all_produits)>0){
														
														$nbre_show=sizeof($all_produits);
														$prix_produits=0;
														$poids_total=0;
														for($i=0;$i<$nbre_show;$i++){
															
															$idProduit=$all_produits[$i]->idProduit;
															$post=$all_produits[$i]->position_menu_ussd;
															if($selected!="")$selected.="-";
															if($qte_livraison!="")$qte_livraison.="-";
															
															$selected.=$idProduit;
															if(isset($tab_selected_indice[$post])){
																$qte=$tab_selected_indice[$post];
															}else{
																$qte="1";												
															}
															$qte_livraison.=$qte;												
															if($info_message!="")$info_message.="\n";
															
															$prix_com=(double)$qte * (double)$all_produits[$i]->prix_vente;
															
															$info_message.="<b>- ".$qte." X ".$all_produits[$i]->denomination."</b> - ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." = ".Utils::show_nbre($prix_com)." Fcfa";	
															
															$prix_produits+=$prix_com;
															$poids_total+=(double)$qte * (double)$all_produits[$i]->poids_unitaire;
														}
														$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONTINUER')."\n2.*.".\Yii::t('app', 'MODIFIER')."\n0.*.".\Yii::t('app', 'CANCEL_CMD');
														
														$orther=$find_transaction->others.',"produit_selected":"'.$selected.'","poids_total":"'.$poids_total.'","qte_livraison":"'.$qte_livraison.'","prix_produits":"'.$prix_produits.'"';
														$find_transaction->message_send=$message;
														$find_transaction->message=$find_transaction->message."\n\n".$info_message;
														$find_transaction->others=$orther;
														$find_transaction->sub_menu=6;
														$find_transaction->save();
													
														
													}else{
													
														if(in_array($provenance,$this->tab_prov)){	
															$message=$find_transaction->message_send;
														}else{
															$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
														}
													}
													
												}else{
													
													if(in_array($provenance,$this->tab_prov)){	
														$message=$find_transaction->message_send;
													}else{
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
													}
												}
											}else{
												$position_produit=trim($test_photo[1]);
												$recup_information=Json::decode("[{".$find_transaction->others."}]");										
												$information=$recup_information[0];							
												$id_acteur= $information['id_acteur'];
												
												$info_produit=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1,'position_menu_ussd'=>$position_produit])->orderBy('position_menu_ussd ASC')->one();
												
												if($info_produit!==null && (int)$position_produit>0){ 
													
													$message=$info_produit->denomination." - ".Utils::show_nbre($info_produit->qte).$info_produit->unite." (".$info_produit->poids_unitaire."Kg) à ".Utils::show_nbre($info_produit->prix_vente)." Fcfa";
													
													if($info_produit->unite=="Kg"){
														$message=$info_produit->denomination." - ".Utils::show_nbre($info_produit->qte).$info_produit->unite." à ".Utils::show_nbre($info_produit->prix_vente)." Fcfa";														
													}
													$message.=$this->photo_info;
													$message.=$this->panier_info;
													$photo_produit=(string)$info_produit->photo_produit;
													if($photo_produit!=""){
														$message.="WA_IMAGEproduit/".$photo_produit."WA_IMAGE";
													}
												}else{
													if(in_array($provenance,$this->tab_prov)){	
														$message=$find_transaction->message_send;
													}else{
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
													}
												}
												

											}							
											
										}
								break;
								
								case "6":
										
										if($response=="1"){	

												$message=\Yii::t('app', 'entrer_lieulivraison')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=7;
												$find_transaction->save();
							
										}else if($response=="2"){
												
												$recup_information=Json::decode("[{".$find_transaction->others."}]");										
												$information=$recup_information[0];							
												$id_acteur= $information['id_acteur'];
										
												$find_acteur = ActeurUser::findOne(['id_acteur_user'=>$id_acteur,'etat'=>1,'type_acteur'=>1]);	
														
												if($find_acteur!=null){
													
													//$this->record_visitemarchand($find_acteur,$find_transaction);
													
													$id_acteur=$find_acteur->id_acteur_user;
													$orther='"type_search":"1","id_acteur":"'.$id_acteur.'","lat_acteur":"'.$find_acteur->lat_acteur.'","long_acteur":"'.$find_acteur->long_acteur.'"';
													
													//recuperer la liste des produits de ce marchand
													$info_message="<b>Résume de la commande</b>";
													$info_message.="\n<b>Marchand: ".$find_acteur->denomination."</b>";
														
													$message="<b>Bienvenue chez ".$find_acteur->denomination."</b>";
													$message.="\n\n".\Yii::t('app', 'help_marchand')."\n";
													
													$all_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1])->orderBy('position_menu_ussd ASC')->all();
													if(sizeof($all_produits)>0){
														$tab_response=array();
														$nbre_show=sizeof($all_produits);
														for($i=0;$i<$nbre_show;$i++){
															if(in_array($provenance,$this->tab_prov)){	
																	$tab_response[]=["marchand"=>$find_acteur->denomination,"photo"=>"produit/".$all_produits[$i]->photo_produit,"code"=>$all_produits[$i]->position_menu_ussd,"denomination"=>$all_produits[$i]->denomination,"qte_vente"=>Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite,"prix_vente"=>Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa"];
															}else{
																if($message!="")$message.="\n";
																$message.=$all_produits[$i]->position_menu_ussd.".*. ".$all_produits[$i]->denomination." - ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." à ".Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa";							
															}	
														}	
														$message.=$this->photo_info;
														$photo_marchand=(string)$find_acteur->photo_acteur;
														if($photo_marchand!=""){
															$message.="WA_IMAGEacteurs/".$photo_marchand."WA_IMAGE";
														}
														
														if(in_array($provenance,$this->tab_prov)){	
															$message=json_encode(array("type"=>"produit","information"=>$tab_response));
														}
														
														$find_transaction->message_send=$message;
														$find_transaction->message=$info_message;
														$find_transaction->others=$orther;
														$find_transaction->sub_menu=5;
														$find_transaction->save();
														
													}else{
														$message =$info_message."\n\n".\Yii::t('app', 'no_produits');
														$find_transaction->others=$orther;
														$find_transaction->message_send=$message;
														$find_transaction->etat_transaction=2;
														$find_transaction->save();	
														
														$this->flah_marchand($find_acteur);
													}
													
												}else{		    
													$message=\Yii::t('app', 'error_reference')."\n".$find_transaction->message_send;		
												}

										}else{
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										}
									
								break ;
								
								case "7":
								
									$recup_information=Json::decode("[{".$find_transaction->others."}]");										
									$information=$recup_information[0];
									$poids= $information['poids_total'];
									
									$orther=$find_transaction->others.',"lieu_livraison":"'.$response.'"';
									$info_message=$find_transaction->message."\n\n<b>Poids de la commande:</b> ".Utils::show_nbre($poids)." Kg\n<b>Lieu de livraison:</b> ".$response;
									
									$message=\Yii::t('app', 'dispose_location')."\n\n1.*. ".\Yii::t('app', 'oui1_location')."\n2.*. ".\Yii::t('app', 'oui2_location')."\n3.*. ".\Yii::t('app', 'non_location');
									$find_transaction->message_send=$message;
									$find_transaction->message=$info_message;
									$find_transaction->others=$orther;
									$find_transaction->sub_menu=8;
									$find_transaction->save();
								  
								break ;
								case "8":
								
									if(in_array($response,array("1","2","3"))){
										
										if($response=="3"){
											$orther=$find_transaction->others.',"localisation_livraison":"0;0","livraison_lat":"0","livraison_long":"0","distance_livraison":"0","prix_livraison":"0"';
											$message=\Yii::t('app', 'entrer_infosupp')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
											
											$recup_information=Json::decode("[{".$find_transaction->others."}]");										
											$information=$recup_information[0];
											$prix_produits=(int)$information['prix_produits'];
											
											$info_message=$find_transaction->message."\n\n<b>Total commande:</b> ".Utils::show_nbre($prix_produits)." Fcfa";
											
											$find_transaction->message_send=$message;
											$find_transaction->message=$info_message;
											$find_transaction->others=$orther;
											$find_transaction->sub_menu=10;
											$find_transaction->save();
										}else if($response=="1"){
											
											$recup_information=Json::decode("[{".$find_transaction->others."}]");										
											$information=$recup_information[0];
											if(isset($information['userlat']) && isset($information['userlong'])){
													
													
													
													$lat_end=$information['userlat'];
													$long_end=$information['userlong'];
													$full_coordonnee=$lat_end.";".$long_end;
													
													$orther=$find_transaction->others.',"localisation_livraison":"'.$full_coordonnee.'","livraison_lat":"'.$lat_end.'","livraison_long":"'.$long_end.'"';
													$message=\Yii::t('app', 'entrer_infosupp')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
													
													$recup_information=Json::decode("[{".$find_transaction->others."}]");										
													$information=$recup_information[0];
													$prix_produits=(int)$information['prix_produits'];
													
													$poids= $information['poids_total'];								
													$lat_from= $information['lat_acteur'];								
													$long_from= $information['long_acteur'];								
													
													
													$convert_distance=Utils::calcul_distance($lat_from,$long_from,$lat_end,$long_end);
													$convert_amount=Utils::calcul_price($convert_distance, $poids);
													
													$orther.=',"distance_livraison":"'.$convert_distance.'","prix_livraison":"'.$convert_amount.'"';
													
													$info_message=$find_transaction->message."\n<b>Distance de livraison:</b> ".$convert_distance." Km\n<b>Prix de livraison:</b> ".Utils::show_nbre($convert_amount)." FCFA\n\n<b>Total à payer:</b> ".Utils::show_nbre($convert_amount+$prix_produits)." FCFA";
													
													//ajout de la distance de livraison et du prix de la livraison
													$find_transaction->message_send=$message;
													$find_transaction->message=$info_message;
													$find_transaction->others=$orther;
													$find_transaction->sub_menu=10;
													$find_transaction->save();
											
											}else{
												$message =\Yii::t('app', 'localisation_livraison');											
												$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location3');
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=9;
												$find_transaction->save();
											}
											
										}else{
											$message =\Yii::t('app', 'localisation_livraison');											
											$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location3');
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=9;
											$find_transaction->save();
										}
									}else{
										$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
									}
								  
								break ;
								case "9":
								
									//verification si la position a ete envoyé
										$test_geo=explode(';',$response);
										if(sizeof($test_geo)==2 && is_numeric(trim($test_geo[0])) && is_numeric(trim($test_geo[1])) ){
											$orther=$find_transaction->others.',"localisation_livraison":"'.$response.'","livraison_lat":"'.$test_geo[0].'","livraison_long":"'.$test_geo[1].'"';
											$message=\Yii::t('app', 'entrer_infosupp')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
											
											$recup_information=Json::decode("[{".$find_transaction->others."}]");										
											$information=$recup_information[0];
											$prix_produits=(int)$information['prix_produits'];
											
											$poids= $information['poids_total'];								
											$lat_from= $information['lat_acteur'];								
											$long_from= $information['long_acteur'];								
											$lat_end=$test_geo[0];
											$long_end=$test_geo[1];
											
											$convert_distance=Utils::calcul_distance($lat_from,$long_from,$lat_end,$long_end);
											$convert_amount=Utils::calcul_price($convert_distance, $poids);
											
											$orther.=',"distance_livraison":"'.$convert_distance.'","prix_livraison":"'.$convert_amount.'"';
											
											$info_message=$find_transaction->message."\n<b>Distance de livraison:</b> ".$convert_distance." Km\n<b>Prix de livraison:</b> ".Utils::show_nbre($convert_amount)." FCFA\n\n<b>Total à payer:</b> ".Utils::show_nbre($convert_amount+$prix_produits)." FCFA";
											
											//ajout de la distance de livraison et du prix de la livraison
											$find_transaction->message_send=$message;
											$find_transaction->message=$info_message;
											$find_transaction->others=$orther;
											$find_transaction->sub_menu=10;
											$find_transaction->save();
											
										}else{
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										}
										
								  
								break ;
								
								case "10":
								
									$orther=$find_transaction->others.',"info_supplementaire":"'.$response.'"';
									
									$info_message=$find_transaction->message;
									$message=$info_message."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'CANCEL')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
									$find_transaction->message_send=$message;
									$find_transaction->message=$info_message;
									$find_transaction->others=$orther;
									$find_transaction->sub_menu=11;
									$find_transaction->save();
								  
								break ;
								
								case "11":
												
									if($response=="1"){	
											$recup_information=Json::decode("[{".$find_transaction->others."}]");										
											$information=$recup_information[0];							
											$id_trans= $find_transaction->id;
											$iduser= $find_transaction->iduser;
											$id_marchand= $information['id_acteur'];
											$id_produits= $information['produit_selected'];
											$qte_livraison= $information['qte_livraison'];
											$lieu_livraison= $information['lieu_livraison'];
											$prix_produits= $information['prix_produits'];
											$poids_total= $information['poids_total'];
											$livraison_lat= $information['livraison_lat'];
											$livraison_long= $information['livraison_long'];
											$info= $information['info_supplementaire'];
											$distance_livraison= $information['distance_livraison'];
											$prix_livraison= $information['prix_livraison'];
											
											$commande_march = new CommandeMarchand();
											$commande_march->id_user = $iduser;
											$commande_march->id_marchand = $id_marchand;
											$commande_march->id_livreur = 0;
											$commande_march->idtransaction = $id_trans;
											$commande_march->produit_commande = $id_produits;
											$commande_march->lieu_livraison = $lieu_livraison;
											$commande_march->info_supplementaire = $info;
											$commande_march->etat = 0;
											$commande_march->etat_confirmation = 0;
											$commande_march->prix_livraison = (int) $prix_livraison;
											$commande_march->prix_produits = (int)$prix_produits;
											$commande_march->prix_commande = (int)$prix_produits;
											$commande_march->produit_livraison = $id_produits;
											$commande_march->poids_livraison =(string)$poids_total;
											$commande_march->distance_livraison =(string)$distance_livraison;
											$commande_march->qte_livraison = $qte_livraison;
											$commande_march->lat_livraison = (double)$livraison_lat;
											$commande_march->long_livraison = (double)$livraison_long;
											$commande_march->commande_key = Yii::$app->security->generateRandomString(32);	
											if($commande_march->save()){
												
												
												$message=\Yii::t('app', 'success_command');
												if( (int) $prix_livraison>0){
													$lien_pay="https://abusiness.store/pay_invoice?key=".$commande_march->commande_key;
													$message="Votre commande a été prise en compte. Pour déclencher le processus de livraison, veuillez effectuer le paiement en cliquant sur le lien ci-dessous:\n\n".$lien_pay;
												}


													
												$find_transaction->message_send=$message;
												$find_transaction->etat_transaction=1;
												$find_transaction->save();	
												
												
												
												$info_user=User::findOne(['id'=>$iduser]);
												$plus=$info_user->nom." ".$info_user->prenoms .". ";
												if($provenance=="TELEGRAM"){
													$recup=explode("___",$find_transaction->idtransaction);
													if(sizeof($recup)>1){				
														$plus.="Lien: https://t.me/".$recup[1]." (".$info_user->username.")";
													}
												}else if($provenance=="WEBSITE"){
													$plus.=" (".$info_user->username.")";													
												}else if($provenance=="WHATSAPP"){											
													$plus.="Lien: https://wa.me/".$find_transaction->username;			
												}else if($provenance=="MESSENGER"){											
													$plus.="Lien: https://m.me/".$find_transaction->username;			
												}
												
												$message.="LIVRA_ISON".$find_transaction->message."\n\n".\Yii::t('app', 'commanded_by')."\n".$plus."LIVRA_ISON";									
											
											}else{
												$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
											}				
										
									}else if($response=="2"){
										
										$message=$this->get_first_menu($find_transaction);
										
									}else {
										$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
									}
										
								break;
							}
						
						}else if($type_search=="3"){
							
							switch (trim($sub_menu)) {
								case "2":
									//verification si la position a ete envoyé
									$test_geo=explode(';',$response);
									if(sizeof($test_geo)==2 && is_numeric(trim($test_geo[0])) && is_numeric(trim($test_geo[1])) ){
										$orther=$find_transaction->others.',"localisation_user":"'.$response.'","userlat":"'.$test_geo[0].'","userlong":"'.$test_geo[1].'"';
										
										
										//recuperer la liste des 10 marchands les plus proches de lui 50 km maxi
										$lat_acteur=$test_geo[0];
										$long_acteur=$test_geo[1];
										
										$distance=$this->distance;
										$all_marchand = ActeurUser::find()->select(['*','(
											  6371 * acos (
											  cos ( radians('.$lat_acteur.') )
											  * cos( radians( lat_acteur ) )
											  * cos( radians( long_acteur ) - radians('.$long_acteur.') )
											  + sin ( radians('.$lat_acteur.') )
											  * sin( radians( lat_acteur ) )
											)
										) AS distance'])
										->where(['etat'=>1,'type_acteur'=>[1]])	
										->having(['<=','distance',$distance])->orderby(['distance'=>SORT_ASC])->all();
										
										if(sizeof($all_marchand)>0){
											
											$message=\Yii::t('app', 'enter_produit_name');											
											
											$find_transaction->others=$orther;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=3;
											$find_transaction->save();
											
										}else{
											
											
											$message =\Yii::t('app', 'no_marchand1')." ".$this->distance."Km.";
											$message.="\n\n".\Yii::t('app', 'no_marchand2');
											$message.="\n\n".\Yii::t('app', 'no_marchand3');
												
											$find_transaction->message_send=$message;
											$find_transaction->etat_transaction=1;
											$find_transaction->save();		
										}							
									}else{
										$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
									}
								break;	
								case "3":
									//verification si la position a ete envoyé
									
										$lat_acteur= $information['userlat'];
										$long_acteur= $information['userlong'];
										$all_produit=explode(" ",$response);
										$tab_produit=array();
										if(sizeof($all_produit)>0){
											foreach($all_produit as $info_produit){
												if(strlen($info_produit)>2){
													$tab_produit[]=$info_produit;
												}
											}
										}
										
										
										$distance=$this->distance;
										$all_marchand = ActeurUser::find() ->leftJoin('mproduits', 'mproduits.id_acteur=acteur_user.id_acteur_user')->select(['acteur_user.*','(
											  6371 * acos (
											  cos ( radians('.$lat_acteur.') )
											  * cos( radians( lat_acteur ) )
											  * cos( radians( long_acteur ) - radians('.$long_acteur.') )
											  + sin ( radians('.$lat_acteur.') )
											  * sin( radians( lat_acteur ) )
											)
										) AS distance'])
										->where(['acteur_user.etat'=>1,'acteur_user.type_acteur'=>[1]])	
										->andWhere(['or like','mproduits.denomination',$tab_produit])
										->having(['<=','distance',$distance])->orderby(['distance'=>SORT_ASC])->limit($this->nbre_limit)->all();
										
										if($provenance=="TELEGRAM"){
											$lien_user=$this->lien_telegram;
										}else {
											$lien_user=$this->lien_whatsapp;											
										}
												
										if(sizeof($all_marchand)>0){
											
											$message=\Yii::t('app', 'searchproduit_result1')."\n\n".\Yii::t('app', 'searchproduit_result2');	
											$i=1;
											$tab_response=array();
											foreach($all_marchand as $infomarchand){
												
												if($infomarchand->distance>=1){
													$calcul_distance=" (".$this->show_nbre($infomarchand->distance)." Km)";										
												}else{
													$calcul_distance=" (".$this->show_nbre($infomarchand->distance*1000)." m)";
												}
												$reference=$infomarchand->code_reference;
												$url=$lien_user."ABSELLER".$reference."ABSELLER";	
												
												if(in_array($provenance,$this->tab_prov)){	
														$tab_response[]=["photo"=>"acteurs/".$infomarchand->photo_acteur,"code"=>"ABSELLER".$reference."ABSELLER","denomination"=>$infomarchand->denomination,"description"=>$infomarchand->description_activite,"adresse"=>$infomarchand->address_acteur.$calcul_distance];
												}else{													
													if($message!="")$message.="\n\n";
													$message.=$i.".*. <b>".$infomarchand->denomination."</b> (".$reference.")\n".$infomarchand->address_acteur.$calcul_distance;		
													$message.="\n".$url;	
												}	

												$i++;
											}
											
											if(in_array($provenance,$this->tab_prov)){	
												$message=json_encode(array("type"=>"boutique","information"=>$tab_response));
											}
											
											$orther=$find_transaction->others.',"produit_search":"'.$response.'"';
											$find_transaction->others=$orther;
											$find_transaction->message_send=$message;
											$find_transaction->etat_transaction=2;
											$find_transaction->save();
											
											
										}else{
											$orther=$find_transaction->others.',"produit_search'.time().'":"'.$response.'"';
											$message=\Yii::t('app', 'enter_produit_found1').$this->distance."Km ".Yii::t('app', 'enter_produit_found2')." ( <b>".$response."</b> )";
											$message.="\n\n".\Yii::t('app', 'enter_produit_name');
											
											$find_transaction->others=$orther;
											$find_transaction->save();		
										}							
									
								break;		
							}
						}
							
					
					
				}
				
			}	
		}

		
	   return $message;
	}
			
	public function credit_marchand($response,$id_trans,$status,$provenance){				
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		$sub_menu=$find_transaction->sub_menu;
		if($status=="0"){
				$message=$this->get_first_menu($find_transaction);
		}else{
			
			if($response=="0"){
					$message=$this->get_first_menu($find_transaction);													
			}else{
					
				switch (trim($sub_menu)) {	
									
					case "2":
						
						if($response=="1" || $response=="2" ){ 
							
							$recup_information=Json::decode("[{".$find_transaction->others."}]");										
							$information=$recup_information[0];
														
															
							$id_trans= $find_transaction->id;
							$id_user= $find_transaction->iduser;
							
							
							$reference_marchand= $information['ref_marchand'];
							$refcredit= $information['ref_vente'];
							$id_acteur= $information['idacteur'];
							$ref_souscription= $information['ref_souscription'];
							$type_operation= $information['type_operation'];
							
							//reverification des informations avant validation
							
							//verification des informations du souscripteurs
							$find_souscripteur = AbcSouscription::findOne(['ref_souscription'=>$ref_souscription,'id_user'=>$id_user,'status_souscription'=>1]);
							
							if($find_souscripteur!=null){
								
								//verification des informations du marchand 
								$find_acteur = ActeurUser::findOne(['id_acteur_user'=>$id_acteur,'code_reference'=>$reference_marchand,'etat'=>1,'type_acteur'=>1]);
						
								if($find_acteur!=null){									
									
									//orienter la transaction vers le type d'operation. 1=> achat a credit, 2 : retrait a credit
									
									if($type_operation=="1"){
										
										//verification de la commande
										$find_credit = AbcVente::findOne(['ref_vente'=>$refcredit,'id_acteur_user'=>$find_acteur->id_acteur_user,'idclient'=>$find_souscripteur->id_souscription,'status_vente_abc'=>[0,1,2]]);										
										if($find_credit!=null){
											
											if($find_credit->status_vente_abc==0){
												
												//verification du solde du client
												if((int)$find_credit->montant_vente_abc <= (int)$find_souscripteur->solde_disponible){
																									
															
														
														$find_credit->status_vente_abc = (int)$response;
														$find_credit->customer_confirm_time = date("Y-m-d H:i:s");
														if($find_credit->save()){
															
															$message="Votre commande a été annulée avec succès. Une notification sera envoyée au marchand.";
															
															$contenmarchand="Le client ".$find_souscripteur->idUser->nom." ".$find_souscripteur->idUser->prenoms." vient de rejeter la commande ".$refcredit;
															$contenmarchand.="\nDescription: ".$find_credit->libelle_vente_abc;
															$contenmarchand.="\nMontant: ".Utils::show_nbre($find_credit->montant_vente_abc)." FCFA";
															
															$continue=true ;
															if($response==1){
																
																$find_souscripteur->solde_disponible-=(int)$find_credit->montant_vente_abc;
																if($find_souscripteur->save()){
																
																	//modification de lhistorique de la personne
																	
																	$find_historique = AbcHistorique::findOne(['mois_operation'=>date("Y-m"),'id_souscription'=>$find_souscripteur->id_souscription]);	
																	if($find_historique!=null){
																		
																		$find_historique->montant_restant-=(int)$find_credit->montant_vente_abc;
																		$find_historique->montant_operation+=(int)$find_credit->montant_vente_abc;
																		$find_historique->save();
																		
																	}
											
																	$message="Vous avez payé votre commande avec succès. Une notification sera envoyée au marchand.";
																	$message.="\nSolde restant: <b>".Utils::show_nbre($find_souscripteur->solde_disponible)." FCFA</b>";
																	$message.="\nReference: ".$refcredit;
																	
																	
																	$contenmarchand="Le client ".$find_souscripteur->idUser->nom." ".$find_souscripteur->idUser->prenoms." vient de valider la commande ".$refcredit;
																	$contenmarchand.="\nDescription: ".$find_credit->libelle_vente_abc;
																	$contenmarchand.="\nMontant: ".Utils::show_nbre($find_credit->montant_vente_abc)." FCFA";
																
																}else{
																	$continue=false ;
																	$find_credit->status_vente_abc = 0;
																	$find_credit->save();
																	
																	$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
																}
																
															}
															
															if($continue==true){
																
																$find_transaction->message_send=$message;
																$find_transaction->etat_transaction=1;
																$find_transaction->save();	
																
																$receiver=$find_acteur->idUser->username;
																$this->send_abcsms($contenmarchand,$receiver);
																//envoie sms vers le marchand
															
															}
															
														}else{
															$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
														}
														
												}else{
													$message="Votre solde est insuffisant pour effectuer cette opération.";
													$message.="\nSolde actuel: ".Utils::show_nbre($find_souscripteur->solde_disponible);
													$message.="\nMontant de l'achat: ".Utils::show_nbre($find_credit->montant_vente_abc)."\nMerci";
												}
											
											}else{
												
												$message="Cette confirmation a déjà été effectuée. Veuillez demander au marchand de vous regénérer une nouvelle transaction.\nMerci";
											}
											
										}else{
											$message=\Yii::t('app', 'TRY_LATER');
										}
									
									}else if($type_operation=="2"){
										
										//verification du retrait
										$find_retrait = AbcRetrait::findOne(['ref_retrait'=>$refcredit,'id_acteur_user'=>$find_acteur->id_acteur_user,'idclient'=>$find_souscripteur->id_souscription,'status_retrait'=>[0,1,2]]);
										if($find_retrait!=null){
											
											if($find_retrait->status_retrait==0){
												
												//verification du solde du client
												if((int)$find_retrait->montant_retrait <= (int)$find_souscripteur->solde_disponible){
												
													$find_retrait->status_retrait = (int)$response;
													$find_retrait->customer_confirm_time = date("Y-m-d H:i:s");
													if($find_retrait->save()){
														
														$message="Votre demande de retrait a été annulée avec succès. Une notification sera envoyée au marchand.";
														
														$contenmarchand="Le client ".$find_souscripteur->idUser->nom." ".$find_souscripteur->idUser->prenoms." vient de rejeter la demande de retrait ".$refcredit;
														$contenmarchand.="\nMontant: ".Utils::show_nbre($find_retrait->montant_retrait)." FCFA";
														
														$continue=true ;
														if($response==1){
															
															$infoFrais=AbcCashout::find()->One();
															$amount=0;
															if($infoFrais!=null){
																
																if($infoFrais->cout>0){
																	$amount=(int)$find_retrait->montant_retrait*$infoFrais->cout/100;
																}
															}
																
															
															$find_souscripteur->solde_disponible-=(int)$find_retrait->montant_retrait+$amount;
															if($find_souscripteur->save()){
															
																//modification de lhistorique de la personne
																
																$find_historique = AbcHistorique::findOne(['mois_operation'=>date("Y-m"),'id_souscription'=>$find_souscripteur->id_souscription]);	
																if($find_historique!=null){
																	
																	$find_historique->montant_restant-=(int)$find_retrait->montant_retrait+$amount;
																	$find_historique->montant_operation+=(int)$find_retrait->montant_retrait+$amount;
																	$find_historique->save();
																	
																}
										
										
																$typeretrait=$find_retrait->typeEnvoiepaiement;
																
																if($typeretrait->ref=="ABC03"){
																
																	$message="Vous avez confirmé votre retrait avec succès. Une notification sera envoyée au marchand pour que vous recevez votre argent.";
																	$message.="\nSolde restant: <b>".Utils::show_nbre($find_souscripteur->solde_disponible)." FCFA</b>";
																	$message.="\nReference: ".$refcredit;
																	
																	
																	$contenmarchand="Le client ".$find_souscripteur->idUser->nom." ".$find_souscripteur->idUser->prenoms." vient de valider la demande de retrait ".$refcredit;
																	$contenmarchand.="\nVous pouvez lui remettre la somme demandée";
																	$contenmarchand.="\nMontant: ".Utils::show_nbre($find_retrait->montant_retrait)." FCFA";
																
																}else{
																	
																	$message="Vous avez confirmé votre retrait avec succès. Une notification vous sera envoyée des que votre compte ".ucfirst($typeretrait->libelle)." sera credité.";
																	$message.="\nSolde restant: <b>".Utils::show_nbre($find_souscripteur->solde_disponible)." FCFA</b>";
																	$message.="\nReference: ".$refcredit;
																	
																	
																	$contenmarchand="Le client ".$find_souscripteur->idUser->nom." ".$find_souscripteur->idUser->prenoms." vient de valider la demande de retrait ";
																	$contenmarchand.="\nLa somme lui sera envoye sur son compte ".ucfirst($typeretrait->libelle);
																	$contenmarchand.="\nMontant: ".Utils::show_nbre($find_retrait->montant_retrait)." FCFA";
																	
																	
																	//enregistrer l'operation dans une table paiement , et ce paiement sera declenchE plus tard par un service cron															
																	
																	Common::save_cashout($find_retrait);
																}
															
															}else{
																$continue=false ;
																$find_retrait->status_retrait = 0;
																$find_retrait->save();
																
																$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
															}
															
														}
														
														if($continue==true){
															
															$find_transaction->message_send=$message;
															$find_transaction->etat_transaction=1;
															$find_transaction->save();	
															
															$receiver=$find_acteur->idUser->username;
															$this->send_abcsms($contenmarchand,$receiver);
															//envoie sms vers le marchand
														
														}
														
													}else{
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
													}
													
													
												}else{
													$message="Votre solde est insuffisant pour effectuer cette opération.";
													$message.="\nSolde actuel: ".Utils::show_nbre($find_souscripteur->solde_disponible);
													$message.="\nMontant du retrait: ".Utils::show_nbre($find_credit->montant_retrait)."\nMerci";
												}
											
											}else{
												
												$message="Cette confirmation a déjà été effectuée. Veuillez demander au marchand de vous regénérer une nouvelle transaction.\nMerci";
											}
											
										}else{
											$message=\Yii::t('app', 'TRY_LATER');
										}
													
									}else{
										$message=\Yii::t('app', 'TRY_LATER');
									}
								}else{
									$message=\Yii::t('app', 'TRY_LATER');
								}	
						
							}else{
								$message=\Yii::t('app', 'TRY_LATER');
							}
							
						
						}else {
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
							
					break;
					
				}
				
			}	
		}

		
	   return $message;
	}
	
	
	public function boutique_marchand($response,$id_trans,$status,$provenance){				
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		$sub_menu=$find_transaction->sub_menu;
		if($status=="0"){
			
				$message=\Yii::t('app', 'SELECT_BOUTIQUE')."\n\n".\Yii::t('app', 'tape');
				$message.="\n1.*. ".\Yii::t('app', 'boutique1')."\n\n2.*. ".\Yii::t('app', 'boutique2')."\n\n3.*. ".\Yii::t('app', 'boutique3')."\n\n4.*. ".\Yii::t('app', 'boutique4')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
				$find_transaction->message_send=$message;
				$find_transaction->sub_menu=1;
				$find_transaction->save();
		}else{
			
			if($response=="0"){
					$message=$this->get_first_menu($find_transaction);													
			}else{
					
				switch (trim($sub_menu)) {	
					case "1":
						 $tab_response=array("1","2","3","4");								
						if(in_array($response,$tab_response)){

							
							$orther='"type_operation":"'.$response.'"';
							$test_acteur=ActeurUser::find()->where(['type_acteur'=>1,'id_user'=>$find_transaction->iduser])->orderBy(['id_acteur_user'=>SORT_DESC])->one();
							if($test_acteur!=null){
									
								if($response==2){	
								
									if($test_acteur->etat==1){
										$tab_request['mproduits.etat']=['1','2'];
										$tab_request['acteur_user.id_user']=$find_transaction->iduser;
										
				
										$max_produit=$test_acteur->nbre_produit;				
										$test_produits=Mproduits::find() ->leftJoin('acteur_user', 'acteur_user.id_acteur_user=mproduits.id_acteur')->where($tab_request)->all();
										if(sizeof($test_produits)<$max_produit){
													
											$message=\Yii::t('app', 'bdenomination_produit');									
											$find_transaction->message_send=$message;
											$find_transaction->others=$orther;
											$find_transaction->sub_menu=2;
											$find_transaction->save();	
											
										}else{				
																
											$message=\Yii::t('app', 'produit_max_atteint')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
											$find_transaction->message_send=$message;
											$find_transaction->etat_transaction=2;
											$find_transaction->save();	
																				
										}	
										
									}else if($test_acteur->etat==0){
										
										$nom_boutique=$test_acteur->denomination;
										$content="Pour pouvoir publier vos produits, vous devez ajouter la position de géolocalisation de votre boutique et ensuite envoyer une photo de votre commerce.\nMerci";
										
										$info_user=$test_acteur->idUser;
										if($provenance=="TELEGRAM"){			
											$phone=$info_user->canal_key;
										}else{
											$phone=$info_user->username;
										}
										
			
										Utils::send_information($phone,$content,$provenance);

										
										$message =\Yii::t('app', 'localisation_boutique1')." <b>".trim($nom_boutique)."</b>\n\n".\Yii::t('app', 'localisation_boutique2');
										$message.="WA_IMAGElocation.jpegWA_IMAGE\n\n\n".\Yii::t('app', 'help_location1');
										
										$orther='"id_acteur":"'.$test_acteur->id_acteur_user.'","type_acteur":"1"';
												
										$find_transaction->others=$orther;
										$find_transaction->message_send=$message;
										$find_transaction->sub_menu=20;
										$find_transaction->save();
																
										
									}else{
										$info_user=User::findOne(['id'=>$find_transaction->iduser]);
										//boutique desactivE
										$message="Cher(e) ".$info_user->nom." ".$info_user->prenoms."\n";
										$message.="Votre <b>compte marchand</b> sur <b>ABusiness</b> a été <b>désactivé</b>";									
										$message.="\n\n<b>Raison de la désactivation:</b> ".$info_user->raison_desactivation;										
									}
												
								}else if($response==1){
									//recuperer la liste des produits de sa boutique								
									$message=$this->get_produitsmarchand($find_transaction);
								}else {
									//recuperer la liste des produits de sa boutique								
									$message=$this->get_commandesmarchand($test_acteur->id_acteur_user,$find_transaction,$response);
								}
								
							}else{
								$message=\Yii::t('app', 'TRY_LATER')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
								$find_transaction->message_send=$message;
								$find_transaction->etat_transaction=2;
								$find_transaction->save();	
							}	
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}								
						
					break ;
					
					case "20":
						//verification si la position a ete envoyé
						$test_geo=explode(';',$response);
						if(sizeof($test_geo)==2 && is_numeric(trim($test_geo[0])) && is_numeric(trim($test_geo[1])) ){
							$orther=$find_transaction->others.',"localisation_boutique":"'.$response.'","lat":"'.$test_geo[0].'","long":"'.$test_geo[1].'"';
							
							$recup_information=Json::decode("[{".$find_transaction->others."}]");										
							$information=$recup_information[0];
								
							$message =\Yii::t('app', 'photo_boutique');
							
							if($information['type_acteur']==2){
								$message =\Yii::t('app', 'photo_livreur');
							}
							
							
							$find_transaction->others=$orther;
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=21;
							$find_transaction->save();
						
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
					break ;
					
					case "21":
						
						$test_photo1=explode("https:",urldecode($response));
						$test_photo2=explode("http:",urldecode($response));
						if(sizeof($test_photo1)>1 || sizeof($test_photo2)>1){	
			
							
								//telecharger la photo envoyer
								$photo_url = "boutique_".$find_transaction->iduser."_".time().".jpg";										
								$url=Yii::$app->basePath;
								$recup=explode(DIRECTORY_SEPARATOR."console",$url);
								$target_path= $recup[0].DIRECTORY_SEPARATOR."backend".DIRECTORY_SEPARATOR."web".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."acteurs".DIRECTORY_SEPARATOR;	
								$source_url = $target_path.$photo_url;
										
				
								$otherserver_url = file_get_contents(str_replace("\\","",$response));
								if(file_put_contents($source_url, $otherserver_url)){
									
									$orther=$find_transaction->others.',"photo_boutique":"'.$photo_url.'"';							
									$message=\Yii::t('app', 'select_sexe')."\n\n1.*. ".\Yii::t('app', 'sexe1')."\n2.*. ".\Yii::t('app', 'sexe2');							
									
									$find_transaction->others=$orther;
									$find_transaction->message_send=$message;
									$find_transaction->sub_menu=22;
									$find_transaction->save();
									
										
									
								}else{
									$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
								}
							
						
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
												
					break ;
					case "22":
						
						if(in_array($response,array("1","2"))){
							
							$orther=$find_transaction->others.',"sexe_acteur":"'.$response.'"';							
							$message=\Yii::t('app', 'acteur_age');							
							
							$find_transaction->others=$orther;
							$find_transaction->message_send=$message;
							$find_transaction->sub_menu=23;
							$find_transaction->save();
							
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}
												
					break ;
					case "23":
						
						$convert=trim(str_replace("ans","",strtolower($response)));						
						if(is_numeric($convert) && (int)$convert>0){
							
							$orther=$find_transaction->others.',"age":"'.$convert.'"';
							
							$recup_information=Json::decode("[{".$find_transaction->others."}]");										
							$information=$recup_information[0];	
							
							
							$test_acteur=ActeurUser::find()->where(['type_acteur'=>$information['type_acteur'],'id_acteur_user'=>$information['id_acteur']])->one();
							
							$test_acteur->lat_acteur=$information['lat'];
							$test_acteur->long_acteur=$information['long'];
							$test_acteur->sexe_acteur=(int)$information['sexe_acteur'];
							$test_acteur->naissance_acteur=(date("Y")-(int)$convert)."-01-01";
							$test_acteur->photo_acteur=$information['photo_boutique'];
							$test_acteur->etat=1;
							$test_acteur->save();							
							
							$phone_collecteur=$find_transaction->username;
							$content="";
							
							$info_user=$test_acteur->idUser; 
							
							if($information['type_acteur']==1){
								
								$content="La validation de votre <b>compte marchand</b> sur <b>ABusiness</b> a été effectueé avec succès. Vous pouvez desormais publier vos produits.\nMerci";	
								
								$info_user->id_user_profil=4;
								$info_user->save();
								
							}else if($information['type_acteur']==2){
								
								$content="La validation de votre <b>compte livreur</b> sur <b>ABusiness</b> a été effectueé avec succès. Vous pouvez desormais recevoir des livraisons.\nMerci";	
							}		

							if($provenance=="TELEGRAM"){			
								$phone=$info_user->canal_key;
							}else{
								$phone=$info_user->username;
							}
							
							Utils::send_information($phone,$content,$provenance);
							
							$test_collecteur=Collecteur::find()->where(['idCollecteur'=>$test_acteur->id_collecteur])->one();
							if($test_collecteur !== null && $test_acteur->id_collecteur!=1 && trim($test_collecteur->phone_collecteur)!=""){
								
									$code_collecteur=trim($test_collecteur->info_supplementaire);									
									
									if($test_acteur->type_acteur==1){
										//envoyer le code de reference au collecteur
										$content ="La demande de <b>compte marchand</b> sur <b>ABusiness</b> a été acceptée.";	
										$content.="\n\n<b>Denomination:</b> ".$test_acteur->denomination;	
										$content.="\n".$test_acteur->description_activite;	
										$content.="\n<b>Adresse lieu:</b> ".$test_acteur->address_acteur;	
										$content.="\n<b>Autre contact:</b> ".$test_acteur->other_number;
										$content.="\n\n<b>Code d'identification marchand:</b> ".$test_acteur->code_reference;
										$content.="\n\n".$info_user->nom." ".$info_user->prenoms." - Contact:".$info_user->username;	
										
										Utils::send_information($code_collecteur,$content,"TELEGRAM");

										$phone_collecteur=trim($test_collecteur->phone_collecteur);
										Utils::send_information($phone_collecteur,$content,"WHATSAPP");	
									}else{
										$content ="La demande de <b>compte livreur</b> sur <b>ABusiness</b> a été acceptée.";	
										$content.="\n\n<b>Moyen de deplacement:</b> ".$test_acteur->denomination;
										$content.="\n<b>Adresse lieu:</b> ".$test_acteur->address_acteur;	
										$content.="\n<b>Autre contact:</b> ".$test_acteur->other_number;
										$content.="\n\n<b>Code d'identification marchand:</b> ".$test_acteur->code_reference;
										$content.="\n\n".$info_user->nom." ".$info_user->prenoms." - Contact:".$info_user->username;		
										
										
										Utils::send_information($code_collecteur,$content,"TELEGRAM");	
										
										$phone_collecteur=trim($test_collecteur->phone_collecteur);
										Utils::send_information($phone_collecteur,$content,"WHATSAPP");	
									}
																	
								
							}
							
							
							$message=$this->get_first_menu($find_transaction);	
								
						}else{
							$message="<b>Le format de l'age est incorrect</b>\n\n".$find_transaction->message_send;
						}
																			
					break ;
					
					default:
					
						$message="Erreur";
						
						$recup_information=Json::decode("[{".$find_transaction->others."}]");										
						$information=$recup_information[0];							
						$type_operation= $information['type_operation'];
						if($type_operation=="1"){							
							
							switch (trim($sub_menu)) {	
									case "2":
											
										$id_user=$find_transaction->iduser;
										//recuperer l'id de lacteur en question
										$test_acteur=ActeurUser::find()->where(['etat'=>1,'type_acteur'=>1,'id_user'=>$id_user])->one();										
										if($test_acteur!=null){
											
											//recuperer les informations sur le produit en question
											$id_acteur=$test_acteur->id_acteur_user;
											$test_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1,'position_menu_ussd'=>$response])->orderBy('position_menu_ussd ASC')->one();
											
											if($test_produits!=null && (int)$response>0){ 
										
												$message="";
												
												$orther=$find_transaction->others.',"id_acteur":"'.$id_acteur.'","idProduit":"'.$test_produits->idProduit.'"';
												
												
												$resume=$test_produits->denomination.", ".$this->show_nbre($test_produits->qte)." ".$test_produits->unite." à ".$this->show_nbre($test_produits->prix_vente)." F CFA";
												
												$photo_produit=(string)$test_produits->photo_produit;
												if($photo_produit!=""){
													$message.="WA_IMAGEproduit/".$photo_produit."WA_IMAGE";
												}
												$message.=$resume."\n\n1.*. ".\Yii::t('app', 'UPDATEINFO')."\n2.*. ".\Yii::t('app', 'UPDATEPHOTO')."\n3.*. ".\Yii::t('app', 'DELETEPRODUIT')."\n4.*. ".\Yii::t('app', 'MPRODUITS');
												
												
												$find_transaction->others=$orther;
												$find_transaction->message=$resume;
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=3;
												$find_transaction->save();
											
											}else{
												
												$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
											}
											
										}else{
									
											$message=\Yii::t('app', 'TRY_LATER')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
											$find_transaction->message_send=$message;
											$find_transaction->etat_transaction=2;
											$find_transaction->save();	
											
										}
											
											
									break;
									case "3":
										if($response=="1"){	
										    $message="<b>Modification du produit:</b>\n".$find_transaction->message."\n\n";
											$message.=\Yii::t('app', 'ubdenomination_produit');
											$orther=$find_transaction->others.',"suite_operation":"UPDATE_INFO"';
											
											$find_transaction->message_send=$message;
											$find_transaction->others=$orther;
											$find_transaction->sub_menu=4;
											$find_transaction->save();
											 
										}else if($response=="2"){	
										    $message="<b>Modification de la photo du produit:</b>\n".$find_transaction->message."\n\n";
											$message.=\Yii::t('app', 'ubphoto_produit');
											$orther=$find_transaction->others.',"suite_operation":"UPDATE_PHOTO"';
											
											$find_transaction->message_send=$message;
											$find_transaction->others=$orther;
											$find_transaction->sub_menu=4;
											$find_transaction->save();
											 
										}else if($response=="3"){	
										
												$message="<b>Suppression du produit:</b>\n".$find_transaction->message;
												$message.="\n\n1.*. ".\Yii::t('app', 'CONFIRM')."\n2.*. ".\Yii::t('app', 'MPRODUITS');
												$orther=$find_transaction->others.',"suite_operation":"DELETE"';
												
												$find_transaction->message_send=$message;
												$find_transaction->others=$orther;
												$find_transaction->sub_menu=4;
												$find_transaction->save();
												
										}else if($response=="4"){	
										
											$message=$this->get_produitsmarchand($find_transaction);
										}else{
											$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
										}
											
									break;
									default:
										$suite_operation= $information['suite_operation'];
										if($suite_operation=="DELETE"){
											
											switch (trim($sub_menu)) {	
												case "4":	
										
													if($response=="1"){	
													
														$id_acteur= $information['id_acteur'];
														$idProduit= $information['idProduit'];											 
														$test_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1,'idProduit'=>$idProduit])->orderBy('position_menu_ussd ASC')->one();
														if($test_produits!=null){
															
															$test_produits->etat=3;
															if($test_produits->save()){
																
																$message="Suppression du produit effectueé avec succès";	
																$message.="\n\n1.*.".\Yii::t('app', 'MPRODUITS')."\n2.*.".\Yii::t('app', 'CONTINUE_PUBLICATION')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
																$find_transaction->message_send=$message;
																$find_transaction->sub_menu=5;
																$find_transaction->save();
																
															}else{
																		
																$message=\Yii::t('app', 'TRY_LATER')."1\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
																$find_transaction->message_send=$message;
																$find_transaction->etat_transaction=2;
																$find_transaction->save();	
																									
															}
															
														}else{
												
															$message=\Yii::t('app', 'TRY_LATER')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
															$find_transaction->message_send=$message;
															$find_transaction->etat_transaction=2;
															$find_transaction->save();	
															
														}
														
													}else if($response=="2"){	
													
														$message=$this->get_produitsmarchand($find_transaction);
															
													}else {	
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;											
													}
														
												break;
												case "5":
													if($response=="1"){	
													
														$message=$this->get_produitsmarchand($find_transaction);
														
													}else if($response=="2"){	
															$orther='"type_operation":"2"';
															$message=\Yii::t('app', 'bdenomination_produit');
															
															$find_transaction->message_send=$message;
															$find_transaction->others=$orther;
															$find_transaction->message_send="";
															$find_transaction->sub_menu=2;
															$find_transaction->save();	
														
															
													}else {	
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;											
													}
														
												break;
											
											}	
										
										}else if($suite_operation=="UPDATE_INFO"){
											
											switch (trim($sub_menu)) {	
												case "4":	
										
													$orther=$find_transaction->others.',"nom_produit":"'.$response.'"';
													$message="<b>Modification du produit:</b>\n".$find_transaction->message."\n\n";
													$message.=\Yii::t('app', 'bunite_produit')." ( <b>".trim($response)."</b> )";
													
													
													$find_transaction->others=$orther;
													$find_transaction->message_send=$message;
													$find_transaction->sub_menu=5;
													$find_transaction->save();
														
												break;
												case "5":
													
													$nom_produit=$information['nom_produit'];
													$orther=$find_transaction->others.',"unite_produit":"'.$response.'"';
													
													$message="<b>Modification du produit:</b>\n".$find_transaction->message."\n\n";
													$message.=\Yii::t('app', 'bqte_produit')." ( <b>".trim($nom_produit)."</b> )";
													
													
													$find_transaction->others=$orther;
													$find_transaction->message_send=$message;
													$find_transaction->sub_menu=6;
													$find_transaction->save();
											
													
														
												break;
												
												case "6":
														if(is_numeric($response) && (int)$response>0){
															$nom_produit=$information['nom_produit'];
															$unite_produit= $information['unite_produit'];
															$uqte_produit= $response;
														
															$orther=$find_transaction->others.',"uqte_produit":"'.$response.'"';
															
															$message="<b>Modification du produit:</b>\n".$find_transaction->message."\n\n";
															$message.=\Yii::t('app', 'poids_unitaire')."\n<b>".trim($nom_produit)." ".trim($uqte_produit).trim($unite_produit)."</b> ";
															
															
															
															$find_transaction->others=$orther;
															$find_transaction->message_send=$message;
															$find_transaction->sub_menu=7;
															$find_transaction->save();
														}else{
															$message="<b>Le format de la quantité est incorrect</b>\n\n".$find_transaction->message_send;
														}
														
														
												break;
												case "7":
														if(is_numeric($response) && (double)$response>0){
															$nom_produit=$information['nom_produit'];
															$unite_produit= $information['unite_produit'];
															$uqte_produit= $information['uqte_produit'];
														
															$orther=$find_transaction->others.',"poids_produit":"'.$response.'"';
															$message="<b>Modification du produit:</b>\n".$find_transaction->message."\n\n";
															$message.=\Yii::t('app', 'bprix_produit')."\n<b>".trim($nom_produit)." ".trim($uqte_produit).trim($unite_produit)."</b> ";
															
															
															
															$find_transaction->others=$orther;
															$find_transaction->message_send=$message;
															$find_transaction->sub_menu=8;
															$find_transaction->save();
														}else{
															$message="<b>Le format du poids unitaire est incorrect</b>\n\n".$find_transaction->message_send;
														}
														
														
												break;
												case "8":
								
														if(is_numeric($response) && (int)$response>0){
															
															$orther=$find_transaction->others.',"prix_produit":"'.(int)$response.'"';															
															$message="<b>Modification du produit:</b>\n".$find_transaction->message."\n\n";
															
															$nom_produit= $information['nom_produit'];
															$unite_produit= $information['unite_produit'];
															$uqte_produit= $information['uqte_produit'];
															$prix_produit= (int)$response;
											
															
															$resume="Vente du produit : ".$nom_produit.", ".$this->show_nbre($uqte_produit)." ".$unite_produit." à ".$this->show_nbre($prix_produit)." F CFA";
											
															$message.=$resume."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'CANCEL');															

															
															$find_transaction->others=$orther;
															$find_transaction->message_send=$message;
															$find_transaction->sub_menu=9;
															$find_transaction->save();
														
														}else{
															$message="<b>Le format du prix est incorrect. Ex: 12000</b>\n\n".$find_transaction->message_send;
														}
														
												break;
												case "9":
													if($response=="1"){	
														//enregistrement duproduit
													
														$id_acteur= $information['id_acteur'];
														$idProduit= $information['idProduit'];											 
														$test_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1,'idProduit'=>$idProduit])->orderBy('position_menu_ussd ASC')->one();
														if($test_produits!=null){
															
															$nom_produit= $information['nom_produit'];
															$unite_produit= $information['unite_produit'];
															$uqte_produit= $information['uqte_produit'];
															$prix_produit= $information['prix_produit'];
															$poids_produit= $information['poids_produit'];
															
															$test_produits->denomination=$nom_produit;
															$test_produits->unite=$unite_produit;
															$test_produits->qte=$uqte_produit;
															$test_produits->prix_vente=$prix_produit;
															$test_produits->poids_unitaire=$poids_produit;
															$test_produits->etat=1;
															if($test_produits->save()){
																
																$message="Modification du produit effectueé avec succès";	
																$message.="\n\n1.*.".\Yii::t('app', 'MPRODUITS')."\n2.*.".\Yii::t('app', 'CONTINUE_PUBLICATION')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
																$find_transaction->message_send=$message;
																$find_transaction->sub_menu=10;
																$find_transaction->save();
																
															}else{
																		
																$message=\Yii::t('app', 'TRY_LATER')."1\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
																$find_transaction->message_send=$message;
																$find_transaction->etat_transaction=2;
																$find_transaction->save();	
																									
															}
															
														}else{
												
															$message=\Yii::t('app', 'TRY_LATER')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
															$find_transaction->message_send=$message;
															$find_transaction->etat_transaction=2;
															$find_transaction->save();	
															
														}
														
													
													}else  if($response=="2"){	
													
														$message=$this->get_produitsmarchand($find_transaction);
														
													}else{
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
														
													}
												
												break;
												case "10":
													if($response=="1"){	
													
														$message=$this->get_produitsmarchand($find_transaction);
														
													}else if($response=="2"){	
															$orther='"type_operation":"2"';
															$message=\Yii::t('app', 'bdenomination_produit');
															
															$find_transaction->message_send=$message;
															$find_transaction->others=$orther;
															$find_transaction->message_send="";
															$find_transaction->sub_menu=2;
															$find_transaction->save();	
														
															
													}else {	
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;											
													}
														
												break;
											
											}
											
										}else if($suite_operation=="UPDATE_PHOTO"){
											
											switch (trim($sub_menu)) {	
												
												case "4":
													$test_photo1=explode("https:",urldecode($response));
													$test_photo2=explode("http:",urldecode($response));
													if(sizeof($test_photo1)>1 || sizeof($test_photo2)>1){		
										
														//telecharger la photo envoyer
														$photo_url = "produit_".$find_transaction->iduser."_".time().".jpg";
																
														$url=Yii::$app->basePath;
														$recup=explode(DIRECTORY_SEPARATOR."console",$url);
														$target_path= $recup[0].DIRECTORY_SEPARATOR."backend".DIRECTORY_SEPARATOR."web".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."produit".DIRECTORY_SEPARATOR;	
														$source_url = $target_path.$photo_url;
																
										
														$otherserver_url = file_get_contents(str_replace("\\","",$response));
														if(file_put_contents($source_url, $otherserver_url)){
															
															$orther=$find_transaction->others.',"photo_produit":"'.$photo_url.'"';
															$resume="<b>Modification de la photo du produit:</b>\n".$find_transaction->message."\n\n";
															$message=$resume."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'MPRODUITS')."\n0.*.".\Yii::t('app', 'CANCEL');
															
															$find_transaction->others=$orther;
															$find_transaction->message_send=$message;
															$find_transaction->sub_menu=5;
															$find_transaction->save();
															
														}else{
															$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
														}
													}else{
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
													}
												break;
												case "5":
													if($response=="1"){	
														//enregistrement duproduit
													
														$id_acteur= $information['id_acteur'];
														$idProduit= $information['idProduit'];											 
														$photo_produit= $information['photo_produit'];											 
														$test_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1,'idProduit'=>$idProduit])->orderBy('position_menu_ussd ASC')->one();
														if($test_produits!=null){
															
															$test_produits->photo_produit=$photo_produit;
															$test_produits->etat=1;
															if($test_produits->save()){
																
																$message="Modification de la photo du produit effectueé avec succès";	
																$message.="\n\n1.*.".\Yii::t('app', 'MPRODUITS')."\n2.*.".\Yii::t('app', 'CONTINUE_PUBLICATION')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
																$find_transaction->message_send=$message;
																$find_transaction->sub_menu=6;
																$find_transaction->save();
																
															}else{
																		
																$message=\Yii::t('app', 'TRY_LATER')."1\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
																$find_transaction->message_send=$message;
																$find_transaction->etat_transaction=2;
																$find_transaction->save();	
																									
															}
															
														}else{
												
															$message=\Yii::t('app', 'TRY_LATER')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
															$find_transaction->message_send=$message;
															$find_transaction->etat_transaction=2;
															$find_transaction->save();	
															
														}
														
													
													}else  if($response=="2"){	
													
														$message=$this->get_produitsmarchand($find_transaction);
														
													}else{
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
														
													}
												
												break;
												case "6":
													if($response=="1"){	
													
														$message=$this->get_produitsmarchand($find_transaction);
														
													}else if($response=="2"){	
															$orther='"type_operation":"2"';
															$message=\Yii::t('app', 'bdenomination_produit');
															
															$find_transaction->message_send=$message;
															$find_transaction->others=$orther;
															$find_transaction->message_send="";
															$find_transaction->sub_menu=2;
															$find_transaction->save();	
														
															
													}else {	
														$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;											
													}
														
												break;
											
											}
											
										}
									
									break;
									
									
									
							}
						
						}else if($type_operation=="2"){	
						
							switch (trim($sub_menu)) {	
									case "2":
					
											$orther=$find_transaction->others.',"nom_produit":"'.$response.'"';
											$message =\Yii::t('app', 'bunite_produit')." ( <b>".trim($response)."</b> )";
											
											
											$find_transaction->others=$orther;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=3;
											$find_transaction->save();
											
											
									break;
									case "3":
											$nom_produit=$information['nom_produit'];
											$orther=$find_transaction->others.',"unite_produit":"'.$response.'"';
											$message =\Yii::t('app', 'bqte_produit')." ( <b>".trim($nom_produit)."</b> )";
											
											
											$find_transaction->others=$orther;
											$find_transaction->message_send=$message;
											$find_transaction->sub_menu=4;
											$find_transaction->save();
											
											
									break;
									case "4":
											if(is_numeric($response) && (int)$response>0){
												$nom_produit=$information['nom_produit'];
												$unite_produit= $information['unite_produit'];
												$uqte_produit= $response;
											
												$orther=$find_transaction->others.',"uqte_produit":"'.$response.'"';
												$message =\Yii::t('app', 'poids_unitaire')."\n<b>".trim($nom_produit)." ".trim($uqte_produit).trim($unite_produit)."</b> ";
												
												
												
												$find_transaction->others=$orther;
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=5;
												$find_transaction->save();
											}else{
												$message="<b>Le format de la quantité est incorrect</b>\n\n".$find_transaction->message_send;
											}
											
											
									break;
									case "5":
											if(is_numeric($response) && (double)$response>0){
												$nom_produit=$information['nom_produit'];
												$unite_produit= $information['unite_produit'];
												$uqte_produit= $information['uqte_produit'];
											
												$orther=$find_transaction->others.',"upoids_produit":"'.$response.'"';
												$message =\Yii::t('app', 'bprix_produit')."\n<b>".trim($nom_produit)." ".trim($uqte_produit).trim($unite_produit)."</b> ";
												
												
												
												$find_transaction->others=$orther;
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=6;
												$find_transaction->save();
											}else{
												$message="<b>Le format du poids unitaire est incorrect</b>\n\n".$find_transaction->message_send;
											}
											
											
									break;
									case "6":
					
											if(is_numeric($response) && (int)$response>0){
												$nom_produit=$information['nom_produit'];
												$orther=$find_transaction->others.',"prix_produit":"'.(int)$response.'"';
												$message =\Yii::t('app', 'bphoto_produit')." ( <b>".trim($nom_produit)."</b> )";
												
												
												$find_transaction->others=$orther;
												$find_transaction->message_send=$message;
												$find_transaction->sub_menu=7;
												$find_transaction->save();
											
											}else{
												$message="<b>Le format du prix est incorrect. Ex: 12000</b>\n\n".$find_transaction->message_send;
											}
											
											
									break;
									
									
									case "7":
									
											//verifier si cest lien

											$test_photo1=explode("https:",urldecode($response));
											$test_photo2=explode("http:",urldecode($response));
											if(sizeof($test_photo1)>1 || sizeof($test_photo2)>1){		
								
												//telecharger la photo envoyer
												$photo_url = "produit_".$find_transaction->iduser."_".time().".jpg";
														
												$url=Yii::$app->basePath;
												$recup=explode(DIRECTORY_SEPARATOR."console",$url);
												$target_path= $recup[0].DIRECTORY_SEPARATOR."backend".DIRECTORY_SEPARATOR."web".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."produit".DIRECTORY_SEPARATOR;	
												$source_url = $target_path.$photo_url;
														
								
												$otherserver_url = file_get_contents(str_replace("\\","",$response));
												if(file_put_contents($source_url, $otherserver_url)){
													
													$orther=$find_transaction->others.',"photo_produit":"'.$photo_url.'"';
													
													$nom_produit= $information['nom_produit'];
													$unite_produit= $information['unite_produit'];
													$uqte_produit= $information['uqte_produit'];
													$prix_produit= $information['prix_produit'];
													$poids_produit= $information['upoids_produit'];
													
													$resume="Vente du produit : ".$nom_produit.", ".$this->show_nbre($uqte_produit)." ".$unite_produit." à ".$this->show_nbre($prix_produit)." F CFA";
													
													$message=$resume."\n\n1.*.".\Yii::t('app', 'CONFIRM')."\n2.*.".\Yii::t('app', 'UPDATE')."\n0.*.".\Yii::t('app', 'CANCEL');
													
													$find_transaction->others=$orther;
													$find_transaction->message_send=$message;
													$find_transaction->sub_menu=8;
													$find_transaction->save();
													
												}else{
													$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
												}
											}else{
												$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
											}
											
											
									break;
									
									case "8":
											if($response=="1"){	
												//enregistrement duproduit
											
												$id_user=$find_transaction->iduser;
												//recuperer l'id de lacteur en question
												$test_acteur=ActeurUser::find()->where(['etat'=>1,'type_acteur'=>1,'id_user'=>$id_user])->one();
												
												if($test_acteur!=null){
												
														$id_acteur=$test_acteur->id_acteur_user;
														
														$nom_produit= $information['nom_produit'];
														$unite_produit= $information['unite_produit'];
														$prix_produit= $information['prix_produit'];
														$uqte_produit= $information['uqte_produit'];
														$photo_produit= $information['photo_produit'];
														$poids_produit= $information['upoids_produit'];
														
														
														$test_position=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>[1,2]])->orderBy(['position_menu_ussd'=>SORT_DESC])->one();
														if($test_position!=null){
															$position_menu_ussd=((int)$test_position->position_menu_ussd)+1;
														}else{
															$position_menu_ussd=1;
														}
														
														$model=new Mproduits;
														$model->id_acteur=$id_acteur;
														$model->photo_produit=$photo_produit;
														$model->denomination=$nom_produit;
														$model->unite=$unite_produit;
														$model->qte=$uqte_produit;
														$model->prix_vente=$prix_produit;
														$model->position_menu_ussd=$position_menu_ussd;
														$model->poids_unitaire=$poids_produit;
														$model->etat=1;
														$model->created_by=$id_user;
														$model->key_produit=Yii::$app->security->generateRandomString(32);
														
														if($model->save()){		
														
															$code_produit="TTB".$id_acteur."U".$position_menu_ussd."TTBCLIN";
															
															if($provenance=="TELEGRAM"){
																$url=$this->lien_telegram."ABSELLER".$test_acteur->code_reference.$code_produit."ABSELLER";
															}else {
																$url=$this->lien_whatsapp."ABSELLER".$test_acteur->code_reference.$code_produit."ABSELLER";											
															}
										
															
															$first_message="<b>".$test_acteur->denomination."</b> vient de publier <b>".$nom_produit."</b> sur <b>ABusiness</b>. Vous pourrez voir le produit et passer une commande ici:\n".$url;
															
															$message=\Yii::t('app', 'success_publication');		
															$message.="GOOD_DIAGNOSTIC".$first_message."GOOD_DIAGNOSTIC";
															$message.="\n\n1.*.".\Yii::t('app', 'CONTINUE_PUBLICATION')."\n2.*.".\Yii::t('app', 'MPRODUITS')."\n0.*.".\Yii::t('app', 'MENU_PRINCIPAL');
															
															
															$find_transaction->message_send=$message;
															$find_transaction->sub_menu=9;
															$find_transaction->save();	
														
														}else{
															
															
															$message=\Yii::t('app', 'TRY_LATER')."1\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
															$find_transaction->message_send=$message;
															$find_transaction->etat_transaction=2;
															$find_transaction->save();	
																								
														}
												
												}else{
													
													$message=\Yii::t('app', 'TRY_LATER')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
													$find_transaction->message_send=$message;
													$find_transaction->etat_transaction=2;
													$find_transaction->save();	
													
												}
									
											
											}else  if($response=="2"){	
											
												
												$orther='"type_operation":"2"';
												$message=\Yii::t('app', 'bdenomination_produit');
												
												$find_transaction->message_send=$message;
												$find_transaction->others=$orther;
												$find_transaction->message_send=$find_transaction->others;
												$find_transaction->sub_menu=2;
												$find_transaction->save();									
											}else{
												$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
											}
										
									break;
									case "9":
											if($response=="1"){	
												//enregistrement duproduit
											
												
	
												$tab_request['mproduits.etat']=['1','2'];
												$tab_request['acteur_user.id_user']=$find_transaction->iduser;
												$test_acteur=ActeurUser::find()->where(['type_acteur'=>1,'id_user'=>$find_transaction->iduser])->orderBy(['id_acteur_user'=>SORT_DESC])->one();
												$max_produit=$test_acteur->nbre_produit;				
												$test_produits=Mproduits::find() ->leftJoin('acteur_user', 'acteur_user.id_acteur_user=mproduits.id_acteur')->where($tab_request)->all();
												if(sizeof($test_produits)<$max_produit){
															
													$orther='"type_operation":"2"';
													$message=\Yii::t('app', 'bdenomination_produit');
													
													$find_transaction->message_send=$message;
													$find_transaction->others=$orther;
													$find_transaction->message_send="";
													$find_transaction->sub_menu=2;
													$find_transaction->save();
													
												}else{				
																		
													$message=\Yii::t('app', 'produit_max_atteint')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
													$find_transaction->message_send=$message;
													$find_transaction->etat_transaction=2;
													$find_transaction->save();	
																						
												}	
									
											
											}else  if($response=="2"){	
											
											
												//recuperer la liste des produits de sa boutique								
												$message=$this->get_produitsmarchand($find_transaction);
								
											}else{
												$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
											}
																					
									break;
									
							}
						
						}else if($type_operation=="3" || $type_operation=="4"){	
						
							switch (trim($sub_menu)) {	
									case "2":
											$test_acteur=ActeurUser::find()->where(['type_acteur'=>1,'id_user'=>$find_transaction->iduser])->one();
											$all_commande=$this->list_commande($test_acteur->id_acteur_user,$type_operation);
											
											if((int)$response<=sizeof($all_commande) && (int)$response>0){
												
												//recuperer les info details de la commandes
												
												$model=$all_commande[(int)$response-1];
												$idcommande=$model->id_commande_marchand;
												
												
												$entete_commande ="Commande N°: <b>".$model->num_commande."</b>\n---------------------\n";
												$entete_commande.="<b>Détail du client</b> \n";
												$entete_commande.="Nom: ".$model->idUser->nom." ".$model->idUser->prenoms."\n";
												$entete_commande.="Contact: ".$model->idUser->username."\n---------------------\n";
												
												$pied_commande="";
													
												$info_commande="<b>Détail de la commande</b>\n";
												$tab_produits=explode("-",$model->produit_livraison);														
												$qte_livraison=explode("-",$model->qte_livraison);														
												$all_produits=Mproduits::find()->where(['idProduit'=>$tab_produits])->orderBy('position_menu_ussd ASC')->all();
												
												$total_commande=0;
												if(sizeof($all_produits)>0){
													$index=0;
													foreach($all_produits as $info_produits){
														
														$prix=$qte_livraison[$index]*$info_produits->prix_vente;													
														
														$info_commande.="\n<b>".$qte_livraison[$index]." X ".$info_produits->denomination."</b> - ".Utils::show_nbre($info_produits->qte)." ".$info_produits->unite." =".Utils::show_nbre($prix)." FCFA";
														
														$total_commande+=$prix;
														$index++;
													}
												}
												$info_commande.="\n\n<b>Total:</b> ".Utils::show_nbre($total_commande)." FCFA";
												
												
												
												$find_livreur = ActeurUser::find()->where(['id_acteur_user'=>$model->id_livreur,'type_acteur'=>2])->one();

												if($find_livreur!=null){
													
													$find_marchand=$model->idMarchand;;
												
													//envoie de la notification aux marchand 
													$photo_livreur=(string)$find_livreur->photo_acteur;
													$info_user=$find_livreur->idUser;
																									
													$entete_commande.="<b>Détail du livreur</b> \n";
													$entete_commande.="Nom: ".$info_user->nom." ".$info_user->prenoms."\n";
													$entete_commande.="Contact: ".$info_user->username;													
													$entete_commande.="\n---------------------\n";
													
													if($type_operation==3 && $model->etat_confirmation==0){
														$code_commande="TTK".$model->id_user."U".$find_marchand->idUser->id."U".$model->id_commande_marchand."TTKCLIN";
														
														if($provenance=="TELEGRAM"){
															$lien_client=$this->lien_telegram."ABSELLER".$find_marchand->code_reference.$code_commande."ABSELLER";
														}else {
															$lien_client=$this->lien_whatsapp."ABSELLER".$find_marchand->code_reference.$code_commande."ABSELLER";									
														}
															
														$pied_commande="\n\nVeuillez confirmer la récupération de la commande par le livreur en cliquant sur le lien suivant\n".$lien_client;	
													}
												}
											
												$message=$entete_commande.$info_commande.$pied_commande;
												
												
											}else{
												$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
											}
											
									break;
									
							}
						
						}else{
							$message=$this->get_first_menu($find_transaction);
						}
						
						
					break ;
							
					
					
					
				}
				
			}	
		}

		
	   return $message;
	}
				
	public function condition_generale($response,$id_trans,$status,$provenance){				
		
		$find_transaction = Ussdtransation::findOne(['id'=>$id_trans,'reference'=>$provenance]);
		$sub_menu=$find_transaction->sub_menu;
		if($status=="0"){
			
				$message=\Yii::t('app', 'SELECT_TYPEACTEUR')."\n\n".\Yii::t('app', 'tape');
				$message.="\n1.*. ".\Yii::t('app', 'cgu_acteur1')."\n2.*. ".\Yii::t('app', 'cgu_acteur2')."\n3.*. ".\Yii::t('app', 'cgu_acteur3')."\n\n0.*. ".\Yii::t('app', 'cancel_process');
				$find_transaction->message_send=$message;
				$find_transaction->sub_menu=1;
				$find_transaction->save();
		}else{
			
			if($response=="0"){
					$message=$this->get_first_menu($find_transaction);													
			}else{
					
				switch (trim($sub_menu)) {	
					case "1":
						$tab_response=array("1","2","3");								
						if(in_array($response,$tab_response)){
							
							
							$message=Cgu_content_fr::cgu_entete0."\n\n";
							$message.=Cgu_content_fr::cgu_entete."\n\n";	
							
							if($response==1)$message.=Cgu_content_fr::cgu_content1."\n\n";	
							if($response==2)$message.=Cgu_content_fr::cgu_content2."\n\n";	
							if($response==3)$message.=Cgu_content_fr::cgu_content3."\n\n";	
							
							$message.=Cgu_content_fr::cgu_bas;
							$message.="\n\n0.*. ".\Yii::t('app', 'MENU_PRINCIPAL');
							
							
						}else{
							$message=\Yii::t('app', 'ERROR').": \n".$find_transaction->message_send;
						}								
						
					break ;					
				}
				
			}	
		}

		
	   return $message;
	}
	
	
	public function get_ppartenaire($niveau,$id_user,$phone_user){
		
		$content="";
		if($this->adjafi=="2" || $phone_user=="22891393958" || $phone_user=="1093179373" || $phone_user=="1600952859494"){	
			$tab_support=array("2"=>"photo","3"=>"audio","4"=>"video");
			$affichage="WELCOME";
			
			if($niveau=="1"){
				$affichage="WELCOME";
			}else if($niveau=="3"){
				$affichage="VALIDATION";
			}
			
			$query_get=HistoriquePubMessage::find()->select(['id_partenaire'])->where(['id_user'=>$id_user,'date_send'=>date("Y-m-d")]);
			$get_publicite=PublicitePartenaire::find()->where(['etat_publicite'=>1,'type_support'=>[2,4]])->andWhere(['like','niveau_affichage',$affichage])->andWhere(['not in','id_publicite',$query_get])->one();
			if($get_publicite!=null && trim($get_publicite->support_publicite)){
				
				$content=array();
				$content[0]=$tab_support[$get_publicite->type_support];
				$content[1]=$get_publicite->support_publicite;
				
				$publicite = new HistoriquePubMessage();
				$publicite->id_partenaire = $get_publicite->id_publicite;
				$publicite->id_user =$id_user;
				$publicite->date_send = date("Y-m-d");
				$publicite->date_create = date("Y-m-d H:i:s");
				$publicite->save();
			}
			
		}
		return $content;
	}
	
	public function flah_marchand($find_acteur){
		
		$info_user=$find_acteur->idUser; 
		$phone_collecteur=$info_user->username;
		
		$content="Cher(e) ".$info_user->nom." ".$info_user->prenoms;
		$content.="\n\nUn client vient de visiter votre boutique ( <b>".$find_acteur->denomination."</b> ) sur <b>Abusiness</b>. Malheureusement, votre boutique est vide. Veuillez utiliser le <b>menu 5</b> pour publier vos produits, rendre votre compte opérationnel ✅✅✅ et gagner des milliers de clients 🧕🏾👩🏽‍💼👲🏽 qui vous attendent.\n\nPour commencer envoyez <b>AB</b>";
		
		$provenance=$info_user->canal;
		if($provenance=="TELEGRAM"){			
			$phone=$info_user->canal_key;
		}else{
			$phone=$info_user->username;
		}
		Utils::send_information($phone,$content,$provenance);
		
	}
	
	public function record_visitemarchand($find_acteur,$find_transaction){
		
		$bvisite = new BoutiqueVisite();
		$bvisite->id_acteur = $find_acteur->id_acteur_user;
		$bvisite->id_user =   $find_transaction->iduser;
		$bvisite->id_transaction =   $find_transaction->id;
		$bvisite->date_visite = date("Y-m-d");
		$bvisite->date_create = date("Y-m-d H:i:s");
		$bvisite->save();
	}
	
	public function list_commande($id_acteur,$type){
		
		$tab_request["id_marchand"]=$id_acteur;
		if($type=="3"){
			$tab_request["etat"]=1;
			$tab_request["etat_confirmation"]=[0,1];
		}else {
			$tab_request["etat"]=[1,2];
			$tab_request["etat_confirmation"]=2;
		}
		return CommandeMarchand::find()->where($tab_request)->orderBy('id_commande_marchand ASC')->limit(10)->all();
		
	}
	
	public function get_commandesmarchand($id_acteur,$find_transaction,$type){
		
		$message="";
		$empty_message="";
		
		$tab_request["id_marchand"]=$id_acteur;
		if($type=="3"){
			$empty_message=\Yii::t('app', 'empty_attentecommande');
			$message=\Yii::t('app', 'attentecommande');
		}else {
			$empty_message=\Yii::t('app', 'empty_finishcommande');
			$message=\Yii::t('app', 'finishcommande');
		}
		$all_commandes=$this->list_commande($id_acteur,$type);
		
		if(sizeof($all_commandes)>0){
			
			$nbre_show=sizeof($all_commandes);
			for($i=0;$i<$nbre_show;$i++){
				if($message!="")$message.="\n\n";
				$message.=($i+1).".*. Commande N°: <b>".$all_commandes[$i]->num_commande."</b> - ".$all_commandes[$i]->idUser->nom." ".$all_commandes[$i]->idUser->prenoms." - <b>".Utils::show_nbre($all_commandes[$i]->prix_produits)." Fcfa</b>";							
			}
			
			$orther='"type_operation":"'.$type.'"';
			$find_transaction->message_send=$message;
			$find_transaction->others=$orther;									;
			$find_transaction->sub_menu=2;
			$find_transaction->save();
		
		}else{
			
			$message =$empty_message;
			$find_transaction->message_send=$message;
			$find_transaction->etat_transaction=2;
			$find_transaction->save();	
		}										
	
		return $message;

	}
		
	public function get_produitsmarchand($find_transaction){
		
		$message="";
		
		$provenance=$find_transaction->reference;
		$id_user=$find_transaction->iduser;
												//recuperer l'id de lacteur en question
		$test_acteur=ActeurUser::find()->where(['etat'=>1,'type_acteur'=>1,'id_user'=>$id_user])->one();
		if($test_acteur!=null){
			
			$id_acteur=$test_acteur->id_acteur_user;
			$all_produits=Mproduits::find()->where(['id_acteur'=>$id_acteur,'etat'=>1])->orderBy('position_menu_ussd ASC')->all();
			if(sizeof($all_produits)>0){
				$message=\Yii::t('app', 'help_bmarchand')."\n\n";
				$nbre_show=sizeof($all_produits);
				
				$tab_response=array();
				for($i=0;$i<$nbre_show;$i++){					
					
					if(in_array($provenance,$this->tab_prov)){	
						$tab_response[]=["marchand"=>$test_acteur->denomination,"photo"=>"produit/".$all_produits[$i]->photo_produit,"code"=>$all_produits[$i]->position_menu_ussd,"denomination"=>$all_produits[$i]->denomination,"qte_vente"=>Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite,"prix_vente"=>Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa"];
					}else{							
						if($message!="")$message.="\n";
						$message.=$all_produits[$i]->position_menu_ussd.".*. ".$all_produits[$i]->denomination.", ".Utils::show_nbre($all_produits[$i]->qte).$all_produits[$i]->unite." à ".Utils::show_nbre($all_produits[$i]->prix_vente)." Fcfa";							
					}
				}	
				
				$photo_marchand=(string)$test_acteur->photo_acteur;
				if($photo_marchand!=""){
					$message.="WA_IMAGEacteurs/".$photo_marchand."WA_IMAGE";
				}
				
				if(in_array($provenance,$this->tab_prov)){	
					$message=json_encode(array("type"=>"my_produit","information"=>$tab_response));
				}
				
				$orther='"type_operation":"1"';
				$find_transaction->message_send=$message;
				$find_transaction->others=$orther;									;
				$find_transaction->sub_menu=2;
				$find_transaction->save();
			}else{
				$info_message="<b>Marchand: ".$test_acteur->denomination."</b>";
				$message =$info_message."\n\n".\Yii::t('app', 'bno_produits');
				$find_transaction->message_send=$message;
				$find_transaction->etat_transaction=2;
				$find_transaction->save();	
			}										
		
		}else{
			$provenance=$find_transaction->reference;
			$message=\Yii::t('app', 'ACCOUNT_NOTVALID')."\n\n".\Yii::t('app', 'parler_operateur'.$provenance);										
			$find_transaction->message_send=$message;
			$find_transaction->etat_transaction=2;
			$find_transaction->save();	
			
		}
		
		return $message;

	}
		
	public function diagnostique_resultat($orther){
		
		
		$recup_information=Json::decode("[{".$orther."}]");										
		$information=$recup_information[0];		

		$facteur_gravite1=0;
		$facteur_gravite2=0;
		$facteur_pronostic=0;
		
		$age=$information["question_9"];
		$toux=$information["question_2"];
		$odorat=$information["question_3"];
		$gorge=$information["question_4"];
		$diaree=$information["question_5"];
		
		$fievre=0;
		if($information["question_1"]==3 || $information["question_1"]==1){
			if($information["question_100"]>=39){
				$fievre=1;
				$facteur_gravite1++;
			}else{
				$fievre=0;				
			}
		}
		
		if($information["question_6"]==1){
			if($information["question_600"]==1){
				$facteur_gravite1++;
			}
		}
		
		if($information["question_7"]==1){
			$facteur_gravite2++;
		}
		
		if($information["question_8"]==1){
			$facteur_gravite2++;
		}
		
		
		
		if($information["question_9"]>=70) $facteur_pronostic++;
		
		$poids=$information["question_10"];
		$taille=$information["question_11"];
		$taille_carre=($taille/100)*($taille/100);
		$imc=$poids/$taille_carre;
		if($imc>=30){
			$facteur_pronostic++;
		}
		
		if($information["question_12"]==1)$facteur_pronostic++;
		if($information["question_13"]==1)$facteur_pronostic++;
		if($information["question_14"]==1)$facteur_pronostic++;
		if($information["question_15"]==1)$facteur_pronostic++;
		if($information["question_16"]==1)$facteur_pronostic++;
		if($information["question_17"]==1)$facteur_pronostic++;
		if($information["question_18"]==1)$facteur_pronostic++;
		if($information["question_19"]==1)$facteur_pronostic++;
		if($information["question_20"]==1)$facteur_pronostic++;
		
		
		$facteur_gravite=$facteur_gravite1+$facteur_gravite2;
		$type_message=0;
		$message=\Yii::t('app', 'resulat_diagnostic00')."\n\n".\Yii::t('app', 'resulat_diagnostic000');
		
		if( $fievre==1 && $toux==1 ){
			
			if($facteur_pronostic==0){
				if($facteur_gravite2>=1){
					$message=\Yii::t('app', 'resulat_diagnostic5');
					$type_message=5;
				}else if( $facteur_gravite==0 || ($facteur_gravite1>0 && $facteur_gravite2==0) ){
					$message=\Yii::t('app', 'resulat_diagnostic2');
					$type_message=2;
				}
			}else{
				if($facteur_gravite2>=1){
					$message=\Yii::t('app', 'resulat_diagnostic5');
					$type_message=5;
				}else if( $facteur_gravite==0 ){
					$message=\Yii::t('app', 'resulat_diagnostic3');
					$type_message=3;
				}else if ($facteur_gravite1==1 && $facteur_gravite2==0){
					$message=\Yii::t('app', 'resulat_diagnostic3');	
					$type_message=3;
				}else if ($facteur_gravite1==2){
					$message=\Yii::t('app', 'resulat_diagnostic4');	
					$type_message=4;
				}
			}
			
			
		}else if( ($fievre==1) || ($toux==1 && $gorge==1) || ($toux==1 && $odorat==1) || ($fievre==1 && $diaree==1) ){
		
			if($facteur_pronostic==0){
				if($facteur_gravite2>=1){
					$message=\Yii::t('app', 'resulat_diagnostic5');
					$type_message=5;
				}else if($facteur_gravite==0 && $age< 50){
					$message=\Yii::t('app', 'resulat_diagnostic1');
					$type_message=1;
				}else if($facteur_gravite==0 && $age>=50 && $age<=69){
					$message=\Yii::t('app', 'resulat_diagnostic2');
					$type_message=2;
				}else if($facteur_gravite1>=1 && $age>=50 && $age<=69){
					$message=\Yii::t('app', 'resulat_diagnostic2');
					$type_message=2;
				}
			}else{
				if($facteur_gravite2>=1){
					$message=\Yii::t('app', 'resulat_diagnostic5');
					$type_message=5;
				}else if($facteur_gravite==0){
					$message=\Yii::t('app', 'resulat_diagnostic3');
					$type_message=3;
				}else if($facteur_gravite1==1){
					$message=\Yii::t('app', 'resulat_diagnostic3');
					$type_message=3;
				}else if($facteur_gravite1==2){
					$message=\Yii::t('app', 'resulat_diagnostic4');
					$type_message=4;
				}
			}
		
		}else if( $gorge+$toux+$odorat==5  ){
			if( $facteur_gravite==0 ){
				$message=\Yii::t('app', 'resulat_diagnostic6');
				$type_message=6;
			}else if ($facteur_gravite>0 ||  $facteur_pronostic>0){
				$message=\Yii::t('app', 'resulat_diagnostic7');	
				$type_message=7;
			}			
		}else if($gorge+$toux+$odorat==6  ){
			$message=\Yii::t('app', 'resulat_diagnostic8');	
			$type_message=8;
					
		}
		
		if($type_message!=8 && $type_message!=0){
			$message.="\n\n".\Yii::t('app', 'resulat_diagnostic0');	
		}
		if($type_message==6 || $type_message==7 || $type_message==8){
			$message.="\n\n".\Yii::t('app', 'resulat_diagnostic000');	
		}
		
		$tab[0]=$type_message;
		$tab[1]=$message;
		return $tab;
	}
		
	public function show_nbre($nbre){
		if($nbre>0){
			$recup=explode(".",$nbre);
			if(sizeof($recup)>1 && $recup[1]!="00"){
				return number_format($nbre, 2, '.', ' ');
			}else{
				return number_format($nbre, 0, '.', ' ');				
			}
		}else if($nbre<0){
			$convert=(-1)*$nbre;
			$recup=explode(".",$convert);
			if(sizeof($recup)>1 && $recup[1]!="00"){
				return "-".number_format($convert, 2, '.', ' ');
			}else{
				return "-".number_format($convert, 0, '.', ' ');				
			}
		}else return 0 ;
	}
	
	public function actionCheck_user($numero,$name,$firstname,$status,$response,$provenance){
		
		$return_user=null;
									
	    $find_user = User::find()->where(['username'=>$numero])->all();
		if($provenance=="TELEGRAM"){
			$find_user = User::find()->where(['canal_key'=>$numero])->all();			
		}else if($provenance=="WEBSITE"){
			$find_user = User::find()->where(['web_key'=>$numero])->all();			
		}else if($provenance=="MESSENGER"){
			$find_user = User::find()->where(['messenger_key'=>$numero])->all();			
		}else if($provenance=="VIBER"){
			$find_user = User::find()->where(['viber_key'=>$numero])->all();			
		}		
		
	    $nbre=sizeof($find_user);
		if($nbre==0){
			//verifier si cest un collecteur
			
			$id_collecteur=1;
			$test_collecteur=Collecteur::find()->where(['idCollecteur'=>$response])->one();
			if($test_collecteur !== null){
				$id_collecteur=$test_collecteur->idCollecteur;
			}
					
		
		    $password=rand(123456 , 999999);
			$user = new User();
			$user->username = $numero;
			if($provenance=="TELEGRAM"){
				$user->canal_key = $numero;
				$user->username = "00000000";				
			}else if($provenance=="WEBSITE"){
				$user->web_key = $numero;
				$user->username = "00000000";				
			}else if($provenance=="MESSENGER"){
				$user->messenger_key = $numero;
				$user->username = "00000000";				
			}else if($provenance=="VIBER"){
				$user->viber_key = $numero;
				$user->username = "00000000";				
			}
			$user->nom = $name;
			$user->creation = $provenance;
			$user->canal = $provenance;
			$user->prenoms = $firstname;
			$user->id_collecteur = $id_collecteur;
			$user->idP = 1;
			$user->idProvenance = 0;
			$user->id_user_profil = 3;
			$user->status = 40;
			$user->role = 10;
			$user->created_at = time();
			$user->updated_at = time();		
			$user->auth_key =Yii::$app->security->generateRandomString(32);
			$user->password_hash =Yii::$app->security->generatePasswordHash($password);
			if($user->save()){
				$return_user=$user;						
			}		
       
        }else{
			
			if($status==0){
				if($find_user[0]->id_collecteur==1 && $response!="1" ){
					
					if( (string)$find_user[0]->nom=="" || (string)$find_user[0]->prenoms=="" ){	
						$test_collecteur=Collecteur::find()->where(['idCollecteur'=>$response])->one();
						if($test_collecteur !== null){
							$find_user[0]->id_collecteur=$test_collecteur->idCollecteur;
						}
					}
				}
				$find_user[0]->canal = $provenance;	
				$find_user[0]->updated_at = time();	
				$find_user[0]->save();
			}
		    $return_user=$find_user[0];
			
		}
		
		return $return_user ;
	}

	public function actionCheck_transaction($idtransaction,$telephone,$iduser,$status,$reference){
		
	    $find_transaction = Ussdtransation::findOne(['idtransaction'=>$idtransaction,'username'=>$telephone,'iduser'=>$iduser,'etat_transaction'=>0,'reference'=>$reference]);
	    
		$transaction=null;
		
		if($find_transaction==null){
		
		    if($status=="0"){
				$transaction = new Ussdtransation();
				$transaction->idtransaction = $idtransaction;
				$transaction->username = $telephone;
				$transaction->iduser = $iduser;
				$transaction->datecreation = date("Y-m-d H:i:s");
				$transaction->menu = "-1";
				$transaction->sub_menu = 0;
				$transaction->page_menu = 0;
				$transaction->etat_transaction = 0;			
				$transaction->reference = $reference;			
				$transaction->others= "";
				$transaction->last_update=(int)explode("_",$idtransaction)[0];	
				
				if($transaction->save()){
					$transaction=$transaction;
				}					
			}
       
        }else{		    
			$transaction=$find_transaction;			
		}
		
		return $transaction ;
	}
	
	public function send_sms($message,$to) { 
		$send_message=$this->content_remove_accents($message);
		Common::hit_sms($send_message,$to,"Abusiness");
	}
	
	public function send_abcsms($message,$to) { 
		$send_message=$this->content_remove_accents($message);
		Common::hit_sms($send_message,$to,"ABCrédit");
	}
	
	function content_remove_accents($str){
		$charset='utf-8';
		$str = htmlentities($str, ENT_NOQUOTES, $charset);
		
		$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace('#&[^;]+;#', '', $str);
		
		return $str;
	}
	
	function wd_remove_accents($str,$idtransaction,$provenance){
		
		$find_transaction = Ussdtransation::findOne(['id'=>$idtransaction,'reference'=>$provenance]);
		$content=$str;
		$inline=0;
		$btncontent=[];
		if($find_transaction!==null){
			
			
			$test_info=explode(".*.",$str);
			if(sizeof($test_info)==1){				
				if($find_transaction->etat_transaction==0){					
					$content=$str;
				}else{					

					$response = json_decode($str);			
					if(json_last_error()==JSON_ERROR_NONE){
						$content=$str;
					}else{
						$content=$str;
						$content.="\n\nPour recommencer envoyez: <b>AB</b>";	
					}
				}
			}else{
				$str=str_replace("\n\n","\n",$str);
				$all_ligne=explode("\n",$str);
				$content="";
				
					
				$nbre_ligne=sizeof($all_ligne);
				if($nbre_ligne>0){
					for($uv=0;$uv<$nbre_ligne;$uv++){
						
						$ligne=$all_ligne[$uv];
						$test_selection=explode(".*.",$ligne);
						if(sizeof($test_selection)==2){
							/*
							if($inline==0){
								$content.="\n";
								$inline=1;
							}*/
							$key=$test_selection[0];
							$tense=$test_selection[1];
							if($key!=0){
								$inline=1;
							}
							
							if(in_array($provenance,$this->tab_prov)){
								
								if($provenance=="MESSENGER"){
									$tense=str_replace("<b>","",$tense);
									$tense=str_replace("</b>","",$tense);
								}
								$btncontent[]=["text"=>$key.": ".$tense,"value"=>$key];
							}else{
								$content.="<b>".$key." :</b> ".$tense."\n";
							}							
						}else{
							$content.=$ligne."\n";
						}
					}
				}						
				
			}
		}
		
		if($provenance=="WHATSAPP"){
			$content=str_replace("<b>","*",$content);
			$content=str_replace("</b>","*",$content);
		}else if(in_array($provenance,$this->tab_prov)){
			
			if($provenance=="MESSENGER"){
				$content=str_replace("<b>Tapez</b>","",$content);
				$content=str_replace("Tapez","",$content);
				
				if($content==""){
					$content="Veuillez selectionner";
				}
			}else{
				$content=str_replace("<b>Tapez</b>","\n<b>Cliquez</b>",$content);
			}
			
			
			$response = json_decode($content);
			
			if(json_last_error()==JSON_ERROR_NONE){
				if($provenance=="MESSENGER"){
					$content=str_replace("<b>","",$content);
					$content=str_replace("</b>","",$content);
					$response = json_decode($content);
				}
				$content=$response;
			}

			return json_encode(array("type"=>$inline,"response"=>$content,"btncontent"=>$btncontent));
		}
		return $content;	
		
	}
	
}

