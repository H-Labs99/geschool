<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "abc_retraitmobile".
 *
 * @property integer $id_abc_retraitmobile
 * @property string $key_retraitmobile
 * @property string $numero_paiement
 * @property integer $montant
 * @property integer $id_retrait
 * @property integer $idclient
 * @property integer $etat
 * @property string $date_create
 * @property string $date_update
 * @property string $date_paiement
 * @property string $ref_transaction
 * @property string $gateway_response
 * @property string $paiement_response
 */
class AbcRetraitmobile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abc_retraitmobile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_retraitmobile', 'numero_paiement', 'montant', 'id_retrait', 'idclient', 'etat', 'date_create', 'date_update', 'ref_transaction'], 'required'],
            [['montant', 'id_retrait', 'idclient', 'etat'], 'integer'],
            [['date_create', 'date_update', 'date_paiement'], 'safe'],
            [['gateway_response', 'paiement_response'], 'string'],
            [['key_retraitmobile', 'ref_transaction'], 'string', 'max' => 50],
            [['numero_paiement'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_abc_retraitmobile' => 'Id Abc Retraitmobile',
            'key_retraitmobile' => 'Key Retraitmobile',
            'numero_paiement' => 'Numero Paiement',
            'montant' => 'Montant',
            'id_retrait' => 'Id Retrait',
            'idclient' => 'Idclient',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'date_paiement' => 'Date Paiement',
            'ref_transaction' => 'Ref Transaction',
            'gateway_response' => 'Gateway Response',
            'paiement_response' => 'Paiement Response',
        ];
    }
}
