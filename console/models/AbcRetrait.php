<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "abc_retrait".
 *
 * @property integer $id
 * @property string $key_retrait_abc
 * @property string $ref_retrait
 * @property integer $idclient
 * @property string $numero_beneficiare
 * @property integer $montant_retrait
 * @property integer $type_envoiepaiement
 * @property double $reel_out
 * @property double $marchand_benef
 * @property double $abc_benef
 * @property integer $status_retrait
 * @property string $date_create
 * @property string $customer_confirm_time
 * @property integer $id_acteur_user
 *
 * @property AbcTypePaiement $typeEnvoiepaiement
 * @property ActeurUser $idActeurUser
 */
class AbcRetrait extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abc_retrait';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_retrait_abc', 'ref_retrait', 'idclient', 'numero_beneficiare', 'montant_retrait', 'type_envoiepaiement', 'reel_out', 'marchand_benef', 'abc_benef', 'status_retrait', 'date_create', 'id_acteur_user'], 'required'],
            [['idclient', 'montant_retrait', 'type_envoiepaiement', 'status_retrait', 'id_acteur_user'], 'integer'],
            [['reel_out', 'marchand_benef', 'abc_benef'], 'number'],
            [['date_create', 'customer_confirm_time'], 'safe'],
            [['key_retrait_abc'], 'string', 'max' => 35],
            [['ref_retrait'], 'string', 'max' => 10],
            [['numero_beneficiare'], 'string', 'max' => 20],
            [['type_envoiepaiement'], 'exist', 'skipOnError' => true, 'targetClass' => AbcTypePaiement::className(), 'targetAttribute' => ['type_envoiepaiement' => 'id']],
            [['id_acteur_user'], 'exist', 'skipOnError' => true, 'targetClass' => ActeurUser::className(), 'targetAttribute' => ['id_acteur_user' => 'id_acteur_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key_retrait_abc' => 'Key Retrait Abc',
            'ref_retrait' => 'Ref Retrait',
            'idclient' => 'Idclient',
            'numero_beneficiare' => 'Numero Beneficiare',
            'montant_retrait' => 'Montant Retrait',
            'type_envoiepaiement' => 'Type Envoiepaiement',
            'reel_out' => 'Reel Out',
            'marchand_benef' => 'Marchand Benef',
            'abc_benef' => 'Abc Benef',
            'status_retrait' => 'Status Retrait',
            'date_create' => 'Date Create',
            'customer_confirm_time' => 'Customer Confirm Time',
            'id_acteur_user' => 'Id Acteur User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeEnvoiepaiement()
    {
        return $this->hasOne(AbcTypePaiement::className(), ['id' => 'type_envoiepaiement']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdActeurUser()
    {
        return $this->hasOne(ActeurUser::className(), ['id_acteur_user' => 'id_acteur_user']);
    }
}
