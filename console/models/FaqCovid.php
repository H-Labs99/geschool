<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "faq_covid".
 *
 * @property integer $idFaq
 * @property string $key_faq
 * @property string $question
 * @property string $reponse
 * @property integer $position_menu_ussd
 * @property integer $type_faq
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 */
class FaqCovid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_covid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_faq', 'reponse', 'position_menu_ussd', 'etat', 'created_by'], 'required'],
            [['reponse'], 'string'],
            [['position_menu_ussd', 'type_faq', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['key_faq', 'question'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idFaq' => 'Id Faq',
            'key_faq' => 'Key Faq',
            'question' => 'Question',
            'reponse' => 'Reponse',
            'position_menu_ussd' => 'Position Menu Ussd',
            'type_faq' => 'Type Faq',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }
}
