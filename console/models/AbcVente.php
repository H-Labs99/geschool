<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "abc_vente".
 *
 * @property integer $id_vente_abc
 * @property string $key_vente_abc
 * @property string $ref_vente
 * @property string $libelle_vente_abc
 * @property string $montant_vente_abc
 * @property string $date_create
 * @property integer $status_vente_abc
 * @property integer $id_acteur_user
 * @property integer $idclient
 * @property integer $type_envoiepaiement
 * @property integer $information_id
 * @property string $provenance
 * @property string $submission_time
 * @property string $customer_confirm_time
 *
 * @property ActeurUser $idActeurUser
 * @property AbcSouscription $idclient0
 */
class AbcVente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abc_vente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_vente_abc', 'ref_vente', 'libelle_vente_abc', 'montant_vente_abc', 'date_create', 'status_vente_abc', 'id_acteur_user', 'idclient', 'type_envoiepaiement', 'provenance', 'submission_time'], 'required'],
            [['libelle_vente_abc'], 'string'],
            [['date_create', 'submission_time', 'customer_confirm_time'], 'safe'],
            [['status_vente_abc', 'id_acteur_user', 'idclient', 'type_envoiepaiement', 'information_id'], 'integer'],
            [['key_vente_abc'], 'string', 'max' => 35],
            [['ref_vente'], 'string', 'max' => 25],
            [['montant_vente_abc'], 'string', 'max' => 10],
            [['provenance'], 'string', 'max' => 15],
            [['id_acteur_user'], 'exist', 'skipOnError' => true, 'targetClass' => ActeurUser::className(), 'targetAttribute' => ['id_acteur_user' => 'id_acteur_user']],
            [['idclient'], 'exist', 'skipOnError' => true, 'targetClass' => AbcSouscription::className(), 'targetAttribute' => ['idclient' => 'id_souscription']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_vente_abc' => 'Id Vente Abc',
            'key_vente_abc' => 'Key Vente Abc',
            'ref_vente' => 'Ref Vente',
            'libelle_vente_abc' => 'Libelle Vente Abc',
            'montant_vente_abc' => 'Montant Vente Abc',
            'date_create' => 'Date Create',
            'status_vente_abc' => 'Status Vente Abc',
            'id_acteur_user' => 'Id Acteur User',
            'idclient' => 'Idclient',
            'type_envoiepaiement' => 'Type Envoiepaiement',
            'information_id' => 'Information ID',
            'provenance' => 'Provenance',
            'submission_time' => 'Submission Time',
            'customer_confirm_time' => 'Customer Confirm Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdActeurUser()
    {
        return $this->hasOne(ActeurUser::className(), ['id_acteur_user' => 'id_acteur_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdclient0()
    {
        return $this->hasOne(AbcSouscription::className(), ['id_souscription' => 'idclient']);
    }
}
