<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "produits_qte".
 *
 * @property integer $idproduits_qte
 * @property integer $idProduit
 * @property integer $idQte
 * @property integer $prixLivraison
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 *
 * @property Produits $idProduit0
 * @property QtePropose $idQte0
 */
class ProduitsQte extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'produits_qte';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProduit', 'idQte', 'prixLivraison', 'etat', 'created_by'], 'required'],
            [['idProduit', 'idQte', 'prixLivraison', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['idProduit'], 'exist', 'skipOnError' => true, 'targetClass' => Produits::className(), 'targetAttribute' => ['idProduit' => 'idProduit']],
            [['idQte'], 'exist', 'skipOnError' => true, 'targetClass' => QtePropose::className(), 'targetAttribute' => ['idQte' => 'idQte']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idproduits_qte' => 'Idproduits Qte',
            'idProduit' => 'Id Produit',
            'idQte' => 'Id Qte',
            'prixLivraison' => 'Prix Livraison',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduit0()
    {
        return $this->hasOne(Produits::className(), ['idProduit' => 'idProduit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQte0()
    {
        return $this->hasOne(QtePropose::className(), ['idQte' => 'idQte']);
    }
}
