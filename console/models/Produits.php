<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "produits".
 *
 * @property integer $idProduit
 * @property string $key_produit
 * @property integer $type_produit
 * @property string $denomination
 * @property string $unite
 * @property integer $prix_unitaire
 * @property integer $position_menu_ussd
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 *
 * @property PackageInfo[] $packageInfos
 * @property ProduitsQte[] $produitsQtes
 */
class Produits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'produits';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_produit', 'type_produit', 'unite', 'position_menu_ussd', 'etat', 'created_by'], 'required'],
            [['type_produit', 'prix_unitaire', 'position_menu_ussd', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['key_produit', 'denomination'], 'string', 'max' => 255],
            [['unite'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProduit' => 'Id Produit',
            'key_produit' => 'Key Produit',
            'type_produit' => 'Type Produit',
            'denomination' => 'Denomination',
            'unite' => 'Unite',
            'prix_unitaire' => 'Prix Unitaire',
            'position_menu_ussd' => 'Position Menu Ussd',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackageInfos()
    {
        return $this->hasMany(PackageInfo::className(), ['idProduit' => 'idProduit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduitsQtes()
    {
        return $this->hasMany(ProduitsQte::className(), ['idProduit' => 'idProduit']);
    }
}
